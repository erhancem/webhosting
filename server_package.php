<?php
$page_settings['session'] = true;
include("inc.include.php");

$cm_token	= cm_get_request('cm_token');
$action		= cm_get_request('action');
$package	= cm_get_request('package');
if($package and cm_empty($package)){
	$package = str_replace("-","_",$package);
}
$theme		= 'server_package';
$process	= 1;

if($package == 'vds'){
	$product_type_id = 10;
}elseif($package == 'vps'){
	$product_type_id = 11;
}elseif($package == 'dedicated'){
	$product_type_id = 9;
}elseif($package == 'configure'){
	if(!isset($_SESSION['product_server'])){
		cm_redirect(cm_urlwrite('sunucu','vds'));
	}
	$os_list	= cm_os_list();
	$product 	= product_list($_SESSION['product_server']);
	$theme		= 'server_configure';
	$process	= 2;
	$currency	= $product['currency'];
	$periodKey	= array_keys($product['prices_list']);
	$product['product_price'] = $product['prices_list'][$periodKey[0]]['price'];
	if($currency != $cm_config['default_currency']){
		$product_price = cm_currency_convert($product['product_price'],$currency,$cm_config['default_currency']);
		$product['product_price']	= $product_price['price'];
		$product['currency']		= $product_price['currency'];
	}
	if($product['ipv4_select'] == 1){
		$ipvPrice = cm_currency_convert($product['ip_price_v4'],$currency,$cm_config['default_currency']);
		$product['ip_price_v4'] = $ipvPrice['price'];
	}
}else{
	cm_redirect(cm_urlwrite('sunucu','vps'));
}

if($process == 1 and isset($_SESSION['product_server'])){
	unset($_SESSION['product_server']);
}

if($action){
	if($process == 1){
		if($action == 'add'){
			$product_id	= cm_get_request('product_id');
			if($product_id and cm_numeric($product_id) and $product_id > 0){
				$product	= product_list($product_id);
				if($product and $product['product_type_id'] == $product_type_id){
					$_SESSION['product_server']	= $product_id;
					cm_redirect(cm_urlwrite('sunucu','configure'));
				}
			}
		}else{
			cm_redirect(cm_urlwrite('sunucu','vds'));
		}
	}elseif($process == 2){	/** Sunucu yapılandırma */
		if(isset($_SESSION['product_server'])){
			if($action == 'os_select'){
				$os_id = cm_get_request('os_id');
				$type_id = cm_get_request('type_id');
				list($json_status,$json_message) = product_panelselect($os_id,$type_id);
				$jsonOutput = array(
					'status'	=> $json_status,
					'message'	=> $json_message,
				);
				echo cm_json_encode($jsonOutput);
				exit;
			}elseif($action == 'add'){
				include(cm_path.'include/post/basket.php');
			}
		}else{
			cm_redirect(cm_urlwrite('sunucu','vds'));
		}
	}
}
$template_call 	= $template_directory.$theme.'.php';
if(!cm_isfile($template_call)){ echo cm_lang('##theme## tema dosyası bulunamadı',array('theme'=>$theme)); exit; }
include($template_call);