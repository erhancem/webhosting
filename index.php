<?php
$page_settings['session']   = true;
$page_settings['minify']    = true;

include("inc.include.php");

$template_call = $template_directory . 'index.php';
if (!cm_isfile($template_call)) {
    echo cm_lang('##theme## tema dosyası bulunamadı', ['theme' => $template_call]);
    exit;
}

include($template_call);
