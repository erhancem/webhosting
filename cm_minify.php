<?php
$page_settings['minify'] = true;
include("inc.include.php");
if(isset($_REQUEST["type"]) and in_array($_REQUEST["type"],array('css','js')) and isset($_REQUEST["path"])) {
    $_REQUEST["type"] = strtolower($_REQUEST["type"]);
    $lib = $cm_dirlist["library"].'minify/'.$_REQUEST["type"].'.php';
    if(file_exists($lib)) {
        $file = cm_path.'/'.$_REQUEST["path"].'.'.$_REQUEST["type"];
        if(file_exists($file)) {
            if($_REQUEST["type"] == 'css'){
                $lastModified       = filemtime(__FILE__);
                $etagFile           = md5_file(__FILE__);
                $ifModifiedSince    = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
                $etagHeader         = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

                header("Content-type: text/css");
                header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified) . " GMT");
                header("Etag: $etagFile");
                header('Cache-Control: public');
                if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) and strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModified || $etagHeader == $etagFile) {
                    header("HTTP/1.1 304 Not Modified");
                    exit;
                }
            }else{
                $lastModified       = filemtime(__FILE__);
                $etagFile           = md5_file(__FILE__);
                $ifModifiedSince    = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
                $etagHeader         = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

                header('Content-Type: application/javascript; charset=utf-8');
                header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified) . " GMT");
                header("Etag: $etagFile");
                header('Cache-Control: public');
                if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) and strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModified || $etagHeader == $etagFile) {
                    header("HTTP/1.1 304 Not Modified");
                    exit;
                }
            }
            if(preg_match('#^(.*?)\.min$#', $_REQUEST["path"]) or (isset($cm_config["minify"]) and $cm_config["minify"] != 1)) {
                include($file);
            }else{
                include($lib);
            }
        }
    }
}