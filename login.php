<?php
$page_settings['session'] = true;
include("inc.include.php");


$theme      = 'client_login';
$login      = new \Cembilisim\Webhosting\Auth\Login();
if($login->getAuth->check() == true){
    cm_redirect(cm_link("client.php"));
}

$template   = new \Cembilisim\Webhosting\CMSystem\Template(new Smarty());
$output     = new \Cembilisim\Webhosting\CMSystem\Response();
$response   = $login->action(new \Cembilisim\Webhosting\CMSystem\Request());

if($response){
    $output->output($response);
}

$template->cm_templatedir($cm_dirlist["template"].$cm_config["template"].'/');
$template->cm_variable('directory',$directory);
$template->cm_variable('template', $cm_config['template']);
$template->cm_variable('cm_lang', $cm_lang);
$template->cm_variable('cm_title','CEM Bilişim - Web Hosting Hizmetleri');
$template->cm_variable('keywords','cem bilişim,vps,hosting,vds,fiziksel sunucu,Arma 3,oyun sunucu, Arma 3 oyun sunucu,sınırsız hosting, SSD hosting');
$template->cm_variable('description','Cem bilişim Kaliteli hosting (Sınırsız SSD Hosting), VPS, VDS, Fiziksel Sunucu, Oyun sunucuları ve Yazılım Hizmeti');
$template->cm_variable('copyright','Copyright © 2016, Cem Bilişim - Sunucu ve Yazılım Hizmetleri');
$template->cm_variable('cm_config',$cm_config);

$template->cm_variable('cm_response', $response);

$template->cm_variable('cm_set_token',cm_set_token());
unset($response,$output,$login);

$template->cm_Templateview($theme);