<?php
if(!isset($cm_config)){ exit; }

$reqURL = ''; if(isset($_SERVER["REQUEST_URI"])){ $reqURL = ltrim($_SERVER["REQUEST_URI"],"/"); }

if(isset($cm_config["ssl_redirect"]) and $cm_config["ssl_redirect"] == 1 and (!isset($_SERVER['HTTPS']) or (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] != "on"))){
    //echo cm_siteurl().$reqURL; exit;
    cm_redirect(cm_siteurl().$reqURL);
}

if(isset($_SERVER["SERVER_NAME"]) and $_SERVER["SERVER_NAME"] != $cm_config["domain"]){
    //echo cm_siteurl().$reqURL; exit;
    cm_redirect(cm_siteurl().$reqURL);
}

if(isset($page_settings["minify"]) and $page_settings["minify"] == true and isset($cm_config["minify"]) and $cm_config["minify"] == 1 and isset($_REQUEST["minify"]) and $_REQUEST["minify"] == 1 and isset($_SERVER['HTTP_ACCEPT_ENCODING']) and substr_count($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip')) {
    ob_start("ob_gzhandler");
}elseif((!isset($_SERVER['REDIRECT_URL']) or (isset($_SERVER['REDIRECT_URL']) and substr($_SERVER['REDIRECT_URL'],-3) != ".js")) and (!isset($page_settings["html_compress"]) or (isset($page_settings["html_compress"]) and $page_settings["html_compress"] == true)) and isset($cm_config["html_compress"]) and $cm_config["html_compress"] == 1){
    ob_start("cm_htmlcompress");
}

$template_directory	= cm_path."templates/".$cm_config["template"]."/";
$template_url		= 'index';
if(isset($_SERVER['REDIRECT_URL'])){
    $template_url = trim($_SERVER['REDIRECT_URL'],"/");
    $template_url = str_replace(array("_","-"),array("","_"),$template_url);
}