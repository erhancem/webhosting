<?php
function domain_verify($domain_name,$idn = 0){
    if($idn == 0){
        return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i", $domain_name) && preg_match("/^.{1,253}$/", $domain_name) && preg_match("/^[^\.]{1,63}(\.[^\.]{1,63})*$/", $domain_name));
    }else{
        return (preg_match("/^([a-z\d](-*[a-z\d])*)(\.([a-z\d](-*[a-z\d])*))*$/i",$domain_name));
    }
}

function domainGetDetails($params){
	if(!is_array($params)){
		if(cm_numeric($params)){
			$params = array('domain_id'=>$params);
		}else{
			return false;
		}
	}
	if(!isset($params['domain_id']) or !cm_numeric($params['domain_id']) or $params['domain_id'] <= 0){ return false; }
	global $cm_config,$cm_db;
	$where = array();
	$db_domain_columns	= $cm_db->sql_tablo_info('domains');
 	$params				= $cm_db->db_params_control($params,$db_domain_columns);
	foreach($params as $key=>$value){
		$where[] = 'd.'.$key."='".$cm_db->sql_escape($value)."'";
	}
	$DomainQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."domains AS d WHERE ".implode(" AND ",$where));
	if($cm_db->sql_errno() == 0){
		if($cm_db->sql_num_rows($DomainQuery) == 1){
			$domain = $cm_db->sql_fetch_assoc($DomainQuery);
			$tld = $domain['domain_name'];
			$tld = explode(".",$tld);
			unset($tld[0]);
			$tld = implode(".",$tld);
			$domain['tld'] = $cm_db->sql_assoc('tlds',array('tld_name'=>$tld));
			$contact = $cm_db->sql_assoc('domain_contacts',array('module_id'=>$domain['module_id'],'contact_id'=>$domain['contact_id']));
			$domain['contact'] = false;
			if($contact){
				$domain['contact'] = $contact;
			}else{
				if(in_array($domain['domain_status'],array(0))) {
					$domain['contact'] = domainContactAddress(array('user_id' => $domain['user_id']));
				}
            }
			$domain['dns'] = false;
			$dnsX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."domain_dns WHERE domain_id='".$cm_db->sql_escape($domain['domain_id'])."' ORDER BY number ASC");
			if($cm_db->sql_errno() == 0){
				if($cm_db->sql_num_rows($dnsX) > 0){
					while($dns = $cm_db->sql_fetch_assoc($dnsX)){
						$domain['dns']['ns'.$dns['number']] = $dns['dns'];
					}
				}
			}
			if($domain['dns'] == false){
				if(in_array($domain['domain_status'],array(0))) {
					$domain['dns'] = domainDefaultNameserver(array('user_id' => $domain['user_id']));
				}
            }
			$domain['ns'] = false;
			$nsX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."domain_ns WHERE domain_id='".$cm_db->sql_escape($domain['domain_id'])."' ORDER BY domain_ns_id ASC");
			if($cm_db->sql_errno() == 0){
				if($cm_db->sql_num_rows($nsX) > 0){
                    $domain['ns'] = array();
					while($ns = $cm_db->sql_fetch_assoc($nsX)){
						$domain['ns'][] = $ns;
					}
				}
			}
			return $domain;
		}
	}
	return false;
}

function domainContactAddress($params){
	global $cm_db;
	$contact = $cm_db->sql_assoc('contacts',array('user_id'=>$params['user_id']));
	if($contact){
		unset($contact['contact_id'],$contact['user_id']);
		$contact['name'] = $contact['first_name']." ".$contact['last_name'];
		return $contact;
	}else{
		$user_address = userGetDetails($params['user_id']);
		if($user_address){
			$return = array(
				'name'			=> $user_address['first_name'].' '.$user_address['last_name'],
				'email_address'	=> $user_address['email_address'],
				'phone_code'	=> $user_address['phone_code'],
				'phone_number'	=> $user_address['phone_number'],
				'fax_code'		=> null,
				'fax_number'	=> null,
				'city'			=> $user_address['address']['city'],
				'state'			=> $user_address['address']['state'],
				'postcode'		=> $user_address['address']['postcode'],
				'country'		=> $user_address['address']['country'],
			);
			if($user_address['address']['billing_type'] == 0){
				$return['company_name'] = "N/A";
			}else{
				$return['company_name'] = $user_address['address']['name'];
			}
			$return['address1']	= substr( $user_address['address']['address'], 0, 64 );
			if(64 < $user_address['address']['address']){
				$return['address2'] = substr( $user_address['address']['address'], 64, 128 );
			}else{
				$return['address2'] = null;
			}
			$return['address3'] = null;
			return $return;
		}
	}
	return false;
}

function domainDefaultNameserver($params){
	global $cm_db;
	$ns = array();
	$nameserver = $cm_db->sql_assoc('nameservers',array('user_id'=>$params['user_id']));
	if($nameserver){
		$say = 1;
		foreach(json_decode($nameserver['nameserver'],true) as $value){
			$ns['ns'.$say] = $value;
			$say++;
		}
	}else{
		$ns = array(
			'ns1'	=> 'ns1.cembilisim.com',
			'ns2'	=> 'ns2.cembilisim.com',
		);
	}
	return $ns;
}

function domainTransferCode($domain_id){
	list($status,$response) = ModuleCallFunction($domain_id,'TransferCode','domain');
	if($status == true){
		if(!array_key_exists('message',$response)){
			$response['message'] = cm_lang('İşleminiz yapıldı');
		}
	}
	return array($status,$response);
}

/** ------------------------------ Domain Contact Start ------------------------------ */

function domainAddContact($domain_id,$contact){
	$data = ['contact' => $contact];
	list($status,$response) = ModuleCallFunction($domain_id,'ChangeContact','domain',$data);
	if($status == true){
		if(!array_key_exists('message',$response)){
			$response['message'] = cm_lang('İşleminiz yapıldı');
		}
	}
	return array($status,$response);
}

function domainEditContact($domain_id,$contact){
	$data = ['contact' => $contact];
	list($status,$response) = ModuleCallFunction($domain_id,'EditContact','domain',$data);
	if($status == true){
		if(!array_key_exists('message',$response)){
			$response['message'] = cm_lang('İşleminiz yapıldı');
		}
	}
	return array($status,$response);
}

function domainGetContact($domain_id){
	list($status,$response) = ModuleCallFunction($domain_id,'GetContact','domain');
	return array($status,$response);
}

function domainVerifyStatusContact($domain_id){
	global $cm_db;
	list($status,$response) = domainGetContact($domain_id);
	if($status){
		if(isset($response['status']) and in_array($response['status'],array(1))){
			$update = array(
				'contact_verify'	=> 1,
			);
			$cm_db->sql_query_update('domains', $update, ['domain_id' => $domain_id]);
			$response = [
				'message'	=> cm_lang('İletişim bilgileri onaylandı'),
			];
		}else{
			$status 	= false;
			$response 	= cm_lang('İletişim bilgileri onaysız');
		}
	}
	return [$status,$response];
}

/** ------------------------------ Domain Contact End ------------------------------ */

/** ------------------------------ Domain CNS Start ------------------------------ */

function domainAddCNS($domain_id,$cns_address){
	$data = $cns_address;
	list($status,$response) = ModuleCallFunction($domain_id,'AddCNS','domain',$data);
	if($status == true){
		if(!array_key_exists('message',$response)){
			$response['message'] = cm_lang('İşleminiz yapıldı');
		}
	}
	return array($status,$response);
}

function domainDeleteCNS($domain_id,$cns_address){
	$data = $cns_address;
	list($status,$response) = ModuleCallFunction($domain_id,'DeleteCNS','domain',$data);
	if($status == true){
		if(!array_key_exists('message',$response)){
			$response['message'] = cm_lang('İşleminiz yapıldı');
		}
	}
	return array($status,$response);
}

/** ------------------------------ Domain CNS END ------------------------------ */

/** ------------------------------ Domain TransferLock Start ------------------------------ */

function domainTransferLock($domain_id,$lock_status){
	$data = [
		'domain_transfer_protection'	=> $lock_status,
	];
	list($status,$response) = ModuleCallFunction($domain_id,'transferlock','domain',$data);
	if($status == true){
		if(!array_key_exists('message',$response)){
			$response['message'] = cm_lang('İşleminiz yapıldı');
		}
	}
	return array($status,$response);
}

/** ------------------------------ Domain TransferLock End ------------------------------ */

/** ------------------------------ Domain DNS Start ------------------------------ */

function domainSaveDNS($domain_id,$nameserver){
    $data = array(
        'dns'	=> $nameserver,
    );
    list($status,$response) = ModuleCallFunction($domain_id,'savedns','domain',$data);
    if($status == true){
        if(!array_key_exists('message',$response)){
            $response['message'] = cm_lang('İşleminiz yapıldı');
        }
    }
    return array($status,$response);
}

function domainGetDNS($domain_id){
	list($status,$response) = ModuleCallFunction($domain_id,'GetDNS','domain');
	return array($status,$response);
}

/** ------------------------------ Domain DNS End ------------------------------ */

function domainRegister($domain_id){
    list($status,$response) = ModuleCallFunction($domain_id,'Register','domain');
    return array($status,$response);
}

function domainTransfer($domain_id){
	global $cm_db;
	$params = domainGetDetails($domain_id);
	if($params){
		$params['contact'] = domainContactAddress($params);
		if($params['contact']){
			list($status,$message) = domainCallFunction($params,'Transfer');
			if($status == true){
				$update = array();
				$update['domain_status'] = 4;
				if(isset($message['contact_id'])){
					$update['contact_id'] = $message['contact_id'];
				}
				if(isset($message['domainid'])){
					$update['domainid'] = $message['domainid'];
				}
				if(isset($message['customer_id'])){
					$update['customer_id'] = $message['customer_id'];
				}
				if(isset($message['end_time'])){
					$update['domain_end_time'] = $message['end_time'];
				}
				if(count($update) > 0){
					$cm_db->sql_query_update('domains',$update,array('domain_id'=>$params['domain_id']));
				}
			}
		}else{
			$message = cm_lang('İletişim bilgileri hatalı');
		}
	}else{
		$message = cm_lang('Alan adı bilgileri alınamadı');
	}
	return array($status,$message);
}

function domainRenew($domain_id){
	global $cm_db;
	$params = domainGetDetails($domain_id);
	if($params){
		list($status,$message) = domainCallFunction($params,'Renew');
		if($status == true){
			$update = array();
			$update['domain_status'] = 1;
			if(isset($message['end_time'])){
				$update['domain_end_time'] = $message['end_time'];
			}
			if(count($update) > 0){
				$cm_db->sql_query_update('domains',$update,array('domain_id'=>$params['domain_id']));
			}
		}
	}else{
		$message = cm_lang('Alan adı bilgileri alınamadı');
	}
	return array($status,$message);
}

function domainAdd($domain_name,$module_id,$user){ // AYARLAAAAAAAAAAAAA
	$params = array(
		'domain_name'	=> $domain_name,
		'module_id'		=> $module_id,
	);
	list($status,$message) = domainCallFunction($params,'AddDomain');
	print_r($message); exit;
	if($status == true){
		$update = array();
		$update['domain_status'] = 1;
		if(isset($message['end_time'])){
			$update['domain_end_time'] = $message['end_time'];
		}
		if(count($update) > 0){
			$cm_db->sql_query_update('domains',$update,array('domain_id'=>$params['domain_id']));
		}
	}
	return array($status,$message);
}

function domainCalculation($product_type_id,$params){
	global $cm_db,$cm_db;
	$Status = false;
	$product_type 	= $params['product_type']['product_type_id'];
	$productFeature	= $params['feature'];
	$Domain			= $params['domain'];
	if(array_key_exists('domains',$params)){
		$DomainX		= $params['domains'];
	}
	$DomainTld		= $params['tld'];
	$ErrorMessage	= null;
	if($ErrorMessage == null){
        if(in_array($product_type,array(5,6,8))){ // backorder,kayıt,transfer
			$DomainPrice	= domainPrice($product_type,$DomainTld,$productFeature['year']);
			if($DomainPrice){
				$Status			= true;
				$DomainPrice	= $DomainPrice[$productFeature['year']];
				$response		= array(
					'price'		=> $DomainPrice['price'],
					'currency'	=> $DomainPrice['currency'],
					'discount'	=> $DomainPrice['discount'],
					'message'	=> null
				);
			}else{
				$response = cm_lang('Alan adı fiyatı hesaplanamadı');
			}
        }elseif($product_type == 7){ // alan adı uzatma
			if($DomainTld['expire_time'] == 0 or $DomainX['domain_end_time'] < strtotime("-".$DomainTld['expire_time']." day")){
				$ErrorMessage = cm_lang('Alan adı süresi ve ek uzatma süresi doldu uzatma işlemi için destek talebi oluşturun');
			}elseif($DomainX['domain_end_time'] < strtotime("-".$DomainTld['extra_renew_time']." day") and $productFeature['year'] != 1){
				$ErrorMessage = cm_lang('Alan adı cezalı ödemeye girdiği için maksimum ##year## Yıl uzatma yapabilirsiniz',array('year'=>1));
				$MaxLimit = 1;
			}
			if($ErrorMessage == null){
				$Status = true;
				if($DomainX['domain_end_time'] > strtotime("-".$DomainTld['extra_renew_time']." day")){
					$DomainPrice	= domainPrice($product_type,$DomainTld,$productFeature['year']);
					$DomainPrice	= $DomainPrice[$productFeature['year']];
					$dmessage		= null;
				}else{
					$DomainPrice['price']		= cm_money_format(($DomainTld['restore_price']*$productFeature['year']));
					$DomainPrice['discount']	= cm_money_format(0.00);
					$DomainPrice['currency']	= $DomainTld['tld_currency'];
					$dmessage					= cm_lang('Cezalı alan adı uzatması');
					$promocode					= null;
				}
				$response	= array(
					'price'		=> $DomainPrice['price'],
					'currency'	=> $DomainPrice['currency'],
					'discount'	=> $DomainPrice['discount'],
					'message'	=> $dmessage,
				);
			}else{
				$response = $ErrorMessage;
			}

        }elseif($product_type == 3){ // alan adı satış
        	$response = cm_lang('Alan adı satışı aktif değil');
        }
    }else{
    	$response = $ErrorMessage;
    }
	return array($Status,$response);
}

function domainAvailable($params){
	$WhoisGroup = array();
	foreach($params as $key=>$value){
		if($value['search_status'] == 1){
	    	$WhoisGroup[$value['module_id']][$key] = $value;
	    }
	}
	if(count($WhoisGroup) > 0){
		$Status = true;
		foreach($WhoisGroup as $key=>$whois_send){
			if($key == 0){
				foreach($whois_send as $key1=>$value){
					list($QueryStatus,$QueryMessage,$QueryArray) = domainWhois($value);
					if($QueryStatus == true){
						if($params[$key1]['product_type_id'] == 6){
							$params[$key1]['status']	= (isset($QueryArray['domain_status']) and $QueryArray['domain_status']==0)?1:0;
							$params[$key1]['message']	= (isset($QueryArray['domain_status']) and $QueryArray['domain_status']==0)?cm_lang('Alan adı müsait'):cm_lang('Alan adı dolu');
							if($QueryArray['domain_status'] == 1){
								if(!stristr($params[$key1]['tld']['transfer_price'],'-')){
									$params[$key1]['alternative'][] = array(
										'domain'			=> $params[$key1]['domain'],
										'name'				=> $params[$key1]['name'],
										'tld'				=> $params[$key1]['tld']['tld_name'],
										'product_type_id'	=> 8,
										'status'			=> (in_array($QueryArray['status'],array('ok','active'))?1:0),
										'message'			=> (in_array($QueryArray['status'],array('ok','active'))?cm_lang('Alan adı transfer edilebilir'):cm_lang('Alan adı transfer kilidi aktif')),
										'idn_status'		=> $params[$key1]['idn_status'],
										'min_year'			=> 1,
										'max_year'			=> 1,
										'price'				=> domainPrice(8,$params[$key1]['tld']),
									);
								}
								if(!stristr($params[$key1]['tld']['backorder_price'],'-')){
									$params[$key1]['alternative'][] = array(
										'domain'			=> $params[$key1]['domain'],
										'name'				=> $params[$key1]['name'],
										'tld'				=> $params[$key1]['tld']['tld_name'],
										'product_type_id'	=> 5,
										'status'			=> ($QueryArray['domain_end_time'] > 0 and $QueryArray['domain_end_time'] < cm_time())?1:0,
										'message'			=> ($QueryArray['domain_end_time'] > 0 and $QueryArray['domain_end_time'] < cm_time())?cm_lang('Alan adı backorder yapılabilir'):cm_lang('Alan adı backorder yapılamaz süresi devam ediyor'),
										'idn_status'		=> $params[$key1]['idn_status'],
										'min_year'			=> 1,
										'max_year'			=> 1,
										'price'				=> domainPrice(5,$params[$key1]['tld']),
									);
								}
							}
						}elseif($params[$key1]['product_type_id'] == 5){
							if($QueryArray['domain_status'] == 1){
								$params[$key1]['status']	= ($QueryArray['domain_end_time'] > 0 and $QueryArray['domain_end_time'] < cm_time())?1:0;
								$params[$key1]['message']	= ($QueryArray['domain_end_time'] > 0 and $QueryArray['domain_end_time'] < cm_time())?cm_lang('Alan adı backorder yapılabilir'):cm_lang('Alan adı backorder yapılamaz süresi devam ediyor');
							}else{
								$params[$key1]['status']	= 0;
								$params[$key1]['message']	= cm_lang('Alan adı boş satın alabilirsiniz');
							}
						}elseif($params[$key1]['product_type_id'] == 8){
							if($QueryArray['domain_status'] == 1){
								$params[$key1]['status']	= (in_array($QueryArray['status'],array('ok','active'))?1:0);
								$params[$key1]['message']	= (in_array($QueryArray['status'],array('ok','active'))?cm_lang('Alan adı transfer edilebilir'):cm_lang('Alan adı transfer kilidi aktif'));
							}else{
								$params[$key1]['status']	= 0;
								$params[$key1]['message']	= cm_lang('Alan adı boş satın alabilirsiniz');
							}
						}
					}else{
						$params[$key1]['message']	= $QueryMessage;
					}
				}
			}else{
				list($ModuleStatus,$ModuleMessage) = domainCallFunction($whois_send,'available');
				if($ModuleStatus == true){
					foreach($ModuleMessage as $key1=>$value1){
						if(isset($params[$key1])){
							$params[$key1]['status']	= $value1['status'];
							$params[$key1]['message']	= $value1['message'];
						}
					}
				}
			}
		}
		$Message = $params;
	}else{
		$Status	= true;
		$Message = $params;
	}
	return array($Status,$Message);
}

function domainwhois($domain){
    global $cm_db,$cm_config;
    $site   = array();
    $new    = array();
    foreach($domain as $key => $value){
        $site[$key] = array(
            'url'           => $value['tld']['whois_server'],
            'port'          => $value['tld']['whois_port'],
            'transfer'      => 1,
            'customrequest' => $value['domain']."\r\n",
        );
    }

    $DomainResult = cm_multicurl($site);
    foreach($site as $key => $value){
        if(isset($DomainResult[$key])){
            $Result = $DomainResult[$key];
            if($Result['output'] != ''){
                $domain[$key]['status']   = 'none';
                $domain[$key]['message']  = cm_lang('Alan adı sorgulaması tamamlanamadı');
                $rows       = explode("\n",$Result['output']);
                $tld_preg   = cm_json_decode($domain[$key]['tld']['whois_preg']);
                foreach($rows as $val){
                    $line = explode(":",$val);
                    $line[0] = trim($line[0]);
                    if(stristr($line[0],'domain status')){
                        $status = trim($line[1]);
                        if(strstr($line[1],' ')) {
                            $status = explode(" ", trim($line[1]));
                            $status = trim($status[0]);
                        }
                        $domain[$key]['status']   = $status;
                        $domain[$key]['message']  = cm_lang('Alan adı çıktısı okunamadı');
                        //break;
                    }
                    if(stristr($line[0],'whois server')){
                        $whois_address = strtolower(trim($line[1]));
                        if($domain[$key]['tld']['whois_port'] != $whois_address){
                            $new[$key] = array(
                                'url'               => $whois_address,
                                'port'              => $domain[$key]['tld']['whois_port'],
                                'returntransfer'    => 1,
                                'customrequest'     => $domain[$key]['domain'] . "\r\n",
                            );
                        }
                    }

                    if(is_array($tld_preg) and $tld_preg['create'] and stristr($line[0],$tld_preg['create'])){
                        preg_match('#'.$tld_preg['create'].'(.*?)$#si',$val,$date);
                        $date   = explode(":",$date[1]);
                        unset($date[0]);
                        $date   = str_replace(array('.'),'',trim(implode(":",$date),':'));
                        $time   = strtotime($date);
                        if($time){
                            $domain[$key]['domain_create_time'] = $time;
                        }
                    }

                    if(is_array($tld_preg) and $tld_preg['update'] and stristr($line[0],$tld_preg['update'])){
                        preg_match('#'.$tld_preg['update'].'(.*?)$#si',$val,$date);
                        $date   = explode(":",$date[1]);
                        unset($date[0]);
                        $date   = str_replace(array('.'),'',trim(implode(":",$date),':'));
                        $time   = strtotime($date);
                        if($time){
                            $domain[$key]['domain_update_time'] = $time;
                        }
                    }

                    if(is_array($tld_preg) and $tld_preg['end'] and stristr($line[0],$tld_preg['end'])){
                        preg_match('#'.$tld_preg['end'].'(.*?)$#si',$val,$date);
                        $date   = explode(":",$date[1]);
                        unset($date[0]);
                        $date   = str_replace(array('.'),'',trim(implode(":",$date),':'));
                        $time   = strtotime($date);
                        if($time){
                            $domain[$key]['domain_end_time'] = $time;
                        }
                    }
                }
                $string_encoding	= mb_detect_encoding($Result['output'], "UTF-8, ISO-8859-1, ISO-8859-15", true);
                $string_utf8		= mb_convert_encoding($Result['output'], "UTF-8", $string_encoding);
                $string_utf8		= htmlspecialchars($string_utf8, ENT_COMPAT, "UTF-8", true);
                if($domain[$key]['status'] != 'none'){
                    $domain[$key]['domain_status']      = strtolower($domain[$key]['status']);
                    $domain[$key]['status']             = 2;
                    $domain[$key]['message']            = cm_lang('Alan Adı sorgulama başarılı');
                    $domain[$key]['whois']              = $string_utf8;
                    unset($new[$key]);
                }else{
                    $domain[$key]['message']            = cm_lang('Alan Adı sorgulama başarılı');
                    if(stristr($string_utf8,$domain[$key]['tld']['whois_return'])){
                        $domain[$key]['status']             = 1;
                        $domain[$key]['domain_status']      = 'null';
                    }elseif(stristr($string_utf8,'Organization')){
                        $domain[$key]['status']             = 2;
                        $domain[$key]['domain_status']      = 'transferno';
                    }else{
                        $domain[$key]['status']             = 0;
                        $domain[$key]['domain_status']      = 'no';
                        $domain[$key]['message']            = cm_lang('Alan adı sorgulanamadı');
                    }
                    $domain[$key]['whois']              = $string_utf8;
                }
                if($cm_config['whois_log'] == 1){
                    $add = $domain[$key];
                    $add['create_time'] = cm_time();
                    $add['update_time'] = cm_time();
                    $add['tld_name']    = $domain[$key]['tld']['tld_name'];
                    unset($add['whois'],$add['tld'],$add['transfer']);
                    $whois_controlX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."whois WHERE `domain`='".$cm_db->sql_escape($domain[$key]['domain'])."'");
                    if($cm_db->sql_errno() == 0){
                        if($cm_db->sql_num_rows($whois_controlX) == 1){
                            $whois_control = $cm_db->sql_fetch_assoc($whois_controlX);
                            $add['amount'] = ($whois_control['amount']+1);
                            unset($add['create_time']);
                            $cm_db->sql_query_update('whois', $add, array('whois_id' => $whois_control['whois_id']));
                        }else{
                            $cm_db->sql_insert_add('whois', $add);
                        }
                    }
                    unset($add);
                }
            }else{
                $domain[$key]['status']     = 0;
                $domain[$key]['message']    = cm_lang('Alan adı çıktısı okunamadı');
                $domain[$key]['whois']      = null;
            }
        }else{
            $domain[$key]['status']     = 0;
            $domain[$key]['message']    = cm_lang('Alan adı sorgulama yapılamadı');
            $domain[$key]['whois']      = null;
        }
    }
    return $domain;
}

function domainGetList($params = null){
	if(!is_array($params)){
		if(cm_numeric($params)){
			$params = array('user_id'=>$params);
		}else{
			return false;
		}
	}
	global $cm_config,$cm_db;
	$prefix 			= 'd';
	$tablo 				= 'domains';
	$db_domain_columns	= $cm_db->sql_tablo_info($tablo);
	if(!isset($params['user_id']) or (isset($params['user_id']) and !cm_numeric($params['user_id']))){ return false; }
	$Query = "SELECT --sql_query-- FROM ".$cm_config['db_prefix'].$tablo." AS ".$prefix." WHERE ".$prefix.".user_id='".$cm_db->sql_escape($params['user_id'])."'";
	if(isset($params['and']) and is_array($params['and']) and count($params['and']) > 0){
		$And = array();
 		$params['and']		= $cm_db->db_params_control($params['and'],$db_domain_columns);
		foreach($params['and'] as $key=>$value){
			$And[] = $prefix.'.'.$key."='".$cm_db->sql_escape($value)."'";
		}
		$Query .= " AND ".implode(" AND ",$And);
	}
	if(isset($params['or']) and is_array($params['or']) and count($params['or']) > 0){
		$Or = array();
 		$params['or']		= $cm_db->db_params_control($params['or'],$db_domain_columns);
		foreach($params['or'] as $key=>$value){
			$Or[] = $prefix.'.'.$key."='".$cm_db->sql_escape($value)."'";
		}
		$Query .= " AND (".implode(" OR ",$Or).")";
	}
	if(isset($params['orderby']) and is_array($params['orderby'])){
		$key = array_keys($params['orderby']);
		$Query .= " ORDER BY ".$prefix.'.'.$key[0]." ".$params['orderby'][$key[0]];
	}
	$ListID = $cm_db->sql_tablo_info($tablo,true);
	if(count($ListID) == 0){
		return false;
	}
	foreach($ListID as $key=>$value){
		if($value['Extra'] == "auto_increment"){
			$Listauto_increment = $value['Field'];
		}
	}
	if(!isset($Listauto_increment)){ return false; }
	$ReturnList = array();
	$QueryTotal = str_ireplace('--sql_query--','COUNT('.$prefix.'.'.$Listauto_increment.') as Total',$Query);
	$ListX = $cm_db->sql_query($QueryTotal);
	if($cm_db->sql_errno() == 0){
		$List = $cm_db->sql_fetch_assoc($ListX);
		$ReturnList['total'] = $List['Total'];
	}else{
		return false;
	}
	if(isset($params['limit']) and is_array($params['limit'])){
		$key = array_keys($params['limit']);
		if(cm_numeric($key[0]) and cm_numeric($params['limit'][$key[0]])){
			$Query .= " LIMIT ".$key[0].",".$params['limit'][$key[0]];
		}
	}
	$QueryList = str_ireplace('--sql_query--','*',$Query);
	$ListX = $cm_db->sql_query($QueryList);
	if($cm_db->sql_errno() == 0){
		while($List = $cm_db->sql_fetch_assoc($ListX)){
			$tld = explode(".",$List['domain_name']);
			unset($tld[0]);
			$tld = $cm_db->sql_assoc('tlds',array('tld_name'=>implode(".",$tld)));
			$List['tld'] = $tld;
			$ReturnList['list'][$List[$Listauto_increment]] = $List;
		}
	}
	return $ReturnList;
}

function domainTld($tld_name = null,$status = 0){
	global $cm_db,$cm_config;
	if($tld_name != ""){
		$Tld = $cm_db->sql_assoc('tlds',array('tld_name'=>$tld_name));
		if($Tld){ return $Tld; }
	}else{
		$Return = array();
		$tldX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."tlds WHERE tld_status in (".($status==0?'0,1':'1').") ORDER BY tld_order ASC");
		if($cm_db->sql_errno() == 0){
			while($tld = $cm_db->sql_fetch_assoc($tldX)){
				$Return[$tld['tld_name']] = $tld;
			}
			if(count($Return) > 0){ return $Return; }
		}
	}
	return false;
}

function domain_idn_asci($domain_name){
	return idn_to_ascii($domain_name);
}

function domain_idn_utf8($domain_name){
	return idn_to_utf8($domain_name);
}

function domainName($params){
	$domain		= trim(str_replace(array("https","http",":",'/','www.'),'',$params));
	if(!strstr($domain,".")){ $domain = $domain.'.com';	}
	if(strstr($domain,'---') or strstr($domain,'-.') or strstr($domain,'.-')){ return false; }
	$OldDomain = $domain;
	$IDNStatus = 0;
	if(domain_verify($domain) == false){
		$domain	= domain_idn_asci($domain);
		if(!$domain){
			return false;
		}
		$IDNStatus	= 1;
	}elseif(substr($domain,0,4) == 'xn--'){
		$IDNStatus	= 1;
	}
	$domain		= explode(".",$domain);
	$DomainName	= $domain[0];
	unset($domain[0]);
	$DomainExt	= implode(".",$domain);
	$Return = array(
		'domain'		=> ($IDNStatus==1?domain_idn_utf8($DomainName.'.'.$DomainExt):$DomainName.'.'.$DomainExt),
		'name'			=> $DomainName,
		'tld'			=> $DomainExt,
		'idn_status'	=> $IDNStatus
	);
	if(domain_verify($Return['name'].'.'.$Return['tld']) == false){ return false; }
	return $Return;
}

function domainPrice($product_type_id,$tld = null,$year = 0){
	$priceList = array();
	if(!is_array($tld)){
		$domain = domainName($tld);
		if($domain){
			$tld = domainTld($domain['tld']);
		}
		if(!is_array($tld)){ return false; }
	}
	if($product_type_id == 5){
		$MaxYear 	= 1;
		$MinYear 	= 1;
		$Domprice	= $tld['backorder_price'];
		$Domdiscount= 0.00;
	}elseif($product_type_id == 8){
		$MaxYear 	= 1;
		$MinYear 	= 1;
		$Domprice	= $tld['transfer_price'];
		$Domdiscount= $tld['transfer_discount'];
	}elseif($product_type_id == 6){
		$MaxYear	= $tld['max_register'];
		$MinYear	= $tld['min_register'];
		$Domprice	= $tld['register_price'];
		$Domdiscount= $tld['register_discount'];
	}elseif($product_type_id == 7){
		$MaxYear 	= $tld['max_renewal'];
		$MinYear 	= $tld['min_renewal'];
		$Domprice	= $tld['renewal_price'];
		$Domdiscount= $tld['renewal_discount'];
	}
	if($year != 0){
		if($MaxYear >= $year and $MinYear <= $year){
			$MaxYear = $year;
			$MinYear = $year;
		}else{
			return false;
		}
	}
	$currency	= $tld['tld_currency'];
	for($i=$MinYear;$i<=$MaxYear;$i++){
		if($i == 1 or $tld['register_dis_type'] == 0){
			$price		= (($Domprice*$i));
			$discount	= ($Domdiscount*$i);
		}else{
			$price		= ($Domprice*$i);
			$discount	= $Domdiscount;
		}
		$priceList[$i] = array(
			'price'		=> cm_money_format($price),
			'currency'	=> $currency,
			'discount'	=> cm_money_format($discount),
		);
	}
	return (is_array($priceList) and count($priceList) > 0)?$priceList:false;
}