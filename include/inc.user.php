<?php
/** Üye İşlemleri */
$login_admin_id	= 0;
$login_user_id	= 0;
$admin 			= array('user_id' => 0);
$user 			= array('user_id' => 0);
if(isset($session_id)){
    if(isset($_SESSION['admin_id']) and cm_numeric($_SESSION['admin_id'])){
        $login_admin_id = $_SESSION['admin_id'];
    }
    if(isset($_SESSION['user_id']) and cm_numeric($_SESSION['user_id'])){
        $login_user_id = $_SESSION['user_id'];
    }
    if($login_admin_id > 0){
        $adminX = userGetDetails($login_admin_id);
        if($adminX){
            $admin = $adminX;
            unset($adminX);
        }
    }
    if($login_user_id > 0){
        $userX = userGetDetails($login_user_id);
        if($userX){
            $user = $userX;
            unset($userX);
        }
    }
    $logout = cm_get_request('logout');
    if($logout == 1 and $user['user_id'] > 0){	/** Üye Çıkış */
        userLogout($user);
        cm_redirect();
    }elseif($logout == 2 and $admin['user_id'] > 0){ /** Admin Çıkış */
        unset($_SESSION['admin_id']);
        cm_redirect($admindir.'/login.php');
    }
}

$cm_user    = $user;
$cm_admin   = $admin;
unset($user,$admin);

if(isset($page_settings['login']) and $page_settings['login'] == true and $cm_user['user_id'] == 0){	/** Üye sayfaları kontrol */
    cm_redirect(cm_link('login', ['return' => urlencode(trim($_SERVER["REQUEST_URI"]))]));
}


function userlog($log,$user = array()){
    global $cm_browser,$cm_db,$cm_config,$ip,$cm_user,$session_id,$ip_port;
    $query = "SELECT * FROM ".$cm_config['db_prefix']."userlogin WHERE session_id='".$cm_db->sql_escape($session_id)."'";

    $user_id = 0;
    if(count($user) > 0){
        $query .= " AND user_id=".$user['user_id'];
        $user_id = $user['user_id'];
    }elseif($cm_user['user_id'] > 0){
        $query .= " AND user_id=".$cm_user['user_id'];
        $user_id = $cm_user['user_id'];
    }elseif(isset($_SESSION['user_id']) and $_SESSION['user_id'] > 0){
        $query .= " AND user_id=".$_SESSION['user_id'];
        $user_id = $_SESSION['user_id'];
    }

    $userLogQuery = $cm_db->sql_query($query);
    if($cm_db->sql_errno() == 0){
        $login = array();
        if($cm_db->sql_num_rows($userLogQuery) == 0){
            NewLogCreate:
            $login = array(
                'user_id'	    => $user_id,
                'admin_id'	    => isset($_SESSION['admin_id'])?$_SESSION['admin_id']:0,
                'ip_address'	=> isset($ip)?$ip:null,
                'ip_port'	    => $ip_port,
                'session_id'    => isset($session_id)?$session_id:null,
                'browser'       => $cm_browser['browser'],
                'login_system'  => $cm_browser['sistem'],
                'login_time'	=> cm_time()
            );
            list($add_status,$add_id) = $cm_db->sql_insert_add('userlogin',$login);
            if($add_status){
                $login['login_id'] = $add_id;
            }else{
                return false;
            }
        }else{
            $login = $cm_db->sql_fetch_assoc($userLogQuery);
            if (isset($_SESSION['user_id']) and $_SESSION['user_id'] != $login['user_id']) {
                goto NewLogCreate;
            }
        }

        if($login["user_id"] == 0 and isset($_SESSION['user_id']) and $_SESSION['user_id'] > 0){
            $cm_db->sql_query_update('userlogin', array('user_id' => $_SESSION['user_id']), array('login_id' => $login['login_id']));
        }

        $AddLog = array(
            'ip'            => $ip,
            'ip_port'       => $ip_port,
            'login_id'      => $login['login_id'],
            'browser_info'  => cm_json_encode($cm_browser),
            'log'           => $log,
            'log_time'      => cm_time(),
        );
        list($lod_status,$log_id) = $cm_db->sql_insert_add('userprocess',$AddLog);
        return $lod_status;
    }
    return false;
}

function userGetDetails($user_id){
    global $cm_config,$cm_db;
    if(cm_numeric($user_id)){
        $userX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."users WHERE user_id='".$cm_db->sql_escape($user_id)."'");
        if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($userX) == 1){
            $user = $cm_db->sql_fetch_assoc($userX);
            $user['address'] = null;
            $userAddressX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."user_address WHERE default_address=1 AND user_id='".$cm_db->sql_escape($user_id)."'");
            if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($userAddressX) == 1){
                $userAddress = $cm_db->sql_fetch_assoc($userAddressX);
                $country = $cm_db->sql_assoc('country',array('country_id'=>$userAddress['country_id']));
                $userAddress['country'] = $country['country_code'];
                $user['address'] = $userAddress;
            }
            return $user;
        }
    }
    return false;
}

function userLogin($user,$password,$adminLogin = false){
    global $cm_config,$cm_db,$session_id;
    $status = false;
    $message = null;
    $loginX = $cm_db->sql_query("SELECT user_id,rank_id,password FROM ".$cm_config['db_prefix']."users WHERE (email_address='".$cm_db->sql_escape($user)."' OR username='".$cm_db->sql_escape($user)."') LIMIT 1");
    if($cm_db->sql_errno() == 0){
        if($cm_db->sql_num_rows($loginX) == 1){
            $login = $cm_db->sql_fetch_assoc($loginX);
            if($login['password'] == cm_user_password($password)){
                if($login['rank_id'] != 0){
                    if($adminLogin == true){    /** Admin Kontrol Alanı */
                        if($login['rank_id'] < 10){
                            $message = cm_lang('Bu alana giriş izniniz bulunmuyor');
                        }
                    }
                    if($message == null) {
                        $cm_db->sql_query_update('baskets', array('user_id' => $login['user_id']), array('session_id' => $session_id));
                        $status = true;
                        $message = array(
                            'user_id' => $login['user_id'],
                            'message' => cm_lang('Giriş başarılı'),
                        );
                        $_SESSION['user_id'] = $login['user_id'];
                        if ($adminLogin == true) {
                            $_SESSION['admin_id'] = $login['user_id'];
                        }
                        userlog(cm_lang('Oturum açıldı'),$login);
                    }
                }else{
                    userlog(cm_lang('Hesabınız kapatıldı.Oturum açılmadı'),$login);
                    $message = cm_lang('Hesabınız kapatıldı');
                }
            }else{
                userlog(cm_lang('Şifre hatalı girildi.Oturum açılmadı'),$login);
                $message = cm_lang('Şifreniz hatalı');
            }
        }else{
            $message = cm_lang('Böyle bir üyelik bulunamadı');
        }
    }else{
        $message = cm_lang('Geçici hata oluştu');
    }
    return array($status,$message);
}

function userAdd($user_data,$user = array()){
    global $cm_db,$cm_config,$ip;
    $status = false;
    $message = null;
    if(count($user_data) > 0){
        if(isset($user_data['password_control'])){ unset($user_data['password_control']); }
        $email_verify = $cm_db->sql_query("SELECT user_id FROM ".$cm_config['db_prefix']."users WHERE email_address='".$cm_db->sql_escape($user_data['email_address'])."'");
        if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($email_verify) == 0){
            $password = cm_user_password($user_data['password']);
            $AddUser = array(
                'email_address'		=> $user_data['email_address'],
                'password'			=> $password,
                'registration_date'	=> cm_time(),
                'registration_ip'	=> $ip,
                'last_login'		=> cm_time(),
                'last_ip'			=> $ip,
            );
            list($add_status,$add_message) = $cm_db->sql_insert_add('users',$AddUser);
            if($add_status == true){
                $status = true;
                $message = array(
                    'user_id'	=> $add_message,
                    'message'	=> cm_lang('Üyelik açıldı'),
                );
                $_SESSION['user_id'] = $add_message;
                userlog(cm_lang('Yeni üyelik oluşturuldu'));
            }else{
                $message = cm_lang('Üyelik açılamadı tekrar deneyiniz');
            }
        }else{
            $message = cm_lang('Email adresi sistemde kayıtlı');
        }
    }else{
        $message = cm_lang('Eksik veya hatalı veri');
    }
    return array($status,(($status == false and is_array($message))?implode("<br />",$message):$message));
}

function userLogout($user){
    if(isset($_SESSION['user_id'])){
        userlog('Oturumdan çıkış yapıldı');
        unset($_SESSION['user_id']);
    }
}

function userEdit($user,$user_data){
    global $cm_db;
    if(cm_numeric($user)){
        $user = userGetDetails($user);
    }
    $status = false;
    if(is_array($user_data) and count($user_data) > 0){
        list($status,$response) = $cm_db->sql_query_update('users', $user_data, [ 'user_id' => $user["user_id"]]);
        if($status == true){
            $user = array_merge($user,$user_data);
            $response = [
                'message'   => cm_lang('Kullanıcı bilgileri güncellendi'),
                'user'      => $user,
            ];
            userlog(cm_lang('Üye bilgileri güncellendi'),$user);
        }else{
            $response = cm_lang('Üye bilgileri güncellenemedi');
        }
    }else{
        $response = cm_lang('Kullanıcı bilgileri eksik');
    }
    return [$status,$response];
}

function userAddress($user,$user_data,$users = array()){
    global $cm_db,$cm_config;
    $status = false;
    $message = null;
    if(!is_array($user)){
        $user = userGetDetails($user);
    }

    if(array_key_exists('tax_office',$user_data)){
        $billingAddress['tax_office'] = $user_data['billing_type']==0?null:$user_data['tax_office'];
    }
    if(array_key_exists('tax_number',$user_data)){
        $billingAddress['tax_number'] = $user_data['tax_number'];
    }

    $billingX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."user_address WHERE user_id='".$cm_db->sql_escape($user['user_id'])."' LIMIT 1");
    if($cm_db->sql_errno() == 0){
        if($cm_db->sql_num_rows($billingX) == 0){
            list($billing_status,$billing_message) = $cm_db->sql_insert_add('user_address', $user_data);
        }else{
            $billing = $cm_db->sql_fetch_assoc($billingX);
            list($billing_status,$billing_message) = $cm_db->sql_query_update('user_address', $user_data, ['user_address_id' => $billing['user_address_id']]);
        }
        if($billing_status == true){
            $status = true;
            $message = array(
                'message'	=> cm_lang('Fatura adresi güncellendi'),
                'user'		=> userGetDetails($user['user_id']),
            );
            userlog(cm_lang('Fatura adresi güncellendi'),$user);
        }else{
            $message = cm_lang('Fatura adresi güncellenemedi');
        }
    }else{
        $message = cm_lang('Geçici hata oluştu');
    }
    return array($status,(($status == false and is_array($message))?implode("<br />",$message):$message));
}