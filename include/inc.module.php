<?php

function cm_module_list($module_id = null){
    global $cm_db,$cm_config;
    $response   = false;
    $module     = $cm_db->sql_assoc('modules',array('module_id'=>$module_id));
    if($module){
        $module_configX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."module_config WHERE module_id=".$module['module_id']);
        while($module_config = $cm_db->sql_fetch_assoc($module_configX)){
            $module[$module_config['module_feature_key']] = cm_decode($module_config['module_feature_value']);
        }
        $response	= $module;
    }
    return $response;
}

function cm_module_call($module_id){

    if(cm_numeric($module_id)) {
        $Module = cm_module_list($module_id);
        if (!is_array($Module)) {
            return array(false, cm_lang('Modül bulunamadı'));
        }
    }else{
        $Module = $module_id;
    }

    global $cm_dirlist;
    $type                           = $Module["module_dir"];
    $Module["module_directory"]     = $cm_dirlist['module'].$Module['module_dir'].'/'.$Module['module'].'/';;
    $module_name 	                = $Module['module'];
    $module_variable                = $Module['module'].'_'.$type;

    global ${$module_variable};

    if(isset(${$module_variable})){ /** Aynı ad farklı modül olur ise çakışır. Kontrol et */
        goto GoModule;
    }

    $IsFile = cm_path.'modules/'.$type.'/';
    if(!is_dir($IsFile)){ return array(false,cm_lang('Modül klasörü bulunamadı')); }

    $module_dir = $IsFile.$module_name.'/';
    if(!is_dir($module_dir)){ return array(false,cm_lang('Modül klasörü bulunamadı')); }

    $module_fileConfig = $module_dir.$module_name.'.config.php';
    if(!file_exists($module_fileConfig)){ return array(false,cm_lang('Modül config dosyası bulunamadı')); }

    $module_file = $module_dir.$module_name.'.php';
    if(!file_exists($module_file)){ return array(false,cm_lang('Modül dosyası bulunamadı')); }

    include($module_fileConfig);
    include($module_file);

    if(!isset(${$module_variable})){
        return array(false,cm_lang('Modül değişkeni tanımlı değil'));
    }

    GoModule:
    if(isset(${$module_variable}['data']['form'])) {
        foreach (${$module_variable}['data']['form'] as $key=>$value) {
            if(!isset($Module[$key])){
                $Module[$key] = (isset($value["value"])?$value["value"]:null);
            }
        }
    }

    //return array(true,array_merge(array('module'=>$Module),${$module_variable}));
    return array(true,$Module);
}

function ModuleGetParams($id,$type){
    $response   = null;
    if(cm_numeric($id) and $id > 0){
        $info = serviceGetDetails($id,$type);
        if($info) {
            $array  = [];
            list($status, $module) = cm_module_call($info["module_id"]);
            if($status == true){
                $response = array_merge($module,$info);
            }else{
                $response = $module;
            }
        }else{
            $response = cm_lang('Hizmet bilgileri çekilemedi');
        }
    }else {
        $response = cm_lang('ID hatalı');
    }
    return $response;
}

function ModuleCallFunction($id,$func_name,$type,$data = []){
    $status = false;
    $params = ModuleGetParams($id,$type);
    if(is_array($params)) {
        if(count($data) > 0) {
            $params = array_merge($params, $data);
        }
        if (function_exists($params['module'] . "_" . $func_name)) {
            $response = call_user_func($params['module'] . "_" . $func_name, $params);
            if (is_array($response) and count($response) == 2) {
                return $response;
            } else {
                $response = cm_lang('Modül çıktısı okunamadı');
            }
        } else {
            $Log = 'Modülde ilgi fonksiyon bulunamadı fonksiyon : ##function##';
            cm_systemlog(cm_lang($Log, ['function' => $func_name])."\n".var_export($params, true));
            $response = cm_lang('Modül fonksiyon bulunamadı');
        }
    }else{
        $response = $params;
    }
    return [$status,$response];
}

function ModuleAdminPanel($id,$type){
    return ModuleCallFunction($id,'AdminPanel',$type);
}

function ModuleUserPanel($id,$type){
    return ModuleCallFunction($id,'UserPanel',$type);
}

function ModuleCreate($id,$type){
    return ModuleCallFunction($id,'Create',$type);
}

function ModuleStart($id,$type){
    return ModuleCallFunction($id,'Start',$type);
}

function ModuleReset($id,$type){
    return ModuleCallFunction($id,'Reset',$type);
}

function ModuleSuspend($id,$type){
    return ModuleCallFunction($id,'Suspend',$type);
}