<?php
function serviceGetList($service_type,$data = []){
	global $cm_config,$cm_db,$cm_user;
    if(isset($data['user']) and count($data['user']) > 0){
	    $cm_user = $data['user'];
    }
	if(!$cm_user or $cm_user['user_id'] == 0){
		return cm_lang('Kullanıcı bulunamadı');
	}
	$where = [];
	if($service_type == 'server'){
		$prefix 			= 's';
		$tablo 				= 'servers';
        $where[]            = $prefix.".server_delete=0";
        if(isset($data['serviceID'])){
            $where[]            = $prefix.".server_id=".$data['serviceID'];
        }
	}elseif($service_type == 'domain'){
		$prefix 			= 'd';
		$tablo 				= 'domains';
		$where[]            = $prefix.".domain_delete=0";
		if(isset($data['serviceID'])){
            $where[]            = $prefix.".domain_id=".$data['serviceID'];
        }
	}elseif($service_type == 'hosting'){
		$prefix 			= 'h';
		$tablo 				= 'hosting';
        $where[]            = $prefix.".hosting_delete=0";
        if(isset($data['serviceID'])){
            $where[]            = $prefix.".hosting_id=".$data['serviceID'];
        }
	}elseif($service_type == 'ssl'){
		$prefix 			= 's';
		$tablo 				= 'ssl';
	}elseif($service_type == 'service'){
		$prefix 			= 's';
		$tablo 				= 'services';
	}elseif($service_type == 'ticket'){
		$prefix 			= 't';
		$tablo 				= 'tickets';
		$where[]            = $prefix.".ticket_delete=0";
		if(isset($data['serviceID'])){
			$where[]            = $prefix.".ticket_id=".$data['serviceID'];
		}
	}elseif($service_type == 'order'){
		$prefix 			= 'o';
		$tablo 				= 'orders';
		$where[]            = $prefix.".order_delete=0";
		if(isset($data['serviceID'])){
			$where[]            = $prefix.".order_id=".$data['serviceID'];
		}
	}elseif($service_type == 'module'){
		if(isset($data['serviceID'])) {
			$module = $cm_db->sql_assoc('modules', ['module_id' => $data['serviceID']], ["module_id", "module_id AS `id`", "'module' AS `mod_type`"]);
			if ($module) {
				return $module;
			}
		}
		return false;
	}else{
	    return cm_lang('Hizmet bulunamadı');
    }
    $where[] = $prefix.".user_id=".$cm_user['user_id'];
	$db_domain_columns	= $cm_db->sql_tablo_info($tablo);

	$Query = "SELECT --sql_query-- FROM ".$cm_config['db_prefix'].$tablo." AS ".$prefix." WHERE ".implode(" AND ",$where);
	if(isset($data['and']) and is_array($data['and']) and count($data['and']) > 0){
		$And = array();
 		$data['and']		= $cm_db->db_params_control($data['and'],$db_domain_columns);
		foreach($data['and'] as $key=>$value){
			if(is_array($value)){
				list($ids,$sts) = $value;
				if($sts == 1){
					$n = 'IN';
				}else{
					$n = 'NOT IN';
				}
				$df = array();
				foreach($ids as $dg){
					$df[] = "'".$cm_db->sql_escape($dg)."'";
				}
				$And[] = $prefix.'.'.$key." ".$n." (".implode(',',$df).")";
			}else{
				$And[] = $prefix.'.'.$key."='".$cm_db->sql_escape($value)."'";
			}
		}
		$Query .= " AND ".implode(" AND ",$And);
	}
	if(isset($data['or']) and is_array($data['or']) and count($data['or']) > 0){
		$Or = array();
 		$data['or']		= $cm_db->db_params_control($data['or'],$db_domain_columns);
		foreach($data['or'] as $key=>$value){
			$Or[] = $prefix.'.'.$key."='".$cm_db->sql_escape($value)."'";
		}
		$Query .= " AND (".implode(" OR ",$Or).")";
	}
	if(isset($data['orderby']) and is_array($data['orderby'])){
		$key = array_keys($data['orderby']);
		$Query .= " ORDER BY ".$prefix.'.'.$key[0]." ".$data['orderby'][$key[0]];
	}
	$ListID = $db_domain_columns;
	if(count($ListID) == 0){
		return cm_lang('Kontrol sağlanamadı');
	}

	foreach($ListID as $key=>$value){
        $Listauto_increment = $value;
        break;
	}
	if(!isset($Listauto_increment)){ return false; }
	$ReturnList = array();
	$QueryTotal = str_ireplace('--sql_query--','COUNT('.$prefix.'.'.$Listauto_increment.') as Total',$Query);
	$ListX = $cm_db->sql_query($QueryTotal);
	if($cm_db->sql_errno() == 0){
		$List = $cm_db->sql_fetch_assoc($ListX);
		$ReturnList['total'] = $List['Total'];
	}else{
		return cm_lang('Geçici DB hatası oluştu');
	}
	if(isset($data['limit'])){
		if(is_array($data['limit'])) {
			$key = array_keys($data['limit']);
			if (cm_numeric($key[0]) and cm_numeric($data['limit'][$key[0]])) {
				$Query .= " LIMIT " . $key[0] . "," . $data['limit'][$key[0]];
			}
		}elseif(cm_numeric($data['limit'])){
			$Query .= " LIMIT " . $data['limit'];
		}
	}
	//echo $Query;
	$QueryList = str_ireplace('--sql_query--','*',$Query);
	$ListX = $cm_db->sql_query($QueryList);
	if($cm_db->sql_errno() == 0){
		$product = array();
		while($List = $cm_db->sql_fetch_assoc($ListX)){
		    if(isset($List['password'])){
                $List['password'] = cm_decode($List['password']);
            }
		    if(isset($List['product_id']) and $List['product_id'] != 0) {
                if (!isset($product[$List['product_id']])) {
                    $product[$List['product_id']] = product_list($List['product_id']);
                }
                $ReturnList['list'][$List[$Listauto_increment]] = array_merge($List, array('product' => $product[$List['product_id']]));
            }else{
                $ReturnList['list'][$List[$Listauto_increment]] = $List;
            }
		}
	}
	return $ReturnList;
}

function serviceGetDetails($serviceid,$service_type){
    $data = [
        'serviceID' => $serviceid,
    ];
    $Response = serviceGetList($service_type,$data);
	if(is_array($Response) and $Response['total'] == 1){
	    return $Response['list'][$serviceid];
    }
	return cm_lang('Hizmet bulunamadı');
}
