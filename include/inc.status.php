<?php
$delete_status = array(
	1	=> array(
		'name'	=> 'Silindi',
		'color'	=> 'red',
	),
);
$ticket_status = array(
	1	=> array(
		'name'	=> 'Açık',
		'color'	=> 'green',
	),
	2	=> array(
		'name'	=> 'Beklemede',
		'color'	=> 'orange',
	),
	3	=> array(
		'name'	=> 'Kapalı',
		'color'	=> 'red',
	),
);
$module_status = array(
    0	=> array(
        'name'	=> 'Kurulum Yapılmadı',
        'color'	=> 'gray',
    ),
    1	=> array(
        'name'	=> 'Aktif',
        'color'	=> 'green',
    ),
    2	=> array(
        'name'	=> 'Pasif',
        'color'	=> 'red',
    ),
);
$order_status = array(
	0	=> array(
		'name'	=> 'Ödeme Bekliyor',
		'color'	=> 'gray',
	),
	1	=> array(
		'name'	=> 'Ödendi',
		'color'	=> 'green',
	),
	2	=> array(
		'name'	=> 'Onay Bekliyor',
		'color'	=> 'orange',
	),
	3	=> array(
		'name'	=> 'İptal Edildi',
		'color'	=> 'red',
	),
);
$domain_status = array(
	0	=> array(
		'name'	=> 'Kayıt Bekliyor',
		'color'	=> 'gray',
	),
	1	=> array(
		'name'	=> 'Aktif',
		'color'	=> 'green',
	),
	2	=> array(
		'name'	=> 'Askıya Alındı',
		'color'	=> 'red',
	),
	3	=> array(
		'name'	=> 'Transfer Başlat',
		'color'	=> 'gray',
	),
	4	=> array(
		'name'	=> 'Transfer Sürecinde',
		'color'	=> 'orange',
	),
	5	=> array(
		'name'	=> 'Backorder',
		'color'	=> 'blue',
	),
	6	=> array(
		'name'	=> 'Süresi Uzatılacak',
		'color'	=> 'gray',
	),
);
$server_status = array(
	0	=> array(
		'name'	=> 'Kuruluyor',
		'color'	=> 'blue',
	),
	1	=> array(
		'name'	=> 'Aktif',
		'color'	=> 'green',
	),
	2	=> array(
		'name'	=> 'Askıya Alındı',
		'color'	=> 'orange',
	),
	3	=> array(
		'name'	=> 'Kapalı',
		'color'	=> 'red',
	),
);
$hosting_status = array(
	0	=> array(
		'name'	=> 'Kuruluyor',
		'color'	=> 'blue',
	),
	1	=> array(
		'name'	=> 'Aktif',
		'color'	=> 'green',
	),
	2	=> array(
		'name'	=> 'Askıya Alındı',
		'color'	=> 'orange',
	),
	3	=> array(
		'name'	=> 'Kapalı',
		'color'	=> 'red',
	),
);
$ssl_status = array(
	0	=> array(
		'name'	=> 'Kuruluyor',
		'color'	=> 'blue',
	),
	1	=> array(
		'name'	=> 'Aktif',
		'color'	=> 'green',
	),
	2	=> array(
		'name'	=> 'Askıya Alındı',
		'color'	=> 'orange',
	),
	3	=> array(
		'name'	=> 'Kapalı',
		'color'	=> 'red',
	),
);
$service_status = array(
	0	=> array(
		'name'	=> 'Bekliyor',
		'color'	=> 'blue',
	),
	1	=> array(
		'name'	=> 'Aktif',
		'color'	=> 'green',
	),
	2	=> array(
		'name'	=> 'Askıya Alındı',
		'color'	=> 'orange',
	),
);