<?php

function cm_systemlog($detail){
    global $cm_browser,$cm_db,$cm_config,$ip,$user,$session_id,$ip_port;
    $LogAdd = array(
        'user_id'	=> isset($_SESSION['user_id'])?$_SESSION['user_id']:0,
        'admin_id'	=> isset($_SESSION['admin_id'])?$_SESSION['admin_id']:0,
        'ip'		=> isset($ip)?$ip:null,
        'ip_port'	=> $ip_port,
        'session_id'=> isset($session_id)?$session_id:null,
        'browser'	=> isset($cm_browser['browser'])?$cm_browser['browser']:null,
        'log'		=> ($detail!=""?var_export($detail,true):cm_lang('Log detayı yazılmamış')),
        'server_log'=> var_export(array('SERVER'=>$_SERVER,'REQUEST'=>$_REQUEST),true),
        'time'		=> cm_time()
    );
    if(isset($cm_config['report_mail']) and $cm_config['report_mail']){	// Log için mail gönder

    }
    list($LogStatus,$LogInfo) = $cm_db->sql_insert_add('systemlogs',$LogAdd);
    return $LogStatus;
}

function cm_service_info($id,$type,$info = true){
	echo 'Sil La bunu'; exit;
	global $cm_db;
	$response = false;
	if($type == 'domain'){
		if($info) {
			$domain = domainGetDetails($id);
		}else{
			$domain = $cm_db->sql_assoc('domains',array('domain_id' => $id),array("module_id","domain_id AS `id`","'domain' AS `mod_type`","user_id"));
		}
		if($domain){
			$response = $domain;
		}
	}elseif($type == 'server'){

	}elseif($type == 'hosting'){

	}elseif($type == 'module'){
		$module = $cm_db->sql_assoc('modules',array('module_id' => $id),array("module_id","module_id AS `id`","'module' AS `mod_type`"));
		if($module){
			$response = $module;
		}
	}
	return $response;
}

function cm_dirList($dir){
	$dirList = false;
	if(is_dir($dir)) {
        $dirList = array();
		if ($handle = opendir($dir)) {
			while (false !== ($entry = readdir($handle))) {
				$entry = trim($entry);
				if ($entry != "" and preg_match("#^[a-zA-Z0-9_-]+$#",$entry) and $entry != "." and $entry != "..") {
					$dirList[] = $entry;
				}
			}
			closedir($handle);
		}
	}
	return $dirList;
}

function cm_statusWrite($module,$status){
    $return = null;
    if($module == 'module'){
        global $module_status;
        if($module_status[$status]){
            $return = '<span style="color: '.$module_status[$status]['color'].';">'.cm_lang($module_status[$status]['name']).'</span>';
        }
    }
    return $return;
}

function cm_siteurl(){
	global $cm_config;
	$protocol = 'http://';
	if(isset($cm_config["ssl_redirect"]) and $cm_config["ssl_redirect"] == 1){ $protocol = 'https://'; }
	$cm_config["domain"] = trim($cm_config["domain"],"/")."/";
	return $protocol.$cm_config["domain"];
}

function cm_db_dataControl($data,$optional = []){
	foreach($data as $key => $value){
		$delete = null;
		if(!$value or is_array($value)){
			$delete = $key;
		}
		if($delete == null and array_key_exists('delete',$optional) and count($optional['delete']) > 0 and in_array($key,$optional['delete'])){
			$delete = $key;
		}

		if($delete != null){
			unset($data[$delete]);
		}
	}
	return $data;
}

function cm_htmlcompress($buffer) {
	$search = array(
		'/\>[^\S ]+/s',  // strip whitespaces after tags, except space
		'/[^\S ]+\</s',  // strip whitespaces before tags, except space
		'/(\s)+/s'       // shorten multiple whitespace sequences
	);
	$replace = array(
		'>',
		'<',
		'\\1'
	);
	$buffer = preg_replace($search, $replace, $buffer);
	return $buffer;
}

function cm_inputType($data,$activeData = array()){
    $response = null;
    $value = '';
    if(count($activeData) > 0){
        if(isset($activeData[$data['name']])){
            $value = cm_htmlclear($activeData[$data['name']]);
        }
    }elseif(isset($data['value'])){
        $value = cm_htmlclear($data['value']);
    }

    if($data['tag'] == 'input'){

        $response = '<div class="form-group">
                        <label for="exampleInputEmail1">'.cm_htmlclear($data['form_name']).'</label>
                        <input'.((isset($data['required']) and $data['required'] == 1)?' required=""':null).' type="'.cm_htmlclear($data['type']).'" name="'.cm_htmlclear($data['name']).'" class="form-control" value="'.$value.'" placeholder="'.cm_htmlclear($data['placeholder']).'">
                        <small class="form-text text-muted">'.cm_htmlclear($data['form_desc']).'</small>
                    </div>';

    }elseif($data['tag'] == 'select'){

        $option = array();
        if(isset($data['options'])) {
            foreach ($data['options'] as $key=>$val) {
                $option[] = '<option value="'.cm_htmlclear($key).'"'.($key==$value?' selected=""':null).'>'.cm_htmlclear($val).'</option>';
            }
        }
        $response = '<div class="form-group">
                            <label for="exampleSelect1">'.cm_htmlclear($data['form_name']).'</label>
                            <select name="'.cm_htmlclear($data['name']).'" class="form-control">
                                '.implode("\n",$option).'
                            </select>
                            <small class="form-text text-muted">'.cm_htmlclear($data['form_desc']).'</small>
                        </div>';

    }elseif($data['tag'] == 'radio'){

        $option = array();
        if(isset($data['options'])) {
            foreach ($data['options'] as $key=>$val) {
                $option[] = '<div class="form-check"><label class="form-check-label"><input'.((isset($data['required']) and $data['required']==true)?' required=""':null).' type="radio" class="form-check-input" name="'.cm_htmlclear($data['name']).'" value="'.cm_htmlclear($key).'" '.($value==$key?'checked=""':null).' /> '.cm_htmlclear($val).' </label></div>';
            }
        }
        $response = '<fieldset class="form-group">
<legend>'.$data['form_name'].'</legend>
'.implode("\n",$option).'
<small class="form-text text-muted">'.cm_htmlclear($data['form_desc']).'</small>
</fieldset>';

    }
    return $response;
}

function cm_server_list($server_id = 0){
    global $cm_db,$cm_config;
    $return = false;
    $query = "SELECT * FROM ".$cm_config["db_prefix"]."serverlists WHERE `delete`=0".($server_id>0?" AND server_list_id=".$server_id:null)." ORDER BY `name` ASC";
    $query = $cm_db->sql_query($query);
    if($cm_db->sql_num_rows($query) > 0) {
        $return = [];
        while ($row = $cm_db->sql_fetch_assoc($query)) {
            if($row['password'] != ""){
                $row['password'] = cm_decode($row['password']);
            }
            $return[] = $row;
        }
    }
    return $return;
}

function cm_modulepermissionControl($permission, $value){
    $return = false;
    if($permission == 'server_list' and $value and cm_numeric($value) and $value > 0){
        $return = [
            'server_list_id'    => $value,
        ];
    }elseif($permission == 'test'){

    }
    return $return;
}

function cm_modulepermission($permission,$datas = array()){
    $return = false;
    if($permission == 'server_list'){
        $list = cm_server_list();
        if($list){
            $opt = ['' => cm_lang('Lütfen sunucu seçin')];
            foreach ($list as $key => $value){
                $opt[$value['server_list_id']] = $value['name'];
            }
            $data = [
                'form_name' => cm_lang('Sunucu seçin'),
                'form_desc' => null,
                'name'      => $permission,
                'tag'       => 'select',
                'options'   => $opt,
            ];
            $datas['server_list'] = $datas['server_list_id'];
            $return = cm_inputType($data, $datas);
        }
    }elseif($permission == 'server_list2'){
        $return = null;
    }
    return $return;
}

function cm_json_encode($json){
	return json_encode($json,JSON_UNESCAPED_UNICODE);
}

function cm_json_decode($json){
	return json_decode($json,true);
}

function cm_logdatadecode($string){
	return cm_json_decode(base64_decode(cm_decode($string)));
}

function cm_clear_token(){
	if(isset($_SESSION['cm_token'])){ //$_SESSION['cm_tokenurl']
		unset($_SESSION['cm_token']);
	}
}

function cm_set_token(){
	global $session_id,$ip,$ip_port,$cm_db;
	$token_new = false;
	if(!isset($_SESSION['cm_token'])){
		$token_new = true;
	}
	if($token_new == true){
		$cm_key = md5(base64_encode($session_id."-".$ip."|".cm_micro_time()));
		$add = array(
			'user_id'		=> (isset($_SESSION["user_id"])?$_SESSION["user_id"]:0),
			'admin_id'		=> (isset($_SESSION["admin_id"])?$_SESSION["admin_id"]:0),
			'cm_token'		=> $cm_key,
			'ip'			=> $ip,
			'ip_port'		=> $ip_port,
			'session_id'	=> $session_id,
			'create_time'	=> cm_time(),
		);
		if(isset($_SERVER["REQUEST_URI"])){
			$add['url'] = $_SERVER["REQUEST_URI"];
		}
		list($tkn_status,$tkn_message) = $cm_db->sql_insert_add('tokenlog',$add);
		if($tkn_status){
			$_SESSION['cm_token'] = $cm_key;
		}else{
			$cm_key = null;
		}
	}else{
		$cm_key = $_SESSION['cm_token'];
	}
	return $cm_key;
}

function cm_get_token($token){
	global $cm_db,$action,$cm_action,$session_id;
	$status = false;
	if(isset($_SESSION['cm_token']) and $_SESSION['cm_token'] != ""){
		$update = array(
			'end_time'	=> cm_time(),
			't_status'	=> 2,
		);
		$ctoken = $_SESSION['cm_token'];
		cm_clear_token();
		if($ctoken == $token){
            $tokenControl = $cm_db->sql_assoc('tokenlog', ['cm_token' => $ctoken, 'session_id' => $session_id]);
            if($tokenControl) {
                $update['t_status'] = 1;
                $status = true;
            }
            unset($tokenControl);
		}
		if(isset($action) and $action){
			$update['action'] = $action;
		}
		if(isset($cm_action) and $cm_action){
			$update['cm_action'] = $cm_action;
		}
		if(isset($_SERVER["REQUEST_URI"])){
			$update['end_url'] = $_SERVER["REQUEST_URI"];
		}
		if($token != "") {
			$cm_db->sql_query_update('tokenlog', $update, array('cm_token' => $token));
			if(isset($_REQUEST) and count($_REQUEST) > 0){
				$add = array(
					'cm_token'	=> $ctoken,
					'data'		=> cm_encode(base64_encode(cm_json_encode($_REQUEST))),
				);
				$cm_db->sql_insert_add('tokendata',$add);
			}
		}
	}
	return $status;
}

function cm_get_request($variable = null,$method = null){
	$ReqMethod = array('REQUEST','POST','GET');
	$Output = '';
	if($variable == ''){ return $Output; }
	if($method != ""){ $method = strtoupper($method); }

	if($method == '' or $method == 'REQUEST'){
		$Output = isset($_REQUEST[$variable])?$_REQUEST[$variable]:$Output;
	}elseif($method == 'POST'){
		$Output = isset($_POST[$variable])?$_POST[$variable]:$Output;
	}elseif($method == 'GET'){
		$Output = isset($_GET[$variable])?$_GET[$variable]:$Output;
	}
	if($Output == ''){ return false; }
	return $Output;
}
function cm_isfile($file){
	return is_file($file);
}
function cm_ipverify($ip,$v = 0){
	$status = false;
	if(in_array($v,array(0,4)) and filter_var($ip, FILTER_VALIDATE_IP)){
		$status = true;
	}elseif(in_array($v,array(0,6)) and filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)){
		$status = true;
	}
	return $status;
}
function cm_urlwrite($file,$action){
	if(strstr($file,'.')){
		list($name,$type) = explode(".",$file,2);
	}else{
		$name = $file;
	}
	return $name.'/'.str_replace("_","-",$action);
	return $file.'?cm_action='.$action;
}
function cm_empty($string = null){
	if($string != ""){
		return true;
	}
	return false;
}
function cm_product_time($time_type,$time,$old_time = 0){
	if($time_type == 1){
		$time_name = 'day';
	}elseif($time_type == 2){
		$time_name = 'month';
	}elseif($time_type == 3){
		$time_name = 'year';
	}else{
		return 0;
	}
	if($old_time == 0){
		$unixtime = strtotime("+".$time." ".$time_name);
	}else{
		$unixtime = strtotime("+".$time." ".$time_name,$old_time);
	}
	return $unixtime;
}
function cm_encode($string = null){
    global $cm_config;
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = $cm_config['license'];
    $secret_iv = md5('cmWebHst');
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
    return $output;
}
function cm_decode($string = null){
    global $cm_config;
    $output = false;
    $encrypt_method = "AES-256-CBC";
    $secret_key = $cm_config['license'];
    $secret_iv = md5('cmWebHst');
    $key = hash('sha256', $secret_key);
    $iv = substr(hash('sha256', $secret_iv), 0, 16);
    $output = openssl_decrypt($string, $encrypt_method, $key, 0, $iv);
    return $output;
}
function cm_email_verify($email){
	return filter_var($email,FILTER_VALIDATE_EMAIL);
}
function cm_user_password($password){
	return md5(md5($password));
}
function cm_header($string = null){
	$string = ltrim($string,"/");
	header("Location: /".$string);
	exit;
}
function cm_url_go($string = null){
	$string = ltrim($string,"/");
	header("Location: /".$string);
	exit;
}
function cm_redirect($string = null){
	global $directory;
	$string = ltrim($string,"/");
	if(stristr($string,'http:') or stristr($string,'https:') or stristr($string,$directory)){
		$string = $string;
	}else{
		$string = $directory."/".$string;
	}
	header("Location: ".$string);
	exit;
}
function cm_gender($user){
	return $user['gender']==1?cm_lang('Erkek'):cm_lang('Bayan');
}
function cm_time(){
	return time();
}
function cm_htmlclear($string){
	if($string == ""){
		return null;
	}
	return htmlspecialchars($string);
}
function cm_textshow($string){
	return nl2br(str_replace("\t", '&nbsp;&nbsp;&nbsp;&nbsp;', cm_htmlclear($string)));
}
function cm_date($string = null,$time = 0){
	if($time == 0){
		return date(($string==null?"d-m-Y H:i:s":$string));
	}else{
		return date(($string==null?"d-m-Y H:i:s":$string),$time);
	}
}

function cm_function_call($include = null){
	$IsFile = cm_path.'include/functions/';
	$status = false;
	if(is_string($include)){
		if(function_exists($include."Functions")){
			return true;
		}
		if(file_exists($IsFile.$include.'.functions.php')){
			include($IsFile.$include.'.functions.php');
			$status = true;
		}
	}elseif(is_array($include)){
		foreach($include as $value){
			if(!function_exists($value."Functions") and file_exists($IsFile.$value.'.functions.php')){
				include($IsFile.$value.'.functions.php');
				$status = true;
			}else{
				$status = true;
			}
		}
	}
	return $status;
}

function cm_countrytax($country){
	global $cm_config,$cm_db;
	if(cm_numeric($country) and $country > 0){
		$pre = 'country_id';
	}else{
		$pre = 'country_code';
	}
	$country = $cm_db->sql_assoc('country',array($pre=>$country));
	if($country){
		$tax_query = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."countrytax WHERE country='".$cm_db->sql_escape($country['country_code'])."'");
		if($cm_db->sql_errno() == 0){
			if($cm_db->sql_num_rows($tax_query) == 0){
				return false;
			}
		}
	}
	return true;
}
function cm_promotions_calculation($promocode,$product_type_id,$price,$user,$order = 0,$feature = array()){
	global $cm_config,$cm_db,$order_id;
	if($price < 0 or $promocode == ""){
		return array(false,cm_lang('Lütfen promosyon hesaplanması için ücret girin'));
	}
	$Errno  = 0;
	$UserID = 0;
	$Status = false;
	if(isset($user['user_id']) and cm_numeric($user['user_id']) and $user['user_id'] > 0){
		$UserID = $user['user_id'];
		list($promotionsControl,$promotionsMessage) = $cm_db->sql_count('order_products',array('user_id'=>$UserID,'promocode'=>$promocode));
		if($promotionsControl){
			if($order == 0 and $promotionsMessage > 0){
				$Message = cm_lang("Bu promosyon kodunu zaten kullandınız");
				$Errno = 1;
			}
		}else{
			$Message = cm_lang("Promosyon kodu geçmiş kullanım kontrol edilemedi");
			$Errno = 1;
		}
	}
	if(!isset($Message)){
		$promotions = $cm_db->sql_assoc('promotions',array('promocode'=>$promocode));
		if($promotions){
			$ErrorMessage = null;
			if($promotions['start_time'] > cm_time()){
				$ErrorMessage = cm_lang('Henüz promosyon kodunun süresi başlamadı');
				$Errno = 1;
			}elseif($promotions['end_time'] < cm_time()){
				$ErrorMessage = cm_lang('Promosyon kodu süresi bitti');
				$Errno = 1;
			}elseif($promotions['user_id'] > 0 and $promotions['user_id'] != $UserID){
				$ErrorMessage = cm_lang('Promosyon kodu size ait değil');
				$Errno = 1;
			}elseif($promotions['status'] == 0){
				$ErrorMessage = cm_lang('Promosyon kodu aktif değil');
				$Errno = 1;
			}elseif($product_type_id != $promotions['product_type_id']){
				$ErrorMessage = cm_lang('Promosyon kodu bu ürün için geçerli değil');
			}elseif($promotions['min_value'] > 0 and $price < $promotions['min_value']){
				$ErrorMessage = cm_lang('Promosyon kodu bu üründe geçersiz');
			}
			if($ErrorMessage == null){
				if($promotions['time_type'] != 0){
					if(!isset($feature['year']) and (!isset($feature['time_type']) or $feature['time_type'] != $promotions['time_type'])){
						$ErrorMessage = cm_lang('Promosyon kodu ürün için zaman türünü sağlamıyor');
					}elseif(isset($feature['year']) and $promotions['time_type'] != 3 or ($promotions['max_time'] < $feature['year'] or $promotions['min_time'] > $feature['year'])){
						$ErrorMessage = cm_lang('Promosyon kodu bu ürün için geçerli değil');
					}elseif(isset($feature['time']) and ($promotions['max_time'] < $feature['time'] or $promotions['min_time'] > $feature['time'])){
						$ErrorMessage = cm_lang('Promosyon kodu bu ürün için geçerli değil');
					}
				}
				if($ErrorMessage == null and $promotions['buy_type'] != 0){
					if($promotions['buy_type'] == 1 and $feature['type_id'] != 0){
						$ErrorMessage = cm_lang('Promosyon kodu bu ürün için geçerli değil');
					}elseif($promotions['buy_type'] == 2 and $feature['type_id'] == 0){
						$ErrorMessage = cm_lang('Promosyon kodu bu ürün için geçerli değil');
					}
				}
			}
			if($ErrorMessage == null){
				list($TotalStatus,$TotalMessage) = $cm_db->sql_count('order_products',array('promocode'=>$promocode));
				if($TotalStatus == true){
					if($promotions['max_limit'] > 0 and $promotions['max_limit'] < $TotalMessage){
						$ErrorMessage = cm_lang('Promosyon kodu kullanım limiti bitti');
						$Errno = 1;
					}
				}else{
					$ErrorMessage = cm_lang('Promosyon kodu geçmiş kullanım kontrol edilemedi');
					$Errno = 1;
				}
			}
			if($ErrorMessage == null){
				if($promotions['type'] == 1){
					$priceReturn = ($price-$promotions['value']);
					$discount = ($price-$priceReturn);
					if($priceReturn < 0){
						$priceReturn = 0.00;
						$discount = $price;
					}
				}elseif($promotions['type'] == 2){
					$rate		= explode(".",$promotions['value']);
					$rate		= "0.".strlen($rate[0])==1?"0".$rate[0]:$rate[0];
					$rateprice	= cm_money_format(($price*$rate));
					$priceReturn = ($price-$rateprice);
					$discount = ($price-$priceReturn);
					if($priceReturn < 0){
						$priceReturn = 0.00;
						$discount = $price;
					}
				}else{
					$priceReturn = 0.00;
					$discount = $price;
				}
				$Status		= true;
				$Message	= array(
					'price'		=> $priceReturn,
					'discount'	=> cm_money_format($discount),
				);
			}else{
				$Message = $ErrorMessage;
			}
		}else{
			$Message = cm_lang('Promosyon kodu hatalı veya geçersiz');
			$Errno = 1;
		}
	}
	return array($Status,$Message,$Errno);
}
function cm_money_format($price){
	if(strstr($price,'.')){
		$priceS = explode(".",$price);
		if(strlen($priceS[1]) > 2){
			$priceS[1]	= substr($priceS[1],0,2);
			$price		= implode(".",$priceS);
		}
	}
	//return $price;
	//return sprintf("%.2f",$price);
	//return round($price,2);
	return number_format((float)$price,2,'.','');
}

function cm_currency_convert($price,$old_currency,$new_currency,$type = 1){
	global $cm_db,$cm_config;
	if($type == 1){
		$type = 'selling';
	}else{
		$type = 'buying';
	}
	$old_currency 	= strtoupper($old_currency);
	$new_currency	= strtoupper($new_currency);
	if($price < 1 or $old_currency == $new_currency){
		return array(
			'price'		=> cm_money_format($price),
			'currency'	=> $cm_config['default_currency'],
		);
	}
	$old 	= $cm_db->sql_assoc('currency',array('currency'=>$old_currency));
	$new 	= $cm_db->sql_assoc('currency',array('currency'=>$new_currency));
	$deger 	= cm_money_format(($price * $old['selling']));
 	$sonuc	= cm_money_format(($deger / $new['selling']));
 	return array(
		'price'		=> $sonuc,
		'currency'	=> $new_currency,
	);
}
function arrayMultiSearch($products, $field, $value,$type = 0){
   foreach($products as $key => $product){
		if(!is_array($product)){
			if($key == $field and ($product == $value or ($product == "" and $value != ""))){
				if($type == 0){
					return true;
				}else{
					return $key;
				}
			}
		}else{
			if(array_key_exists($field,$product)){
				if((($product[$field] != "" and $product[$field] == $value) or ($product[$field] == "" and $value != ""))){
					if($type == 0){
						return true;
					}else{
						return $key;
					}
				}
			}else{
				foreach($product as $key1=>$value1){
					if(array_key_exists($field,$value1) and ($value1[$field] == $value or ($value1[$field] == "" and $value != ""))){
						if($type == 0){
							return true;
						}else{
							return $key;
						}
					}
				}
			}
		}
   }
   return false;
}
function arrayMultiValue($products, $field){
   foreach($products as $key => $product){
		if(!is_array($product)){
			if($key == $field){
				return $product;
			}
		}else{
			if(array_key_exists($field,$product)){
				return $product[$field];
			}else{
				foreach($product as $key1=>$value1){
					if(array_key_exists($field,$value1)){
						return $value1[$field];
					}
				}
			}
		}
   }
   return false;
}
function whois_time_find($find,$string){
	if($find != "" and $string != "" and stristr($string,$find)){
		if(preg_match("/".$find."(.*)/", $string, $match)){
			date_default_timezone_set('UTC');
			$time  = strtotime($match[1]);
			return $time;
		}
	}
	return false;
}
function cm_price_format($price,$currency){
	if($price == "" or $currency == ""){
		return null;
	}
	return number_format($price, 2, ',', '.')." ".$currency;
}
function cm_os_list($os_id = null){
	global $cm_config,$cm_db;
	if(!cm_numeric($os_id)){
		$os_id = 0;
	}
	$os_dbX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."server_os as os LEFT JOIN ".$cm_config['db_prefix']."serversystem AS s ON os.system_id=s.system_id WHERE os.os_status=1 AND os.os_delete=0".($os_id>0?" AND os_id='".$cm_db->sql_escape($os_id)."'":null));
	if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($os_dbX) > 0){
		$os_list = array();
		while($os_db = $cm_db->sql_fetch_assoc($os_dbX)){
			$os_db['name'] = $os_db['system_name'].' '.$os_db['os_name'].' ('.$os_db['os_bit'].' bit)';
			if($os_db['currency'] != $cm_config['default_currency']){
				$os_price = cm_currency_convert($os_db['price'],$os_db['currency'],$cm_config['default_currency']);
				$os_db['price'] = $os_price['price'];
				$os_db['currency'] = $cm_config['default_currency'];
			}
			$os_list[$os_db['os_id']] = $os_db;
		}
		if(count($os_list) > 0){
			return $os_list;
		}
	}
	return false;
}
function cm_panel_list($panel_id = null){
	global $cm_config,$cm_db;
	if(!cm_numeric($panel_id)){
		$panel_id = 0;
	}
	$os_dbX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."panels as p WHERE p.panel_status=1 AND p.panel_delete=0".($panel_id>0?" AND panel_id='".$cm_db->sql_escape($panel_id)."'":null));
	if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($os_dbX) > 0){
		$os_list = array();
		while($os_db = $cm_db->sql_fetch_assoc($os_dbX)){
			if($os_db['currency'] != $cm_config['default_currency']){
				$os_price = cm_currency_convert($os_db['panel_server_price'],$os_db['currency'],$cm_config['default_currency']);
				$os_db['panel_server_price'] = $os_price['price'];
				$os_price = cm_currency_convert($os_db['panel_dedicated_price'],$os_db['currency'],$cm_config['default_currency']);
				$os_db['panel_dedicated_price'] = $os_price['price'];
				$os_db['currency'] = $cm_config['default_currency'];
			}
			$os_list[$os_db['panel_id']] = $os_db;
		}
		if(count($os_list) > 0){
			return $os_list;
		}
	}
	return false;
}
function isTcKimlik($tc){
	if(strlen($tc) < 11){ return false; }
	if($tc[0] == '0'){ return false; }
	$plus = ($tc[0] + $tc[2] + $tc[4] + $tc[6] + $tc[8]) * 7;
	$minus = $plus - ($tc[1] + $tc[3] + $tc[5] + $tc[7]);
	$mod = $minus % 10;
	if($mod != $tc[9]){ return false; }
	$all = '';
	for($i = 0 ; $i < 10 ; $i++){ $all += $tc[$i]; }
	if($all % 10 != $tc[10]){ return false; }

	return true;
}
function TcDogrula($TcNo,$Ad,$Soyad,$DogTarih){
	if(!isTcKimlik($TcNo)){
		return array(false,cm_lang('TC Kimlik numarası hatalı'));
	}
	$DogTarih = explode("-",$DogTarih);
	$DogTarih = $DogTarih[0];
	$Ad	= @mb_convert_case(str_replace(array("i","ı"),array("İ","I"),$Ad), MB_CASE_UPPER, "UTF-8");
	$Soyad	= @mb_convert_case(str_replace(array("i","ı"),array("İ","I"),$Soyad), MB_CASE_UPPER, "UTF-8");
	$GonderilenVeri = array(
		"TCKimlikNo" =>	$TcNo,
		"Ad" =>			$Ad,
		"Soyad" =>		$Soyad,
		"DogumYili" =>	$DogTarih,
	);
	try {
		$TcSoap	= new SoapClient("https://tckimlik.nvi.gov.tr/Service/KPSPublic.asmx?WSDL");
		$GelenVeri = $TcSoap->TCKimlikNoDogrula($GonderilenVeri);
		if($GelenVeri->TCKimlikNoDogrulaResult){
			return array(true,'TC Doğru');
		}else{
			return array(false,'TC Hatalı');
		}
	}
	catch (Exception $Hata){
		return array(false,$Hata->faultstring);
	}
}
function file_mime_types(){
	$return = array('txt','jpg','jpeg','zip','rar','png','gif');
	return $return;
}

function cm_micro_time(){
	$micro_date = microtime();
	$date_array = explode(" ",$micro_date);
	return $date_array[1].str_replace(".","",$date_array[0]);
}

function cm_file_upload($Veriler = array(),$path = null,$user_id = 0){
    global $cm_dirlist;
	$Durum = false;
	if(is_array($Veriler) and count($Veriler) > 0){
		$ky = array_keys($Veriler);
		$ky = $ky[0];
		global $cm_db,$ip;
		if(isset($Veriler[$ky]) and is_array($Veriler[$ky]) and count($Veriler[$ky]) > 0){	/** Normal html upload */
			$Dosyalar = null;
			$now = cm_time();
			if(is_array($Veriler[$ky]['error'])){
				foreach($Veriler[$ky]['error'] as $key=>$value){
					if(isset($Veriler[$ky]['error'][$key]) and $Veriler[$ky]['error'][$key] == 0){
						$DosyaUzanti = explode(".",$Veriler[$ky]['name'][$key]);
						$Ekle = array(
							'user_id'		=> $user_id,
							'time'			=> $now,
							'file_type'		=> end($DosyaUzanti),
							'old_file_name'	=> $Veriler[$ky]['name'][$key],
							'file_path'		=> $path,
							'approval'		=> 3,
							'ip_address'	=> $ip
						);
						list($EkleDurum,$EkleBilgi) = $cm_db->sql_insert_add('file_additional',$Ekle);
						if($EkleDurum == true){
							$Dosyalar[$key] = array('id'=>$EkleBilgi,'name'=>md5(cm_micro_time()));
						}
					}
				}
			}else{
				if($Veriler[$ky]['error'] == 0){
					$DosyaUzanti = explode(".",$Veriler[$ky]['name']);
					$Ekle = array(
						'user_id'		=> $user_id,
						'time'			=> $now,
						'file_type'		=> end($DosyaUzanti),
						'old_file_name'	=> $Veriler[$ky]['name'],
						'file_path'		=> $path,
						'approval'		=> 3,
						'ip_address'	=> $ip
					);
					list($EkleDurum,$EkleBilgi) = $cm_db->sql_insert_add('file_additional',$Ekle);
					if($EkleDurum == true){
						$Dosyalar[] = array('id'=>$EkleBilgi,'name'=>md5(cm_micro_time()));
					}
				}
			}
			if($Dosyalar != null){
			    $fileCall = $cm_dirlist['library'].'CMupload/upload.php';
			    if(file_exists($fileCall)) {
			        include ($fileCall);
                    $Yukle = new CMUpload($Veriler[$ky], $path, 0, $Dosyalar);
                    list($Drm, $Bilgi) = $Yukle->upload();
                    if ($Drm == true) {
                        if (!is_array($Bilgi['id'])) {
                            $Bilgi['id'] = array($Bilgi['id']);
                        }
                        foreach ($Bilgi['id'] as $anahtar => $deger) {
                            $Guncelle = array(
                                'file_size' => isset($Bilgi['file_size'][$anahtar]) ? $Bilgi['file_size'][$anahtar] : null,
                                'approval' => 0,
                                'file_name' => isset($Bilgi['file_name'][$anahtar]) ? $Bilgi['file_name'][$anahtar] : null,
                            );
                            if (isset($Bilgi['file_path'][$anahtar])) {
                                $Guncelle['file_path'] = $Bilgi['file_path'][$anahtar];
                            }
                            $cm_db->sql_query_update('file_additional', $Guncelle, "additional_id='" . $cm_db->sql_escape($deger) . "'");
                        }
                        $Durum = true;
                        $Mesaj = $Bilgi['id'];
                    } else {
                        $Mesaj = cm_lang('Could not load files');
                    }
                    $cm_db->sql_delete('file_additional', array('approval' => 3, 'time' => $now)); //değiştir
                }else{
                    $Mesaj = cm_lang('Upload kütüphanesi bulunamadı');
                }
			}else{
				$Mesaj = cm_lang('Dosya seçmediniz');
			}
		}else{
			$Mesaj = cm_lang('Dosya seçilmedi');
		}
	}else{
		$Mesaj = cm_lang('Verileri okunamadı');
	}
	return array($Durum,$Mesaj);
}
function cm_file_open($data = array()){
	if(is_array($data) and count($data) > 0){
		if(isset($data['file_name']) and isset($data['file_path'])){
			$filename = $data['file_path'].$data['file_name'];
			if(isset($data['additional_id']) and strlen($data['additional_id']) > 1){
				//$filename = $data['file_path'].implode("/",str_split(substr($data['additional_id'],0,(strlen($data['additional_id'])-1)))).'/'.$data['file_name'];
			}
			if(file_exists($filename)){
				$DosyaUzanti = explode(".",$data['file_name']);
				$DosyaUzanti = end($DosyaUzanti);
				$FileMimeType = file_mime_types();
				if(isset($_REQUEST['d']) and $_REQUEST['d'] == 1){	/** Güvenlik açığı kontrol '!!!!!!!' */
					header('Content-Description: File Transfer');
					header("Content-Type: ".$FileMimeType[$DosyaUzanti]);
				    header('Content-Disposition: attachment; filename='.basename($data['old_file_name']));
				    header('Content-Transfer-Encoding: binary');
				    header('Expires: 0');
				    header('Cache-Control: must-revalidate');
				    header('Pragma: public');
				    header('Content-Length: ' . filesize($filename));
				    return readfile($filename);
				}elseif(in_array($DosyaUzanti,array('jpg','gif','png','jpeg'))){
					$Durum = false;
					$image_info = @getimagesize($filename);
					if($image_info != false){
						$image_type = $image_info[2];
						if($image_type == IMAGETYPE_JPEG){
							if(@imagecreatefromjpeg($filename)){
								$Durum = true;
							}
						}elseif($image_type == IMAGETYPE_GIF){
							if(@imagecreatefromgif($filename)){
								$Durum = true;
							}
						}elseif($image_type == IMAGETYPE_PNG){
							if(@imagecreatefrompng($filename)){
								$Durum = true;
							}
						}
					}
					if($Durum == true){
						header("Content-Type: ".$FileMimeType[$DosyaUzanti]);
						//header("Content-Type: jpeg");
					    return readfile($filename);
					}else{
						goto ResimAcilmiyor;
					}
				}elseif(in_array($DosyaUzanti,array('txt'))){
					if(is_file($filename)){
						header("Content-Type: text/plain");
						return @file_get_contents($filename);
					}else{
						goto ResimAcilmiyor;
					}
				}else{
					ResimAcilmiyor:
					return cm_lang('Dosya açılamıyor').'<br /><br /><a href="'.$_SERVER['REQUEST_URI'].'&d=1" target="_blank">'.cm_lang('İndir').'</a>';
				}
			}else{
				return cm_lang('Dosya bulunamadı');
			}
		}
	}
	return cm_lang('File missing or deleted');
}
function cm_credit($user,$price,$type = 2,$message = null,$order_id = 0){
	global $cm_db,$cm_config;
	if(!is_array($user)){
		$user = userGetDetails($user);
	}
	if($type == 1){
		$new_credit = ($user['credit']+$price);
	}else{
		$new_credit = ($user['credit']-$price);
	}
	list($credit_status,$credit_response) = $cm_db->sql_query_update('users',array('credit'=>$new_credit),array('user_id'=>$user['user_id']));
	if($credit_status == false){
		return array(false,cm_lang('Kredi işlemi yapılamadı'));
	}
	$add = array(
		'user_id'	=> $user['user_id'],
		'order_id'	=> $order_id,
		'amount'	=> $price,
		'old_credit'=> $user['credit'],
		'new_credit'=> $new_credit,
		'detail'	=> $message,
		'time'		=> cm_time(),
		'status'	=> $type
	);
	list($add_status,$add_id) = $cm_db->sql_insert_add('creditlogs',$add);
	if($add_status == false){
		//return array(false,cm_lamg('Kredi logu yazılamadı'));
		$add_id = 0;
	}
	return array(true,array('credit_id'=>$add_id));
}
function creditPaid($order,$user){
	global $cm_db,$cm_config;
	$status = false;
	if(!is_array($user)){
		$user = userGetDetails($user);
	}
	if(!is_array($order)){
		$order = $cm_db->sql_assoc('orders',array('order_id'=>$order));
	}
	if($order and $order['order_delete'] == 0 and $order['user_id'] == $user['user_id']){
		if($order['order_status'] == 0){
			if($user['credit'] >= $order['paid']){
				list($credit_status,$credit_response) = cm_credit($user,$order['paid'],2,cm_lang('Sipariş numarası ##order_id## için ödeme yapıldı',array('order_id'=>$order['order_id'])),$order['order_id']);
				if($credit_status == true){
					orderPaid($order['order_id'],'credit');
					$status = true;
					$response = array('message'=>cm_lang('Ödendi'));
				}else{
					$response = $credit_response;
				}
			}else{
				$response = cm_lang('Yeterli krediniz yok');
			}
		}else{
			$response = cm_lang('Sipariş zaten aktif');
		}
	}else{
		$response = cm_lang('Sipariş bulunamadı');
	}
	return array($status,$response);
}
function cm_number_format($number){
	return $number;
}
function cm_admin_user_link($user){
	global $admindir;
	$return = '<a href="'.$admindir.'/members.php?id='.$user['user_id'].'">'.cm_htmlclear($user['first_name'].' '.$user['last_name']).'<br />'.cm_htmlclear($user['username']).'</a>';
	return $return;
}
function cm_user_name($user){
	global $admindir,$cm_db;
	if(cm_numeric($user) and $user > 0){
        $user = $cm_db->sql_assoc('users',['user_id' => $user]);
    }
	if($user['first_name'] != "" and $user['last_name'] != ""){
		$name = cm_htmlclear($user['first_name'].' '.$user['last_name']);
	}elseif($user['username'] != ""){
		$name = cm_htmlclear($user['username']);
	}elseif($user['email_address'] != ""){
		$name = explode("@",$user['email_address']);
		$name = cm_htmlclear($name[0]);
	}
	return $name;
}
function cm_sitedomain(){
    echo 'eski fonksiyon'; exit;
	global $cm_config;
	return 'http://'.$cm_config["domain"].'/';
}
function cm_link($page,$path = array()){
	global $directory;
	return $directory.'/'.$page.((is_array($path) and count($path) > 0)?'?'.http_build_query($path):'');
}
function cm_link_seo($seo){
	return $seo;
}
function cm_ip_list($ip){
	$return = array();
	if(strstr($ip,";")){
		$return = explode(";",$ip);
	}else{
		$return = explode("\n",$ip);
	}
	return $return;
}
function PageList($Bilgiler = array()){
    global $ListTotalRows,$TotalRows;
    $ToplamSayfa = $Bilgiler["TotalPage"];
    $sayfa = $Bilgiler["Page"];
    $page_name = 'page';
    if($ToplamSayfa>0){
        $GosterListe = '<ul class="pagination pagination-large">';
        $sayfa_goster = 9; // gösterilecek sayfa sayısı
        $en_az_orta = ceil($sayfa_goster/2);
        $en_fazla_orta = ($ToplamSayfa+1) - $en_az_orta;
        $sayfa_orta = $sayfa;
        if($sayfa_orta < $en_az_orta) $sayfa_orta = $en_az_orta;
        if($sayfa_orta > $en_fazla_orta) $sayfa_orta = $en_fazla_orta;
        $sol_sayfalar = round($sayfa_orta - (($sayfa_goster-1) / 2));
        $sag_sayfalar = round((($sayfa_goster-1) / 2) + $sayfa_orta);
        if($sol_sayfalar < 1) $sol_sayfalar = 1;
        if($sag_sayfalar > $ToplamSayfa) $sag_sayfalar = $ToplamSayfa;

        if($sayfa > 1){
            $GosterListe .= '<li><a href="?'.$page_name.'='.($sayfa-1).''.($Bilgiler["parametre"]!=null?'&'.$Bilgiler["parametre"]:null).'" rel="prev">«</a></li>';
        }else{
            $GosterListe .= '<li class="disabled"><span>«</span></li>';
        }
        for($s = $sol_sayfalar; $s <= $sag_sayfalar; $s++) {
            if($sayfa == $s) {
                //echo '<li><a href="#" style="color: red;">[' . $s . ']</a></li>';
                $GosterListe .= '<li class="disabled"><span style="color:red;">['.$s.']</span></li>';
            } else {
                //echo '<li><a href="?'.$parametre.'sayfa='.$s.'">'.$s.'</a></li>';
                $GosterListe .= '<li><a href="?'.$page_name.'='.$s.''.($Bilgiler["parametre"]!=null?'&'.$Bilgiler["parametre"]:null).'">'.$s.'</a></li>';
            }
        }
        if($sayfa != $ToplamSayfa){
            $GosterListe .= '<li><a href="?'.$page_name.'='.($sayfa+1).''.($Bilgiler["parametre"]!=null?'&'.$Bilgiler["parametre"]:null).'" rel="prev">»</a></li>';
        }else{
            $GosterListe .= '<li class="disabled"><span>»</span></li>';
        }
        $GosterListe .= '</ul>';
        if(isset($ListTotalRows)){
        	$Bilgi = '<br /><p>Toplam '.$TotalRows["Rows"].' Kayıt Bulundu. '.$ToplamSayfa.' Sayfadan '.$sayfa.'. Sayfadasınız. Her Sayfada '.$ListTotalRows.' Adet Kayıt Vardır.</p>';
        }else{
        	$Bilgi = null;
        }
        return $GosterListe.$Bilgi;
    }else{
        return null;
    }
}

function cm_mailSend($email,$subject,$message,$data = [],$log = 1){
    global $cm_db,$cm_config;
    $params = [
        'email'     => $email,
        'subject'   => $subject,
        'message'   => $message,
        'data'      => $data,
    ];
    list($status,$response) = ModuleCallFunction($cm_config['mail_module'],'Send','module',$params);
    if($status == true){
        $response = [
            'message'   => cm_lang('Mail Gönderildi'),
        ];
    }
    if($log == 1 and isset($cm_db)){
        $add = [
            'email_address' => $email,
            'subject'       => $subject,
            'message'       => $message,
            'send_time'     => cm_time(),
            'send_status'   => ($status?1:0),
        ];
        $cm_db->sql_insert_add('maillogs', $add);
    }
    return array($status,$response);
}

function cm_mailtemplate($type,$data = []){
    global $cm_db;
    $status     = false;
    $return     = cm_lang('Mail şablonu bulunamadı');
    $template   = $cm_db->sql_assoc('mailtemplates',['type' => $type, 'lang' => cm_getLang()]);
    if($template){
        if($template['filename'] != ""){

        }elseif($template['text'] != ""){
            if(count($data) > 0){
                $find   = [];
                $change = [];
                foreach($data as $key => $value){
                    $find[]     = '##'.$key.'##';
                    $change[]   = $value;
                }
                $template['text'] = str_ireplace($find,$change,$template['text']);
            }
            $status = true;
            $return = [
                'subject'   => $template['subject'],
                'message'   => nl2br($template['text']),
            ];
        }else{
            $return = cm_lang('Mail şablon eksik');
        }
    }
    return [$status,$return];
}

function cm_multicurl($data){
    $MultiCurl		= curl_multi_init();
    $CurlBaglanti	= array();
    $BasZaman		= cm_time();
    $GeriBilgi		= array();

    foreach($data as $key => $value){
        if(isset($value["return"])){
            $GeriBilgi[$key] = $value["return"];
        }
        $HedearBilgi = array();
        if(isset($value["httpheader"])){
            $HedearBilgi = $value['httpheader'];
        }

        if(isset($value['random_ip'])){
            $FakeIP = long2ip(mt_rand(0, 65537) * mt_rand(0, 65535));
            $HedearBilgi	= array_merge($HedearBilgi,array(
                "X_FORWARDED_FOR: " . $FakeIP,
                "REMOTE_ADDR: " . $FakeIP,
                "X-Client-IP: ".$FakeIP,
                "Client-IP: ".$FakeIP,
                "X-Forwarded-For: ".$FakeIP
            ));
        }

        $ArrayG = array(
            CURLOPT_URL				=> $value['url'],
            CURLOPT_RETURNTRANSFER	=> (isset($value["transfer"])?$value["transfer"]:true),
            CURLOPT_HEADER			=> (isset($value["header"])?$value["header"]:false),
            CURLOPT_ENCODING		=> (isset($value["encoding"])?$value["encoding"]:false),
            CURLOPT_COOKIESESSION	=> false,
            CURLOPT_CONNECTTIMEOUT	=> isset($value["timeout"])?$value["timeout"]:20,
            CURLOPT_TIMEOUT			=> isset($value["timeout"])?$value["timeout"]:20,
        );

        if(isset($value["proxy"])){
            $ArrayG[CURLOPT_PROXY]	= $value["proxy"];
            $ArrayG[CURLOPT_HTTPPROXYTUNNEL]	= 0;
        }

        if(isset($value["port"])){
            $ArrayG[CURLOPT_PORT] = $value["port"];
        }

        if(isset($value["customrequest"])){
            $ArrayG[CURLOPT_CUSTOMREQUEST]	= $value["customrequest"];
        }

        if(count($HedearBilgi) > 0){
            $ArrayG[CURLOPT_HTTPHEADER]	= $HedearBilgi;
        }

        if(strstr($value['url'],'https')){
            $ArrayG[CURLOPT_SSL_VERIFYPEER] = false;
            $ArrayG[CURLOPT_SSL_VERIFYHOST] = false;
        }

        if(isset($value["location"])){
            $ArrayG[CURLOPT_FOLLOWLOCATION]	= $value["location"];
        }

        if(isset($value['referer'])){
            $ArrayG[CURLOPT_REFERER]	= $value["referer"];
        }

        if(isset($value["post"])){
            $ArrayG[CURLOPT_POST]	      = true;
            $ArrayG[CURLOPT_POSTFIELDS]	  = (is_array($value["post"])?http_build_query($value["post"]):$value["post"]);
        }

        if(isset($value["cookie"])){
            $ArrayG[CURLOPT_COOKIEJAR]	  = $value["cookie"];
            $ArrayG[CURLOPT_COOKIEFILE]	  = $value["cookie"];
        }

        if(isset($value['tarayici'])){
            $ArrayG[CURLOPT_USERAGENT]  = $value['tarayici'];
        }
        /**
        if(isset($Ayarlar["site_ip_adresleri"]) and $Ayarlar["site_ip_adresleri"] != ''){
        $SiteIP = explode("\n",$Ayarlar["site_ip_adresleri"]);
        if(count($SiteIP) > 0){ $ArrayG[CURLOPT_INTERFACE]	= $SiteIP[array_rand($SiteIP)]; }
        }
         */
        $CurlBaglanti[$key] = curl_init();
        curl_setopt_array($CurlBaglanti[$key], $ArrayG);
        curl_multi_add_handle($MultiCurl, $CurlBaglanti[$key]);
    }

    $active = null;
    do {
        curl_multi_exec($MultiCurl, $active);
    }
    while ($active > 0);
    $SiteCikti 		= array();
    foreach($CurlBaglanti as $anahtar=>$UrlCikti) {
        $Cikti = curl_multi_getcontent($UrlCikti);
        $Bilgi = curl_getinfo($UrlCikti);
        if(isset($GeriBilgi[$anahtar])){
            $Sonuc = array('output'=>$Cikti,'return'=>$GeriBilgi[$anahtar]);
        }else{
            $Sonuc = array('output'=>$Cikti);
        }
        $Sonuc = array_merge($Sonuc,array('info'=>$Bilgi));
        $SiteCikti[$anahtar] = $Sonuc;
        curl_multi_remove_handle($MultiCurl, $UrlCikti);
        curl_close($UrlCikti);
    }
    curl_multi_close($MultiCurl);
    $SonZaman   = cm_time();
    return $SiteCikti;
}

function cm_postfile($filename){
    $filename = str_replace("\\","/",$filename);
    return ((preg_match("#(^[a-z0-9_./]+)#si",$filename) and file_exists($filename))?true:false);
}

function cm_isdir($dir,$create = false){
    if($create == true and !is_dir($dir)){
        if(mkdir($dir,0777,true)){
            return true;
        }
    }
    return is_dir($dir);
}