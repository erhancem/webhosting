<?php
function cm_numeric($deger){
    if(is_numeric($deger)){
        if(preg_match('/^\d+$/',$deger)){
            if($deger >= 0){
                return true;
            }
        }
    }
    return false;
}

function cm_browser($UserAgent){
    if($UserAgent == null and isset($_SERVER['HTTP_USER_AGENT'])){
        $UserAgent	= $_SERVER['HTTP_USER_AGENT'];
    }
    $Bilgi	= array(
        'bot_mu'	=> false,
        'browser'	=> null,
        'sistem'	=> null,
        'mobil_mi'	=> false,
    );
    $Botlar	= array(
        '; +http://'=> 'Bot',
        '; +https:/'=> 'Bot',
        ' (+http://'=> 'Bot',
        'spider'	=> 'Bot',
        '/bot.'		=> 'Bot',
        '/bots'		=> 'Bot',
        '/robot'	=> 'Bot',
        'crawler'	=> 'Bot',
        'Wget/'		=> 'Wget Bot',
        'Mikrotik/'	=> 'Mikrotik',
        'curl/'		=> 'Curl Bot',
        'msnbot'	=> 'MSN Bot',
        'bingbot'	=> 'Bing Bot',
        'yandexbot'	=> 'Yandex Bot',
        'googlebot'	=> 'Google Bot',
        '.facebook.'=> 'Facebook',
        '.yahoo.'	=> 'Yahoo Bot',
        '/dotbot'	=> 'Moz DotBot',
        '/yooz.ir'	=> 'Yooz Bot',
    );
    foreach($Botlar as $Botx => $Adi){
        if(stristr($UserAgent,$Botx)){
            $Bilgi['bot_mu']	= true;
            $Bilgi['browser']	= $Adi;
            $Bilgi['sistem']	= 'Bot';
        }
    }
    if(!$Bilgi['bot_mu']){
        $Tarayicilar	= array(
            ' Mozilla/'	=> 'Mozilla',
            ' Safari/'	=> 'Safari',
            ' Chrome/'	=> 'Chrome',
            ' CriOS/'	=> 'Chrome',
            ' Firefox/'	=> 'Mozilla',
            'Opera/'	=> 'Opera',
            'MSIE 8.0'	=> 'IE 8',
            'MSIE 9.0'	=> 'IE 9',
            'MSIE 10.0'	=> 'IE 10',
            'rv:11.0)'	=> 'IE 11',
        );
        foreach($Tarayicilar as $Brwx => $Adi){
            if(stristr($UserAgent,$Brwx)){
                $Bilgi['browser']	= $Adi;
            }
        }
        $Sistemler	= array(
            'Windows NT 5.1'	=> 'XP',
            'Windows NT 6.0'	=> 'Vista',
            'Windows NT 6.1'	=> 'Win7',
            'Windows NT 6.2'	=> 'Win8',
            'Windows NT 6.3'	=> 'Win8.1',
            'Windows NT 10.0'	=> 'Win10',
            ' Linux '			=> 'Linux',
            ' Android '			=> 'Android',
            ' (iPad'			=> 'iOS',
            ' iPhone OS '		=> 'iOS',
            ' (Macintosh; '		=> 'MAC',
        );
        foreach($Sistemler as $Sisx => $Adi){
            if(stristr($UserAgent,$Sisx)){
                $Bilgi['sistem']	= $Adi;
            }
        }
        if(stristr($UserAgent,' mobile ') and stristr($UserAgent,' mobile/')){
            $Bilgi['mobil_mi']	= true;
        }
    }
    return $Bilgi;
}
$cm_browser	= cm_browser(null);

if(isset($_SERVER['HTTP_X_FORWARDED_FOR'])){
	$ip	= $_SERVER['HTTP_X_FORWARDED_FOR'];
}elseif(isset($_SERVER['HTTP_C_IP'])){
	$ip	= $_SERVER['HTTP_C_IP'];
}elseif(isset($_SERVER['HTTP_CLIENT_IP'])){
	$ip	= $_SERVER['HTTP_CLIENT_IP'];
}elseif(isset($_SERVER['HTTP_NGINX_ADDR'])){
	$ip	= $_SERVER['HTTP_NGINX_ADDR'];
}elseif(isset($_SERVER['HTTP_REMOTE_ADDR'])){
	$ip	= $_SERVER['HTTP_REMOTE_ADDR'];
}else{
	$ip = $_SERVER['REMOTE_ADDR'];
}

$ip	= trim($ip);
if(strstr($ip,',')){
	$ip = explode(",",$ip);
	$ip = trim($ip[0]);
}

if(!preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/',$ip) or strstr(":".$ip,':10.') or strstr(":".$ip,':192.') or strstr(":".$ip,':127.')){
	//echo 'IP Adresiniz Tespit Edilemedi.';
	//exit;
}
$ip_port	= (isset($_SERVER['HTTP_SRCPORT'])?$_SERVER['HTTP_SRCPORT']:$_SERVER['REMOTE_PORT']);

function cm_systemlogDir($detail){
    global $cm_browser,$cm_config,$ip,$session_id,$ip_port,$cm_dirlist;
    $LogAdd = array(
        'user_id'	=> isset($_SESSION['user_id'])?$_SESSION['user_id']:0,
        'admin_id'	=> isset($_SESSION['admin_id'])?$_SESSION['admin_id']:0,
        'ip'		=> isset($ip)?$ip:null,
        'ip_port'	=> $ip_port,
        'session_id'=> isset($session_id)?$session_id:null,
        'browser'	=> isset($cm_browser['browser'])?$cm_browser['browser']:null,
        'log'		=> ($detail!=""?var_export($detail,true):'Log detayı yazılmamış'),
        'server_log'=> var_export(array('SERVER'=>$_SERVER,'REQUEST'=>$_REQUEST),true),
        'time'		=> time()
    );
    if(isset($cm_config['report_mail']) and $cm_config['report_mail']){	// Log için mail gönder

    }
    $date_array = explode(" ",microtime());
    cm_isdir($cm_dirlist["log"].'security/',true);
    $filename = $cm_dirlist["log"].'security/log-'.md5($date_array[1].str_replace(".","",$date_array[0]));
    if(file_put_contents($filename,json_encode($LogAdd))){
        return true;
    }
    return false;
}

if(!isset($page_settings['security'])){ $page_settings['security'] = true; } // Eğer koruma gönderilmediyse otomatik aktif et

if($page_settings['security'] == true){
	$security			= true;
	$securityMessage	= null;
	if(isset($_SERVER['HTTP_REFERER']) and isset($_POST)){ //Siteye POST edilen bilgi var ve POST edilen site girilen site değil ise 404 hatası ver.
		if((count(array_keys($_POST)) > 0 and !strstr($_SERVER['HTTP_REFERER'],$_SERVER['SERVER_NAME'])) and trim($_SERVER['HTTP_REFERER']) != ''){
            cm_systemlogDir("security.php de kaynak ile referans farkli post engellemesi.\nPost Sayisi: ".count(array_keys($_POST))."\nReferans Site: ".$_SERVER['HTTP_REFERER']."\nBu Site: ".$_SERVER['SERVER_NAME']);
			$security = false;
		}
	}
	if($security == true and @count(@array_keys(@$_REQUEST)) > 0){ // Sql enjeksiyon koruması
		foreach($_REQUEST as $adi1 => $deger){
			if(is_string($deger)){
				if(!isset($page_settings['sql_sec'])){
					if((stristr($deger,'union') and stristr($deger,'select') and stristr($deger,'from') and stristr($deger,'concat')) or stristr($deger,'*/select/*') or (stristr($deger,' union ') and stristr($deger,'select ')) or (stristr($deger,'select ') and stristr($deger,' from ')) or stristr($deger,' distinct ') or stristr($deger,'union select') or stristr($deger,'uye_kredi_karti') or stristr($deger,'convert(int,')){
						$security = false;
						$securityMessage = "Sql-1 hackleme engellendi.";
						break;
					}
				}
				if(in_array($adi1,array('id','order_id','user_id','server_id','Id','no','module','module_id','domain_id','service_id','basket_id','product_id','product_type_id','ticket_id','hosting_id','sid','ssl_id'))){
					if($deger != 'credit' and !cm_numeric($deger)){
						$securityMessage = "Sql-3 hackleme engellendi.";
						$security = false;
						break;
					}
				}
			}elseif(is_array($deger)){
				foreach($deger as $adi => $deger){
					if(!isset($page_settings['sql_sec'])){
						if(is_string($deger)){
							if((stristr($deger,'union') and stristr($deger,'select') and stristr($deger,'from') and stristr($deger,'concat')) or stristr($deger,'*/select/*') or (stristr($deger,' union ') and stristr($deger,'select ')) or (stristr($deger,'select ') and stristr($deger,' from ')) or stristr($deger,' distinct ') or stristr($deger,'union select') or stristr($deger,'uye_kredi_karti') or stristr($deger,'convert(int,')){
								$securityMessage = "Sql-2 hackleme engellendi.";
								$security = false;
								break;
							}
						}
					}
					if(in_array($adi1,array('id','order_id','user_id','server_id','Id','no','module','module_id','domain_id','service_id','basket_id','product_id','product_type_id','ticket_id','hosting_id','sid','ssl_id'))){
						//if($deger != 'credit' and !cm_numeric($deger)){
							$securityMessage = "Sql-4 hackleme engellendi.\nBu olamaz: ".$adi1." = ".$deger;
							$security = false;
							break;
						//}
					}
				}
			}
		}
	}
	if($security == true and isset($_FILES) and @count(@array_keys($_FILES)) > 0){	// Dosya yükleme kontrol
		if(isset($page_settings['security']['upload']) and $page_settings['security']['upload'] == true){

		}else{
			$security = false;
			$securityMessage = 'İzin verilmeyen yerde dosya yükleme işlemi';
		}
	}
	if($security == false){
		//unset($_REQUEST,$_POST,$_GET);
		echo "<pre>Güvenlik korumasına takıldınız : ";
		print_r($cm_browser);
        cm_systemlogDir($securityMessage);
		exit;
	}
}