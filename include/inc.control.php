<?php
if(isset($cm_config['error_reporting']) and $cm_config['error_reporting'] == 1){
    error_reporting(E_ALL); ini_set("display_errors",true);
}else{
    error_reporting(0); ini_set("display_errors",false);
}
date_default_timezone_set('Europe/Istanbul');
if(isset($page_settings['session']) and $page_settings['session'] == true){
    session_start();
    $session_id = session_id();
}

$client_tabListMax	= 20;
$directory			= cm_path;
$directory			= explode("/",$directory);
$directory			= trim(end($directory));
if($directory != ""){ $directory = "/".$directory; }
$DomainControl		= [3,5,6,7,8];
$BasketFeatureClear	= ['product_id','server_id','service_id','hosting_id','time_type','time','action'];

$cm_dirlist			= [
    'library'		=> cm_path.'library/',
    'module'		=> cm_path.'modules/',
    'lang'			=> cm_path.'language/',
    'upload'		=> cm_path.'uploads/',
    'cache'			=> cm_path.'cache/',
    'template'		=> cm_path.'templates/',
    'log'		    => cm_path.'logs/',
    'post'		    => cm_path.'include/post/',
    'template_html' => cm_path.'library/smarty/libs/Smarty.class.php',
];

$ModulesDirList	= array(
    'addons'    => array(
        'name'  => $cm_lang->langGet('Eklenti Modülleri'),
    ),
    'domain'    => array(
        'name'  => $cm_lang->langGet('Alan Adı Modülleri'),
    ),
    'hosting'   => array(
        'name'  => $cm_lang->langGet('Hosting Modülleri'),
    ),
    'ip'        => array(
        'name'  => $cm_lang->langGet('IP Modülleri'),
    ),
    'payment'   => array(
        'name'  => $cm_lang->langGet('Ödeme Modülleri'),
    ),
    'server'    => array(
        'name'  => $cm_lang->langGet('Sunucu Modülleri'),
    ),
    'service'   => array(
        'name'  => $cm_lang->langGet('Hizmet Modülleri'),
    )
);