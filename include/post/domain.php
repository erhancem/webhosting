<?php

if(!defined('cm_path') or !function_exists('cm_get_request') or !isset($cm_user)){
	exit;
}
if(!isset($cm_method)){
	$cm_method = 'POST';
}
$cm_header_control = true;
if($action){
    $id = cm_get_request('id',$cm_method);
    include($cm_dirlist["post"]."control.php");

    if(!isset($token_message) and !in_array($action, ['available','whoiscm'])) {
        if($cm_user['user_id'] == 0) {
            $token_message = cm_lang('Lütfen üye girişi yapın');
        }elseif($cm_user['rank_id'] == 0){
            $token_message = cm_lang('Hesabınız aktif değil');
        }
    }

	if(!isset($token_message)){

		if(in_array($action,array('available','whoiscm'))) {    /** Alan adı sorgulaması */

            $domain_name = cm_get_request('domain_name', $cm_method);
            if ($domain_name and is_string($domain_name) and $domain_name != "") {
                $add_domain = array();
                $domains = explode(",", $domain_name);
                if (!is_array($domains) or count($domains) == 0) {
                    echo 'Hata oluştu';
                    exit;
                }
                $tld = array();
                foreach ($domains as $val) {
                    $val = mb_strtolower(trim($val), "utf8");
                    $dom = domainName($val);
                    if ($dom) {
                        $message = null;
                        if (!isset($tld[$dom['tld']])) {
                            $dom_tld = domainTld($dom['tld']);
                        } else {
                            $dom_tld = $tld[$dom['tld']];
                        }
                        $write = 'yes';
                        $transfer = 0;
                        if (!$dom_tld) {
                            $message = cm_lang('Alan adı uzantısı geçersiz');
                        } else {
                            if ($dom['idn_status'] != 0 and $dom_tld['idn_status'] == 0) {
                                $message = cm_lang('Uzantı IDN desteklemiyor');
                            }
                            if (!stristr($dom_tld['transfer_price'], '-')) {
                                $transfer = 1;
                            }
                            if (strlen($dom['name']) > $dom_tld['max_length'] or $dom_tld['min_length'] > strlen($dom['name'])) {
                                $message = cm_lang('Alan adı minimum ##min## maksimum ##max## karakter olmalı', array('min' => $dom_tld['min_length'], 'max' => $dom_tld['max_length']));
                            }
                            if ($dom_tld['tld_status'] == 0) {
                                $message = cm_lang('Uzantı aktif değil');
                            }
                        }
                        if ($message != null) {
                            $write = 'no';
                        }
                        $add_domain[$write][$val] = array(
                            'domain' => $dom['name'] . '.' . $dom['tld'],
                            'idn_status' => $dom['idn_status'],
                            'transfer' => $transfer,
                        );
                        if ($write == 'no') {
                            $add_domain[$write][$val]['status'] = 0;
                            $add_domain[$write][$val]['message'] = $message;
                        }
                        $add_domain[$write][$val]['tld'] = $dom_tld;
                    } else {
                        $add_domain['no'][cm_htmlclear($val)] = array(
                            'domain' => cm_htmlclear($val),
                            'tld' => null,
                            'idn_status' => 0,
                            'status' => 0,
                            'message' => cm_lang('Hatalı alan adı'),
                        );
                    }
                }

                $result = array();
                if (array_key_exists('yes', $add_domain)) {
                    $SearchResult = domainwhois($add_domain['yes']);
                    $add_domain['yes'] = $SearchResult;
                }

                if (isset($add_domain['yes']) and isset($add_domain['no'])) {
                    $result = array_merge($add_domain['yes'], $add_domain['no']);
                } elseif (isset($add_domain['no'])) {
                    $result = $add_domain['no'];
                } elseif (isset($add_domain['yes'])) {
                    $result = $add_domain['yes'];
                }

                $domainResult = array();
                foreach ($result as $key => $value) {
                    if ($action == 'whoiscm') {
                        $domainResult[$key] = $value;
                    } else {
                        $domainResult[$key] = array(
                            'domain' => $value['domain'],
                            'status' => $value['status'],
                            'domain_status' => $value['domain_status'],
                            'message' => $value['message'],
                            'idn_status' => $value['idn_status'],
                        );
                        if ($value['status'] == 2) {
                            $domainResult[$key]['transfer'] = $value['transfer'];
                            $domainResult[$key]['transfer_price'] = domainPrice(8, $value['tld']);
                        }
                        if ($value['status'] == 1) {
                            $domainResult[$key]['register_price'] = domainPrice(6, $value['tld']);
                        }
                    }
                }

                $cm_status = true;
                $cm_message = $domainResult;
            } else {
                $cm_message = cm_lang("Lütfen alan adı giriniz");
            }

        }elseif($action == 'register'){    /** Alan adı kayıt işlemi */

        }elseif($action == 'backorder'){    /** Alan adı backorder işlemi */

        }elseif($action == 'domain_add'){    /** Alan adı veritabanına ekleme */

            $data = array(
                'domain_name'   => cm_get_request('domain_name',$cm_method),
            );
            if(!isset($_SESSION["admin_id"]) or $_SESSION["admin_id"] == 0){
                $cm_message = cm_lang('Yetkisiz işlem');
            }elseif(!$data['domain_name'] or !domain_verify($data['domain_name'])){
                $cm_message = cm_lang('Hatalı veya geçersiz alan adı');
            }else{
                $data['domain_name'] = strtolower($data['domain_name']);
            }

		}else{

            $error = null;
            if (!$id or !cm_numeric($id) or $id == 0) {
                $error = cm_lang('İşlem yapılacak alan adınızı seçin');
            } elseif (!$domain = domainGetDetails($id)) {
                $error = cm_lang('Böyle bir alan adı bulunamadı');
            }elseif($cm_user["user_id"] != $domain['user_id'] or $domain["domain_delete"] != 0){
                $error = cm_lang('Böyle bir alan adınız yok');
            }elseif($domain['domain_status'] != 1){
                $error = cm_lang('Alan adınız aktif değil yönetemezsiniz');
            }elseif($domain['module_id'] == 0){
                $error = cm_lang('Alan adı sağlayıcısı aktif değil');
            }elseif($domain['domain_end_time'] < time()){   //API için ayarlar süre olayını şuan api süresi geçenlerde çalışmaz
                $error = cm_lang('Alan adınızın süresi doldu yönetmek için uzatma işlemi yapmalısınız');
            }elseif($action == 'renew' and strtotime("+10 day",$domain['domain_end_time']) < cm_time()){
                $error = cm_lang('Alan adı uzatılamaz silinme sürecine girdi');
            }

            if($error == null) {

                if ($action == 'transfer_lock') { /** Alan adı transfer kilidi */

                    $status = cm_get_request('status', $cm_method);
                    if ($domain["tld"]["manage_transfer_lock"] == 0) {
                        $cm_message = cm_lang('Alan adı uzantısı için transfer koruması desteklemiyor');
                    } elseif (!cm_numeric($status)) {
                        $cm_message = cm_lang('Lütfen kilit durumu seçin');
                    } elseif (!in_array($status, [0, 1])) {
                        $cm_message = cm_lang('Kilit durumu 1 (Aktif) veya 0 (Pasif) olabilir');
                    } elseif ($domain['domain_transfer_protection'] == 1 and $status == 1) {
                        $cm_message = cm_lang('Transfer kilidi zaten aktif');
                    } elseif ($domain['domain_transfer_protection'] == 0 and $status == 0) {
                        $cm_message = cm_lang('Transfer kilidi zaten pasif');
                    }else{
                        list($cm_status,$cm_message) = domainTransferLock($domain['domain_id'],$status);
                    }

                }elseif($action == 'cns_add') {     /** Alt isim sunucu oluşturma */

                    $cns_name = cm_get_request('cns_name', $cm_method);
                    $cns_ip = cm_get_request('cns_ip', $cm_method);

                    if ($domain["tld"]["manage_ns_dns"] == 0) {
                        $cm_message = cm_lang('Alan adı uzantısı için NS işlemlerine izin verilmemektedir');
                    } elseif (!$cns_name or $cns_name == '' or is_array($cns_name) or substr_count($cns_name,".") < 1) {
                        $cm_message = cm_lang('Lütfen isim sunucu için ad girin');
                    } elseif (!$cns_ip or $cns_ip == '' or is_array($cns_ip)) {
                        $cm_message = cm_lang('Lütfen IP adresi girin');
                    } elseif (!domain_verify($cns_name)) {
                        $cm_message = cm_lang('Ad sunucu adresi hatalı');
                    } elseif (!cm_ipverify($cns_ip)) {
                        $cm_message = cm_lang('IP adresi hatalı');
                    } else {

                        $error = null;
                        if($domain["ns"] and count($domain["ns"]) > 0){
                            foreach($domain["ns"] as $key => $value){
                                if($value["ns"] == $cns_name and $value["ip"] == $cns_ip){
                                    $error = cm_lang('Alt isim sunucu zaten ekli');
                                    break;
                                }
                            }
                        }
                        if($error != null){
                            $cm_message = $error;
                        }else{

                            $data = [
                                'new_ns'   => [$cns_name => $cns_ip],
                            ];
                            list($cm_status,$cm_message) = domainAddCNS($domain["domain_id"],$data);

                        }

                    }

                }elseif($action == 'cns_delete') {     /** Alt isim sunucu silme */

                    $cns_name = cm_get_request('cns_name', $cm_method);
                    $cns_ip = cm_get_request('cns_ip', $cm_method);

                    if ($domain["tld"]["manage_ns_dns"] == 0) {
                        $cm_message = cm_lang('Alan adı uzantısı için NS işlemlerine izin verilmemektedir');
                    } elseif (!$cns_name or $cns_name == '' or is_array($cns_name)) {
                        $cm_message = cm_lang('Lütfen isim sunucu için ad girin');
                    } elseif (!$cns_ip or $cns_ip == '' or is_array($cns_ip)) {
                        $cm_message = cm_lang('Lütfen IP adresi girin');
                    } elseif (!domain_verify($cns_name)) {
                        $cm_message = cm_lang('Ad sunucu adresi hatalı');
                    } elseif (!cm_ipverify($cns_ip)) {
                        $cm_message = cm_lang('IP adresi hatalı');
                    } else {

                        $error = cm_lang('Böyle bir alt isim sunucu bulunmamaktadır');
                        if($domain["ns"] and count($domain["ns"]) > 0){
                            foreach($domain["ns"] as $key => $value){
                                if($value["ns"] == $cns_name and $value["ip"] == $cns_ip){
                                    $error = null;
                                    break;
                                }
                            }
                        }
                        if($error != null){
                            $cm_message = $error;
                        }else{
                            $data = [
                                'delete_ns'   => [$cns_name => $cns_ip],
                            ];
                            list($cm_status,$cm_message) = domainDeleteCNS($domain["domain_id"],$data);
                        }

                    }

                }elseif($action == 'transfer_user'){    /** Farklı kullanıcıya alan adı transfer etme */

                }elseif($action == 'dns') {     /** Alan adı dns güncelleme */

                    $dns = cm_get_request('ns', $cm_method);
                    if ($domain["tld"]["manage_dns"] == 0) {
                        $cm_message = cm_lang('Alan adı uzantısı için DNS değişikliği izin verilmemektedir');
                    }elseif (!$dns or !is_array($dns) or count($dns) == 0) {
                        $cm_message = cm_lang('Lütfen dns güncellemesi için dns adresi girin');
                    } else {

                        $nsControl = 0;
                        $error = null;
                        $total = 0;
                        foreach ($domain['dns'] as $key => $value) {
                            if ($value != "" and !array_search($value, $dns)) {
                                ++$total;
                            }
                        }
                        foreach ($dns as $key => $val) {
                            if (in_array($key, [1, 2]) and $val != "") {
                                ++$nsControl;
                            }
                            if ($val != "" and !domain_verify($val)) {
                                $error = cm_lang('DNS adresi hatalı');
                                break;
                            }
                            if ($val != "" and !array_search($val, $domain['dns'])) {
                                ++$total;
                            }
                        }
                        if ($nsControl != 2) {
                            $cm_message = cm_lang('Lütfen iki adet dns adresi girin');
                        } elseif ($error != null) {
                            $cm_message = $error;
                        } elseif ($total == 0) {
                            $cm_message = cm_lang('DNS adresleri zaten aktif');
                        } else {
                            list($cm_status, $cm_message) = domainSaveDNS($domain["domain_id"], $dns);
                        }
                    }
                }elseif($action == 'contact'){      /** İletişim bilgileri güncelleme */

                    $data       = [];
                    $error      = null;
                    $Required   = [
                        'name'			=> cm_lang('Lütfen adınızı girin'),
                        'company_name'	=> cm_lang('Lütfen firma adı girin'),
                        'email_address'	=> cm_lang('Lütfen Email adresi girin'),
                        'phone_code'	=> cm_lang('Lütfen telefon kodu girin'),
                        'phone_number'	=> cm_lang('Lütfen telefon numaranızı girin'),
                        'address1'		=> cm_lang('Lütfen adresinizi girin'),
                        'city'			=> cm_lang('Lütfen şehir girin'),
                        'state'			=> cm_lang('Lütfen ilçe girin'),
                        'postcode'		=> cm_lang('Lütfen posta kodu girin'),
                        'country'		=> cm_lang('Lütfen ülke seçin'),
                    ];
                    $Optional = [
                        'address2',
                        'address3',
                        'fax_code',
                        'fax_number'
                    ];

                    foreach($Required as $key => $value){
                        $form = cm_get_request($key,$cm_method);
                        if(!$form or $form == ""){
                            $error = $value;
                            break;
                        }else{
                            $data[$key] = $form;
                        }
                    }
                    foreach($Optional as $value){
                        $form = cm_get_request($value,$cm_method);
                        if($form and $form != ""){
                            $data[$value] = $form;
                        }else{
                            $data[$value] = null;
                        }
                    }

                    if($domain["tld"]['manage_contact'] == 0) {
                        $cm_message = cm_lang('Alan adı uzantısı iletişim bilgisi güncelleme desteklemiyor');
                    }elseif($error != null){
                        $cm_message = $error;
                    }elseif($domain["contact_verify"] == 0) {
                        $cm_message = cm_lang('İletişim adresleriniz onaylanmamış değişiklik için öncelikle onaylama yapmalısınız');
                    }elseif(!cm_email_verify($data["email_address"])){
                        $cm_message = cm_lang('Email adresini hatalı girdiniz');
                    }else{

                        $new_contact = false;
                        if($domain["contact"]['email_address'] != $data["email_address"]){
                            $new_contact = true;
                        }

                        if($new_contact == true){
                            list($cm_status,$cm_message) = domainAddContact($domain["domain_id"],$data);
                        }else{
                            list($cm_status,$cm_message) = domainEditContact($domain["domain_id"],$data);
                        }

                    }

                }elseif($action == 'contact_verify'){   /** Alan adı iletişim bilgisi onay kontrol */

                    if($domain["tld"]['manage_contact'] == 0) {
                        $cm_message = cm_lang('Alan adı uzantısı iletişim bilgisi güncelleme desteklemiyor');
                    }elseif($domain["contact_verify"] == 1){
                        $cm_message = cm_lang('İletişim adresiniz onaylı');
                    }else{
                        list($cm_status,$cm_message) = domainVerifyStatusContact($domain["domain_id"]);
                    }

                }elseif($action == 'transfercode'){     /** Alan adı transfer kodu */

                    $sendtype = cm_get_request('sendtype',$cm_method);

                    if($domain["tld"]['manage_transfer_code'] == 0) {
                        $cm_message = cm_lang('Alan adı uzantısı transfer kodu desteklemiyor');
                    }elseif($domain["contact_verify"] == 0){
                        $cm_message = cm_lang('Transfer kodunu almanız için iletişim bilgileriniz onaylı olması gerekir');
                    }elseif($domain["domain_transfer_protection"] == 1){
                        $cm_message = cm_lang('Transfer korumanız aktif kodu alabilmeniz için pasif yapmanız gerekir');
                    }elseif((!$sendtype or !in_array($sendtype,[1,2,3])) or ($sendtype == 3 and !isset($cm_api))){ // 1: Whois mail,2: Üyelik Mail,3: API için direk kodu göster
                        $cm_message = cm_lang('Lütfen gönderim türünü seçin');
                    }elseif($sendtype != 3 and $domain["domain_codecreate"] != 0 and $domain["domain_codecreate"] > (cm_time()-(3600*12))){
                        $cm_message = cm_lang('Tekrar transfer kodu talebi için ##hour## saat beklemeniz gerekir',['hour' => 12]);
                    }else{

                        list($cm_status,$cm_message) = domainTransferCode($domain["domain_id"]);
                        if($cm_status == true){
                            if(in_array($sendtype,[1,2])){
                                $cm_db->sql_query_update("domains", ['domain_codecreate' => cm_time(), 'authcode' => $cm_message['code']], ['domain_id' => $domain["domain_id"]]);
                                $data = [
                                    'fullname'  => cm_user_name($cm_user),
                                    'domain'    => $domain['domain_name'],
                                    'code'      => $cm_message['code'],
                                ];
                                list($MailStatus,$MailTemplate)   = cm_mailtemplate('domaintransferCode',$data);
                                $email = $cm_user['email_address'];
                                if($sendtype == 1){
                                    $email = $domain['contact']['email_address'];
                                }
                                cm_mailSend($email,$MailTemplate['subject'],$MailTemplate['message']);
                                $cm_message = [
                                    'message'   => cm_lang('Transfer kodu Email olarak gönderildi'),
                                ];
                            }else{
                                $cm_message = [
                                    'message'   => cm_lang('Transfer kodu oluşturuldu'),
                                    'code'      => $cm_message["code"],
                                ];
                            }
                        }

                    }

                }else{
                    $cm_message = cm_lang('İşlem bulunamadı');
                }

            }else{
                $cm_message = $error;
            }

        }

	}else{
		$cm_message = $token_message;
	}
}else{
    $cm_message = cm_lang('İşlem seçmediniz');
}