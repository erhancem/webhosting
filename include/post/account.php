<?php

if(!defined('cm_path') or !function_exists('cm_get_request') or !isset($cm_user)){
    exit;
}
if(!isset($cm_method)){
    $cm_method = 'POST';
}
$cm_header_control = true;
if($action){

    $id = cm_get_request('id',$cm_method);
    include($cm_dirlist["post"]."control.php");

    if(!isset($token_message) and !in_array($action, ['available','whoiscm'])) {
        if($cm_admin['user_id'] == 0 or ($cm_admin['user_id'] != 0 and $cm_admin['user_id'] == $cm_user['user_id'])){
            if($cm_user['user_id'] == 0){
                $token_message = cm_lang('Lütfen üye girişi yapın');
            }elseif($cm_user['rank_id'] == 0){
                $token_message = cm_lang('Hesabınız aktif değil');
            }
        }else{
            if(!$id or !cm_numeric($id) or $id == 0){
                $token_message = cm_lang('Lütfen güncellenecek üye seçin');
            }elseif(!$cm_user = userGetDetails($id)){
                $token_message = cm_lang('Böyle bir üye bulunamadı');
            }
        }
    }

    if(!isset($token_message)){

        if($action == 'edit'){    /** Account update */

            $error  = null;
            $unset  = [];
            $user_data = [
                'user_type'         => cm_get_request('user_type',$cm_method),
                'company_name'      => cm_get_request('company_name',$cm_method),
                'first_name'        => cm_get_request('first_name',$cm_method),
                'last_name'         => cm_get_request('last_name',$cm_method),
                'birth_date'        => cm_get_request('birth_date',$cm_method),
                'country_id'        => cm_get_request('country_id',$cm_method),
                'country_no'        => cm_get_request('country_no',$cm_method),
                'phone_verify'      => cm_get_request('phone_verify',$cm_method),
                'email_address'     => cm_get_request('email_address',$cm_method),
                'username'          => cm_get_request('username',$cm_method),
                'gender'            => cm_get_request('gender',$cm_method),
            ];

            if($cm_admin["user_id"] == 0 and $cm_user['user_verify'] == 0){

                if(!$user_data['user_type'] or !in_array($user_data['user_type'],[0,1])){
                    $error = cm_lang("Üyelik türünüzü seçin");
                }elseif($user_data['user_type'] == 1 and !array_key_exists('company_name',$user_data)){
                    $error = cm_lang("Kurum adınızı girmediniz");
                }elseif(!$user_data['first_name']){
                    $error = cm_lang("Adınızı girin");
                }elseif(strlen($user_data['first_name']) > 255){
                    $error = cm_lang("Adınızı çok uzun");
                }elseif(!$user_data['last_name']){
                    $error = cm_lang("Soyadınızı girin");
                }elseif(strlen($user_data['last_name']) > 255){
                    $error = cm_lang("Soyadınızı çok uzun");
                }elseif(!$user_data['birth_date']){
                    $error = cm_lang("Doğum tarihinizi girin");
                }elseif(!strtotime($user_data['birth_date'])){
                    $error = cm_lang("Doğum tarihi formatı hatalı");
                }elseif(!$user_data['country_id'] or !cm_numeric($user_data['country_id']) or $user_data['country_id'] == 0){
                    $error = cm_lang("Ülkenizi girin");
                }else{
                    $country_id = $user_data['country_id'];
                    if($country_id == 176 and !$user_data["country_no"]){
                        $error = cm_lang("TC Kimlik numaranızı girin");
                    }elseif($country_id == 176 and !isTcKimlik($user_data['country_no'])){
                        $error = cm_lang("TC Kimlik numaranızı hatalı");
                    }elseif($country_id == 176 and !$tcControl = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."users WHERE user_id != '".$cm_db->sql_escape($cm_user['user_id'])."' AND country_no='".$cm_db->sql_escape($user_data['country_no'])."'")){
                        $error = cm_lang("TC Kimlik sistem üzerinden kontrol edilemedi");
                    }elseif($country_id == 176 and $cm_db->sql_num_rows($tcControl) > 0){
                        $error = cm_lang("TC Kimlik sistemde kayıtlı");
                    }
                }

            }else{

                if($cm_admin["user_id"] == 0 and $cm_user['user_verify'] == 1){
                    $unset = ['user_type','first_name','last_name','birth_date','country_id','country_no'];
                }

            }

            if($error == null){

                if($cm_admin["user_id"] == 0 and $cm_user['phone_verify'] == 0 and !$user_data['phone_number']) {
                    $error = cm_lang("Telefon numaranızı girin");
                }elseif(!$user_data["email_address"]){
                    $error = cm_lang("Email adresi girmediniz");
                }elseif(!cm_email_verify($user_data['email_address'])){
                    $error = cm_lang('Email adresi hatalı');
                }elseif(!$emailControl = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."users WHERE user_id != '".$cm_db->sql_escape($cm_user['user_id'])."' and email_address='".$cm_db->sql_escape($user_data['email_address'])."'")){
                    $error = cm_lang("Email adresi kontrol edilemedi");
                }elseif($cm_db->sql_num_rows($emailControl) > 0){
                    $error = cm_lang("Email adresi sistemde kayıtlı");
                }elseif(!$user_data["username"]){
                    $error = cm_lang("Kullanıcı adı girmediniz");
                }elseif(!cm_numeric($user_data['username']) and !strstr($user_data['username'],'@') and !preg_match("/^([a-z\d_])*$/i",$user_data['username'])){
                    $error = cm_lang('Geçersiz kullanıcı adı');
                }elseif(!$usernameControl = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."users WHERE user_id != '".$cm_db->sql_escape($cm_user['user_id'])."' and username='".$cm_db->sql_escape($user_data['username'])."'")){
                    $error = cm_lang("Kullanıcı adı kontrol edilemedi");
                }elseif($cm_db->sql_num_rows($usernameControl) > 0){
                    $error = cm_lang("Kullanıcı adı sistemde kayıtlı");
                }

            }

            if($error == null){

                $user_data = cm_db_dataControl($user_data, ['delete' => $unset]);

                list($cm_status,$cm_message) = userEdit($cm_user, $user_data);
                if($cm_status == true){
                    $cm_user = $cm_message['user'];
                    unset($cm_message['user']);
                }

            }else{
                $cm_message = $error;
            }

        }elseif($action == 'address'){  /** Account billing address */

            $unset      = [];
            $user_data  = [
                'name'          => cm_get_request('name',$cm_method),
                'billing_type'  => cm_get_request('billing_type',$cm_method),
                'country_id'    => cm_get_request('country_id',$cm_method),
                'address'       => cm_get_request('address',$cm_method),
                'city'          => cm_get_request('city',$cm_method),
                'state'         => cm_get_request('state',$cm_method),
                'postcode'      => cm_get_request('postcode',$cm_method),
                'tax_number'    => cm_get_request('tax_number',$cm_method),
                'tax_office'    => cm_get_request('tax_office',$cm_method),
            ];

            if(!$user_data['name'] or !$user_data['billing_type'] or !$user_data['country_id'] or !$user_data['address'] or !$user_data['city'] or !$user_data['state'] or !$user_data['postcode']){
                $cm_message = cm_lang('Zorunlu alanları doldurun');
            }elseif(!in_array($user_data['billing_type'],[0,1])){
                $cm_message = cm_lang('Fatura türü hatalı');
            }elseif(!cm_numeric($user_data['country_id'])){
                $cm_message = cm_lang('Ülke seçimi hatalı');
            }elseif(!cm_numeric($user_data['postcode'])){
                $cm_message = cm_lang('Posta kodu hatalı');
            }elseif($user_data['country_id'] == 176 and !$user_data['tax_number']){
                $cm_message = cm_lang('TC veya Vergi no hatalı');
            }elseif($user_data['country_id'] == 176 and $user_data['billing_type'] == 1 and !$user_data['tax_office']){
                $cm_message = cm_lang('Vergi dairesi girin');
            }elseif($user_data['country_id'] == 176 and !isTcKimlik($user_data['tax_number'])){
                $cm_message = cm_lang('Vergi veya TC No hatalı');
            }else{

                $user_data = cm_db_dataControl($user_data, ['delete' => $unset]);

                list($cm_status,$cm_message) = userAddress($cm_user, $user_data);
                if($cm_status == true){
                    $cm_user = $cm_message['user'];
                    unset($cm_message['user']);
                }

            }

        }else{
            $cm_message = cm_lang('İşlem bulunamadı');
        }

    }else{
        $cm_message = $token_message;
    }

}else{
    $cm_message = cm_lang('İşlem seçmediniz');
}