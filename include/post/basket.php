<?php
if(!defined('cm_path') or !function_exists('cm_get_request') or !isset($cm_user)){
	exit;
}
if(!isset($cm_method)){
	$cm_method = 'POST';
}
$cm_status 			= false;
$cm_message			= cm_lang('İşlem bulunamadı');
$action				= cm_get_request('action',$cm_method);
$product_type_id	= cm_get_request('product_type_id',$cm_method);
$id 				= cm_get_request('id',$cm_method);
$data				= array();

if($action){
	if($action == 'delete'){	/** Sepet ürün silme */
		if($id and cm_numeric($id) and $id > 0){
			list($delete_status,$delete_response) = basket_delete($id,$cm_user);
			if($delete_status == true){
				$cm_message = cm_lang('Ürün sepetinizden silindi');
				$cm_status = true;
			}else{
				$cm_message = $delete_response;
			}
		}else{
			$cm_message = cm_lang('Sepetinizden sileceğiniz ürünü seçin');
		}
	}elseif($action == 'domain_year'){	/** Sepet alan adı süre güncelle */
		$year = cm_get_request('year');
		if($year and cm_numeric($year) and $year > 0){
			if($id and cm_numeric($id) and $id > 0){
				list($time_status,$time_response) = basket_domain_year($id,$year,$cm_user);
				if($time_status == true){
					$cm_message = cm_lang('Alan adı süresi değiştirildi');
					$cm_status = true;
				}else{
					$cm_message = $time_response;
				}
			}else{
				$cm_message = cm_lang('Sepetinizden ürün seçin');
			}
		}else{
			$cm_message = cm_lang('Lütfen alan adı için yıl seçimi yapın');
		}
	}elseif($action == 'amount'){	/** Sepet alan adı süre güncelle */
		$amount = cm_get_request('amount');
		if($amount and cm_numeric($amount) and $amount > 0){
			if($id and cm_numeric($id) and $id > 0){
				list($time_status,$time_response) = basket_amount($id,$amount,$cm_user);
				if($time_status == true){
					$cm_message = cm_lang('Miktar güncellendi');
					$cm_status = true;
				}else{
					$cm_message = $time_response;
				}
			}else{
				$cm_message = cm_lang('Sepetinizden ürün seçin');
			}
		}else{
			$cm_message = cm_lang('Lütfen alan adı için yıl seçimi yapın');
		}
	}elseif($action == 'period_change'){	/** Sepet zaman güncelleme */
		if($id and cm_numeric($id) and $id > 0){
			$period = cm_get_request('period');
			if($period and cm_numeric($period) and $period > 0){
				list($time_status,$time_response) = basket_period($id,$period,$cm_user);
				if($time_status == true){
					$cm_message = cm_lang('Ürün süresi güncellendi');
					$cm_status = true;
				}else{
					$cm_message = $time_response;
				}
			}else{
				$cm_message = cm_lang('Süre hatalı');
			}
		}else{
			$cm_message = cm_lang('Sepetinizdeki ürünü seçin');
		}
	}elseif(in_array($action,array('add','renew'))){
		if($product_type_id and cm_numeric($product_type_id) and $product_type_id > 0){
			$product_type = productTypes($product_type_id);
			if($product_type){
				$product_type		= $product_type[$product_type_id];
				$data['product_type'] = $product_type;
				$product_feature	= product_feature_edit($product_type_id,$_REQUEST);
				$product_feature 	= $product_feature['array'];
				if($action == 'add'){	/** Sepet ürün ekleme */
					if(in_array($product_type_id,array(1))){	/** Bakiye ekleme (Kredi) */
						if($cm_user['user_id'] == 0){
							$message = cm_lang('Kredi bakiyesi eklemek için giriş yapmanız gerekir');
						}elseif(!array_key_exists('credit',$product_feature)){
							$message = cm_lang('Lütfen eklenecek bakiye miktarı girin');
						}elseif(!preg_match('/^[0-9]*(\.[0-9]+)?$/',$product_feature["credit"])){
							$message = cm_lang('Kredi miktarını hatalı girdiniz');
						}elseif($product_feature["credit"] < 5){
							$message = cm_lang('Minimum ##min## bakiye eklemesi yapabilirsiniz',array('min'=>cm_price_format(5,$cm_config['default_currency'])));
						}elseif($product_feature["credit"] > 100000){
							$message = cm_lang('Maksimum ##max## bakiye eklemesi yapabilirsiniz',array('max'=>cm_price_format(100000,$cm_config['default_currency'])));
						}else{
							$product_feature["credit"] = (string)round($product_feature["credit"],2);
							if(!stristr($product_feature["credit"],'.')){
								$product_feature["credit"] = $product_feature["credit"].'.00';
							}
						}
					}elseif(in_array($product_type_id,$DomainControl)){ /** Alan adı */
						if(in_array($product_type_id,array(5,6,8))){
							if(!array_key_exists('domain_name',$product_feature)){
								$message = cm_lang('Lütfen alan adı girin');
							}elseif($product_feature['domain_name'] == "" or !$DomainName = domainName($product_feature['domain_name'])){
								$message = cm_lang('Alan adı geçersiz');
							}else{
								$product_feature['domain_name'] = (string)$DomainName['name'].'.'.$DomainName['tld'];
							}
						}
						if(!isset($message) and in_array($product_type_id,array(3))){
							$message = cm_lang('Alan adı satış aktif değil');
						}
						if(!isset($message) and in_array($product_type_id,array(5,6,7,8))){
							if(!array_key_exists('year',$product_feature)){
								$message = cm_lang('Lütfen yıl seçin');
							}elseif(!cm_numeric($product_feature['year']) or $product_feature['year'] == 0){
								$message = cm_lang('Yıl seçimi hatalı');
							}
						}
						if(!isset($message) and in_array($product_type_id,array(8))){
							if(!array_key_exists('code',$product_feature)){
								$message = cm_lang('Lütfen transfer kodu girin');
							}elseif($product_feature['code'] == ""){ /** formatı öğren preg_mact yap */
								$message = cm_lang('Lütfen transfer kodu girin');
							}
						}
						if(!isset($message) and in_array($product_type_id,array(7))){	/** Alan adı uzatma */
							if(!isset($cm_user) or (isset($cm_user) and $cm_user['user_id'] == 0)){
								$message = cm_lang('Lütfen giriş yapın');
							}elseif(!array_key_exists('id',$product_feature)){
								$message = cm_lang('Uzatacağınız alan adını seçin');
							}elseif(!cm_numeric($product_feature['id']) or $product_feature['id'] == 0){
								$message = cm_lang('Alan adı id hatalı');
							}elseif(!$domain = $cm_db->sql_assoc('domains',array('domain_id'=>$product_feature['id'],'user_id'=>$cm_user['user_id']))){
								$message = cm_lang('Böyle bir alan adınız bulunamadı');
							}
						}
						if(!isset($message)){
							if(isset($domain)){
								$DomainName			= domainName($domain['domain_name']);
								$data['domains']	= $domain;
								$product_feature['domain_name'] = (string)$DomainName['name'].'.'.$DomainName['tld'];
							}
							$data['domain'] = $DomainName;
							$data['tld']	= domainTld($DomainName["tld"]);
							if(!$data['tld']){
								$message = cm_lang('Alan adı uzantısı geçersiz');
							}elseif(isset($domain) and $domain['domain_status'] != 1){
								$message = cm_lang('Ala adı aktif değil uzatamazsınız');
							}elseif($data['tld']['tld_status'] == 0){
								$message = cm_lang('Uzantı aktif değil');
							}elseif($data['tld']['idn_status'] == 0 and $data['domain']['idn_status'] == 1){
								$message = cm_lang('Uzantı IDN desteklemiyor');
							}elseif($data['tld']['max_length'] < strlen($data['domain']['name']) or $data['tld']['min_length'] > strlen($data['domain']['name'])){
								$message = cm_lang('Alan adı minimum ##min## maksimum ##max## karakter olmalı',array('min'=>$data['tld']['min_length'],'max'=>$data['tld']['max_length']));
							}elseif(in_array($product_type_id,array(5,6)) and ($data['tld']['max_register'] < $product_feature['year'] or $data['tld']['min_register'] > $product_feature['year'] or $product_feature['year'] == 0)){
								$message = cm_lang('Alan adı minimum ##min## maksimum ##max## yıl yapabilirsiniz',array('min'=>$data['tld']['min_register'],'max'=>$data['tld']['max_register']));
							}elseif(in_array($product_type_id,array(7)) and ($data['tld']['max_renewal'] < $product_feature['year'] or $data['tld']['min_renewal'] > $product_feature['year'] or $product_feature['year'] == 0)){
								$message = cm_lang('Alan adı minimum ##min## maksimum ##max## yıl uzatabilirsiniz',array('min'=>$data['tld']['min_renewal'],'max'=>$data['tld']['max_renewal']));
							}elseif($product_type_id == 8 and $data['tld']['transfer_status'] == 0){
								$message = cm_lang("Uzantı transfer desteklemiyor");
							}elseif($product_type_id == 5 and $data['tld']['backorder_status'] == 0){
								$message = cm_lang("Uzantı backorder desteklemiyor");
							}elseif($product_type_id == 5 and strstr($data['tld']['backorder_price'],'-')){
								$message = cm_lang("Uzantı backorder desteklemiyor");
							}elseif($product_type_id == 6 and strstr($data['tld']['register_price'],'-')){
								$message = cm_lang("Uzantı kayıt desteklemiyor");
							}elseif($product_type_id == 7 and strstr($data['tld']['renewal_price'],'-')){
								$message = cm_lang("Uzantı yenileme desteklemiyor");
							}elseif($product_type_id == 8 and strstr($data['tld']['transfer_price'],'-')){
								$message = cm_lang("Uzantı transfer desteklemiyor");
							}
						}
					}else{
						$product_id	= cm_get_request('product_id');
						if($product_id and cm_numeric($product_id) and $product_id > 0){
							$period		= cm_get_request('period');
							$product	= product_list($product_id);
							if($product){ /** stock kontrol v.s burada ayarla */
								if($product['product_type_id'] == $product_type_id){
									$data['product'] = $product;
									if($product['price_status'] != 0 and !array_key_exists('period',$product_feature)){
										$message = cm_lang('Ödeme periyodu seçilmedi');
									}elseif($product['os_select'] == 1 and (!array_key_exists('os_id',$product_feature) or (array_key_exists('os_id',$product_feature) and (!cm_numeric($product_feature['os_id']) or $product_feature['os_id'] == 0)))){
										$message = cm_lang('Lütfen işletim sistemi seçin');
									}elseif($product['os_select'] == 1 and !$os = cm_os_list($product_feature['os_id'])){
										$message = cm_lang('İşletim sistemi geçersiz');
									}elseif($product['panel_select'] == 1 and (!array_key_exists('panel_id',$product_feature) or (array_key_exists('panel_id',$product_feature) and !cm_numeric($product_feature['panel_id'])))){
										$message = cm_lang('Panel seçimi hatalı');
									}elseif($product['panel_select'] == 1 and $product_feature['panel_id'] > 0 and !in_array($product_feature['panel_id'],explode(",",$os[$product_feature['os_id']]['panel_ids']))){
										$message = cm_lang('Kontrol paneli desteklemiyor');
									}elseif($product['domain_select'] == 1 and !array_key_exists('domain_name',$product_feature)){
										$message = cm_lang('Lütfen alan adı seçin');
									}elseif($product['domain_select'] == 1 and ($product_feature['domain_name'] == "" or !$domainName = domainName($product_feature['domain_name']))){
										$message = cm_lang('Alan adı geçersiz');
									}elseif($product['ipv4_select'] == 1 and array_key_exists('ipv4',$product_feature) and !cm_numeric($product_feature['ipv4'])){
										$message = cm_lang('IP v4 adresi hatalı');
									}elseif($product['ipv6_select'] == 1 and array_key_exists('ipv6',$product_feature) and !cm_numeric($product_feature['ipv6'])){
										$message = cm_lang('IP v6 adresi hatalı');
									}else{
										if(array_key_exists('domain_name',$product_feature)){
											$product_feature['domain_name'] = (string)$domainName['name'].'.'.$domainName['tld'];
										}
										if($product['custom_config'] == 1){

										}
										if($product['price_status'] != 1){
											$periodKey = array_keys($product['prices_list']);
											if(!in_array($product_feature['period'],$periodKey)){
												$product_feature['period'] = (string)$periodKey[0];
											}
										}
									}
								}else{
									$message = cm_lang('Ürün türü uyumsuz');
								}
							}else{
								$message = cm_lang('Ürün bulunamadı');
							}
						}else{
							$message = cm_lang('Lütfen ürün seçin');
						}
					}
				}elseif($action == "renew"){	/** Sepet Ürün uzatma */
					if($cm_user['user_id'] > 0){
						if($id and cm_numeric($id) and $id > 0){
							$service = serviceGetDetails($id,$product_type['product_group_name']);
							if($service){
								if($service['user_id'] == $cm_user['user_id']){
									$product_id = $service['product_id'];
									if($product_id and cm_numeric($product_id) and $product_id > 0 and $product = product_list($product_id)){
										$data['product'] = $product;
										$data['service'] = $service;
									}else{
										$message = cm_lang('Uzatma yapacağınız hizmet bulunamadı destek ile iletişime geçin');
									}
								}else{
									$message = cm_lang('Hizmet bulunamadı');
								}
							}else{
								$message = cm_lang('Hizmet bulunamadı');
							}
						}else{
							$message = cm_lang('Uzatmak istediğiniz ürünü seçin');
						}
					}else{
						$message = cm_lang('Uzatma siparişi için üye girişi yapın');
					}
				}
				if(!isset($message)){
					list($basket_status,$basket_response) = basket_add($product_type_id,$product_feature,$data,$cm_user);
					if($basket_status == true){
						$cm_message = $basket_response['message'];
						$cm_status	= true;
					}else{
						$cm_message = $basket_response;
					}
				}else{
					$cm_message = $message;
				}
			}else{
				$cm_message = cm_lang('Böyle bir ürün bulunamadı');
			}
		}else{
			$cm_message = cm_lang('Lütfen ürün kodu girin');
		}
	}else{
		$cm_message = cm_lang('İşlem bulunamadı');
	}
}