<?php
if(!defined('cm_path') or !function_exists('cm_get_request') or !isset($action)){
	exit;
}
if(!isset($cm_method)){
	$cm_method = 'POST';
}

if($action){
    include($cm_dirlist["post"]."control.php");

	if(!isset($token_message)){
		if($action == 'login'){

			$username   = cm_get_request("username",$cm_method);
			$password   = cm_get_request("password",$cm_method);
			if(!$username or ($username and $username == "")){
				$message = cm_lang('Lütfen kullanıcı adınızı girin');
			}elseif(strstr($username,'@') and !cm_email_verify($username)){
				$message = cm_lang('Email adresi geçersiz');
			}elseif(!cm_numeric($username) and !strstr($username,'@') and !preg_match("/^([a-z\d_])*$/i",$username)){
				$message = cm_lang('Geçersiz kullanıcı adı');
			}elseif(!$password or ($password and $password == "")){
				$message = cm_lang('Lütfen şifrenizi girin');
			}

			if(!isset($message)){
				list($login_status,$login_response) = userLogin($username,$password,false);
				if($login_status == true){
					$cm_status  = true;
					$cm_message = array(
						'message'	=> cm_lang('Üye giriş yapıldı'),
					);
				}else{
					$cm_message = $login_response;
				}
			}else{
				$cm_message = $message;
			}

		}elseif($action == 'register'){

            $cm_status = false;
            $user_data = array(
                'email_address'     => cm_get_request('email_address',$cm_method),
                'password'          => cm_get_request('password',$cm_method),
                'password_control'  => cm_get_request('password_control',$cm_method),
            );

            $message = null;
            if(!array_key_exists('email_address',$user_data)){
                $message = cm_lang("Email adresi girmediniz");
            }elseif(!cm_email_verify($user_data['email_address'])){
                $message = cm_lang('Email adresi hatalı');
            }elseif(!array_key_exists('password',$user_data)){
                $message = cm_lang("Şifre belirlemediniz");
            }elseif(strlen($user_data['password']) < 8){
                $message = cm_lang("Şifreniz minimum ##min## karakter olmalıdır",array('min'=>8));
            }elseif(!array_key_exists('password_control',$user_data)){
                $message = cm_lang("Tekrar şifre girmediniz");
            }elseif($user_data['password_control'] != $user_data['password']){
                $message = cm_lang("Şifreleriniz uyuşmuyor");
            }elseif(isset($_SESSION['user_id'])){
                $message = cm_lang("Zaten oturum açmış durumdasınız");
            }

			if($message == null){
				list($cm_status,$cm_message) = userAdd($user_data);
			}else{
				$cm_message = $message;
			}

		}elseif($action == 'reset'){

		}else{
			$cm_message = cm_lang('İşlem bulunamadı');
		}
	}else{
		$cm_message = $token_message;
	}
}