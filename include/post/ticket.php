<?php


if(!defined('cm_path') or !function_exists('cm_get_request') or !isset($action)){
	exit;
}
if(!isset($cm_method)){
	$cm_method = 'POST';
}

if($action){
    include($cm_dirlist["post"]."control.php");

	if($cm_user['user_id'] == 0){
		$token_message = cm_lang('Lütfen üye girişi yapın');
	}

	if(!isset($token_message)){

	    if($action == 'new'){

            $ticket_subject     = cm_get_request("subject",$cm_method);
            $ticket_message     = cm_get_request("message",$cm_method);
            $ticket_dep_id      = cm_get_request("dep_id",$cm_method);

            if(!$ticket_subject or strlen($ticket_subject) < 5){
                $message = cm_lang('Destek konusu minimum ##min## karakter olmalıdır',array('min'=>5));
            }elseif(!$ticket_subject or strlen($ticket_subject) > 250){
                $message = cm_lang('Destek konusu maksimum ##max## karakter olmalıdır',array('max'=>250));
            }elseif(!$ticket_message or strlen($ticket_message) < 10){
                $message = cm_lang('Destek mesajı minimum ##min## karakter olmalıdır',array('min'=>10));
            }elseif(!$ticket_message or strlen($ticket_message) > 5000){
                $message = cm_lang('Destek mesajı maksimum ##max## karakter olmalıdır',array('max'=>5000));
            }elseif(!$ticket_dep_id or !cm_numeric($ticket_dep_id) or $ticket_dep_id == 0){
                $message = cm_lang('Destek departmanı seçin');
            }

            if(!isset($message)){
                $params = array(
                    'ticket_dep_id'     => $ticket_dep_id,
                    'ticket_subject'    => $ticket_subject,
                    'ticket_message'    => $ticket_message
                );
                unset($ticket_dep_id,$ticket_subject,$ticket_message);
                list($cm_status,$cm_message) = ticketCreate($params);
            }else{
                $cm_message = $message;
            }

        }elseif($action == 'answer'){

            $id             = cm_get_request("id");
            $ticket_message = cm_get_request("message",$cm_method);

            if(!$ticket_message or strlen($ticket_message) < 10){
                $message = cm_lang('Destek mesajı minimum ##min## karakter olmalıdır',array('min'=>10));
            }elseif(!$ticket_message or strlen($ticket_message) > 5000){
                $message = cm_lang('Destek mesajı maksimum ##max## karakter olmalıdır',array('max'=>5000));
            }elseif(!$id or !cm_numeric($id)){
                $message = cm_lang('Lütfen destek talebi seçin');
            }elseif(!$ticket = $cm_db->sql_assoc('tickets',array('ticket_id' => $id))){
                $message = cm_lang('Böyle bir destek talebi bulunamadı');
            }elseif($cm_user['user_id'] != $ticket['user_id'] or $ticket['ticket_delete'] == 1){
                $message = cm_lang('Böyle bir destek talebiniz bulunmamaktadır');
            }

            if(!isset($message)){
                $params = array(
                    'ticket_id'         => $id,
                    'ticket_message'    => $ticket_message,
                );
                unset($id,$ticket_message);
                list($cm_status,$cm_message) = ticketAnswer($params);
            }else{
                $cm_message = $message;
            }

        }elseif($action == 'delete'){


		}else{
			$cm_message = cm_lang('İşlem bulunamadı');
		}

	}else{
		$cm_message = $token_message;
	}

}