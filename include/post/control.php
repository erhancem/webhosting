<?php
if(!defined('cm_path') or !function_exists('cm_get_request') or !isset($cm_user)){
    exit;
}

if(!isset($cm_api)){
    if(!in_array($action,array('available','whoiscm'))){
        if(!$cm_token or ($cm_token and !cm_get_token($cm_token))){
            $token_message = cm_lang('İşlem token geçersiz sayfa yenileyin');
        }

        if(!isset($token_message) and isset($cm_header_control) and $cm_header_control == true){

            $reqcontrol = [
                'accept'    => false,
                'referer'   => false,
                'user-agent'=> false,
            ];
            $headers = apache_request_headers();
            foreach ($headers as $key => $value){
                $key = strtolower($key);
                if($key == 'accept' and stristr($value,'application/json') and stristr($value,'text/javascript')){
                    $reqcontrol[$key] = true;
                }
                if($key == 'referer' and stristr($value,cm_siteurl())){
                    $reqcontrol[$key] = true;
                }
                if($key == 'user-agent' and $value != ""){
                    $reqcontrol[$key] = true;
                }
            }
            foreach ($reqcontrol as $key => $value){
                if($value == false){
                    $token_message = cm_lang('Token kontrol başarısız');
                    break;
                }
            }

        }
    }else{
        if(!isset($_SESSION['cm_whois']) or $_SESSION['cm_whois'] < cm_time()){
            $token_message = cm_lang('İşlem token geçersiz sayfa yenileyin');
        }else{
            $_SESSION['cm_whois'] = (cm_time()+(60*20));
        }
    }
}