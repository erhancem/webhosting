<?php
if(!defined('cm_path') or !function_exists('cm_get_request') or !isset($cm_user)){
	exit;
}
if(!isset($cm_method)){
	$cm_method = 'POST';
}
$cm_status 			= false;
$cm_message			= cm_lang('İşlem bulunamadı');
$action				= cm_get_request('action',$cm_method);
$product_type_id	= cm_get_request('product_type_id',$cm_method);
$cm_token			= cm_get_request('cm_token');

if($action){
    include($cm_dirlist["post"]."control.php");

	if($cm_user['user_id'] == 0){
		$token_message = cm_lang('Lütfen üye girişi yapın');
	}

	if(!isset($token_message)){
		if($action == 'edit'){
			/**
			list($cm_status,$cm_message) = userEdit($cm_user,$_REQUEST);
			if($cm_status == true){
				$cm_user		= $cm_message['user'];
				$cm_message	= cm_lang('Bilgiler güncellendi');
			}
			*/
		}else{
			$cm_message = cm_lang('İşlem bulunamadı');
		}
	}else{
		$cm_message = $token_message;
	}
}