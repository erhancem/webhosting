<?php
function ticketFunctions(){

}
function ticket_departments(){
	global $cm_config,$cm_db;
	$depX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."ticket_departments WHERE ticket_dep_status=1 ORDER BY ticket_dep_name ASC");
	if($cm_db->sql_errno() == 0){
		if($cm_db->sql_num_rows($depX) > 0){
			$return = array();
			while($dep = $cm_db->sql_fetch_assoc($depX)){
				$return[$dep['ticket_dep_id']] = $dep;
			}
			return $return;
		}
	}
	return false;
}

function ticketUpload($params){
	global $cm_db,$cm_db;
	if(isset($_FILES)){
		$ticket_dir = cm_path.'uploads/ticket/';
		list($status,$message) = cm_file_upload($_FILES,$ticket_dir,$params['user']['user_id']);
		if($status == true){
			foreach($message as $value){
				$add = array( //
					'ticket_answer_id'	=> $params['ticket']['ticket_answer_id'],
					'additional_id'		=> $value,
				);
				$cm_db->sql_insert_add('ticket_answer_files',$add);
			}
		}
	}else{
		$status = true;
		$message = cm_lang('Dosya seçmediniz');
	}
	return array($status,$message);
}

function ticketCreate($params){
    global $cm_db,$ip,$cm_user;
    $now = cm_time();
    $Create_Ticket = array(
        'user_id'			=> $cm_user['user_id'],
        'ticket_dep_id'		=> $params['ticket_dep_id'],
        'ticket_status'		=> 1,
        'ticket_subject'	=> $params['ticket_subject'],
        'ticket_start_time'	=> $now,
        'ticket_end_time'	=> $now,
    );
    list($create_status,$ticket_id) = $cm_db->sql_insert_add('tickets',$Create_Ticket);
    if($create_status == true){
        $ticket_answer = array(
            'ticket_id'				=> $ticket_id,
            'user_id'				=> $cm_user['user_id'],
            'ticket_message'		=> $params['ticket_message'],
            'ticket_answer_ip'		=> $ip,
            'ticket_answer_time'	=> $now,
        );
        list($answer_status,$answer_id) = $cm_db->sql_insert_add('ticket_answer',$ticket_answer);
        if($answer_status == true){
            $status = true;
            $params['ticket_answer_id'] = $answer_id;
            $params['ticket_id'] = $ticket_id;
            ticketUpload($params);
            $message = array(
                'ticket_id'	=> $ticket_id,
                'message'	=> cm_lang('Destek talebi oluşturuldu'),
            );
        }else{
            $cm_db->sql_delete('tickets',array('ticket_id'=>$ticket_id));
            $message = cm_lang('Destek ccevabı oluşturulamadı');
        }
    }else{
        $message = cm_lang('Destek talebi açılamadı');
    }
    return array($status,$message);
}

function ticketAnswer($params){
    global $cm_user,$cm_db,$ip;
    $status = false;
    $now = cm_time();
    $ticket_answer = array(
        'ticket_id'				=> $params['ticket_id'],
        'user_id'				=> $cm_user['user_id'],
        'ticket_message'		=> $params['ticket_message'],
        'ticket_answer_ip'		=> $ip,
        'ticket_answer_time'	=> $now,
    );
    list($answer_status,$answer_id) = $cm_db->sql_insert_add('ticket_answer',$ticket_answer);
    if($answer_status == true){
        $cm_db->sql_query_update('tickets',array('ticket_status' => 1,'ticket_end_time' => $now),array('ticket_id' => $params['ticket_id']));
        $status = true;
        $params['ticket']['ticket_answer_id'] = $answer_id;
        $params['ticket']['ticket_id'] = $params['ticket_id'];
        $params['user'] = $cm_user;
        ticketUpload($params);
        $message = array(
            'ticket_id'			=> $params['ticket_id'],
            'user_id'	        => $answer_id,
            'message'			=> cm_lang('Destek talebi cevaplandı'),
        );
    }else{
        $message = cm_lang('Destek cevabı oluşturulamadı');
    }
    return array($status,$message);
}