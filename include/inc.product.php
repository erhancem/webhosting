<?php
function productTypes($product_id = 0){
	global $cm_config,$cm_db;
	if(!cm_numeric($product_id)){ $product_id = 0; }
	$productX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."product_type WHERE product_type_id ".($product_id==0?"NOT IN (0)":"IN ('".$cm_db->sql_escape($product_id)."')"));
	if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($productX) > 0){
		$return = array();
		while($product = $cm_db->sql_fetch_assoc($productX)){
			$return[$product['product_type_id']] = $product;
		}
		if(count($return) > 0){
			return $return;
		}
	}
	return false;
}
function product_list($product_id = null){
	global $cm_config,$cm_db;
	if(!cm_numeric($product_id)){
		$product_id = 0;
	}
	$productX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."products WHERE product_id ".($product_id==0?"NOT IN (0)":"IN ('".$cm_db->sql_escape($product_id)."')"));
	if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($productX) > 0){
		$return = array();
		while($product = $cm_db->sql_fetch_assoc($productX)){
			$pricesList = array();
			$pricesX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."product_prices WHERE product_id='".$cm_db->sql_escape($product['product_id'])."'");
			if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($pricesX) > 0){
				while($prices = $cm_db->sql_fetch_assoc($pricesX)){
					$pricesList[$prices['value']] = $prices;
				}
			}
			$product['prices_list'] = $pricesList;
			$return[$product['product_id']] = $product;
		}
		$Total = count($return);
		if($Total > 0){
			if($Total == 1){
				return $return[$product_id];
			}elseif($Total > 1){
				return $return;
			}
		}
	}
	return false;
}
function product_panelselect($os_id = null,$product_type_id = 10){
	global $cm_config;
	$status = false;
	if(!in_array($product_type_id,array(10,11,14,9))){
		$product_type_id = 10;
	}
	if(cm_numeric($os_id) and $os_id > 0){
		$osList = cm_os_list($os_id);
		if($osList){
			$status = true;
			$return = array();
			$osList = $osList[$os_id];
			if($osList['panel_ids'] != ""){
				$return["0"] = array(
					'name'			=> cm_lang('Panel İstemiyorum'),
					'price'			=> cm_money_format(0.00),
					'currency'		=> $cm_config['default_currency'],
				);
				foreach(explode(",",$osList['panel_ids']) as $value){
					$panel_list = cm_panel_list($value);
					if($panel_list){
						$panel_list = $panel_list[$value];
						$price = array(
							'price'		=> (in_array($product_type_id,array(14,9))?$panel_list['panel_dedicated_price']:$panel_list['panel_server_price']),
							'currency'	=> $panel_list['currency'],
						);
						$price = cm_currency_convert($price['price'],$price['currency'],$cm_config['default_currency']);
						$return[$value] = array(
							'name'			=> $panel_list['panel_name'],
							'price'			=> $price['price'],
							'currency'		=> $price['currency'],
						);
					}
				}
			}else{
				$return[0] = array(
					"name"			=> cm_lang('Seçtiğiniz sistem panel desteklemiyor'),
					'price'			=> cm_money_format(0.00),
					'currency'		=> $cm_config['default_currency'],
				);
			}
			$message = $return;
		}else{
			$message = cm_lang('İşletim sistemi hatalı');
		}
	}else{
		$status			= true;
		$message[null] 	= array(
			"name"			=> cm_lang('Önce İşletim sistemi seçin'),
			'price'			=> cm_money_format(0.00),
			'currency'		=> $cm_config['default_currency'],
		);
	}
	return array($status,$message);
}
function product_feature_edit($product_type_id,$product_feature){
	global $DomainControl;
	$product_type	= true;
	if(!is_array($product_feature)){
		$new_product_feature = @cm_json_decode($product_feature);
		if(!is_array($new_product_feature)){
			@parse_str($product_feature,$new_product_feature);
			if(is_array($new_product_feature)){
				$product_feature = $new_product_feature;
			}
		}else{
			$product_feature = $new_product_feature;
		}
	}
	if($product_type_id == 1){	/** Kredi ekleme */
		$product_type = false;
		$feature = array(
			'credit'	=> '50'
		);
	}elseif($product_type_id == 2){	/** Hizmet Eklentileri */
		$feature = array(

		);
	}elseif($product_type_id == 3){	/** ALan adı satış */
		$feature = array(
			'domain_name'	=> '',
		);
	}elseif($product_type_id == 4){	/** Hosting */
		$feature = array(
			'domain_name'	=> '',
		);
	}elseif($product_type_id == 5){	/** Alan adı backorder */
		$feature = array(
			'domain_name'	=> '',
		);
	}elseif($product_type_id == 6){	/** Alan adı kayıt */
		$feature = array(
			'domain_name'	=> '',
		);
	}elseif($product_type_id == 7){	/** Alan Adı yenileme */
		$feature = array(
			'id'			=> '0',
			'domain_name'	=> "",
		);
	}elseif($product_type_id == 8){	/** Alan Adı Transfer */
		$feature = array(
			'domain_name'	=> '',
			'code'			=> '',
		);
	}elseif($product_type_id == 9){	/** Fiziksel Sunucu */
		$feature = array(
			'ipv4'		=> '0',
			'ipv6'		=> '0',
			'panel_id'	=> '0',
			'os_id'		=> '0',
		);
	}elseif($product_type_id == 10){	/** VDS Sunucu */
		$feature = array(
			'hdd'		=> '',
			'ram'		=> '',
			'cpu'		=> '',
			'traffic'	=> '',
			'ipv4'		=> '0',
			'ipv6'		=> '0',
			'panel_id'	=> '0',
			'os_id'		=> '0',
		);
	}elseif($product_type_id == 11){	/** VPS Sunucu */
		$feature = array(
			'hdd'		=> '',
			'ram'		=> '',
			'cpu'		=> '',
			'traffic'	=> '',
			'ipv4'		=> '0',
			'ipv6'		=> '0',
			'panel_id'	=> '0',
			'os_id'		=> '0',
		);
	}elseif($product_type_id == 12){	/** Marka Tescili */

	}elseif($product_type_id == 13){	/** SSL */

	}elseif($product_type_id == 14){	/** Sunucu Barındırma */

	}elseif($product_type_id == 15){	/** IPv4 */

	}elseif($product_type_id == 16){	/** IPv6 */

	}elseif($product_type_id == 17){	/** Diğer Ürün/Hizmet */

	}
	$new_feature = array();
	$action = 'add';
	if(array_key_exists('action',$product_feature) and in_array($product_feature['action'],array('renew'))){
		$action = 'renew';
	}
	$new_feature['action'] = (string)$action;
	if(in_array($product_type_id,$DomainControl)){
		if(!array_key_exists('year',$product_feature)){
			$year = 1;
		}elseif(!cm_numeric($product_feature['year']) or $product_feature['year'] == 0){
			$year = 1;
		}else{
			$year = $product_feature['year'];
		}
		$new_feature['year']	= (string)$year;
	}else{
		if(!array_key_exists('period',$product_feature)){
			$period = 1;
		}elseif(!cm_numeric($product_feature['period'])){
			$period = 1;
		}else{
			$period = $product_feature['period'];
		}
		$new_feature['period']	= (string)$period;
	}
	if($action == 'add'){	/** Satın alma */
		if(isset($feature) and count($feature) > 0){
			foreach($feature as $key=>$value){
				if(array_key_exists($key,$product_feature) and $product_feature[$key] != "" and (in_array($key,array('domain_name','code','credit')) or cm_numeric($product_feature[$key]))){
					$new_feature[(string)$key] = (string)$product_feature[$key];
				}else{
					$new_feature[(string)$key] = (string)$value;
				}
			}
		}
	}else{	/** Yenileme */
		$new_feature['id'] = (string)$product_feature['id'];
	}
	$return = array(
		'array'		=> $new_feature,
		'string'	=> http_build_query($new_feature),
		'json'		=> cm_json_encode($new_feature)
	);
	return $return;
}
function product_get_data($product_type_id,$product_feature,$product_id = 0){
	global $cm_db,$cm_config,$DomainControl;
	$response		= false;
	if(!is_array($product_feature)){
		$new_product_feature = @cm_json_decode($product_feature);
		if(!is_array($new_product_feature)){
			@parse_str($product_feature,$new_product_feature);
			if(is_array($new_product_feature)){
				$product_feature = $new_product_feature;
			}
		}else{
			$product_feature = $new_product_feature;
		}
	}
	$product_type	= productTypes($product_type_id);
	if($product_type){
		$response		= array();
		$product_type 	= $product_type[$product_type_id];
		$response['product_type']	= $product_type;
		if(in_array($product_type_id,$DomainControl)){
			if($product_type_id == 7){
				$domain				= $cm_db->sql_assoc('domains',array('domain_id'=>$product_feature['id']));
				$response['domains']	= $domain;
				$product_feature['domain_name'] = $domain['domain_name'];
			}
			$DomainName		= domainName($product_feature['domain_name']);
			$response['domain'] = $DomainName;
			$response['tld']	= domainTld($DomainName["tld"]);
		}elseif(in_array($product_type_id,array(1))){

		}else{
			if($product_feature['action'] == 'renew'){
				$service				= serviceGetDetails($product_feature['id'],$product_type['product_group_name']);
				$response['service']	= $service;
				$product_id				= $service['product_id'];
			}
			if($product_id > 0){
				$response['product'] = product_list($product_id);
			}
		}
	}
	return $response;
}
function product_price_calculation($product_type_id,$product_feature,$data = null,$user = array()){
	global $cm_config;
	if(!is_array($product_feature)){
		$product_feature = product_feature_edit($product_type_id,$product_feature);
		$product_feature = $product_feature['array'];
	}
	$status				= false;
	$DomainControl		= array(3,5,6,7,8);
	$action				= $product_feature['action'];
	$product_type		= $data['product_type'];
	$product_id			= (array_key_exists('product_id',$product_feature)?$product_feature['product_id']:0);
	$tax_status			= 0;
	$taxrate			= 0;
	$tax				= 0.00;
	$discount			= 0.00;
	$country			= $cm_config['default_country'];
	$price				= 0.00;
	if(in_array($product_type_id,array(1))){
		$taxrate 	= $product_type['product_taxrate'];
		$tax_status	= $product_type['product_tax_status'];
		$currency	= $cm_config['default_currency'];
		$price		= cm_money_format($product_feature['credit']);
		$status		= true;
	}elseif(in_array($product_type_id,$DomainControl)){	/** Alan adı ücretleri */
		$params = array_merge(array('feature'=>$product_feature),$data);
		list($domain_status,$domain_response) = domainCalculation($product_type_id,$params);
		if($domain_status == true){
			if($domain_response['price'] >= 0){
				$status		= true;
				$taxrate 	= $product_type['product_taxrate'];
				$tax_status	= $product_type['product_tax_status'];
				$price		= $domain_response['price'];
				$discount	= $domain_response['discount'];
				$currency	= $domain_response['currency'];
				if($domain_response['currency'] != $cm_config['default_currency']){
					$PriceConvert	= cm_currency_convert($price,$currency,$cm_config['default_currency']);
					$price 			= $PriceConvert['price'];
					$PriceConvert	= cm_currency_convert($discount,$currency,$cm_config['default_currency']);
					$discount		= $PriceConvert['price'];
					$currency		= $PriceConvert['currency'];
				}
			}else{
				$response = cm_lang('Alan adı ücreti hesaplanamado');
			}
		}else{
			$response = $domain_response;
		}
	}else{
		if(is_array($data) and array_key_exists('product',$data)){
			$product	= $data['product'];
			$tax_status	= $product['tax_status'];
			$taxrate	= $product['taxrate'];

			$price		= $product['product_price'];
			if($product['price_status'] == 2){
				$pricePeriod= $product['prices_list'][$product_feature['period']];
				$price 		= $pricePeriod['price'];
				if($action == 'renew'){
					$price		= $pricePeriod['renew_price'];
					$service	= $data['service'];
				}
			}
			$price		= cm_money_format($price);
			$currency	= $product['currency'];

			/** Paket IP */
			if($product['ipv4_select'] == 1){	/** IPV4 */
				$ipv4_price = $product['ip_price_v4']*$product_feature['period'];
				if($action == 'renew'){
					$ipv4_amount	= $service['ipv4_amount'];
				}else{
					$ipv4_amount	= $product_feature['ipv4'];
				}
				$price	= cm_money_format(($price+($ipv4_price*$ipv4_amount)));
			}
			if($product['ipv6_select'] == 1){	/** IPV6 */
				$ipv6_price = $product['ip_price_v6']*$product_feature['period'];
				if($action == 'renew'){
					$ipv6_amount = $service['ipv6_amount'];
				}else{
					$ipv6_amount = $product_feature['ipv6'];
				}
				$price = cm_money_format(($price+($ipv6_price*$ipv6_amount)));
			}
			/** Paket IP */

			if(isset($service) and array_key_exists('extra_price',$service)){	/** Uzatma işlemlerinde ek ücret */
				if(strstr($service['extra_price'],'-')){
					$price = cm_money_format(($price-str_replace("-","",$service['extra_price'])));
				}elseif(!strstr($service['extra_price'],'-') and $service['extra_price'] > 0){
					$price = cm_money_format(($price+$service['extra_price']));
				}
			}

			if($cm_config['default_currency'] != $currency){
				$PriceConvert	= cm_currency_convert($price,$currency,$cm_config['default_currency']);
				$price 			= $PriceConvert['price'];
				$currency		= $PriceConvert['currency'];
			}

			/** Paket İşletim Sistemi */
			if(array_key_exists('os_id',$product_feature) and $product_feature['os_id'] > 0){
				$os_price	= (float)0.00;
				if($action == 'renew'){
					$os_id = $service['os_id'];
				}else{
					$os_id = $product_feature['os_id'];
				}
				if($os_id > 0){
					$os = cm_os_list($os_id);
					$os = $os[$os_id];
					if($os['price'] > 0){
						$os['price'] = ($os['price']* $product_feature['period']);
						if($currency != $os['currency']){
							$PriceConvert	= cm_currency_convert($os['price'],$os['currency'],$currency);
							$os_price 		= $PriceConvert['price'];
						}else{
							$os_price 		= $os['price'];
						}
						$price = cm_money_format(($price+$os_price));
					}
				}
			}
			/** Paket İşletim Sistemi */

			/** Paket Panel */
			if(array_key_exists('panel_id',$product_feature) and $product_feature['panel_id'] > 0){
				$panel_price	= 0;
				if($action == 'renew'){
					$panel_id = $service['panel_id'];
				}else{
					$panel_id = $product_feature['panel_id'];
				}
				if($panel_id > 0){
					$panel = cm_panel_list($panel_id);
					$panel = $panel[$panel_id];
					if(in_array($product_type_id,array(14,19)) and $panel['panel_dedicated_price'] > 0){
						$panel_price = $panel['panel_dedicated_price'];
					}elseif($panel['panel_server_price'] > 0){
						$panel_price = $panel['panel_server_price'];
					}
					if($panel_price > 0){
						$price 			= cm_money_format(($price+($panel_price*$product_feature['period'])));
					}
				}
			}
			$status = true;
			/** Paket Panel */

		}else{
			$response = cm_lang('Hatalı veya eksik veri');
		}
	}
	if($price < 0){
		$status = false;
		$response = cm_lang('Fiyat hesaplanamadı');
	}
	if($status == true){
		if(array_key_exists('tax_status',$user)){
			$tax_status = $user['tax_status'];
			if($tax_status == 0){
				$taxrate = 0;
			}
			$country	= $user['country_id'];
		}
		/** Vergilendirme */
		if($price > 0 and $tax_status == 1 and $taxrate > 0 and cm_countrytax($country)){
			$price_discount = cm_money_format(($price-$discount));
			$tax = rate_calc($price_discount,$taxrate);
		}
		/** Vergilendirme */

		$response = array(
			'price'		=> cm_money_format($price),
			'currency'	=> $currency,
			'discount'	=> cm_money_format($discount),
			'tax'		=> cm_money_format($tax),
			'taxrate'	=> $taxrate,
		);
	}
	return array($status,$response);
}
function product_period($action,$product_type_id,$period = array()){
	$DomainControl	= array(3,5,6,7,8);
	if(in_array($product_type_id,$DomainControl)){

	}else{
		$write = cm_lang('Gün');
		if($period['time_type'] == 2){
			$write = cm_lang('Ay');
		}elseif($period['time_type'] == 3){
			$write = cm_lang('Yıl');
		}
		$return = $period['value'].' '.$write;
	}
	return $return;
}
function basket_list($user = array()){
	global $cm_db,$cm_config,$session_id,$ip,$DomainControl;
	$basket_list = array();
	if($user['user_id'] > 0){
		$where = "session_id='".$cm_db->sql_escape($session_id)."' OR user_id='".$cm_db->sql_escape($user['user_id'])."'";
	}else{
		$where = "session_id='".$cm_db->sql_escape($session_id)."' AND user_id='0'";
	}
	$basket_price 		= 0;
	$basket_total 		= 0;
	$basket_tax 		= 0;
	$basket_discount 	= 0;
	$basketQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."baskets WHERE ".$where." ORDER BY basket_id ASC");
	if($cm_db->sql_errno() == 0){
		if($cm_db->sql_num_rows($basketQuery) > 0){
			while($basket = $cm_db->sql_fetch_assoc($basketQuery)){
				$data = product_get_data($basket['product_type_id'],$basket['feature'],$basket['product_id']);
				list($price_status,$price_response) = product_price_calculation($basket['product_type_id'],$basket['feature'],$data,$user);
				if($price_status == true){
					$update = array(
						'price'		=> $price_response['price'],
						'discount'	=> $price_response['discount'],
						'currency'	=> $price_response['currency'],
						'tax'		=> $price_response['tax'],
						'taxrate'	=> $price_response['taxrate'],
					);
					if($update['price'] != $basket['price'] or $update['discount'] != $basket['discount'] or $update['tax'] != $basket['tax']){
						list($update_status,$update_response) = $cm_db->sql_query_update('baskets',$update,array('basket_id'=>$basket['basket_id']));
						if($update_status == false){
							echo 'Bir problem var destek ile iletişime geçin';
							exit;
						}else{
							$basket = array_merge($basket,$update);
						}
					}
				}else{
					echo 'Bir problem var destek ile iletişime geçin';
					exit;
				}
				if($basket['product_id'] > 0){
					$basket['product'] = $data['product'];
				}
				if($basket['feature'] != ""){
					$basket['feature'] = cm_json_decode($basket['feature']);
				}
				$tax				= ($basket['tax']*$basket['amount']);
				$discount			= ($basket['discount']*$basket['amount']);
				$price				= ($basket['price']*$basket['amount']);
				$basket_tax 		= cm_money_format($basket_tax+$tax);
				$basket_discount 	= cm_money_format($basket_discount+$discount);
				$basket_price 		= cm_money_format($basket_price+$price);
				$basket_total 		= cm_money_format(($basket_total+(($price-$discount)+$tax)));
				$basket_config		= array();
				if($basket['product_type_id'] == 4 and $basket['feature']['action'] != 'renew'){
					$basket_config[] = array(
						'name'	=> cm_lang('Alan Adı'),
						'value'	=> cm_htmlclear($basket['feature']['domain_name']),
					);
				}elseif(in_array($basket['product_type_id'],$DomainControl)){
					$basket_config[] = array(
						'name'	=> cm_lang('Alan Adı'),
						'value'	=> cm_htmlclear($basket['feature']['domain_name']),
					);
					$basket['domain_price_list'] = domainPrice($basket['product_type_id'],$basket['feature']['domain_name']);
				}
				if(isset($basket['feature']['action'])){
					if($basket['feature']['action'] == 'renew'){
						$basket['product_name'] = $basket['product_name'].' - '.cm_lang('Uzatma');
					}
				}

				$basket['basket_config'] = $basket_config;
				$basket_list[$basket['basket_id']] = $basket;

			}
		}
	}
	if(count($basket_list) > 0){
		$return = array(
			'basket'	=> $basket_list,
			'calc'		=> array(
				'price'		=> $basket_price,
				'discount'	=> $basket_discount,
				'tax'		=> $basket_tax,
				'total'		=> $basket_total,
			)
		);
		return $return;
	}
	return false;
}
function basket_add($product_type_id,$product_feature,$data = array(),$user = array()){
	global $cm_db,$cm_config,$session_id,$ip,$DomainControl;
	$status	= false;
	if(is_array($data) and count($data) > 0 and is_array($product_feature)){
		$now			= cm_time();
		$product_type	= $data['product_type'];
		$Where			= array();
		$Or				= array();
		$sessionQuery	= "session_id='".$cm_db->sql_escape($session_id)."'";
		if($user['user_id'] > 0){
			$Or[]		= "user_id='".$cm_db->sql_escape($user['user_id'])."'";
		}else{
			$sessionQuery .= " AND user_id='0'";
		}
		$Or[]			= $sessionQuery;
		$MultiProduct 	= 0;
		$product_id		= 0;
		if(in_array($product_type['product_group_name'],array('domain'))){
			$MultiProduct= 0;
			$productName= $product_type['product_list_name'];
			$Where[]	= "product_type_id IN (".implode(",",$DomainControl).")";
			$Where[]	= "feature LIKE '%".$cm_db->sql_escape($product_feature['domain_name'])."%'";
		}elseif(in_array($product_type['product_group_name'],array('credit'))){
			$MultiProduct= 0;
			$productName= $product_type['product_list_name'];
			$Where[]	= "product_type_id='".$cm_db->sql_escape($product_type['product_type_id'])."'";
		}else{
			$product	= $data['product'];
			$productName= $product['name'];
			$product_id	= $product['product_id'];
			$MultiProduct = $product['product_multi'];
			if($product_feature['action'] == 'renew'){
				$MultiProduct = 0;
				$Where[]	= "product_id='".$cm_db->sql_escape($product['product_id'])."'";
				$Where[]	= "product_type_id='".$cm_db->sql_escape($product_type['product_type_id'])."'";
				$control	= '"id":"'.$product_feature['id'].'"';
				$Where[]	= "feature LIKE '%".$cm_db->sql_escape($control)."%'";
			}else{
				$Where[]	= "product_id='".$cm_db->sql_escape($product['product_id'])."'";
				$Where[]	= "feature='".$cm_db->sql_escape(cm_json_encode($product_feature))."'";
			}
		}
		$Basket_Query = "SELECT * FROM ".$cm_config['db_prefix']."baskets WHERE ".implode(" AND ",$Where)." AND (".implode(" AND ",$Or).")";
		$Basket_Query = $cm_db->sql_query($Basket_Query);
		if($cm_db->sql_errno() == 0){
			$total_basket = $cm_db->sql_num_rows($Basket_Query);
			if($total_basket == 0 or ($total_basket > 0 and $MultiProduct == 0 and !in_array($product_feature['action'],array('renew')) and !in_array($product_type['product_group_name'],array('domain','credit')))){
				list($status,$response) = product_price_calculation($product_type_id,$product_feature,$data,$user);
				if($status == true){
					$add = array(
						'product_name'		=> $productName,
						'user_id'			=> $user['user_id'],
						'product_id'		=> $product_id,
						'product_type_id'	=> $product_type_id,
						'amount'			=> 1,
						'price'				=> $response['price'],
						'discount'			=> $response['discount'],
						'tax'				=> $response['tax'],
						'taxrate'			=> $response['taxrate'],
						'taxname'			=> "",
						'currency'			=> $response['currency'],
						'session_id'		=> $session_id,
						'ip'				=> $ip,
						'feature'			=> cm_json_encode($product_feature),
						'product_time'		=> 1,
						'time'				=> $now,
					);
					list($add_status,$add_response) = $cm_db->sql_insert_add('baskets',$add);
					if($add_status == true){
						$status 	= true;
						$response	= array(
							'message'	=> cm_lang('Ürün eklendi'),
							'basket_id'	=> $add_response,
						);
					}else{
						$status		= false;
						$response 	= cm_lang('Ürün sepetinize eklenemedi');
					}
				}
			}else{
				if($MultiProduct == 1){
					$Basket = $cm_db->sql_fetch_assoc($Basket_Query);
					$update = array(
						'amount'	=> ($Basket['amount']+1),
					);
					list($update_status,$update_response) = $cm_db->sql_query_update('baskets',$update,array('basket_id'=>$Basket['basket_id']));
					if($update_status == true){
						$status = true;
						$response = array(
							'message'	=> cm_lang('Ürün eklendi'),
							'basket_id'	=> $Basket['basket_id'],
						);
					}else{
						$status		= false;
						$response 	= cm_lang('Ürün adeti güncellenemedi');
					}
				}else{
					$status		= false;
					if(!in_array($product_type['product_group_name'],array('credit'))){
						$response 	= cm_lang('Bu ürün sepetinizde ekli');
					}else{
						$response 	= cm_lang('Sepetinizde bakiye ekleme mevcut yeni ekleme için silmeniz gerekir');
					}
				}
			}
		}else{
			$response = cm_lang('Sepet kontrol edilemedi');
		}
	}else{
		$response = cm_lang('Sepete eklenemedi hatalı veri');
	}
	return array($status,$response);
}
function basket_delete($id,$user = array()){
	global $cm_config,$cm_db,$session_id;
	$status = false;
	if($user['user_id'] > 0){
		$where	= "(session_id='".$cm_db->sql_escape($session_id)."' OR user_id='".$cm_db->sql_escape($user['user_id'])."')";
	}else{
		$where	= "session_id='".$cm_db->sql_escape($session_id)."' AND user_id='0'";
	}
	if(cm_numeric($id) and $id > 0){
		$where .= " AND basket_id='".$cm_db->sql_escape($id)."'";
	}
	$baskeQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."baskets WHERE ".$where);
	if($cm_db->sql_errno() == 0){
		if($cm_db->sql_num_rows($baskeQuery) > 0){
			$cm_db->sql_query("DELETE FROM ".$cm_config['db_prefix']."baskets WHERE ".$where);
			if($cm_db->sql_errno() == 0){
				$status		= true;
				$response	= array(
					'message'	=> cm_lang('Sepetinizdeki ürün silindi'),
				);
			}else{
				$response = cm_lang('Sepetinizdeki ürün silinemedi');
			}
		}else{
			$response = cm_lang('Sepetinizde böyle bir ürün bulunamadı');
		}
	}else{
		$response = cm_lang('Sepet kontrol edilemedi');
	}
	return array($status,$response);
}
function basket_period($id,$period,$user = array()){
	global $cm_config,$cm_db,$session_id,$DomainControl;
	$status	= false;
	$basket	= $cm_db->sql_assoc('baskets',array('basket_id'=>$id));
	if($basket){
		if($basket['session_id'] == $session_id or ($user['user_id'] > 0 and $basket['user_id'] == $user['user_id'])){
			if(!in_array($basket['product_type_id'],$DomainControl) and $basket['product_id'] > 0){
				$data = array();
				$data['product_type']	= productTypes($basket['product_type_id']);
				$data['product_type']	= $data['product_type'][$basket['product_type_id']];
				$data['product']		= product_list($basket['product_id']);
				if($data['product']["price_status"] == 2){
					$product_feature = cm_json_decode($basket['feature']);
					if($product_feature['action'] == 'renew' and array_key_exists('id',$product_feature)){
						$data['service'] = serviceGetDetails($product_feature['id'],$data['product_type']['product_group_name']);
					}
					$product_feature['period'] = (string)$period;
					list($price_status,$price_response) = product_price_calculation($basket['product_type_id'],$product_feature,$data,$user);
					if($price_status == true){
						$price_response['feature'] =  cm_json_encode($product_feature);
						list($update_status,$update_response) = $cm_db->sql_query_update('baskets',$price_response,array('basket_id'=>$basket['basket_id']));
						if($update_status == true){
							$status		= true;
							$response	= array(
								'message'	=> cm_lang('Sepet güncellendi'),
								'basket_id'	=> $basket['basket_id'],
							);
						}else{
							$response = cm_lang('Sepet güncellenemedi');
						}
					}else{
						$response = $price_response;
					}
				}else{
					$response = cm_lang('İşlem yapılamaz');
				}
			}else{
				$response = cm_lang('Ürün türü hatalı');
			}
		}else{
			$response = cm_lang('Sepetinizde ürün bulunamadı');
		}
	}else{
		$response = cm_lang('Sepetinizde ürün bulunamadı');
	}
	return array($status,$response);
}
function basket_domain_year($id,$year,$user = array()){
	global $cm_config,$cm_db,$session_id,$DomainControl;
	$status	= false;
	$basket	= $cm_db->sql_assoc('baskets',array('basket_id'=>$id));
	if($basket){
		if($basket['session_id'] == $session_id or ($user['user_id'] > 0 and $basket['user_id'] == $user['user_id'])){
			if(in_array($basket['product_type_id'],$DomainControl)){
				$product_feature = cm_json_decode($basket['feature']);
				$data = array();
				$data['product_type']	= productTypes($basket['product_type_id']);
				$data['product_type']	= $data['product_type'][$basket['product_type_id']];
				if($basket['product_type_id'] == 7 and array_key_exists('id',$product_feature)){
					$data['domains'] = $cm_db->sql_assoc('domains',array('domain_id'=>$product_feature['id']));
				}
				$data['domain'] = domainName($product_feature['domain_name']);
				$data['tld']	= domainTld($data['domain']["tld"]);
				$product_feature['year'] = (string)$year;
				list($price_status,$price_response) = product_price_calculation($basket['product_type_id'],$product_feature,$data,$user);
				if($price_status == true){
					$price_response['feature'] =  cm_json_encode($product_feature);
					list($update_status,$update_response) = $cm_db->sql_query_update('baskets',$price_response,array('basket_id'=>$basket['basket_id']));
					if($update_status == true){
						$status		= true;
						$response	= array(
							'message'	=> cm_lang('Sepet güncellendi'),
							'basket_id'	=> $basket['basket_id'],
						);
					}else{
						$response = cm_lang('Sepet güncellenemedi');
					}
				}else{
					$response = $price_response;
				}
			}else{
				$response = cm_lang('Alan adı bulunamadı');
			}
		}else{
			$response = cm_lang('Sepetinizde ürün bulunamadı');
		}
	}else{
		$response = cm_lang('Sepetinizde ürün bulunamadı');
	}
	return array($status,$response);
}
function basket_amount($id,$amount,$user = array()){
	global $cm_config,$cm_db,$session_id,$DomainControl;
	$status	= false;
	$basket	= $cm_db->sql_assoc('baskets',array('basket_id'=>$id));
	if($basket){
		if($basket['session_id'] == $session_id or ($user['user_id'] > 0 and $basket['user_id'] == $user['user_id'])){
			if(!in_array($basket['product_type_id'],$DomainControl) and $basket['product_id'] > 0){
				$product = product_list($basket['product_id']);
				if($product["price_status"] == 1 and $product['product_multi'] == 1){
					$update = array(
						'amount'	=> $amount,
					);
					list($up_status,$up_response) = $cm_db->sql_query_update('baskets',$update,array('basket_id'=>$basket['basket_id']));
					if($up_status == true){
						$status = true;
						$response = array(
							'basket_id'	=> $basket['basket_id'],
							'message'	=> cm_lang('Sepet güncellendi'),
						);
					}else{
						$response = cm_lang('Miktar güncellenemedi');
					}
				}else{
					$response = cm_lang('İşlem yapılamaz');
				}
			}else{
				$response = cm_lang('Ürün türü hatalı');
			}
		}else{
			$response = cm_lang('Sepetinizde ürün bulunamadı');
		}
	}else{
		$response = cm_lang('Sepetinizde ürün bulunamadı');
	}
	return array($status,$response);
}