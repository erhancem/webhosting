<?php
class cm_database{
    public $baglan = false;
    private $db_config;
    function __construct(){
		global $cm_config;
		$this->db_config = $cm_config;
		if((!isset($cm_config['db_host']) or isset($cm_config['db_host']) and $cm_config['db_host'] == "") or (!isset($cm_config['db_user']) or isset($cm_config['db_user']) and $cm_config['db_user'] == "") or (!isset($cm_config['db_name']) or isset($cm_config['db_name']) and $cm_config['db_name'] == "") or (!isset($cm_config['db_charset']) or isset($cm_config['db_charset']) and $cm_config['db_charset'] == "")){
			die(cm_lang('Lütfen config.php dosyasında veritabanı bilgilerinizi giriniz'));
		}
    }
    function Bagla(){
        $this->baglan = @mysqli_connect($this->db_config["db_host"],$this->db_config["db_user"],$this->db_config["db_password"],$this->db_config["db_name"]);
        if($this->baglan){
            return true;
        }else{
            return false;
        }
    }
    function baglanti(){
        if(!$this->baglan){
            if($this->Bagla()){
                $this->sql_set_charset($this->db_config['db_charset']);
            }else{
                echo 'Veritabanı bağlantısı kurulamadı. Hata Kodu: '.$this->sql_connect_errno()." Hata Mesajı: ".$this->sql_connect_error();
                exit;
            }
        }
    }
    private function VeriKontrol($veri = array(),$kontrol = array()){
	    $Gonder = array();
	    foreach($kontrol as $key=>$value){
	        if(array_key_exists($value,$veri)){
	            $Gonder[$value] = $veri[$value];
	        }
	    }
	    return $Gonder;
	}
	public function db_params_control($veri = array(),$kontrol = array()){
	    $Gonder = array();
	    foreach($kontrol as $key=>$value){
	        if(array_key_exists($value,$veri)){
	            $Gonder[$value] = $veri[$value];
	        }
	    }
	    return $Gonder;
	}
    function sql_set_charset($karakter){
        return mysqli_set_charset($this->baglan,$karakter);
    }
    function sql_query($sorgu,$show = 0){
    	//echo $sorgu."<br />";
    	//@preg_match_all("#'(.*?)'#si",$sorgu,$datas);
    	//print_r($datas);
    	//file_put_contents(cm_path."1-".date("d-m-Y"),$sorgu."\n",FILE_APPEND);
        $this->baglanti();
        if($show == 1){ echo $sorgu; }
        $query = @mysqli_query($this->baglan,$sorgu);
        if($this->sql_errno() != 0){
        	cm_systemlogDir(cm_lang('SQL sorgusunda hata oluştu sorgu: ##query## hata: ##error## hata kodu: ##errorcode##',array('query'=>$sorgu,'error'=>$this->sql_error(),'errorcode'=>$this->sql_errno())));
        }
        return $query;
    }
    function sql_fetch_assoc($sorgu){
        $this->baglanti();
        return mysqli_fetch_assoc($sorgu);
    }
    function sql_datas($query){
        $response = false;
        $control = $this->sql_query($query);
        if($this->sql_errno() == 0){
            $response = array();
            while($db = $this->sql_fetch_assoc($control)){
                $response[] = $db;
            }
            if(count($response) == 0){
                $response = false;
            }
        }
        return $response;
    }
    function sql_assoc($tablo,$where = array(),$select = array()){
    	global $cm_config;
        $this->baglanti();
        $Kontrol	= $this->sql_tablo_info($tablo);
        $where		= $this->VeriKontrol($where,$Kontrol);
        $wh = array();
        foreach($where as $key=>$value){
            if(!is_array($value)) {
                $value = (string)$value;
                $wh[] = $key . "='" . $this->sql_escape($value) . "'";
            }
        }
        $select_sql = '*';
        if(count($select) > 0){
            $select_sql = implode(",",$select);
        }
        if(count($wh) > 0){
	        $Query = $this->sql_query("SELECT ".$select_sql." FROM ".$cm_config['db_prefix'].$tablo." WHERE ".implode(" AND ",$wh));
	        if($this->sql_errno() == 0){
	        	if($this->sql_num_rows($Query) == 1){
	        		return $this->sql_fetch_assoc($Query);
	        	}
	        }
        }
        return false;
    }
    function sql_count($tablo,$where = array(),$or = array()){
    	global $cm_config;
        $this->baglanti();
        $Kontrol	= $this->sql_tablo_info($tablo);
        $where		= $this->VeriKontrol($where,$Kontrol);
        $wh = array();
        $CountName	= null;
        foreach($where as $key=>$value){
//        	if($value != "" or cm_numeric($value)){
        		$wh[] = $key."='".$this->sql_escape($value)."'";
        		$CountName = $key;
//        	}
        }
     	$or2 = array();
        if(count($or) > 0){
	        $or		= $this->VeriKontrol($or,$Kontrol);
	        foreach($or as $key=>$value){
//	        	if($value != ""){
	        		$or2[] = $key."='".$this->sql_escape($value)."'";
	        		$CountName = $key;
//	        	}
	        }
        }
        if(count($wh) > 0 and $CountName != null){
        	if(count($or2) == 0){
	        	$Query = $this->sql_query("SELECT COUNT(".$CountName.") as total FROM ".$cm_config['db_prefix'].$tablo." WHERE ".implode(" AND ",$wh));
	        }else{
	        	$Where1 = implode(" AND ",$wh);
	        	$Or1	= implode(" OR ",$or2);
	        	$add	= $Where1." AND (".$Or1.")";
	        	$Query = $this->sql_query("SELECT COUNT(".$CountName.") as total FROM ".$cm_config['db_prefix'].$tablo." WHERE ".$add);
	        }
	        if($this->sql_errno() == 0){
	        	$Total = $this->sql_fetch_assoc($Query);
        		return array(true,$Total['total']);
	        }
        }
        return array(false,0);
    }
    function sql_delete($tablo,$where = array()){
    	global $cm_config;
        $this->baglanti();
        $Kontrol	= $this->sql_tablo_info($tablo);
        $where		= $this->VeriKontrol($where,$Kontrol);
        $wh = array();
        foreach($where as $key=>$value){
//        	if($value != ""){
        		$wh[] = $key."='".$this->sql_escape($value)."'";
//        	}
        }
        if(count($wh) > 0){
	        $Query = $this->sql_query("DELETE FROM ".$cm_config['db_prefix'].$tablo." WHERE ".implode(" AND ",$wh));
	        if($this->sql_errno() == 0){
	      		return true;
	        }
        }
        return false;
    }
    function sql_fetch_array($sorgu,$liste = null){
        $this->baglanti();
        if($liste != ""){
            if(stristr($liste,"MYSQLI_NUM")){
                return mysqli_fetch_array($sorgu,MYSQLI_NUM);
            }elseif(stristr($liste,"MYSQLI_ASSOC")){
                return mysqli_fetch_array($sorgu,MYSQLI_ASSOC);
            }elseif(stristr($liste,"MYSQLI_BOTH")){
                return mysqli_fetch_array($sorgu,MYSQLI_BOTH);
            }else{
                return mysqli_fetch_array($sorgu,MYSQLI_BOTH);
            }
        }else{
            return mysqli_fetch_array($sorgu,MYSQLI_ASSOC);
        }
    }
    function sql_assoc_list($sorgu){
    	$this->baglanti();
    	$Durum = false;
    	$ListeX = $this->sql_query($sorgu);
    	if($this->sql_errno() == 0){
    		if($this->sql_num_rows($ListeX) == 1){
    			$Mesaj = $this->sql_fetch_assoc($ListeX);
    			$Durum = true;
    		}else{
    			$Mesaj = 'Kayıt bulunamadı';
    		}
    	}else{
    		$Mesaj = 'Geçici hata oldu '.$this->sql_error();
    	}
    	return array($Durum,$Mesaj);
    }

    function sql_table_list($query,$count = false){
        $response = null;
        $db_query = $this->sql_query($query);
        if($this->sql_errno() == 0) {
            if ($count) {
                $rows 		= $this->sql_fetch_assoc($db_query);
                $response 	= [
                    'count'	=> $rows["CM_count"],
                ];
            }
            if(!is_array($response)){
                $response = array();
                while($rows = $this->sql_fetch_assoc($db_query)){
                    $response[] = $rows;
                }
                if(count($response) == 0){
                    $response = cm_lang('Ekli kayıt bulunamadı');
                }
            }
        }else{
            $response = cm_lang('Liste çekilemedi veritabanı hatası oluştu');
        }
        return $response;
    }

    function sql_fetch_row($sorgu){
        $this->baglanti();
        return mysqli_fetch_row($sorgu);
    }
    function sql_fetch_object($sorgu){
        $this->baglanti();
        return mysqli_fetch_object($sorgu);
    }
    function sql_num_fields($sorgu){
        $this->baglanti();
        return mysqli_num_fields($sorgu);
    }

    function sql_num_rows($sorgu){
        $this->baglanti();
        return mysqli_num_rows($sorgu);
    }
    function sql_rows($sorgu){
        $this->baglanti();
        return $this->sql_num_rows($this->sql_query($sorgu));
    }
    function sql_sonid(){
        $this->baglanti();
        return mysqli_insert_id($this->baglan);
    }
    function sql_insert_id(){
        $this->baglanti();
        return mysqli_insert_id($this->baglan);
    }
    function sql_escape($veri){
    	if(is_array($veri)){
    		echo 'hata oluştu'; exit;
    	}
        $this->baglanti();
        return mysqli_real_escape_string($this->baglan,$veri);
    }
    function sql_info(){
        $this->baglanti();
        return mysqli_info($this->baglan);
    }
    function sql_free_result($sorgu){
        $this->baglanti();
        return mysqli_free_result($sorgu);
    }
    function sql_connect_errno(){
        return mysqli_connect_errno($this->baglan);
    }
    function sql_connect_error(){
        return mysqli_connect_error($this->baglan);
    }
    function sql_error(){
        $this->baglanti();
        return mysqli_error($this->baglan);
    }
    function sql_errno(){
        $this->baglanti();
        return mysqli_errno($this->baglan);
    }
    function sql_insert_add($tablo,$veri,$prefix = 1){
    	global $cm_config;
        if(is_array($veri) and $tablo != ""){
            $this->baglanti();
            $Kontrol    = $this->sql_tablo_info($tablo,false,$prefix);
            $veri       = $this->VeriKontrol($veri,$Kontrol);
            $Value  = array();
            foreach($veri as $anahtar=>$deger){
                if(!is_array($deger)) {
                    $deger = (string)$deger;
                    $Value["`".$anahtar."`"] = "'" . $this->sql_escape($deger) . "'";
                }
            }
            //echo "INSERT INTO ".($prefix==1?$cm_config['db_prefix']:null).$tablo." (".implode(",",array_keys($Value)).") VALUES (".implode(",",$Value).")"; exit;
            $this->sql_query("INSERT INTO ".($prefix==1?$cm_config['db_prefix']:null).$tablo." (".implode(",",array_keys($Value)).") VALUES (".implode(",",$Value).")");
            if($this->sql_errno() == 0){
                return array(true,$this->sql_insert_id());
            }else{
                return array(false,$this->sql_error());
            }
        }else{
            return array(false,'Gelen veriler hatalı');
        }
    }
    public function sql_query_update($tablo,$veriler = array(),$kosul,$prefix = 1){
    	global $cm_config;
        if(is_array($veriler) and $kosul){
      		$Kontrol = $this->sql_tablo_info($tablo,false,$prefix);
        	if(is_array($kosul)){
        		$ksl = array();
            	$kosul = $this->VeriKontrol($kosul,$Kontrol);
        		foreach($kosul as $key=>$value){
        			$value		= (string)$value;
        			$ksl[] = $key."='".$this->sql_escape($value)."'";
        		}
        		$kosul = implode(" AND ",$ksl);
        	}
            $veriler = $this->VeriKontrol($veriler,$Kontrol);
            $Value  = array();
            foreach($veriler as $anahtar=>$deger){
                if(!is_array($deger)) {
                    $deger = (string)$deger;
                    $Value[] = "`".$anahtar."`" . "='" . $this->sql_escape($deger) . "'";
                }
            }
            $this->sql_query("UPDATE ".($prefix==1?$cm_config['db_prefix']:null).$tablo." SET ".implode(",",$Value)." WHERE ".$kosul);
	        if($this->sql_errno() == 0){
	            return array(true,'Güncellendi');
	        }else{
	            return array(false,$this->sql_error());
	        }
        }else{
            return array(false,'Hatalı veri');
        }
    }
    public function sql_tablo_info($tablo,$full = false,$prefix = 1){
    	global $cm_config;
    	$this->baglanti();
        $kayit_tablo_sorgu = $this->sql_query("SHOW COLUMNS FROM `".($prefix==1?$cm_config['db_prefix']:null).$tablo."`");
        $AdamaLis	= array();
        while($kayit_tablo_bilgix = $this->sql_fetch_assoc($kayit_tablo_sorgu)){
            $AdamaLis[] = ($full==false?$kayit_tablo_bilgix['Field']:$kayit_tablo_bilgix);
        }
        return $AdamaLis;
    }
    function Close(){
        if($this->baglan){ mysqli_close($this->baglan); }
    }

    function sql_file_upload($filename) {
        $status     = false;
        $response   = '';
        if($filename != "" and file_exists($filename)){
            $this->baglanti();

            $status     = true;
            $templine   = '';
            $lines      = file($filename);
            foreach ($lines as $line){
                if (substr($line, 0, 2) == '--' || $line == '') { continue; }
                $templine .= $line;
                if (substr(trim($line), -1, 1) == ';'){
                    $this->sql_query($templine);
                    if($this->sql_errno() != 0){
                        $status = false;
                        break;
                    }
                    $templine = '';
                }
            }
            if($status == true){
                $response = cm_lang('Yedek yüklendi');
            }else{
                $response = cm_lang('Yedek yüklenirken hata oluştu');
            }

        }else{
            $response = cm_lang('.sql Dosya yolu bulunamadı');
        }
        return array($status,$response);
    }

    function __destruct(){
        $this->Close();
    }
}
$cm_db = new cm_database();