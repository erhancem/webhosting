<?php
function rate_calc($price,$taxrate){
	$rate		= "0.".(strlen($taxrate)==1?"0".$taxrate:$taxrate);
	$tax		= cm_money_format(($price*$rate));
	return $tax;
}
function createInvoice($order,$payment_commission = null){
	global $cm_config,$cm_db;
	if(!is_array($order)){
		$order = $cm_db->sql_assoc('orders',array('order_id'=>$order));
	}
	list($status,$response) = $cm_db->sql_count('invoices',array('order_id'=>$order['order_id']));
	if($status == true){
		$status = false;
		if($response == 0){
			$paid = 0.00;
			$add = array(
				'user_id'	=> $order['user_id'],
				'order_id'	=> $order['order_id'],
				'module_id'	=> $order['module_id'],
				'price'		=> $order['order_total'],
				'discount'	=> $order['order_discount'],
				'tax'		=> $order['order_tax'],
				'time'		=> cm_time(),
				'status'	=> 1,
			);
			$paid = cm_money_format((($order['order_total']-$order['order_discount'])+$order['order_tax']));
			if(is_array($payment_commission) and count($payment_commission) > 0){
				$add['payment_commission'] 	= $payment_commission['price'];
				$add['payment_tax'] 		= $payment_commission['tax'];
				$paid = cm_money_format(($paid+($add['payment_commission']+$add['payment_tax'])));
			}
			$add['paid'] = $paid;
			list($invoices_status,$invoices_id) = $cm_db->sql_insert_add('invoices',$add);
			if($invoices_status == true){
				$status = true;
				$response = array(
					'invoice_id'	=> $invoices_id,
					'message'		=> cm_lang('Fatura eklendi'),
					'paid'			=> $paid,
				);
			}else{
				$response = cm_lang('Fatura oluşturulamadı');
			}
		}else{
			$response = cm_lang('Sipariş için zaten fatura kesilmiş');
		}
	}else{
		$response = cm_lang('Fatura kontrol yapılamadı');
	}
	return array($status,$response);
}