<?php
if(!isset($admindir)){
	include("settings.php");
}
$PageParams = array();
$p		= cm_get_request('page');
$page	= 1;
if($p and cm_numeric($p) and $p > 0){
	$page = $p;
}
$filtre = array(
	1 => array(
			"name" 	=> "Kullanıcı Adına Göre",
			"query"	=> 'u.username'
		),
	2 => array(
			"name" 	=> "Eposta Hesabına Göre",
			"query"	=> 'u.email_address'
		),
	3 => array(
			"name" 	=> "Destek Başlığına Göre",
			"query"	=> 't.ticket_subject'
		),
	4 => array(
			"name" 	=> "Ad Soyada Göre",
			"query"	=> "CONCAT(u.first_name,' ',u.last_name)"
		),
	5 => array(
			"name" 	=> "Destek ID Göre",
			"query"	=> "t.ticket_id"
		)
);
$OrderBy 	= 't.ticket_end_time DESC';
$odr 		= cm_get_request('order');
if($odr and cm_numeric($odr)){
	$Sira = array(
		1 => "t.ticket_id DESC",
		2 => "t.ticket_id ASC",
		3 => "t.ticket_start_time DESC",
		4 => "t.ticket_start_time ASC",
		5 => "t.ticket_end_time DESC",
		6 => "t.ticket_end_time ASC",
	);
	if(isset($Sira[$odr])){
		$OrderBy = $Sira[$odr];
		$PageParams["order"] = $odr;
	}
}
$Where		= '';
$WhereAry	= array();
$status		= cm_get_request('status');
if(cm_numeric($status) and in_array($status,array_keys($ticket_status))){
	$WhereAry[] = "t.ticket_status='".$cm_db->sql_escape($status)."' AND t.ticket_delete=0";
	$PageParams["status"] = $status;
}
$ticketid = cm_get_request('id');
if(cm_numeric($ticketid) and $ticketid > 0){
	$WhereAry[] = "t.ticket_id='".$cm_db->sql_escape($ticketid)."'";
	$PageParams["id"] = $ticketid;
}
$user_id = cm_get_request('user_id');
if(cm_numeric($user_id) and $user_id > 0){
	$WhereAry[] = "t.user_id='".$cm_db->sql_escape($user_id)."'";
	$PageParams["user_id"] = $user_id;
}
$dep_id = cm_get_request('dep_id');
if(cm_numeric($dep_id) and $dep_id > 0){
	$WhereAry[] = "t.ticket_dep_id='".$cm_db->sql_escape($dep_id)."'";
	$PageParams["dep_id"] = $dep_id;
}
$search			= cm_get_request('search');
if($search and $search != ""){
	if(!cm_get_request('search_type')){
	    $SQLLike = array(
	        "t.ticket_subject like '%".$cm_db->sql_escape($search)."%'",
	        "u.email_address like '%".$cm_db->sql_escape($search)."%'",
	        "u.username like '%".$cm_db->sql_escape($search)."%'",
	        "CONCAT(u.first_name,' ',u.last_name) like '%".$cm_db->sql_escape($search)."%'"
	    );
	    $WhereAry[] = implode(" OR ",$SQLLike);
	    $PageParams["search"] = $search;
	}elseif(cm_get_request('search_type') and cm_get_request("symbol")){
		if(isset($filtre[$_REQUEST["search_type"]])){
			if(isset($Isaret[$_REQUEST["symbol"]])){
				$WhereAry[] = str_ireplace(array("##name##","##value##"),array($filtre[$_REQUEST["search_type"]]["query"],$cm_db->sql_escape($search)),$Isaret[$_REQUEST["symbol"]]["query"]);
			}
		}
	}
}
if(count($WhereAry) > 0){
	$Where = " WHERE ".implode(" AND ",$WhereAry);
}
$orderUrl = $PageParams;
if(array_key_exists('order',$orderUrl)){
	unset($orderUrl['order']);
}
if(count($orderUrl) > 0){
	$orderUrl = $admindir.'/ticket_list.php?'.http_build_query($orderUrl).'&';
}else{
	$orderUrl = $admindir.'/ticket_list.php?';
}
include("navtop.php");
?>
<div class="main">
    <div class="main-content width100">
        <div class="row">
            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading"><?=cm_lang('Destek Talepleri')?></div>
              <div class="panel-body">
    <div class="col-md-12" style="margin-bottom: 15px;">
    <div class="col-md-6"><div class="pull-left" style="padding: 10px;"></div></div>
    <form action="" method="GET" class="" role="search">
	<div class="col-md-6">
        <div id="custom-search-input">
            <div class="input-group  pull-right">
                <div class="form-group">
                    <div style="float: left;"><input type="text" class="form-control" style="float: left;" name="search" value="<?=(cm_get_request('search')?cm_htmlclear(cm_get_request('search')):null)?>" placeholder="<?=cm_lang('Arama Yap')?>..."/></div>
                    <div style="float: left;">
                        <button class="btn btn-danger" style="border-radius: 0px 5px 5px 0;" type="submit">
                            <span class="fa fa-search"></span>
                        </button>
                    </div>
                	<div style="float: left;margin-left: 5px;"><a href="javascript:;" class="btn btn-info" style="float: left;" id="advancedClose"> <?=cm_lang('Gelişmiş')?></a></div>
                </div>
            </div>
        </div>
    </div>
	<div class="clearfix"></div>
	<div class="col-md-12" id="advancedOpen" style="display: none;">
	    <div class="panel panel-yellow">
	        <div class="panel-heading"><?=cm_lang('GELİŞMİŞ ARAMA')?></div>
             <div class="panel-body OtoScroll">
             	<div class="col-md-1"></div>
                 <div class="col-md-4">
                    <div class="row">
                    <select class="form-control search_type">
                        <option value=""><?=cm_lang('Arama Türünü Seçin')?></option>
                        <? foreach($filtre as $key=>$value){ ?>
                        	<option value="<?=$key?>"><?=cm_lang($value["name"])?></option>
                        <? } ?>
                    </select>
                    </div>
                 </div>
                 <div class="col-md-1"></div>
                 <div class="col-md-4">
                    <div class="row">
                    <select class="form-control symbol">
                        <? foreach($Isaret as $key=>$value){ ?>
                        	<option value="<?=$key?>"><?=$value["name"]?></option>
                        <? } ?>
                    </select>
                    </div>
                 </div>
             </div>
	    </div>
	</div>
	</form>
	<div class="clearfix"></div>
</div>
                <table class="table table-bordered table-hover results">
                    <thead>
                      <tr>
                        <th><?=cm_lang('ID')?> <a href="<?=$orderUrl?>order=<?=$odr==1?'2':'1'?>" style="color: white;"><i class="fa fa-fw fa-sort"></i></a></th>
                        <th><?=cm_lang('Üye')?></th>
                        <th><?=cm_lang('Konu')?></th>
                        <th><?=cm_lang('Departman')?></th>
                        <th><?=cm_lang('Tarih')?> <a href="<?=$orderUrl?>order=<?=$odr==3?'4':'3'?>" style="color: white;"><i class="fa fa-fw fa-sort"></i></a></th>
                        <th><?=cm_lang('Son Cevap')?> <a href="<?=$orderUrl?>order=<?=$odr==5?'6':'5'?>" style="color: white;"><i class="fa fa-fw fa-sort"></i></a></th>
                        <th><?=cm_lang('Durumu')?></th>
                        <th><?=cm_lang('İşlem')?></th>
                      </tr>
                    </thead>
                    <tbody>
<?php
$TotalPage = 0;
if($Where == ""){
	$queryTotal = "SHOW TABLE STATUS FROM `".$cm_config['db_name']."` WHERE Name = '".$cm_config["db_prefix"]."tickets';";
}else{
    $queryTotal	= "SELECT COUNT(t.ticket_id) as Rows from ".$cm_config["db_prefix"]."tickets AS t LEFT JOIN ".$cm_config["db_prefix"]."users AS u ON t.user_id=u.user_id".$Where;
}
$TotalList = $cm_db->sql_query($queryTotal);
if($cm_db->sql_errno() == 0){
	$TotalRows = $cm_db->sql_fetch_assoc($TotalList);
    $page = ($page>$TotalRows["Rows"]?1:$page); //Sayfa kontrolü
	if($TotalRows["Rows"] > 0){
		$ListStart		= (($page-1)*$ListTotalRows+1)-1;	//sql alınacak kayıt
		$x				= 10;							//aktif sayfadan önceki/sonraki sayfa gösterim sayısı
		$next_page		= $page + 1;					//sonraki sayfa
		$previous_page	= $page - 1;					//önceki sayfa
		$TotalPage 		= ceil($TotalRows["Rows"]/$ListTotalRows);
    	$ListQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config["db_prefix"]."tickets AS t LEFT JOIN ".$cm_config["db_prefix"]."ticket_departments AS td ON t.ticket_dep_id=td.ticket_dep_id LEFT JOIN ".$cm_config["db_prefix"]."users AS u ON t.user_id=u.user_id".$Where." ORDER BY ".$OrderBy." LIMIT ".$ListStart.",".$ListTotalRows);
    	if($cm_db->sql_errno() == 0){
        	while($List = $cm_db->sql_fetch_assoc($ListQuery)){
        		$usr 		= '<a href="'.$admindir.'/members.php?id='.$List['user_id'].'">'.cm_htmlclear($List['first_name'].' '.$List['last_name']).'<br />'.cm_htmlclear($List['username']).'</a>';
        		$start_time	= cm_date(null,$List['ticket_start_time']);
        		//$EndTime = $cm_db->sql_fetch_assoc($cm_db->sql_query("SELECT ticket_answer_time FROM ".$cm_config['db_prefix']."ticket_answer WHERE ticket_id='".$List['ticket_id']."' AND ticket_answer_delete=0 ORDER BY ticket_answer_time DESC LIMIT 1"));
        		$end_time	= cm_date(null,$List['ticket_end_time']);
        		$list_status = '<span style="color: '.$ticket_status[$List['ticket_status']]['color'].';"><strong>'.cm_lang($ticket_status[$List['ticket_status']]['name']).'</strong></span>';
        		if($List['ticket_delete'] == 1){
        			$list_status = '<span style="color: '.$delete_status[$List['ticket_delete']]['color'].';"><strong>'.cm_lang($delete_status[$List['ticket_delete']]['name']).'</strong></span>';
        		}
?>
                      <tr>
                        <td><?=$List['ticket_id']?></td>
                        <td><?=$usr?></td>
                        <td><?=cm_htmlclear($List['ticket_subject'])?></td>
                        <td><a href="<?=$admindir?>/ticket_list.php?dep_id=<?=$List['ticket_dep_id']?>"><?=cm_htmlclear($List['ticket_dep_name'])?></a></td>
                        <td><?=$start_time?></td>
                        <td><?=$end_time?></td>
                        <td><?=$list_status?></td>
                        <td><a href="<?=$admindir?>/ticket.php?id=<?=$List['ticket_id']?>" class="btn btn-sm btn-primary" title="<?=cm_lang('Mesaj')?>" alt="Hata Bildir" data-toggle="tooltip" title="<?=cm_lang('Mesaj')?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                      </tr>
<?php }
}else{
	$errorMessage = cm_lang('Hata')." ".$cm_db->sql_error();
}
}else{
	$errorMessage = cm_lang('Kayıt bulunamadı');
}
}else{
	$errorMessage = cm_lang('hata')." : ".$cm_db->sql_error();
}
if(isset($errorMessage)){
?>
						<tr>
							<td colspan="8" style="text-align: center;font-weight: bold;"><?=$errorMessage?></td>
						</tr>
<?php } ?>

                    </tbody>
                </table>
                <?php
                echo PageList(array("TotalPage"=>$TotalPage,"Page"=>$page,"parametre"=>http_build_query($PageParams)));
                ?>
              </div>
        </div>
    </div>
	</div>
</div>
<?php include("footer.php");?>
<script>
$(document).ready(function(){
    $("#advancedClose").click(function(){
        $("#advancedOpen").slideToggle(500);
    });
});
$('.search_type').change(function(){
	if(this.value != ''){
		$('.search_type').attr("name","search_type");
		$('.symbol').attr("name","symbol");
	}else{
		$('.search_type').removeAttr("name");
		$('.symbol').removeAttr("name");
	}
});
</script>