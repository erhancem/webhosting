<?php
if(!isset($admindir)){
	include("settings.php");
}

include("navtop.php");
?>
<div class="main">
    <div class="main-content width100">
        <div class="row">
            <div class="panel panel-default">
              <div class="panel-heading">ÜRÜN EKLE</div>
              <div class="panel-body">
                <div class="products_add">
                    <form action="" method="">
                        <!-- Urun ekeleme -->
                        <h3>Adım 1</h3>
                        <table class="form" width="100%" border="0" cellspacing="2" cellpadding="3">
                            <tbody>
                                <tr>
                                    <td width="150">Ürün Paketi</td>
                                    <td>
                                        <div class="form-group">
                                            <select name="deptid" class="form-control input-500">
                                                <option value="1" selected="selected">Hosting / Reseller Destek</option>
                                                <option value="2">Linux Hosting</option>
                                                <option value="3">Windows Hosting</option>
                                                <option value="4">Sunucu Destek</option>
                                                <option value="5">Muhasebe</option>
                                            </select>
                                          </div>
                                    </td>
                                </tr>
                            <tr>
                                <td>Ürün Adı</td>
                                <td><input type="text" class="form-control input-500" name="productname" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><input type="submit" class="btn btn-products" value="Ürün Ekle" /></td>
                            </tr>
                            </tbody>
                        </table>
                        <!-- // Urun ekleme bitti -->
                        <!-- Sonraki Sayfa -->
                        <h3>Adım 2</h3>
                        <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#tab1" aria-expanded="true">Detaylar</a></li>
                          <li class=""><a data-toggle="tab" href="#tab2" aria-expanded="false">Fiyatlandırma</a></li>
                          <li class=""><a data-toggle="tab" href="#tab3" aria-expanded="false">Modül Ayarları</a></li>
                          <li class=""><a data-toggle="tab" href="#tab4" aria-expanded="false">Özel Alanlar</a></li>
                          <li class=""><a data-toggle="tab" href="#tab5" aria-expanded="false">Yapılandırılabilir Şeçeneler</a></li>
                          <li class=""><a data-toggle="tab" href="#tab6" aria-expanded="false">Yükseltme</a></li>
                          <li class=""><a data-toggle="tab" href="#tab7" aria-expanded="false">Ücretsiz Domain</a></li>
                          <li class=""><a data-toggle="tab" href="#tab8" aria-expanded="false">Diğer</a></li>
                          <li class=""><a data-toggle="tab" href="#tab9" aria-expanded="false">Linkler</a></li>
                        </ul>
                        <div class="tab-content">
                            <div id="tab1" class="tab-pane fade active in">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="form" width="100%" border="0" cellspacing="2" cellpadding="3">
                                            <tbody>
                                                <tr>
                                                    <td width="150">Ürün Paketi</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="deptid" class="form-control input-500">
                                                                <option value="1" selected="selected">Hosting / Reseller Destek</option>
                                                                <option value="2">Linux Hosting</option>
                                                                <option value="3">Windows Hosting</option>
                                                                <option value="4">Sunucu Destek</option>
                                                                <option value="5">Muhasebe</option>
                                                            </select>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Ürün Adı</td>
                                                    <td><input type="text" class="form-control input-500" name="productname" /></td>
                                                </tr>
                                                <tr>
                                                    <td>Ürün Açıklaması</td>
                                                    <td><textarea rows="5" class="form-control input-500" name="productname"></textarea>
                                                    <span>Bu alanda HTML kullanabilirsiniz.</span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">Hoşgeldin Maili</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="deptid" class="form-control input-500">
                                                                <option value="1" selected="selected">Hiçbiri</option>
                                                                <option value="2">Hosting Hesap Hoşgeldin</option>
                                                                <option value="3">Reseller Hesap Hoşgeldin</option>
                                                                <option value="4">Sunucu Hesap Hoşgeldin</option>
                                                                <option value="5">Diğer</option>
                                                            </select>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">Domain Gerektir</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label><input type="checkbox" value="" /> Domain kayıt seçeneklerini göstermek için işaretleyin.</label></div>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><input type="submit" class="btn btn-products" value="Ürün Ekle" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="tab2" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="protab table table-bordered" width="100%" border="0" cellspacing="2" cellpadding="3">
                                            <tbody>
                                                <tr>
                                                    <td width="150">Ödeme Türü</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="radio">
                                                                <label><input type="radio" name="payment" value="0" />Ücretsiz </label>
                                                                <label><input type="radio" name="payment" value="1" />Tek Seferlik </label>
                                                                <label><input type="radio" name="payment" value="2" />Yıllık </label>
                                                            </div>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">Çoklu Miktarlara izin ver</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label><input type="checkbox" value="" /> Sipariş verirken aynı üründen birden fazla siparişe izin vermek için işaretleyin (ayrı olarak yapılandırma ayarı gerektirmemektedir)</label></div>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Yinelenen Döngüleri Sınırlandır</td>
                                                    <td>
                                                        <div class="form-group" style="line-height: 30px;">
                                                            <input class="form-control input-80" name="productname" value="0" type="number" min="1" style="float: left;margin-right: 10px;" /> kez ancak sabit sayıda nüksetme bu ürünü sınırlamak için fatura kez toplam sayısını girin (0 = Sınırsız
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Otomatik Durdurma (Silme)/Sabit Dönem</td>
                                                    <td>
                                                        <div class="form-group" style="line-height: 30px;">
                                                            <input class="form-control input-80" name="productname" value="0" type="number" min="1" style="float: left;margin-right: 10px;" /> Aktivasyondan sonra otomatik durdurma (silme) için gün sayısını giriniz (örneğin ücretsiz denemeler, zaman sınırlı ürünler, vs...)
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">Hoşgeldin Maili</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <select name="deptid" class="form-control input-500">
                                                                <option value="1" selected="selected">Hiçbiri</option>
                                                                <option value="2">Hosting Hesap Hoşgeldin</option>
                                                                <option value="3">Reseller Hesap Hoşgeldin</option>
                                                                <option value="4">Sunucu Hesap Hoşgeldin</option>
                                                                <option value="5">Diğer</option>
                                                            </select>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td width="150">Domain Gerektir</td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="checkbox"><label><input type="checkbox" value="" />Domain kayıt seçeneklerini göstermek için işaretleyin.</label></div>
                                                          </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><input type="submit" class="btn btn-products" value="Ürün Ekle" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- // Sonraki Sayfa Bitti -->
                    </form>
                </div>
              </div>
        </div>
    </div>
</div>
</div>
<?php include("footer.php");?>