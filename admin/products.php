<?php
if(!isset($admindir)){
	include("settings.php");
}

include("navtop.php");
?>
<div class="main">
    <div class="main-content width100">
        <div class="row">
            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading"><?=cm_lang('ÜRÜNLER')?></div>
              <div class="panel-body">
                <div class="pull-left">
                    <a href="products_add.php" class="btn btn-products"><i class="fa fa-plus" aria-hidden="true"></i> Ürün Ekle</a> <a href="#" class="btn btn-copy"><i class="fa fa-clone" aria-hidden="true"></i> Ürün Kopyala</a>
                </div>
                <div class="form-group pull-right">
                    <input type="text" class="search form-control" placeholder="Aranacak kelime" />
                </div>
                <div class="clearfix"></div>
                <table class="table table-bordered table-hover results">
                    <thead>
                      <tr>
                        <th>Ürün Adı</th>
                        <th>Türü</th>
                        <th>Ödeme</th>
                        <th>Otomatik Kurulum</th>
                        <th>İşlem</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>Linux Bireysel kalşsjdklşasjdasjklasjlkasjdlaks</td>
                        <td>Hosting (cPanel)</td>
                        <td>3D Ödeme</td>
                        <td>Yıllık Yenileme</td>
                        <td><a href="#" class="btn btn-sm btn-primary" title="Düzenle" alt="Hata Bildir" data-toggle="tooltip" title="Düzenle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-original-title="Çöpe Gönder"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                      </tr>
                      <tr>
                        <td>Windows Standart</td>
                        <td>Hosting (Plesk Panel)</td>
                        <td>Mobil Ödeme</td>
                        <td>Yıllık Yenileme</td>
                        <td><a href="#" class="btn btn-sm btn-primary" title="Düzenle" alt="Hata Bildir" data-toggle="tooltip" title="Düzenle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-original-title="Çöpe Gönder"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                      </tr>
                      <tr>
                        <td>Linux Bireysel</td>
                        <td>Hosting (cPanel)</td>
                        <td>3D Ödeme</td>
                        <td>Yıllık Yenileme</td>
                        <td><a href="#" class="btn btn-sm btn-primary" title="Düzenle" alt="Hata Bildir" data-toggle="tooltip" title="Düzenle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> <a href="#" class="btn btn-sm btn-danger" data-toggle="tooltip" data-original-title="Çöpe Gönder"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                      </tr>
                    </tbody>
                </table>
              </div>
        </div>
    </div>
	</div>
</div>
<?php include("footer.php");?>