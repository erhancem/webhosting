<?php
include("settings.php");
if(cm_get_request('login')){
	$username = cm_get_request('username');
	$password = cm_get_request('password');
	if($username and is_string($username)){
		$user_login = $cm_db->sql_assoc('users',array('username'=>$username));
		if($user_login){
			if(cm_user_password($password) == $user_login['password']){
				if($user_login['rank_id'] >= 10){
					$_SESSION['admin_id'] = $user_login['user_id'];
					cm_url_go($admindir.'/index.php');
				}else{
					$message = cm_lang('Bu alana giriş izniniz yok');
				}
			}else{
				$message = cm_lang('Şifreniz hatalı');
			}
		}else{
			$message = cm_lang('Böyle bir kullanıcı bulunamadı');
		}
	}else{
		$message = cm_lang('Lütfen kullanıcı adı girin');
	}
}
?>
<!DOCTYPE html>
<html lang="tr-TR" xml:lang="tr-TR" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>CemBilisim.Com - Yönetim Paneli</title>
    <meta name="content-language" content="tr" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="<?=$admindir?>/css/login.css"/>
	<link rel="stylesheet" href="<?=$admindir?>/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?=$admindir?>/css/bootstrap.min.css"/>
</head>
<body class="login-page">
<div class="GirisLogo">
    <div class="GirisIcerik">
        <img src="<?=$admindir?>/images/cem_logo.png" />
    </div>
    <h1><?=cm_lang('Yönetim Paneli')?></h1>
</div>
<div class="GirisForm">
    <div class="container">
        <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
            <div class="panel panel-info" >
                <div class="panel-heading">
                    <div class="panel-title" style="text-align: center;font-size: 24px;"><?=cm_lang('GİRİŞ PANELİ')?></div>
                </div>
                <div style="padding-top:30px" class="panel-body">
<?php if(isset($message)){ ?>
                    <div class="text-center text-hata"><?=cm_lang('HATA')?>: <?=$message?></div>
<?php } ?>
                    <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>
                    <form id="loginform" class="form-horizontal" role="form" action="" method="POST">
                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
                            <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="<?=cm_lang('Kullanıcı Adı')?>" required="" />
                        </div>
                        <div style="margin-bottom: 25px" class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock" aria-hidden="true"></i></span>
                            <input id="login-password" type="password" class="form-control" name="password" placeholder="<?=cm_lang('Parola')?>" required="" />
                        </div>
                        <input type="submit" name="login" class="btn" value="<?=cm_lang('Giriş Yap')?>" style="color: #000;"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>