<?php
if(!isset($admindir)){ include("settings.php"); }

$mod            = (cm_get_request('mod')?strtolower(cm_get_request('mod')):'none');
$module_name    = (cm_get_request('module_name')?strtolower(cm_get_request('module_name')):'none');
$module_func    = (cm_get_request('module_func')?strtolower(cm_get_request('module_func')):false);
$module         = false;
$module_status  = false;
$module_assoc   = false;
$module_step    = 0;
$module_message = null;
$module_response= null;
$func_message   = false;

if($mod != 'none' and $module_name != 'none') {

    $module_sql = $cm_db->sql_query("SELECT * FROM " . $cm_config['db_prefix'] . "modules WHERE module_admin = 1 and module_dir = '" . $cm_db->sql_escape($mod) . "' and `module` = '" . $cm_db->sql_escape($module_name) . "'");
    if($cm_db->sql_errno() == 0){
        if($cm_db->sql_num_rows($module_sql) == 1){
            $module_assoc   = $cm_db->sql_fetch_assoc($module_sql);
            $module_status  = true;
        }
    }

    if($module_status == true){
        $module             = $module_assoc['module_name'];
        $module_step        = 1;
        $module_variable    = $module_assoc['module'].'_'.$module_assoc['module_dir'];
        $module_dir         = $cm_dirlist['module'].$module_assoc['module_dir'].'/'.$module_assoc['module'].'/';
        $module_fileconfig  = $cm_dirlist['module'].$module_assoc['module_dir'].'/'.$module_assoc['module'].'/'.$module_assoc['module'].'.config.php';
        $module_file        = $cm_dirlist['module'].$module_assoc['module_dir'].'/'.$module_assoc['module'].'/'.$module_assoc['module'].'.php';

        if($module_func){

        }

        list($module_status,$module_response) = ModuleCallFunction($module_assoc['module_id'],'adminpanel');
        if($module_status){
            if(isset($module_response['variable'])){
                extract($module_response['variable'], EXTR_PREFIX_SAME, $module_assoc['module']);
            }
            if(!is_array($module_response)){
                $module_message = $module_response;
            }elseif(array_key_exists("theme",$module_response)){
                $module_theme	= $module_dir.$module_response['theme'];
                if(file_exists($module_theme)){
                    ob_start();
                    include($module_theme);
                    $module_message = ob_get_contents();
                    ob_end_clean();
                }else{
                    $module_message = cm_lang('Modülün html kaynağı çekilemedi');
                }
            }elseif(array_key_exists("html",$module_response)){
                $module_message	= $module_response['html'];
            }else{
                $module_message = cm_lang('Modül html çıktı gelmedi');
            }
        }else{
            $module_message = (!is_array($module_response)?$module_response:(cm_lang('Modül Hata').' : '.cm_lang('Modül çıktısı okunamadı')));
        }
    }

}
include("navtop.php");
?>
    <div class="main">
        <div class="main-content width100">
            <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading"><?=($module?(cm_lang('Modül').' : '.cm_htmlclear($module)):cm_lang('Eklenti Yönetimi'))?></div>
                    <div class="panel-body">

                        <?php if($module_step != 0){ ?>

                            <?php if($func_message){ ?>
                                <div class="col-md-12">
                                    <p><?=$func_message?></p>
                                </div>
                            <?php } ?>

                            <?php if($module_status){ ?>

                                <?=$module_message?>

                            <?php }else{ ?>

                                <div class="col-md-12">
                                    <p><?=cm_htmlclear($module_message)?></p>
                                </div>

                            <?php } ?>

                        <?php }else{ ?>

                            <div class="col-md-12">
                                <p><?=cm_lang('Lütfen sol menüden eklenti seçimi yapın')?><br><br><strong><?=cm_lang('NOT')?></strong> : <?=cm_lang('Bu bölümdeki eklentiler modüller kısmından aktif ettiğiniz modülün admin yönetimi var ise gelir onun haricinde modül yönetimini Mödüller kısmından yapmanız gerekir.')?><br/><?=cm_lang('Modül durumu pasif ise eklentiler kısmında gözükmez')?></p>
                            </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php include("footer.php");?>