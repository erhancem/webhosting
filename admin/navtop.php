<?php
if(!isset($admindir)){
	include("settings.php");
}
?>
<!DOCTYPE html>
<html lang="tr-TR" xml:lang="tr-TR" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>CemBilisim.Com - Yönetim Paneli</title>
    <meta name="content-language" content="tr" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
	<!-- CSS Dosyalarim -->
    <link rel="stylesheet" href="<?=$admindir?>/css/default.css"/>
    <link rel="stylesheet" href="<?=$admindir?>/css/responsive.css"/>
	<link rel="stylesheet" href="<?=$admindir?>/css/font-awesome.min.css"/>
	<link rel="stylesheet" href="<?=$admindir?>/css/bootstrap.min.css"/>
</head>
<body onload="ZamanSay(31)">
