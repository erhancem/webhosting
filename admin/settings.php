<?php
$page_settings['session'] = true;
$admindir	= explode("/",str_ireplace(array('\\'),array("/"),realpath(dirname(__FILE__))));
$admindir	= '/'.end($admindir);
$incfile	= $_SERVER['DOCUMENT_ROOT'];
if(stristr($incfile,$admindir)){ $incfile 	= rtrim($incfile,$admindir); }
if(!is_file($incfile."/inc.include.php")){ echo 'Konum bulunamadı'; exit; }

include($incfile."/inc.include.php");
if($directory != ""){ $admindir = $directory.$admindir; }

if($cm_config["ip_protection"]['admin_status'] and !in_array($ip,cm_ip_list($cm_config["ip_protection"]['admin_ip']))){
//	header("HTTP/1.0 404 Not Found");
	echo 'IP Adresi izinli değil';
	exit;
}

if(!stristr($_SERVER['SCRIPT_NAME'],'login.php') and $cm_admin['user_id'] == 0){ cm_redirect($admindir.'/login.php'); }
if(stristr($_SERVER['SCRIPT_NAME'],'login.php') and $cm_admin['user_id'] > 0){ cm_redirect($admindir.'/index.php'); } //bakkk

$ListTotalRows 	= 20;
$Isaret 		= array(
	1 => array(
			"name" 	=> "= (Eşittir)",
			"query"	=> "##name## = '##value##'"
		),
	2 => array(
			"name" 	=> ">",
			"query"	=> "##name## > '##value##'"
		),
	3 => array(
			"name" 	=> ">=",
			"query"	=> "##name## >= '##value##'"
		),
	4 => array(
			"name" 	=> "<",
			"query"	=> "##name## < '##value##'"
		),
	5 => array(
			"name" 	=> "<=",
			"query"	=> "##name## <= '##value##'"
		),
	6 => array(
			"name" 	=> "!=",
			"query"	=> "##name## != '##value##'"
		),
	7 => array(
			"name" 	=> "LIKE %...%",
			"query"	=> "##name## like '%##value##%'"
		)
);