<?php
if(!isset($admindir)){
	include("../settings.php");
}
list($RegisterStatus,$TotalRegister) = $cm_db->sql_count('domains',array('domain_status'=>0,'domain_delete'=>0));
list($RenewStatus,$TotalRenew) = $cm_db->sql_count('domains',array('domain_status'=>6,'domain_delete'=>0));
list($TransferStatus,$TotalTransfer) = $cm_db->sql_count('domains',array('domain_status'=>4,'domain_delete'=>0));
list($TransferStartStatus,$TotalTransferStart) = $cm_db->sql_count('domains',array('domain_status'=>3,'domain_delete'=>0));
list($TicketOpenStatus,$TotalTicketOpen) = $cm_db->sql_count('tickets',array('ticket_status'=>1,'ticket_delete'=>0));
list($TicketPedingStatus,$TotalTicketPeding) = $cm_db->sql_count('tickets',array('ticket_status'=>2,'ticket_delete'=>0));
$TotalOpenServer = $cm_db->sql_fetch_assoc($cm_db->sql_query("SELECT COUNT(server_id) AS Total FROM ".$cm_config['db_prefix']."servers WHERE server_end_time > ".cm_time()." AND server_status IN (3)"));
$TotalOpenServer = $TotalOpenServer['Total'];
$TotalCloseServer = $cm_db->sql_fetch_assoc($cm_db->sql_query("SELECT COUNT(server_id) AS Total FROM ".$cm_config['db_prefix']."servers WHERE server_end_time < ".cm_time()." AND server_status NOT IN (3)"));
$TotalCloseServer = $TotalCloseServer['Total'];

include("../navtop.php");
?>
<div class="left-menu">
    <div class="menu-link">
        <ul>
            <li><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <a href="#">Admin Notu Ekle</a></li>
            <li><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <a href="<?=cm_siteurl()?>" target="_blank"><?=cm_lang('Siteye Görüntüle')?></a></li>
        </ul>
    </div>
    <div class="menu-title">
        <div class="pull-left"><?=cm_lang('KUYRUKLAR')?></div>
        <div class="pull-right"><i class="fa fa-bars fa-2x" style="cursor: pointer;color:#fff;display: none;" id="KuyrukAcKapat"></i></div>
        <div class="clearfix"></div>
    </div>
    <div id="KuyrukGoster">
        <div class="menu-link" style="font-weight: bold;">
            <ul>
                <li>
                    <div class="pull-left"><a href="#">Havale,EFT / Ertelenen</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="<?=$admindir?>/domain_list.php?status=0" target="right_content"><?=cm_lang('Domain Kayıt')?></a> / <a href="<?=$admindir?>/domain_list.php?status=6" target="right_content"><?=cm_lang('Uzatma')?></a></div>
                    <div class="pull-right"><a href="<?=$admindir?>/domain_list.php?status=0" target="right_content"><?=cm_number_format($TotalRegister)?></a> / <a href="<?=$admindir?>/domain_list.php?status=6" target="right_content"><?=cm_number_format($TotalRenew)?></a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="<?=$admindir?>/domain_list.php?status=4" target="right_content"><?=cm_lang('Transfer Sürecinde')?></a> / <a href="<?=$admindir?>/domain_list.php?status=3" target="right_content"><?=cm_lang('Başlat')?></a></div>
                    <div class="pull-right"><a href="<?=$admindir?>/domain_list.php?status=4" target="right_content"><?=cm_number_format($TotalTransfer)?></a> / <a href="<?=$admindir?>/domain_list.php?status=3" target="right_content"><?=cm_number_format($TotalTransferStart)?></a></div>
                    <div class="clearfix"></div>
                </li>
                <!--<li>
                    <div class="pull-left"><a href="#">Rezerve Edilen Domainler</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>-->
                <li>
                    <div class="pull-left"><a href="<?=$admindir?>/ticket_list.php" target="right_content"><?=cm_lang('Destek Talepleri')?> (A/B)</a></div>
                    <div class="pull-right"><a href="<?=$admindir?>/ticket_list.php?status=1" target="right_content" title="<?=cm_lang('Açık Destek Talepleri')?>"><?=cm_number_format($TotalTicketOpen)?></a> / <a href="<?=$admindir?>/ticket_list.php?status=2" target="right_content" title="<?=cm_lang('Bekleyen Destek Talepleri')?>"><?=cm_number_format($TotalTicketPeding)?></a></div>
                    <div class="clearfix"></div>
                </li>
                <!--<li>
                    <div class="pull-left"><a href="#">Marka</a> | <a href="#">Başvuru Bekleyen</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>-->
                <li>
                    <div class="pull-left"><a href="#">Host (Uza/Ac/Sil)</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Yazdırılacak Faturalar</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="<?=$admindir?>/server_list.php" target="right_content"><?=cm_lang('Sunucu')?></a> (<a href="<?=$admindir?>/server_list.php?open=1" target="right_content"><?=cm_lang('Aç')?></a> / <a href="<?=$admindir?>/server_list.php?close=1" target="right_content"><?=cm_lang('Kapat')?>)</a></div>
                    <div class="pull-right"><a href="<?=$admindir?>/server_list.php?open=1" target="right_content" title="<?=cm_lang('Açılacak sunucular')?>"><?=cm_number_format($TotalOpenServer)?></a> / <a href="<?=$admindir?>/server_list.php?close=1" target="right_content" title="<?=cm_lang('Kapatılacak sunucular')?>"><?=cm_number_format($TotalCloseServer)?></a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="<?=$admindir?>/server_list.php?status=0" target="right_content">Kurulacak Sunucu</a></div>
                    <div class="pull-right"><a href="#">0</a></div>
                    <div class="clearfix"></div>
                </li>
            </ul>
        </div>
        <div class="menu-title">İSTATİSLİKLER</div>
        <div class="menu-link">
            <ul>
                <li>
                    <div class="pull-left"><a href="#">Aktif Oturumlar</a></div>
                    <div class="pull-right"><a href="#">334</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Sepetteki Ürünler</a></div>
                    <div class="pull-right"><a href="#">334</a></div>
                    <div class="clearfix"></div>
                </li>
            </ul>
        </div>
    <strong class="TimeRefresh" id="OturumYenileniyor">Oturum AÇIK - <strong id="SureYaz">0.00</strong> dk</strong>
    </div>
</div>
<?php include("../footer.php"); ?>