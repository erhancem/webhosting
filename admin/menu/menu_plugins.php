<?php
if(!isset($admindir)){ include("../settings.php"); }

$Plugins = $cm_db->sql_datas("SELECT * FROM ".$cm_config["db_prefix"]."modules WHERE module_admin = 1 and module_status = 1 ORDER BY module_id ASC");

include("../navtop.php");
?>
    <div class="left-menu">
        <div class="menu-title">
            <div class="pull-left"><?=cm_lang('Eklenti Yönetimi')?></div>
            <div class="pull-right"><i class="fa fa-bars fa-2x" style="cursor: pointer;color:#fff;display: none;" id="KuyrukAcKapat"></i></div>
            <div class="clearfix"></div>
        </div>
        <div id="KuyrukGoster">
            <div class="menu-link" style="font-weight: bold;">
                <ul>
                    <?php if($Plugins){ foreach ($Plugins as $key => $value){ ?>
                        <li><a href="<?=$admindir?>/plugins.php?mod=<?=$value['module_dir']?>&module_name=<?=$value['module']?>" target="right_content"><?=htmlspecialchars($value["module_name"])?></a></li>
                    <?php }}else{ ?>
                        <li style="text-align: center;color: red;font-size: 12px;"><?=cm_lang('Kurulu veya Aktif Eklenti Yok')?></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
<?php include("../footer.php"); ?>