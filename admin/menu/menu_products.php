<?php
if(!isset($admindir)){
	include("../settings.php");
}
include("../navtop.php");
?>
<div class="sidebar">
    <div class="menu-title">
        <div class="pull-left">ÜRÜNLER</div>
        <div class="pull-right"><i class="fa fa-bars fa-2x" style="cursor: pointer;color:#fff;display: none;" id="KuyrukAcKapat"></i></div>
        <div class="clearfix"></div>
    </div>
    <div id="KuyrukGoster">
        <div class="menu-link">
            <ul>
                <li>
                    <div class="pull-left"><a href="#">Ürün Ekleme</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Ürün Düzenleme</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Rezerve Edilen Domainler</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Marka</a> | <a href="#">Başvuru Bekleyen</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Host (Uza/Ac/Sil)</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Yazdırılacak Faturalar</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Havale,EFT / Ertelenen</a></div>
                    <div class="pull-right"><a href="#">3</a> / <a href="#">6</a></div>
                    <div class="clearfix"></div>
                </li>
            </ul>
        </div>
        <div class="menu-title">İSTATİSLİKLER</div>
        <div class="menu-link">
            <ul>
                <li>
                    <div class="pull-left"><a href="#">Aktif Oturumlar</a></div>
                    <div class="pull-right"><a href="#">334</a></div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="pull-left"><a href="#">Sepetteki Ürünler</a></div>
                    <div class="pull-right"><a href="#">334</a></div>
                    <div class="clearfix"></div>
                </li>
            </ul>
        </div>
    </div>
</div>
<?php include("../footer.php"); ?>