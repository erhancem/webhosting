<?php
if(!isset($admindir)){
	include("../settings.php");
}
include("../navtop.php");
?>
<div class="left-menu">
    <div class="menu-title">
        <div class="pull-left"><?=cm_lang('DOMAİN YÖNETİMİ')?></div>
        <div class="pull-right"><i class="fa fa-bars fa-2x" style="cursor: pointer;color:#fff;display: none;" id="KuyrukAcKapat"></i></div>
        <div class="clearfix"></div>
    </div>
    <div id="KuyrukGoster">
        <div class="menu-link" style="font-weight: bold;">
            <ul>
                <li><a href="<?=$admindir?>/domain_list.php" target="right_content"><?=cm_lang('Alan Adı Listesi')?></a></li>
                <li><a href="#" target="right_content"><?=cm_lang('Üye Adresleri')?></a></li>
                <li><a href="#" target="right_content">Üye Rütbeleri</a></li>
                <li><a href="#" target="right_content">Üye Adresleri</a></li>
                <li><a href="#" target="right_content">Üye Ekle</a></li>
                <li><a href="#" target="right_content">Üyelik Ayarları</a></li>
                <li><a href="#" target="right_content">Toplu Mail</a></li>
                <li><a href="#" target="right_content">Hizmet Aktarılan</a></li>
            </ul>
        </div>
    </div>
</div>
<?php include("../footer.php"); ?>