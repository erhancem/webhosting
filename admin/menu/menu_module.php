<?php
if(!isset($admindir)){
	include("../settings.php");
}
include("../navtop.php");
?>
<div class="left-menu">
    <div class="menu-title">
        <div class="pull-left"><?=cm_lang('Modül Yönetimi')?></div>
        <div class="pull-right"><i class="fa fa-bars fa-2x" style="cursor: pointer;color:#fff;display: none;" id="KuyrukAcKapat"></i></div>
        <div class="clearfix"></div>
    </div>
    <div id="KuyrukGoster">
        <div class="menu-link" style="font-weight: bold;">
            <ul>
                <?php foreach ($ModulesDirList as $key=>$value){ ?>
                <li><a href="<?=$admindir?>/modules.php?mod=<?=$key?>" target="right_content"><?=$value["name"]?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
<?php include("../footer.php"); ?>