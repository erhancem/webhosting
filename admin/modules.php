<?php
if(!isset($admindir)){ include("settings.php"); }

$mod            = (cm_get_request('mod')?strtolower(cm_get_request('mod')):'none');
$module_name    = (cm_get_request('module_name')?strtolower(cm_get_request('module_name')):'none');
$mod_test       = false;

if($mod != 'none' and in_array($mod,array_keys($ModulesDirList))){
    $moduleDir  = $cm_dirlist['module'].strtolower($mod).'/';
    $moduleDir  = cm_dirList($moduleDir);

    if($module_name != 'none' and in_array($module_name,$moduleDir)){
        $moduleDir = array($module_name);
    }elseif($module_name != 'none'){
        $module_name = 'none';
    }

    $ModuleList = array();
    foreach ($moduleDir as $val){
        $val = strtolower($val);
        $filenameConfig = $cm_dirlist['module'].$mod.'/'.$val.'/'.$val.'.config.php';
        $filename       = $cm_dirlist['module'].$mod.'/'.$val.'/'.$val.'.php';
        if(file_exists($filenameConfig) and file_exists($filename)) {
            include ($filenameConfig);
            $control = $cm_db->sql_query("SELECT * FROM " . $cm_config['db_prefix'] . "modules WHERE module_dir = '" . $cm_db->sql_escape($mod) . "' and `module` = '" . $cm_db->sql_escape($val) . "'");
            if ($cm_db->sql_errno() == 0) {
                $status = 0;
                if ($cm_db->sql_num_rows($control) > 0) {
                    $cnl = $cm_db->sql_fetch_assoc($control);
                    $db_config = array();
                    $module_db_configX = $cm_db->sql_query("SELECT * FROM ".$cm_config["db_prefix"]."module_config WHERE module_id=".$cnl['module_id']." ORDER BY module_feature_id ASC");
                    while($module_db_config = $cm_db->sql_fetch_assoc($module_db_configX)){
                        $db_config[$module_db_config['module_feature_key']] = cm_decode($module_db_config['module_feature_value']);
                    }
                    $status = $cnl['module_status'];
                }
                $ModuleList[$val] = array(
                    'name'      => $val,
                    'status'    => $status,
                );
                if (isset($cnl)) {
                    $ModuleList[$val]['assoc'] = $cnl;
                    unset($cnl);
                }
                if(isset($db_config)){
                    $ModuleList[$val]['assoc'] = array_merge($ModuleList[$val]['assoc'],$db_config);
                    unset($db_config);
                }
                $module_variable = $val.'_'.$mod;
                /**
                 * Modül config dosyasını burada ayraştır.!!!
                 *
                 * Örnek form array gelecek bunları fonksiyonda yapabilirsin
                 *
                 */
                if(isset(${$module_variable})){
                    $ModuleList[$val]['config'] = ${$module_variable};
                }else{
                    unset($ModuleList[$val]);
                }
                unset($status);
            }
        }
    }

    if($module_name != 'none' and isset($ModuleList[$module_name]['config'])){
        if(isset($ModuleList[$module_name]['config']['data']['submit_test'])){
            $mod_test = $ModuleList[$module_name]['config']['data']['submit_test'];
        }

        $name = $ModuleList[$module_name]['name'].' - '.$ModuleList[$module_name]['config']['name'];
        $module_submit  = cm_get_request('module_submit');
        $module_test    = cm_get_request('module_test');

        if($module_submit or ($module_test and $mod_test)){

            $file           = $cm_dirlist["module"].$mod.'/'.$module_name.'/'.$module_name.'.php';
            $filecontrol    = file_get_contents($file);
            if($filecontrol != '<?php') {
                $filecontrol = 1;
                include($file);
            }

            $status     = true;
            $newConfig  = array();
            if(isset($ModuleList[$module_name]['config']['data']['form']) and is_array($ModuleList[$module_name]['config']['data']['form'])) {
                foreach ($ModuleList[$module_name]['config']['data']['form'] as $key => $val) {
                    $input = cm_get_request($val['name']);
                    if ($input) {
                        $newConfig[$val['name']] = $input;
                    } else {
                        if (!isset($val['required']) or (isset($val['required']) and $val['required'] == false)) {
                            $newConfig[$val['name']] = isset($val['value']) ? $val['value'] : null;
                        } else {
                            $status = false;
                            break;
                        }
                    }
                }
            }

            $permission = [];
            if(isset($ModuleList[$module_name]['config']['data']['permissions']) and is_array($ModuleList[$module_name]['config']['data']['permissions']) and count($ModuleList[$module_name]['config']['data']['permissions']) > 0) {
                foreach ($ModuleList[$module_name]['config']['data']['permissions'] as $key => $val) {
                    $input  = cm_get_request($val);
                    $per    = cm_modulepermissionControl($val, $input);
                    if(is_array($per)){
                        $key = array_keys($per);
                        $permission[$key[0]] = $per[$key[0]];
                    }else{
                        $status = false;
                        break;
                    }
                }
            }

            if($status == true) {
                $module_status = cm_get_request('module_status');
                $module_admin = isset($ModuleList[$module_name]['config']['admin']) ? $ModuleList[$module_name]['config']['admin'] : 0;
                $moduleTable = array(
                    'module_dir'        => $mod,
                    'module_type'       => $ModuleList[$module_name]['config']['type'],
                    'module_admin'      => $module_admin,
                    'module'            => $module_name,
                    'module_name'       => $ModuleList[$module_name]['config']['name'],
                    'module_status'     => (in_array($module_status, array(1, 2)) ? $module_status : 1),
                    'module_update_time' => cm_time(),
                );
                $moduleTable = array_merge($moduleTable, $permission);
                if($module_test){

                    $status = false;
                    if($mod_test){
                        if (function_exists($module_name . '_moduletest')) {
                            $params = array_merge($moduleTable, $newConfig);
                            $ModuleReturn = call_user_func($module_name . '_moduletest', $params);
                            if (is_array($ModuleReturn) and count($ModuleReturn) == 2) {
                                list($ModulStatus, $ModulResponse) = $ModuleReturn;
                                if ($ModulStatus) {
                                    $message    = $ModulResponse['message'];
                                    $status     = $ModulStatus;
                                } else {
                                    $message    = $ModulResponse;
                                }
                            } else {
                                $message    = cm_lang('Modül test fonksiyonu çıktısı okunamadı');
                            }
                        }else{
                            $message = cm_lang('Modülde test fonksiyonu bulunamadı');
                        }
                    }else{
                        $message = cm_lang('Modül için test işlemi aktif değil');
                    }

                }else {

                    if (!isset($ModuleList[$module_name]['assoc']) and function_exists($module_name . '_moduleinstall')) {
                        $params = array_merge($moduleTable, $newConfig);
                        $ModuleReturn = call_user_func($module_name . '_moduleinstall', $params);
                        if (is_array($ModuleReturn) and count($ModuleReturn) == 2) {
                            list($ModulStatus, $ModulResponse) = $ModuleReturn;
                            if ($ModulStatus) {

                            } else {
                                $message    = $ModulResponse;
                                $status     = false;
                            }
                        } else {
                            $message    = cm_lang('Modül kurulum fonksiyonu hatalı çıktı okunamıyor');
                            $status     = false;
                        }
                    }

                    if ($status) {
                        $status = false;
                        if (isset($ModuleList[$module_name]['assoc'])) {
                            list($sts, $rsp) = $cm_db->sql_query_update('modules', $moduleTable, array('module_id' => $ModuleList[$module_name]['assoc']['module_id']));
                            if ($sts) {
                                $module_id = $ModuleList[$module_name]['assoc']['module_id'];
                                $status = true;
                            }
                        } else {
                            $moduleTable['module_add_time'] = cm_time();
                            list($sts, $rsp) = $cm_db->sql_insert_add('modules', $moduleTable);
                            if ($sts) {
                                $status = true;
                                $module_id = $rsp;
                            }
                        }

                        if ($status) {
                            $moduleTable['module_id'] = $module_id;
                            $status = false;
                            $cm_db->sql_query("DELETE FROM " . $cm_config['db_prefix'] . "module_config WHERE module_id=" . $module_id);
                            if ($cm_db->sql_errno() == 0) {
                                if(count($newConfig) > 0) {
                                    foreach ($newConfig as $key => $val) {
                                        $add = array(
                                            'module_id'             => $module_id,
                                            'module_feature_key'    => $key,
                                            'module_feature_value'  => cm_encode($val),
                                        );
                                        $cm_db->sql_insert_add('module_config', $add);
                                    }
                                }
                                $status = true;
                                $message = cm_lang('Modül kaydedildi');
                                $ModuleList[$module_name]['assoc'] = array_merge($moduleTable, $newConfig);
                                $ModuleList[$module_name]['status'] = $ModuleList[$module_name]['assoc']['module_status'];
                                unset($moduleTable, $newConfig, $module_admin, $module_id);
                            } else {
                                $message = cm_lang('Modül kaydedilemedi');
                            }
                        } else {
                            $message = cm_lang('Modül kaydedilemedi');
                        }
                    }
                }
            }else{
                $message = cm_lang('Lütfen tüm zorunlu alanları doldurun');
            }
        }
    }
}

include("navtop.php");
?>
<div class="main">
    <div class="main-content width100">
        <div class="row">
            <div class="panel panel-default">
                <!-- Default panel contents -->
                <div class="panel-heading"><?=cm_lang('Modüller')?></div>
                <div class="panel-body">

                    <?php if(in_array($mod,array_keys($ModulesDirList))){ ?>

                    <?php if(!isset($name)){ ?>
                    <div class="col-md-12" style="margin-bottom: 15px;">
                        <div class="col-md-6"><div class="pull-left" style="padding: 0px;"><?=cm_lang('Modül')?> : <strong><?=cm_htmlclear($ModulesDirList[$mod]['name'])?></strong></div></div>
                        <div class="clearfix"></div>
                    </div>
                    <?php } ?>

                    <?php if($module_name == 'none' or count($ModuleList) == 0){ ?>

                        <p><strong><?=cm_lang('NOT')?></strong> : <?=cm_lang('Modül config dosyası eksik ise bu listede görünmez')?></p>
                        <table class="table table-bordered table-hover results">
                            <thead>
                            <tr>
                                <th><?=cm_lang('Modül')?></th>
                                <th><?=cm_lang('Modül Adı')?></th>
                                <th><?=cm_lang('Modül Durumu')?></th>
                                <th> - </th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if(count($ModuleList) > 0){ ?>
                                <?php foreach ($ModuleList as $key=>$value){ ?>
                                    <tr>
                                        <td><?=htmlspecialchars($value["name"])?></td>
                                        <td><?=(isset($value["config"]['name'])?cm_htmlclear($value["config"]['name']):cm_lang('Modül adı girilmemiş'))?></td>
                                        <td><strong><?=cm_statusWrite('module',$value["status"])?></strong></td>
                                        <td><a href="<?=$admindir?>/modules.php?mod=<?=$mod?>&module_name=<?=$value['name']?>" class="btn btn-info btn-sm"><i class="fa fa-cog" aria-hidden="true"></i> <?=cm_lang('YAPILANDIR')?></a></td>
                                    </tr>
                                <?php } ?>
                            <?php }else{ ?>
                                    <tr>
                                        <td colspan="4" style="text-align: center;"><strong><?=cm_lang('Modül bulunamadı')?></strong></td>
                                    </tr>
                            <?php } ?>
                            </tbody>
                        </table>

                    <?php }elseif($module_name != 'none'){ ?>

                        <div class="col-md-2"></div>
                        <div class="col-md-8">
                            <form action="" method="POST">
                                <div class="form-group">
                                    <label for="exampleInputEmail1"><?=cm_lang('Modül')?></label>
                                    <?php if(isset($ModuleList[$module_name]['config']['logo']) and $ModuleList[$module_name]['config']['logo'] != ''){ ?>
                                        <p><img src="<?=$ModuleList[$module_name]['config']['logo']?>" style="height: 150px!important;"></p>
                                    <?php } ?>

                                    <?php if(isset($ModuleList[$module_name]['name'])){ ?>
                                    <p><?=cm_htmlclear($ModuleList[$module_name]["name"])?><?=array_key_exists('config',$ModuleList[$module_name])?' - '.cm_htmlclear($ModuleList[$module_name]['config']['name']):null?></p>
                                    <?php } ?>

                                    <?php if(isset($ModuleList[$module_name]['config']['data']['description'])){ ?>
                                    <small id="emailHelp" class="form-text text-muted"><?=cm_htmlclear($ModuleList[$module_name]['config']['data']['description'])?></small>
                                    <?php } ?>
                                </div>

                                <?php if(isset($status) and isset($message)){ ?>
                                <div class="form-group">
                                    <p style="text-align: center;font-weight: bold;color: <?=$status?'green':'red'?>;"><?=$message?></p>
                                </div>
                                <?php } ?>

                                <?php
                                if(isset($ModuleList[$module_name]['config']['data']['form'])) {
                                    $data = array();
                                    if(isset($ModuleList[$module_name]['assoc'])){
                                        $data = $ModuleList[$module_name]['assoc'];
                                    }
                                    foreach ($ModuleList[$module_name]['config']['data']['form'] as $key => $value) {
                                        echo cm_inputType($value,$data);
                                    }
                                }
                                ?>

                                <?php
                                if(isset($ModuleList[$module_name]['config']['data']['permissions']) and count($ModuleList[$module_name]['config']['data']['permissions']) > 0) {
                                    $data = array();
                                    if(isset($ModuleList[$module_name]['assoc'])){
                                        $data = $ModuleList[$module_name]['assoc'];
                                    }
                                    foreach ($ModuleList[$module_name]['config']['data']['permissions'] as $key => $value) {
                                        echo cm_modulepermission($value,$data);
                                    }
                                }
                                ?>

                                <?php if(isset($ModuleList[$module_name]['assoc'])){ ?>
                                <div class="form-group">
                                    <label for="exampleSelect1"><?=cm_lang('Modül Durumu')?></label>
                                    <select name="module_status" class="form-control" id="exampleSelect1">
                                        <option value="1"<?=$ModuleList[$module_name]['status']==1?' selected=""':null?>><?=cm_lang('Aktif')?></option>
                                        <option value="2"<?=$ModuleList[$module_name]['status']==2?' selected=""':null?>><?=cm_lang('Pasif')?></option>
                                    </select>
                                </div>
                                <?php } ?>
                                <button name="module_submit" value="1" type="submit" class="btn btn-success"><?=$ModuleList[$module_name]['status']==0?cm_lang('Kurulum Yap'):cm_lang('Modül Kaydet')?></button>
                                <?php if($mod_test){ ?>
                                <button name="module_test" value="1" type="submit" class="btn btn-danger"><?=cm_lang('Modül Test')?></button>
                                <?php } ?>
                            </form>
                        </div>
                        <div class="col-md-2"></div>

                    <?php } ?>

                    <?php }else{ ?>
                    <div class="col-md-12">
                        <p><?=cm_lang('Lütfen sol menüden modül seçimi yapın')?></p>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php");?>