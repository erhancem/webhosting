<?php
if(!isset($admindir)){ include("settings.php"); }
cm_function_call('ticket');
$ticketid = cm_get_request("id");
if(!cm_numeric($ticketid) or (cm_numeric($ticketid) and $ticketid == 0)){
	cm_url_go($admindir.'/ticket_list.php');
}
$ticket = $cm_db->sql_assoc('tickets',array('ticket_id'=>$ticketid));
if(!$ticket){
	cm_url_go($admindir.'/ticket_list.php');
}
$ticket_user = null;

/** Destek cevaplama */
$answer = cm_get_request('answer');
if($answer){
	$answer_message = cm_get_request('answer_message');
	if($answer_message and $answer_message != ""){
		$answer_status = cm_get_request('answer_status');
		if(!$answer_status or ($answer_status and !in_array($answer_status,array_keys($ticket_status)))){
			$answer_status = 3;
		}
		$cm_time = cm_time();
		$update = array(
			'ticket_status'		=> $answer_status,
			'ticket_end_time'	=> $cm_time,
		);
		$ticket = array_merge($ticket,$update);
		$cm_db->sql_query_update('tickets',$update,array('ticket_id'=>$ticket['ticket_id']));
		$add = array(
			'ticket_id'			=> $ticket['ticket_id'],
			'user_id'			=> $admin['user_id'],
			'ticket_message'	=> $answer_message,
			'ticket_answer_ip'	=> $ip,
			'ticket_answer_time'=> $cm_time,
		);
		$cm_db->sql_insert_add('ticket_answer',$add);
		//cm_url_go($admindir.'/ticket.php?id='.$ticket['ticket_id']);
	}
}
/** */

/** Temp Mesaj */
$answer_message_temp = cm_get_request('answer_message_temp');
if($answer_message_temp and $answer_message_temp == '1'){
	$temp_message = cm_get_request('message');
	if($temp_message and $temp_message != ""){
		$add = array(
			'ticket_message'	=> $temp_message,
			'user_id'			=> $admin['user_id'],
			'ticket_answer_ip'	=> $ip,
			'ticket_id'			=> $ticket['ticket_id'],
		);
		list($TempStatus,$TotalTemp) = $cm_db->sql_count('ticket_answer_temp',array('ticket_id'=>$ticket['ticket_id'],'user_id'=>$admin['user_id']));
		if($TempStatus){
			if($TotalTemp > 0){
				$cm_db->sql_query_update('ticket_answer_temp',$add,array('ticket_id'=>$ticket['ticket_id'],'user_id'=>$admin['user_id']));
			}else{
				$add['ticket_answer_time'] = cm_time();
				$cm_db->sql_insert_add('ticket_answer_temp',$add);
			}
		}
	}
	exit;
}
/** */

/** Temp List */
$temp_list = cm_get_request('temp_list');
if($temp_list and $temp_list == '1'){
	$temp_listX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."ticket_answer_temp AS t LEFT JOIN ".$cm_config['db_prefix']."users AS u ON t.user_id=u.user_id WHERE t.ticket_id='".$ticket['ticket_id']."' AND t.user_id!=".$admin['user_id']);
	if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($temp_listX) > 0){
		$return = array();
		while($temp_list = $cm_db->sql_fetch_assoc($temp_listX)){
			$return[] = '<p style="padding: 10px;background: #efefef;border: 1px solid #ccc;border-radius: 5px;font-weight: bold;">'.cm_lang('Yazıyor').' : '.cm_user_name($temp_list).'<br /><br />'.cm_textshow($temp_list['ticket_message']).'</p>';
		}
		echo implode('<div style="margin-bottom: 15px;"></div>',$return);
	}
	exit;
}
/** */

/** Destek cevap edit */
$edit = cm_get_request('edit');
if($edit){
	$answer_message = cm_get_request('answer_message');
	if($answer_message and $answer_message != ""){
		$answer_edit_id = cm_get_request('answer_edit_id');
		if($answer_edit_id and cm_numeric($answer_edit_id) and $answer_edit_id > 0){
			$update = array(
				'ticket_message'	=> $answer_message,
			);
			$cm_db->sql_query_update('ticket_answer',$update,array('ticket_answer_id'=>$answer_edit_id));
			cm_url_go($admindir.'/ticket.php?id='.$ticket['ticket_id']);
		}
	}
}
/** */

/** Destek durumu değiştirme */
$ticket_status_change = cm_get_request('ticket_status');
if($ticket_status_change and in_array($ticket_status_change,array_keys($ticket_status))){
	$update = array(
		'ticket_status'	=> $ticket_status_change,
	);
	$cm_db->sql_query_update('tickets',$update,array('ticket_id'=>$ticket['ticket_id']));
	cm_url_go($admindir.'/ticket.php?id='.$ticket['ticket_id']);
}
/** */

/** Destek mesajı silme */
$answer_delete = cm_get_request('answer_delete');
if($answer_delete and cm_numeric($answer_delete) and $answer_delete > 0){
	$update = array(
		'ticket_answer_delete'	=> 1,
	);
	$cm_db->sql_query_update('ticket_answer',$update,array('ticket_answer_id'=>$answer_delete));
	cm_url_go($admindir.'/ticket.php?id='.$ticket['ticket_id']);
}
/** */

/** Destek mesajı geri al */
$answer_back = cm_get_request('answer_back');
if($answer_back and cm_numeric($answer_back) and $answer_back > 0){
	$update = array(
		'ticket_answer_delete'	=> 0,
	);
	$cm_db->sql_query_update('ticket_answer',$update,array('ticket_answer_id'=>$answer_back));
	cm_url_go($admindir.'/ticket.php?id='.$ticket['ticket_id']);
}
/** */

$ticket_stats = '<span style="color: '.$ticket_status[$ticket['ticket_status']]['color'].';"><strong>'.cm_lang($ticket_status[$ticket['ticket_status']]['name']).'</strong></span>';
if($ticket['ticket_delete'] == 1){
	$ticket_stats = '<span style="color: '.$delete_status[$ticket['ticket_delete']]['color'].';"><strong>'.cm_lang($delete_status[$ticket['ticket_delete']]['name']).'</strong></span>';
}
$department		= ticket_departments();
$department		= $department[$ticket['ticket_dep_id']];

list($TicketAnswerStatus,$TotalTicketAnswer) = $cm_db->sql_count('ticket_answer',array('ticket_id'=>$ticket['ticket_id']));
include("navtop.php");
?>
<div class="main">
    <div class="main-content width100">
        <div class="row">
            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading"><strong><?=cm_lang('Destek Talebi Konu')?></strong> : <span style="color: #2d6ee4;"><?=cm_htmlclear($ticket['ticket_subject'])?></span></div>
              <div class="panel-body">
				<p style="text-align: center;">
					<a href="<?=$admindir?>/ticket.php?ticket_status=1&id=<?=$ticket['ticket_id']?>" onclick="return confirm('<?=cm_lang('İşlemi onaylıyormusunuz')?>?');" style="margin-right: 10px;" class="btn btn-success"><i class="fa fa-check" aria-hidden="true"></i> <?=cm_lang('Açık')?></a>
					<a href="<?=$admindir?>/ticket.php?ticket_status=2&id=<?=$ticket['ticket_id']?>" onclick="return confirm('<?=cm_lang('İşlemi onaylıyormusunuz')?>?');" style="margin-right: 10px;" class="btn btn-warning"><i class="fa fa-pause" aria-hidden="true"></i> <?=cm_lang('Beklet')?></a>
					<a href="<?=$admindir?>/ticket.php?ticket_status=3&id=<?=$ticket['ticket_id']?>" onclick="return confirm('<?=cm_lang('İşlemi onaylıyormusunuz')?>?');" style="margin-right: 10px;" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> <?=cm_lang('Kapat')?></a>
				</p>
                <p>
					<strong><?=cm_lang('Destek Durumu')?></strong>: <?=$ticket_stats?>&nbsp;
					<strong><?=cm_lang('Açılış Zamanı')?></strong>: <?=cm_date(null,$ticket['ticket_start_time'])?>&nbsp;
					<strong><?=cm_lang('Son Cevap Zamanı')?></strong>: <?=cm_date(null,$ticket['ticket_end_time'])?>&nbsp;
					<strong><?=cm_lang('Toplam Mesaj')?></strong>: <?=cm_number_format($TotalTicketAnswer)?>&nbsp;
					<strong><?=cm_lang('Departman')?></strong>: <?=cm_htmlclear($department['ticket_dep_name'])?>
				</p>

                <table class="table table-bordered results">
                    <tbody>
<?php
$TotalPage = 0;
$queryTotal	= "SELECT COUNT(t.ticket_id) as Rows from ".$cm_config["db_prefix"]."ticket_answer AS t WHERE t.ticket_id='".$cm_db->sql_escape($ticket['ticket_id'])."'";
$TotalList = $cm_db->sql_query($queryTotal);
if($cm_db->sql_errno() == 0){
	$TotalRows = $cm_db->sql_fetch_assoc($TotalList);
    $page = 1;
    $page = ($page>$TotalRows["Rows"]?1:$page); //Sayfa kontrolü
	if($TotalRows["Rows"] > 0){
		$ListTotalRows	= 2000;
		$ListStart		= (($page-1)*$ListTotalRows+1)-1;	//sql alınacak kayıt
		$x				= 10;							//aktif sayfadan önceki/sonraki sayfa gösterim sayısı
		$next_page		= $page + 1;					//sonraki sayfa
		$previous_page	= $page - 1;					//önceki sayfa
		$TotalPage 		= ceil($TotalRows["Rows"]/$ListTotalRows);
    	$ListQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config["db_prefix"]."ticket_answer AS t LEFT JOIN ".$cm_config["db_prefix"]."users AS u ON t.user_id=u.user_id WHERE t.ticket_id='".$cm_db->sql_escape($ticket['ticket_id'])."' ORDER BY t.ticket_answer_id ASC LIMIT ".$ListStart.",".$ListTotalRows);
    	if($cm_db->sql_errno() == 0){
        	while($List = $cm_db->sql_fetch_assoc($ListQuery)){
        		if($ticket_user == null and $List['user_id'] == $ticket['user_id']){
        			$ticket_user = $List;
        		}
        		$usr 		= cm_admin_user_link($List);
        		$start_time	= cm_date(null,$List['ticket_answer_time']);
        		//$EndTime = $cm_db->sql_fetch_assoc($cm_db->sql_query("SELECT ticket_answer_time FROM ".$cm_config['db_prefix']."ticket_answer WHERE ticket_id='".$List['ticket_id']."' AND ticket_answer_delete=0 ORDER BY ticket_answer_time DESC LIMIT 1"));
?>
				<tr<?=$List['ticket_answer_delete']==1?' style="background: red;"':null?> id="list<?=$List['ticket_answer_id']?>">
					<td style="width: 25%;border-left: 1px solid #ddd;border-right: 1px solid #ddd;border-bottom: 1px solid #ddd;">
						<strong>Üye</strong><br /> <?=$usr?><br />
						<strong>Tarih</strong><br /> <?=$start_time?><br /><br />
						<a href="<?=$admindir?>/ticket.php?id=<?=$ticket['ticket_id']?>&answer_edit=<?=$List['ticket_answer_id']?>#list<?=$List['ticket_answer_id']?>" class="btn btn-sm btn-info"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> <?=cm_lang('Düzenle')?></a>
<?php if($List['ticket_answer_delete'] == 0){ ?>
						<a href="<?=$admindir?>/ticket.php?id=<?=$ticket['ticket_id']?>&answer_delete=<?=$List['ticket_answer_id']?>" onclick="return confirm('<?=cm_lang('İşlemi onaylıyormusunuz')?>?');" class="btn btn-sm btn-danger"><i class="fa fa-trash-o" aria-hidden="true"></i> <?=cm_lang('Sil')?></a>
<?php }else{ ?>
						<a href="<?=$admindir?>/ticket.php?id=<?=$ticket['ticket_id']?>&answer_back=<?=$List['ticket_answer_id']?>" onclick="return confirm('<?=cm_lang('İşlemi onaylıyormusunuz')?>?');" class="btn btn-sm btn-success"><i class="fa fa-undo" aria-hidden="true"></i> <?=cm_lang('Geri')?></a>
<?php } ?>
						<span style="cursor: default;" class="btn btn-sm btn-success<?=$List['end_reading_time']==0?" disabled":null?>" title="<?=cm_lang(($List['end_reading_time']==0?"Kullanıcı mesajı okumadı":"Kullanıcı mesajı okudu"))?>"><i class="fa fa-eye" aria-hidden="true"></i></span>


					<td style="width: 75%;border-right: 1px solid #ddd;border-bottom: 1px solid #ddd;">
<?php if(cm_get_request('answer_edit') and cm_get_request('answer_edit') == $List['ticket_answer_id']){ ?>
<form action="" method="POST">
	<input type="hidden" name="answer_edit_id" value="<?=$List['ticket_answer_id']?>" />
	<textarea name="answer_message" id="answer_message" class="form-control answer_message" rows="7"><?=$List['ticket_message']?></textarea>
	<input type="submit" name="edit" class="btn btn-default" value="<?=cm_lang('Düzenle')?>" style="width: 100%;margin-top: 15px;" />
</form>
<?php }else{ ?>
<?=cm_textshow($List['ticket_message'])?>
<?php } ?>
					</td>
				</tr>
<?php }
}else{
	$errorMessage = cm_lang('Hata')." ".$cm_db->sql_error();
}
}else{
	$errorMessage = cm_lang('Kayıt bulunamadı');
}
}else{
	$errorMessage = cm_lang('hata')." : ".$cm_db->sql_error();
}
if(isset($errorMessage)){
?>
						<tr>
							<td colspan="8" style="text-align: center;font-weight: bold;"><?=$errorMessage?></td>
						</tr>
<?php } ?>
						<tr>
							<form action="" method="POST">
								<td colspan="2" style="border-right: 1px solid #ddd;border-bottom: 1px solid #ddd;">
									<div class="col-md-6 row pull-left" style="margin-bottom: 15px;">
										<div class="col-md-4">
											<select name="answer_status" class="form-control">
										<?php foreach($ticket_status as $key=>$value){ ?>
												<option value="<?=$key?>"<?=$key==3?' selected=""':null?>><?=cm_lang($value['name'])?></option>
										<?php } ?>
											</select>
										</div>
										<div class="col-md-6">
											<select class="form-control" onchange="answer_message_write($(this).val());">
												<option value="0"><?=cm_lang('Hazır cevaplar')?></option>
												<option value="1">Cpanel Bilgileri</option>
											</select>
										</div>
										<div class="col-md-2">
											<button type="button" onclick="answer_message_write(0);" class="btn btn-default"><?=cm_lang('Temizle')?></button>
										</div>
									</div>
									<div class="col-md-6 row pull-right" style="margin-bottom: 15px;">
										<div class="col-md-12">
											<p style="text-align: right;">
												<button type="button" class="btn btn-default ready_reply" value="1515151515"><?=cm_lang('Beklet')?></button>
											</p>
										</div>
									</div>
									<div class="col-md-12">
										<div class="temp_message"></div>
									</div>
									<textarea name="answer_message" id="answer_message" class="form-control answer_message answer_message_temp" rows="7"><?=cm_lang('Sayın')?> <?=cm_user_name($ticket_user)?>




<?=cm_lang('Saygılarımla')?>.
									</textarea>
									<span class="clearfix"></span>
									<input type="submit" class="btn btn-success" style="width: 100%; margin-top: 10px;" name="answer" value="<?=cm_lang('Cevapla')?>" />
								</td>
							</form>
						</tr>
                    </tbody>
                </table>
              </div>
        </div>
    </div>
	</div>
</div>
<?php include("footer.php");?>
<script>
function ready_reply_get(id){
	var message = "";
	if(id > 0){
		message = '\tTest mesaj';
	}
	var return_msg = '<?=cm_lang('Sayın')?> <?=cm_user_name($ticket_user)?>\n\n'+message+'\n\n<?=cm_lang('Saygılarımla')?>.'
	return return_msg;
}
function answer_message_write(id){
	var message 	= ready_reply_get(id);
	var myTextArea 	= $('.answer_message');
	myTextArea.val(message);
}
$(document).ready(function(){
var active_message;
	$(".ready_reply").on("mouseover", function (e) {
		var message_id = $(e.target).attr('value');
		active_message = $('.answer_message').val();
	    answer_message_write(message_id);
	});
	$(".ready_reply").on("mouseout", function (e) {
	    $('.answer_message').val(active_message);
	});
	$(".answer_message").on("mousemove", function (e) {
	    var keyed = $(this).val();
	    post = 'temp_list=1';
		$.ajax({
			url: '',
			global: false,
			type: "POST",
			data: post,
			cache: false,
			success: function(c){
				if(c != ""){
					$(".temp_message").html(c);
				}
			}
		});
	    return false;
	});
	$('.answer_message_temp').keyup(function() {
	    var keyed = $(this).val();
	    post = {'answer_message_temp':'1','message':keyed};
		$.ajax({
			url: '',
			global: false,
			type: "POST",
			dataType: 'json',
			data: post,
			cache: false,
			success: function(c){

			}
		});
	    return false;
	});
	document.getElementById("answer_message").addEventListener('keydown',function(e){
	    if(e.keyCode === 9) {
	        var start = this.selectionStart;
	        var end = this.selectionEnd;

	        var target = e.target;
	        var value = target.value;

	        target.value = value.substring(0, start)
	                    + "\t"
	                    + value.substring(end);

	        this.selectionStart = this.selectionEnd = start + 1;
	        e.preventDefault();
	    }
	},false);

    $("#advancedClose").click(function(){
        $("#advancedOpen").slideToggle(500);
    });
});
</script>