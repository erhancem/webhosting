<?php
include("settings.php");
if(strstr($_SERVER['REQUEST_URI'],'?')){
    $logout_url = $_SERVER['REQUEST_URI'].'&logout=2';
}else{
    $logout_url = $_SERVER['REQUEST_URI'].'?logout=2';
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>CemBilisim.Com - Yönetim Paneli</title>
    <meta name="content-language" content="tr" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <!-- CSS Dosyalarim -->
    <link rel="stylesheet" href="<?=$admindir?>/css/default.css"/>
    <link rel="stylesheet" href="<?=$admindir?>/css/responsive.css"/>
    <link rel="stylesheet" href="<?=$admindir?>/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="<?=$admindir?>/css/bootstrap.min.css"/>
</head>
<body> <!--ZamanSay(31)-->
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?=$admindir?>/"><img src="<?=$admindir?>/images/cem_logo.png" /></a>
        </div>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?=$admindir?>/"><?=cm_lang('Giriş')?></a></li>
                <li><a href="javascript:;" onclick="pageCall('left_menu','<?=$admindir?>/menu/menu_members.php');pageCall('right_content','<?=$admindir?>/members.php');"><?=cm_lang('Üyeler')?></a></li>
                <li><a href="javascript:;" onclick="pageCall('left_menu','<?=$admindir?>/menu/menu_domain.php');pageCall('right_content','<?=$admindir?>/domain_list.php');"><?=cm_lang('Domainler')?></a></li>
                <li><a href="javascript:;" onclick="pageCall('left_menu','<?=$admindir?>/menu/menu_hosting.php');pageCall('right_content','<?=$admindir?>/hosting_list.php');"><?=cm_lang('Hostingler')?></a></li>
                <li><a href="#">Sunucular</a></li>
                <li><a href="#">Siparişler</a></li>
                <li><a href="javascript:;" onclick="pageCall('left_menu','<?=$admindir?>/menu/menu_products.php');pageCall('right_content','<?=$admindir?>/products.php');"><?=cm_lang('Ürünler')?></a></li>
                <li><a href="javascript:;" onclick="pageCall('left_menu','<?=$admindir?>/menu/menu_module.php');pageCall('right_content','<?=$admindir?>/modules.php');"><?=cm_lang('Modüller')?></a></li>
                <li><a href="javascript:;" onclick="pageCall('left_menu','<?=$admindir?>/menu/menu_plugins.php');pageCall('right_content','<?=$admindir?>/plugins.php');"><?=cm_lang('Eklentiler')?></a></li>
                <li><a href="#">Ayarlar</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="clearfix"></div>
<div class="col-md-2 left_menu" style="padding: 0px;margin: 0px;">
    <!--<iframe id="left_menu" name="left_menu" style="display:block; width:100%; height:100vh;" height="100%" width="100%" frameborder="0" align="top" src="<?=$admindir?>/menu/menu_home.php"></iframe>-->
</div>
<div class="col-md-10 right_content" style="padding: 0px;margin: 0px;">
    <!--<iframe id="right_content" name="right_content" style="display:block; width:100%; height:100vh;" height="100%" width="100%" frameborder="0" align="top" src="<?=$admindir?>/home.php"></iframe>-->
</div>
<div class="clearfix"></div>
<div class="footer">
    <ul>
        <li><a href="#"><?=cm_lang('Bilgilerim')?></a></li>
        <li><a href="#"><?=cm_lang('Ayarlarım')?></a></li>
        <li><a href="<?=$logout_url?>"><?=cm_lang('Çıkış Yap')?></a></li>
        <li><i class="fa fa-map-marker" aria-hidden="true"></i> <?=cm_lang('IP adresiniz')?>: <strong><?php echo $ip;?></strong> <?=cm_lang('kayıt edildi')?>.</li>
    </ul>
</div>
<script src="<?=$admindir?>/js/jquery-3.2.0.min.js"></script>
<script src="<?=$admindir?>/js/bootstrap.min.js"></script>
<script src="<?=$admindir?>/js/default.js"></script>
<script>
    $(document).ready(function(){
        pageCall('left_menu','<?=$admindir?>/menu/menu_home.php');
        pageCall('right_content','<?=$admindir?>/home.php');
    });
    function pageCall(name,url){
        $.ajax({
            url: url,
            global: false,
            type: "GET",
            cache: false,
            success: function(c){
                var matches = c.match(/<body onload="ZamanSay\(31\)"><(.*)<\/body>/);
                console.log(matches);
                //$("."+name).html(c);
            }
        });
        return false;
    }
    function pageCall2(name,url){
        var selector = '.nav li';
        $(selector).on('click', function(){
            $(selector).removeClass('active');
            $(this).addClass('active');
        });
        var myframe = document.getElementById(name);
        if(myframe !== null){
            if(myframe.src){
                myframe.src = url;
            }else if(myframe.contentWindow !== null && myframe.contentWindow.location !== null){
                myframe.contentWindow.location = url;
            }else{
                myframe.setAttribute('src', url);
            }
        }
    }
</script>
</body>
</html>