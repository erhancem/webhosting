<?php
if(!isset($admindir)){
	include("settings.php");
}
$PageParams = array();
$p		= cm_get_request('page');
$page	= 1;
if($p and cm_numeric($p) and $p > 0){
	$page = $p;
}
$filtre = array(
	1 => array(
			"name" 	=> "Kullanıcı Adına Göre",
			"query"	=> 'u.username'
		),
	2 => array(
			"name" 	=> "Eposta Hesabına Göre",
			"query"	=> 'u.email_address'
		),
	3 => array(
			"name" 	=> "Sunucu IP adresine Göre",
			"query"	=> 's.server_ip'
		),
	4 => array(
			"name" 	=> "Ad Soyada Göre",
			"query"	=> "CONCAT(u.first_name,' ',u.last_name)"
		),
	5 => array(
			"name" 	=> "Sunucu ID Göre",
			"query"	=> "s.server_id"
		)
);
$OrderBy 	= 's.server_id DESC';
$odr 		= cm_get_request('order');
if($odr and cm_numeric($odr)){
	$Sira = array(
		1 => "s.server_id DESC",
		2 => "s.server_id ASC",
		3 => "s.server_create_time DESC",
		4 => "s.server_create_time ASC",
		5 => "s.server_end_time DESC",
		6 => "s.server_end_time ASC",
	);
	if(isset($Sira[$odr])){
		$OrderBy = $Sira[$odr];
		$PageParams["order"] = $odr;
	}
}
$Where		= '';
$WhereAry	= array();
$status		= cm_get_request('status');
if(cm_numeric($status) and in_array($status,array_keys($server_status))){
	$WhereAry[] = "s.server_status='".$cm_db->sql_escape($status)."' AND s.server_delete=0";
	$PageParams["status"] = $status;
}
$delete		= cm_get_request('delete');
if(cm_numeric($delete) and $delete == 1){
	$WhereAry[] = "s.server_delete=1";
	$PageParams["delete"] = $delete;
}
$serverid = cm_get_request('id');
if(cm_numeric($serverid) and $serverid > 0){
	$WhereAry[] = "s.server_id='".$cm_db->sql_escape($serverid)."'";
	$PageParams["id"] = $serverid;
}
$user_id = cm_get_request('user_id');
if(cm_numeric($user_id) and $user_id > 0){
	$WhereAry[] = "s.user_id='".$cm_db->sql_escape($user_id)."'";
	$PageParams["user_id"] = $user_id;
}
$close = cm_get_request('close');
if(cm_numeric($close) and $close > 0){
	$WhereAry[] = "s.server_end_time < ".cm_time()." AND s.server_status NOT IN (3)";
	$PageParams["close"] = $close;
}
$open = cm_get_request('open');
if(cm_numeric($open) and $open > 0){
	$WhereAry[] = "s.server_end_time > ".cm_time()." AND s.server_status in (3)";
	$PageParams["open"] = $open;
}
$search			= cm_get_request('search');
if($search and $search != ""){
	if(!cm_get_request('search_type')){
	    $SQLLike = array(
	        "t.ticket_subject like '%".$cm_db->sql_escape($search)."%'",
	        "u.email_address like '%".$cm_db->sql_escape($search)."%'",
	        "u.username like '%".$cm_db->sql_escape($search)."%'",
	        "s.server_ip like '%".$cm_db->sql_escape($search)."%'",
	        "CONCAT(u.first_name,' ',u.last_name) like '%".$cm_db->sql_escape($search)."%'"
	    );
	    $WhereAry[] = implode(" OR ",$SQLLike);
	    $PageParams["search"] = $search;
	}elseif(cm_get_request('search_type') and cm_get_request("symbol")){
		if(isset($filtre[$_REQUEST["search_type"]])){
			if(isset($Isaret[$_REQUEST["symbol"]])){
				$WhereAry[] = str_ireplace(array("##name##","##value##"),array($filtre[$_REQUEST["search_type"]]["query"],$cm_db->sql_escape($search)),$Isaret[$_REQUEST["symbol"]]["query"]);
			}
		}
	}
}
if(count($WhereAry) > 0){
	$Where = " WHERE ".implode(" AND ",$WhereAry);
}
$orderUrl = $PageParams;
if(array_key_exists('order',$orderUrl)){
	unset($orderUrl['order']);
}
if(count($orderUrl) > 0){
	$orderUrl = $admindir.'/server_list.php?'.http_build_query($orderUrl).'&';
}else{
	$orderUrl = $admindir.'/server_list.php?';
}
include("navtop.php");
?>
<div class="main">
    <div class="main-content width100">
        <div class="row">
            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading"><?=cm_lang('Sunucular')?></div>
              <div class="panel-body">
    <div class="col-md-12" style="margin-bottom: 15px;">
    <div class="col-md-6"><div class="pull-left" style="padding: 10px;"></div></div>
    <form action="" method="GET" class="" role="search">
	<div class="col-md-6">
        <div id="custom-search-input">
            <div class="input-group  pull-right">
                <div class="form-group">
                    <div style="float: left;"><input type="text" class="form-control" style="float: left;" name="search" value="<?=(cm_get_request('search')?cm_htmlclear(cm_get_request('search')):null)?>" placeholder="<?=cm_lang('Arama Yap')?>..."/></div>
                    <div style="float: left;">
                        <button class="btn btn-danger" style="border-radius: 0px 5px 5px 0;" type="submit">
                            <span class="fa fa-search"></span>
                        </button>
                    </div>
                	<div style="float: left;margin-left: 5px;"><a href="javascript:;" class="btn btn-info" style="float: left;" id="advancedClose"> <?=cm_lang('Gelişmiş')?></a></div>
                </div>
            </div>
        </div>
    </div>
	<div class="clearfix"></div>
	<div class="col-md-12" id="advancedOpen" style="display: none;">
	    <div class="panel panel-yellow">
	        <div class="panel-heading"><?=cm_lang('GELİŞMİŞ ARAMA')?></div>
             <div class="panel-body OtoScroll">
             	<div class="col-md-1"></div>
                 <div class="col-md-4">
                    <div class="row">
                    <select class="form-control search_type">
                        <option value=""><?=cm_lang('Arama Türünü Seçin')?></option>
                        <? foreach($filtre as $key=>$value){ ?>
                        	<option value="<?=$key?>"><?=cm_lang($value["name"])?></option>
                        <? } ?>
                    </select>
                    </div>
                 </div>
                 <div class="col-md-1"></div>
                 <div class="col-md-4">
                    <div class="row">
                    <select class="form-control symbol">
                        <? foreach($Isaret as $key=>$value){ ?>
                        	<option value="<?=$key?>"><?=$value["name"]?></option>
                        <? } ?>
                    </select>
                    </div>
                 </div>
             </div>
	    </div>
	</div>
	</form>
	<div class="clearfix"></div>
</div>
                <table class="table table-bordered table-hover results">
                    <thead>
                      <tr>
                        <th><?=cm_lang('ID')?> <a href="<?=$orderUrl?>order=<?=$odr==1?'2':'1'?>" style="color: white;"><i class="fa fa-fw fa-sort"></i></a></th>
                        <th><?=cm_lang('Ürün/Hizmet')?></th>
                        <th><?=cm_lang('Sunucu IP')?></th>
                        <th><?=cm_lang('Üye')?></th>
                        <th><?=cm_lang('Başlama Tarih')?> <a href="<?=$orderUrl?>order=<?=$odr==3?'4':'3'?>" style="color: white;"><i class="fa fa-fw fa-sort"></i></a></th>
                        <th><?=cm_lang('Bitiş Tarih')?> <a href="<?=$orderUrl?>order=<?=$odr==5?'6':'5'?>" style="color: white;"><i class="fa fa-fw fa-sort"></i></a></th>
                        <th><?=cm_lang('Durumu')?></th>
                        <th><?=cm_lang('İşlem')?></th>
                      </tr>
                    </thead>
                    <tbody>
<?php
$TotalPage = 0;
if($Where == ""){
	$queryTotal = "SHOW TABLE STATUS FROM `".$cm_config['db_name']."` WHERE Name = '".$cm_config["db_prefix"]."servers';";
}else{
    $queryTotal	= "SELECT COUNT(s.server_id) as Rows FROM ".$cm_config["db_prefix"]."servers AS s LEFT JOIN ".$cm_config["db_prefix"]."users AS u ON s.user_id=u.user_id".$Where;
}
$TotalList = $cm_db->sql_query($queryTotal);
if($cm_db->sql_errno() == 0){
	$TotalRows = $cm_db->sql_fetch_assoc($TotalList);
    $page = ($page>$TotalRows["Rows"]?1:$page); //Sayfa kontrolü
	if($TotalRows["Rows"] > 0){
		$ListStart		= (($page-1)*$ListTotalRows+1)-1;	//sql alınacak kayıt
		$x				= 10;							//aktif sayfadan önceki/sonraki sayfa gösterim sayısı
		$next_page		= $page + 1;					//sonraki sayfa
		$previous_page	= $page - 1;					//önceki sayfa
		$TotalPage 		= ceil($TotalRows["Rows"]/$ListTotalRows);
    	$ListQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config["db_prefix"]."servers AS s LEFT JOIN ".$cm_config["db_prefix"]."users AS u ON s.user_id=u.user_id LEFT JOIN ".$cm_config["db_prefix"]."products AS p ON s.product_id=p.product_id".$Where." ORDER BY ".$OrderBy." LIMIT ".$ListStart.",".$ListTotalRows);
    	if($cm_db->sql_errno() == 0){
        	while($List = $cm_db->sql_fetch_assoc($ListQuery)){
        		$usr 		= '<a href="'.$admindir.'/members.php?id='.$List['user_id'].'">'.cm_user_name($List).'</a>';
        		$start_time	= cm_date(null,$List['server_create_time']);
        		$end_time	= cm_date(null,$List['server_end_time']);
        		$list_status = '<span style="color: '.$server_status[$List['server_status']]['color'].';"><strong>'.cm_lang($server_status[$List['server_status']]['name']).'</strong></span>';
        		$statusA = 'status='.$List['server_status'];
        		if($List['server_delete'] == 1){
        			$list_status = '<span style="color: '.$delete_status[$List['server_delete']]['color'].';"><strong>'.cm_lang($delete_status[$List['server_delete']]['name']).'</strong></span>';
        			$statusA = 'delete=1';
        		}
?>
                      <tr>
                        <td><?=cm_htmlclear($List['server_id'])?></td>
                        <td><?=cm_htmlclear($List['name'])?></td>
                        <td><?=cm_htmlclear($List['server_ip'])?></td>
                        <td><?=$usr?></td>
                        <td><?=$start_time?></td>
                        <td><?=$end_time?></td>
                        <td><a href="<?=$admindir?>/server_list.php?<?=$statusA?>"><?=$list_status?></a></td>
                        <td><a href="<?=$admindir?>/server.php?id=<?=$List['server_id']?>" class="btn btn-sm btn-primary" title="<?=cm_lang('Düzenle')?>" alt="<?=cm_lang('Düzenle')?>" data-toggle="tooltip" title="<?=cm_lang('Düzenle')?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                      </tr>
<?php }
}else{
	$errorMessage = cm_lang('Hata')." ".$cm_db->sql_error();
}
}else{
	$errorMessage = cm_lang('Kayıt bulunamadı');
}
}else{
	$errorMessage = cm_lang('hata')." : ".$cm_db->sql_error();
}
if(isset($errorMessage)){
?>
						<tr>
							<td colspan="8" style="text-align: center;font-weight: bold;"><?=$errorMessage?></td>
						</tr>
<?php } ?>

                    </tbody>
                </table>
                <?php
                echo PageList(array("TotalPage"=>$TotalPage,"Page"=>$page,"parametre"=>http_build_query($PageParams)));
                ?>
              </div>
        </div>
    </div>
	</div>
</div>
<?php include("footer.php");?>
<script>
$(document).ready(function(){
    $("#advancedClose").click(function(){
        $("#advancedOpen").slideToggle(500);
    });
});
$('.search_type').change(function(){
	if(this.value != ''){
		$('.search_type').attr("name","search_type");
		$('.symbol').attr("name","symbol");
	}else{
		$('.search_type').removeAttr("name");
		$('.symbol').removeAttr("name");
	}
});
</script>