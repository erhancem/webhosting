<?php
if(!isset($admindir)){
	include("settings.php");
}
$PageParams = array();
$p		= cm_get_request('page');
$page	= 1;
if($p and cm_numeric($p)){
	$page = $p;
}
$filtre = array(
	1 => array(
			"name" 	=> "Kullanıcı Adına Göre",
			"query"	=> 'u.username'
		),
	2 => array(
			"name" 	=> "Eposta Hesabına Göre",
			"query"	=> 'u.email_address'
		),
	3 => array(
			"name" 	=> "Üye ID Göre",
			"query"	=> 'u.user_id'
		),
	4 => array(
			"name" 	=> "Ad Soyada Göre",
			"query"	=> "CONCAT(u.first_name,' ',u.last_name)"
		),
	5 => array(
			"name" 	=> "Üye Kimlik No Göre",
			"query"	=> "u.country_no"
		)
);
$OrderBy 	= 'u.user_id DESC';
$odr 		= cm_get_request('order');
if($odr and cm_numeric($odr)){
	$Sira = array(
		1 => "d.domain_id DESC",
		2 => "d.domain_id ASC",
		3 => "d.domain_create_time DESC",
		4 => "d.domain_create_time ASC",
		5 => "d.domain_name DESC",
		6 => "d.domain_name ASC",
	);
	if(isset($Sira[$odr])){
		$OrderBy = $Sira[$odr];
		$PageParams["order"] = $Sira[$odr];
	}
}
$Where		= '';
$WhereAry	= array();
$userid		= cm_get_request('id');
if(cm_numeric($userid) and $userid > 0){
	$WhereAry[] = "u.user_id='".$cm_db->sql_escape($userid)."'";
}
$search			= cm_get_request('search');
if($search and $search != ""){
	if(!cm_get_request('search_type')){
	    $SQLLike = array(
	        "u.country_no like '%".$cm_db->sql_escape($search)."%'",
	        "u.email_address like '%".$cm_db->sql_escape($search)."%'",
	        "u.username like '%".$cm_db->sql_escape($search)."%'",
	        "CONCAT(u.first_name,' ',u.last_name) like '%".$cm_db->sql_escape($search)."%'"
	    );
	    $WhereAry[] = implode(" OR ",$SQLLike);
	    $PageParams["search"] = $search;
	}elseif(cm_get_request('search_type') and cm_get_request("symbol")){
		if(isset($filtre[$_REQUEST["search_type"]])){
			if(isset($Isaret[$_REQUEST["symbol"]])){
				$WhereAry[] = str_ireplace(array("##name##","##value##"),array($filtre[$_REQUEST["search_type"]]["query"],$cm_db->sql_escape($search)),$Isaret[$_REQUEST["symbol"]]["query"]);
			}
		}
	}
}
if(count($WhereAry) > 0){
	$Where = " WHERE ".implode(" AND ",$WhereAry);
}
include("navtop.php");
?>
<div class="main">
    <div class="main-content width100">
        <div class="row">
            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading"><?=cm_lang('Üyeler')?></div>
              <div class="panel-body">
    <div class="col-md-12" style="margin-bottom: 15px;">
    <div class="col-md-6"><div class="pull-left" style="padding: 10px;"><?php  /** echo implode(" ",$SiralamaDizi); */ ?><!--Üye ID (<a href="?sirala=uye_id+DESC">9-0</a> , <a href="?sirala=uye_id+ASC">0-9</a>) Rütbe (<a href="#">9-0</a> , <a href="#">0-9</a>) Kredi (<a href="#">9-0</a> , <a href="#">0-9</a>) Blk Kredi (<a href="#">9-0</a> , <a href="#">0-9</a>) Borç (<a href="#">9-0</a> , <a href="#">0-9</a>) EkVrsy (<a href="#">9-0</a> , <a href="#">0-9</a>) Admin Not (<a href="#">Z-A</a> , <a href="#">A-Z</a>)--></div></div>
    <form action="" method="GET" class="" role="search">
	<div class="col-md-6">
        <div id="custom-search-input">
            <div class="input-group  pull-right">
                <div class="form-group">
                    <div style="float: left;"><input type="text" class="form-control" style="float: left;" name="search" value="<?=(cm_get_request('search')?cm_htmlclear(cm_get_request('search')):null)?>" placeholder="<?=cm_lang('Arama Yap')?>..."/></div>
                    <div style="float: left;">
                        <button class="btn btn-danger" style="border-radius: 0px 5px 5px 0;" type="submit">
                            <span class="fa fa-search"></span>
                        </button>
                    </div>
                	<div style="float: left;margin-left: 5px;"><a href="javascript:;" class="btn btn-info" style="float: left;" id="advancedClose"> <?=cm_lang('Gelişmiş')?></a></div>
                </div>
            </div>
        </div>
    </div>
	<div class="clearfix"></div>
	<div class="col-md-12" id="advancedOpen" style="display: none;">
	    <div class="panel panel-yellow">
	        <div class="panel-heading"><?=cm_lang('GELİŞMİŞ ARAMA')?></div>
             <div class="panel-body OtoScroll">
             	<div class="col-md-1"></div>
                 <div class="col-md-4">
                    <div class="row">
                    <select class="form-control search_type">
                        <option value=""><?=cm_lang('Arama Türünü Seçin')?></option>
                        <? foreach($filtre as $key=>$value){ ?>
                        	<option value="<?=$key?>"><?=cm_lang($value["name"])?></option>
                        <? } ?>
                    </select>
                    </div>
                 </div>
                 <div class="col-md-1"></div>
                 <div class="col-md-4">
                    <div class="row">
                    <select class="form-control symbol">
                        <? foreach($Isaret as $key=>$value){ ?>
                        	<option value="<?=$key?>"><?=$value["name"]?></option>
                        <? } ?>
                    </select>
                    </div>
                 </div>
             </div>
	    </div>
	</div>
	</form>
	<div class="clearfix"></div>
</div>
                <table class="table table-bordered table-hover results">
                    <thead>
                      <tr>
                        <th><?=cm_lang('ID')?></th>
                        <th><?=cm_lang('Adı Soyad')?></th>
                        <th><?=cm_lang('Kullanıcı')?></th>
                        <th><?=cm_lang('Email Adresi')?></th>
                        <th><?=cm_lang('Kayıt Tarihi')?></th>
                        <th><?=cm_lang('İşlem')?></th>
                      </tr>
                    </thead>
                    <tbody>
<?php
$TotalPage = 0;
if($Where == ""){
	$queryTotal = "SHOW TABLE STATUS FROM `".$cm_config['db_name']."` WHERE Name = '".$cm_config["db_prefix"]."users';";
}else{
    $queryTotal	= "SELECT COUNT(u.user_id) as Rows from ".$cm_config["db_prefix"]."users AS u".$Where;
}
$TotalList = $cm_db->sql_query($queryTotal);
if($cm_db->sql_errno() == 0){
	$TotalRows = $cm_db->sql_fetch_assoc($TotalList);
    $page = ($page>$TotalRows["Rows"]?1:$page); //Sayfa kontrolü
	if($TotalRows["Rows"] > 0){
		$ListStart		= (($page-1)*$ListTotalRows+1)-1;	//sql alınacak kayıt
		$x				= 10;							//aktif sayfadan önceki/sonraki sayfa gösterim sayısı
		$next_page		= $page + 1;					//sonraki sayfa
		$previous_page	= $page - 1;					//önceki sayfa
		$TotalPage 		= ceil($TotalRows["Rows"]/$ListTotalRows);
    	$ListQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config["db_prefix"]."users AS u".$Where." ORDER BY ".$OrderBy." LIMIT ".$ListStart.",".$ListTotalRows);
    	if($cm_db->sql_errno() == 0){
        	while($List = $cm_db->sql_fetch_assoc($ListQuery)){
        		$usr 		= '<a href="'.$admindir.'/members.php?user_id='.$List['user_id'].'">'.cm_htmlclear($List['first_name'].' '.$List['last_name']).'<br />'.cm_htmlclear($List['username']).'</a>';
        		$start_time	= cm_date(null,$List['registration_date']);
?>
                      <tr>
                        <td><?=$List['user_id']?></td>
                        <td><?=cm_htmlclear($List['first_name'].' '.$List['last_name'])?></td>
                        <td><?=cm_htmlclear($List['username'])?></td>
                        <td><?=cm_htmlclear($List['email_address'])?></td>
                        <td><?=$start_time?></td>
                        <td><a href="<?=$admindir?>/member.php?id=<?=$List['user_id']?>" class="btn btn-sm btn-primary" title="Düzenle" alt="Düzenle" data-toggle="tooltip" title="Düzenle"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a> </td>
                      </tr>
<?php }
}else{
	$errorMessage = cm_lang('Hata')." ".$cm_db->sql_error();
}
}else{
	$errorMessage = cm_lang('Kayıt bulunamadı');
}
}else{
	$errorMessage = cm_lang('hata')." : ".$cm_db->sql_error();
}
if(isset($errorMessage)){
?>
						<tr>
							<td colspan="6" style="text-align: center;font-weight: bold;"><?=$errorMessage?></td>
						</tr>
<?php } ?>

                    </tbody>
                </table>
                <?php
                echo PageList(array("TotalPage"=>$TotalPage,"Page"=>$page,"parametre"=>http_build_query($PageParams)));
                ?>
              </div>
        </div>
    </div>
	</div>
</div>
<?php include("footer.php");?>
<script>
$(document).ready(function(){
    $("#advancedClose").click(function(){
        $("#advancedOpen").slideToggle(500);
    });
});
$('.search_type').change(function(){
	if(this.value != ''){
		$('.search_type').attr("name","search_type");
		$('.symbol').attr("name","symbol");
	}else{
		$('.search_type').removeAttr("name");
		$('.symbol').removeAttr("name");
	}
});
</script>