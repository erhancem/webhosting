<?php
include('inc/header.php');
?>
<section>
    <div class="VDSBG">
        <div class="container">
<?php if(stristr($package,'vds')){ ?>
            <h1 class="text-center">VDS Sunucu Paketleri</h1>
<?php }elseif(stristr($package,'vps')){ ?>
			<h1 class="text-center">VPS Sunucu Paketleri</h1>
<?php }elseif(stristr($package,'dedicated')){ ?>
			<h1 class="text-center">Fiziksel Sunucu Paketleri</h1>
<?php } ?>
        </div>
    </div>
<div class="container">
    <div class="BosBG">
      <div class="Bilgi">
<?php if(stristr($package,'vds')){ ?>
        <h2>VDS sunucu nedir?</h2>
        <p>VDS, ana sunucunun <strong>donanımsal</strong> olarak bölümlenmesiyle elde edilir. Bu şekilde sanallaştırılan VDS'ler, tamamen birbirinden bağımsız birer küçük sunucu haline gelirler. her biri farklı işletim sistemine sahip olabilir, her birinin kaynak kullanımı birbirinden tamamen bağımsızdır.</p>
<?php }elseif(stristr($package,'vps')){ ?>
		<h2>VPS sunucu nedir?</h2>
        <p>VPS, ana sunucunun <strong>yazılımsal</strong> olarak bölünmesiyle elde edilir. Bu sanallaştırma sisteminde, o sunucudaki bütün VPS'ler ana sunucuda yüklü olan işletim sistemini kullanmak zorundadır ve hepsi bu işletim sistemine bağlı olarak çalışan küçük parçacıklar gibidirler.</p>
<?php }elseif(stristr($package,'dedicated')){ ?>
		<h2>Fiziksel sunucu nedir?</h2>
        <p>Fiziksel sunucu <strong>datacenter</strong> ortamında tamamen size ayrılmış bir bilgisayardır. Özel olarak ayarlanmış yada daha önceden ayarlanmış stanadart paketler olmak üzere fiziksel ram,cpu,anakartı bulunan bir bilgisayardır.</p>
<?php } ?>
        <div class="clearfix"></div>
      </div>
<?php if(stristr($package,'vds')){ ?>
        <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="cloud-tab-content">
                    <div class="col-md-8 col-sm-12">
                    <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 1</h2></div>
                    <hr class="goster" style="display: none;">
                        <ul class="cloud-property">
                            <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
                                <span>CPU</span>
                                <h3>1 CORE</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
                                <span>RAM</span>
                                <h3>1 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
                                <span>Sabit Disk (HDD)</span>
                                <h3>20 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
                                <span>AYLIK TRAFİK</span>
                                <h3>Limitsiz</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
                                <span>IP ADRESİ</span>
                                <h3>1 Adet</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12">
                            <div class="pricebox">
                                <p class="pricenew">
                                    <span class="fiyatust">VDS Paket 1</span><br>
                                    <span class="fiyatorta">30.00 <i class="fa fa-try" aria-hidden="true"></i></span><br>
                                    <span class="fiyatalt">TÜRK LİRASI</span>
                                </p>
                            </div>
                        <div class="cloud-right">
                            <a class="buy" href="/portal/cart.php?a=add&amp;pid=23"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
                        </div>
                    </div>
        <div class="clearfix"></div>
                </div>
        <div class="clearfix"></div>
                <div class="cloud-tab-content">
                    <div class="col-md-8 col-sm-12">
                    <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 2</h2></div>
                    <hr class="goster" style="display: none;">
                        <ul class="cloud-property">
                            <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
                                <span>CPU</span>
                                <h3>2 CORE</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
                                <span>RAM</span>
                                <h3>2 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
                                <span>Sabit Disk (HDD)</span>
                                <h3>30 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
                                <span>AYLIK TRAFİK</span>
                                <h3>Limitsiz</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
                                <span>IP ADRESİ</span>
                                <h3>1 Adet</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12">
                            <div class="pricebox">
                                <p class="pricenew">
                                    <span class="fiyatust">VDS Paket 2</span><br>
                                    <span class="fiyatorta">40.00 <i class="fa fa-try" aria-hidden="true"></i></span><br>
                                    <span class="fiyatalt">TÜRK LİRASI</span>
                                </p>
                            </div>
                        <div class="cloud-right">
                            <a class="buy" href="/portal/cart.php?a=add&amp;pid=24"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
                        </div>
                    </div>
        <div class="clearfix"></div>
                </div>
        <div class="clearfix"></div>
                <div class="cloud-tab-content">
                    <div class="col-md-8 col-sm-12">
                    <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 3</h2></div>
                    <hr class="goster" style="display: none;">
                        <ul class="cloud-property">
                            <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
                                <span>CPU</span>
                                <h3>3 CORE</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
                                <span>RAM</span>
                                <h3>3 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
                                <span>Sabit Disk</span>
                                <h3>40 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
                                <span>AYLIK TRAFİK</span>
                                <h3>Limitsiz</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
                                <span>IP ADRESİ</span>
                                <h3>1 Adet</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12">
                            <div class="pricebox">
                                <p class="pricenew">
                                    <span class="fiyatust">VDS Paket 3</span><br>
                                    <span class="fiyatorta">50.00 <i class="fa fa-try" aria-hidden="true"></i></span><br>
                                    <span class="fiyatalt">TÜRK LİRASI</span>
                                </p>
                            </div>
                        <div class="cloud-right">
                            <a class="buy" href="/portal/cart.php?a=add&amp;pid=25"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
                        </div>
                    </div>
        <div class="clearfix"></div>
                </div>
        <div class="clearfix"></div>
                <div class="cloud-tab-content">
                    <div class="col-md-8 col-sm-12">
                    <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 4</h2></div>
                    <hr class="goster" style="display: none;">
                        <ul class="cloud-property">
                            <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
                                <span>CPU</span>
                                <h3>4 CORE</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
                                <span>RAM</span>
                                <h3>4 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
                                <span>Sabit Disk</span>
                                <h3>50 GB</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
                                <span>AYLIK TRAFİK</span>
                                <h3>Limitsiz</h3>
                            </li>
                            <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
                                <span>IP ADRESİ</span>
                                <h3>1 Adet</h3>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-sm-12">
                            <div class="pricebox">
                                <p class="pricenew">
                                    <span class="fiyatust">VDS Paket 4</span><br>
                                    <span class="fiyatorta">60.00 <i class="fa fa-try" aria-hidden="true"></i></span><br>
                                    <span class="fiyatalt">TÜRK LİRASI</span>
                                </p>
                            </div>
                        <div class="cloud-right">
                            <a class="buy" href="/portal/cart.php?a=add&amp;pid=26"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
                        </div>
                    </div>
        			<div class="clearfix"></div>
                </div>
        		<div class="clearfix"></div>
              </div>
            </div>
<?php }elseif(stristr($package,'vps')){ ?>
			<div class="row">
		      <div class="col-lg-12 col-md-12">
		        <div class="cloud-tab-content">
		            <div class="col-md-8 col-sm-12">
		            <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 1</h2></div>
		            <hr class="goster" style="display: none;">
		                <ul class="cloud-property">
		                    <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
		                        <span>CPU</span>
		                        <h3>1 CORE</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
		                        <span>RAM</span>
		                        <h3>1 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
		                        <span>Sabit Disk (HDD)</span>
		                        <h3>20 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
		                        <span>AYLIK TRAFİK</span>
		                        <h3>Limitsiz</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
		                        <span>IP ADRESİ</span>
		                        <h3>1 Adet</h3>
		                    </li>
		                </ul>
		            </div>
		            <div class="col-md-4 col-sm-12">
		                    <div class="pricebox">
		                        <p class="pricenew">
		                            <span class="fiyatust">VDS Paket 1</span><br>
		                            <span class="fiyatorta">30.00 <i class="fa fa-try" aria-hidden="true"></i></span><br />
		                            <span class="fiyatalt">TÜRK LİRASI</span>
		                        </p>
		                    </div>
		                <div class="cloud-right">
		                    <a class="buy" href="<?=$directory?>/sunucu/vps?product_id=1&action=add"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
		                </div>
		            </div>
		<div class="clearfix"></div>
		        </div>
		<div class="clearfix"></div>
		        <div class="cloud-tab-content">
		            <div class="col-md-8 col-sm-12">
		            <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 2</h2></div>
		            <hr class="goster" style="display: none;">
		                <ul class="cloud-property">
		                    <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
		                        <span>CPU</span>
		                        <h3>2 CORE</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
		                        <span>RAM</span>
		                        <h3>2 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
		                        <span>Sabit Disk (HDD)</span>
		                        <h3>30 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
		                        <span>AYLIK TRAFİK</span>
		                        <h3>Limitsiz</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
		                        <span>IP ADRESİ</span>
		                        <h3>1 Adet</h3>
		                    </li>
		                </ul>
		            </div>
		            <div class="col-md-4 col-sm-12">
		                    <div class="pricebox">
		                        <p class="pricenew">
		                            <span class="fiyatust">VDS Paket 2</span><br>
		                            <span class="fiyatorta">40.00 <i class="fa fa-try" aria-hidden="true"></i></span><br>
		                            <span class="fiyatalt">TÜRK LİRASI</span>
		                        </p>
		                    </div>
		                <div class="cloud-right">
		                    <a class="buy" href="/portal/cart.php?a=add&amp;pid=24"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
		                </div>
		            </div>
		<div class="clearfix"></div>
		        </div>
		<div class="clearfix"></div>
		        <div class="cloud-tab-content">
		            <div class="col-md-8 col-sm-12">
		            <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 3</h2></div>
		            <hr class="goster" style="display: none;">
		                <ul class="cloud-property">
		                    <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
		                        <span>CPU</span>
		                        <h3>3 CORE</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
		                        <span>RAM</span>
		                        <h3>3 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
		                        <span>Sabit Disk</span>
		                        <h3>40 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
		                        <span>AYLIK TRAFİK</span>
		                        <h3>Limitsiz</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
		                        <span>IP ADRESİ</span>
		                        <h3>1 Adet</h3>
		                    </li>
		                </ul>
		            </div>
		            <div class="col-md-4 col-sm-12">
		                    <div class="pricebox">
		                        <p class="pricenew">
		                            <span class="fiyatust">VDS Paket 3</span><br>
		                            <span class="fiyatorta">50.00 <i class="fa fa-try" aria-hidden="true"></i></span><br>
		                            <span class="fiyatalt">TÜRK LİRASI</span>
		                        </p>
		                    </div>
		                <div class="cloud-right">
		                    <a class="buy" href="/portal/cart.php?a=add&amp;pid=25"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
		                </div>
		            </div>
		<div class="clearfix"></div>
		        </div>
		<div class="clearfix"></div>
		        <div class="cloud-tab-content">
		            <div class="col-md-8 col-sm-12">
		            <div class="PaketAdi text-center" style="display: none;"><h2>VDS PAKET 4</h2></div>
		            <hr class="goster" style="display: none;">
		                <ul class="cloud-property">
		                    <li><div class="icon"><div><span class="icon-cloud1"></span></div></div>
		                        <span>CPU</span>
		                        <h3>4 CORE</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud2"></span></div></div>
		                        <span>RAM</span>
		                        <h3>4 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud3"></span></div></div>
		                        <span>Sabit Disk</span>
		                        <h3>50 GB</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud4"></span></div></div>
		                        <span>AYLIK TRAFİK</span>
		                        <h3>Limitsiz</h3>
		                    </li>
		                    <li><div class="icon"><div><span class="icon-cloud5"></span></div></div>
		                        <span>IP ADRESİ</span>
		                        <h3>1 Adet</h3>
		                    </li>
		                </ul>
		            </div>
		            <div class="col-md-4 col-sm-12">
		                    <div class="pricebox">
		                        <p class="pricenew">
		                            <span class="fiyatust">VDS Paket 4</span><br>
		                            <span class="fiyatorta">60.00 <i class="fa fa-try" aria-hidden="true"></i></span><br>
		                            <span class="fiyatalt">TÜRK LİRASI</span>
		                        </p>
		                    </div>
		                <div class="cloud-right">
		                    <a class="buy" href="/portal/cart.php?a=add&amp;pid=26"><i class="fa fa-cloud fa-3x" aria-hidden="true"></i> <span>SATIN AL</span></a>
		                </div>
		            </div>
		<div class="clearfix"></div>
		        </div>
		<div class="clearfix"></div>
		      </div>
		    </div>
<?php }elseif(stristr($package,'dedicated')){ ?>
			<div class="row">
		      <div class="col-lg-12 col-md-12">
		        <div class="fizikselbg">
		          <div class="row">
		          <div class="col-md-3"><img src="images/r610.png"></div>
		          <div class="col-md-6">
		            <h3>Dell PowerEdge R610</h3>
		            <ul>
		              <li>Intel Xeon E5 2620</li>
		              <li>4GB 1333 MHz Ram</li>
		              <li>500 GB SAS Disk</li>
		              <li>Limitsiz Trafik</li>
		              <li><strong>KVM Desteği:</strong> Yok</li>
		              <li><strong>Port Hızı:</strong> 1 GBit/s</li>
		              <li><strong>Hat Hızı:</strong> 100 Mbit/s</li>
		            </ul>
		          </div>
		          <hr class="goster" style="display: none;">
		          <div class="col-md-3">
		               <div class="ffiyatbg"><h3>TR Plan-100</h3><div class="ffiyat">Fiyat: <strong>170 <i class="fa fa-try" aria-hidden="true"></i></strong></div>
		                    <a href="#" class="btn btn-destek" style="padding: 20px 30%;color: #fff;">Satın Al</a>
		                    <div class="clearfix"></div>
		               </div>
		               <div class="clearfix"></div>
		          </div>
		          </div>
		        </div>
		        <div class="clearfix"></div>
		        <div class="fizikselbg">
		          <div class="row">
		          <div class="col-md-3"><img src="images/r610.png"></div>
		          <div class="col-md-6">
		            <h3>Dell PowerEdge R610</h3>
		            <ul>
		              <li>Intel Xeon E5 2620</li>
		              <li>8GB 1333 MHz Ram</li>
		              <li>1 TB GB SAS Disk</li>
		              <li>Limitsiz Trafik</li>
		              <li><strong>KVM Desteği:</strong> Yok</li>
		              <li><strong>Port Hızı:</strong> 1 GBit/s</li>
		              <li><strong>Hat Hızı:</strong> 100 Mbit/s</li>
		            </ul>
		          </div>
		          <hr class="goster" style="display: none;">
		          <div class="col-md-3">
		               <div class="ffiyatbg"><h3>TR Plan-101</h3><div class="ffiyat">Fiyat: <strong>190 <i class="fa fa-try" aria-hidden="true"></i></strong></div>
		                    <a href="#" class="btn btn-destek" style="padding: 20px 30%;color: #fff;">Satın Al</a>
		                    <div class="clearfix"></div>
		               </div>
		               <div class="clearfix"></div>
		          </div>
		          </div>
		        </div>
		        <div class="clearfix"></div>
		        <div class="fizikselbg">
		          <div class="row">
		          <div class="col-md-3"><img src="images/r610.png"></div>
		          <div class="col-md-6">
		            <h3>Dell PowerEdge R610</h3>
		            <ul>
		              <li>Intel Xeon E5 2620</li>
		              <li>16 GB 1333 MHz Ram</li>
		              <li>1 TB SAS Disk</li>
		              <li>Limitsiz Trafik</li>
		              <li><strong>KVM Desteği:</strong> Yok</li>
		              <li><strong>Port Hızı:</strong> 1 GBit/s</li>
		              <li><strong>Hat Hızı:</strong> 200 Mbit/s</li>
		            </ul>
		          </div>
		          <hr class="goster" style="display: none;">
		          <div class="col-md-3">
		               <div class="ffiyatbg"><h3>TR Plan-102</h3><div class="ffiyat">Fiyat: <strong>220 <i class="fa fa-try" aria-hidden="true"></i></strong></div>
		                    <a href="#" class="btn btn-destek" style="padding: 20px 30%;color: #fff;">Satın Al</a>
		                    <div class="clearfix"></div>
		               </div>
		               <div class="clearfix"></div>
		          </div>
		          </div>
		        </div>
		        <div class="clearfix"></div>
		        <div class="fizikselbg">
		          <div class="row">
		          <div class="col-md-3"><img src="images/r610.png"></div>
		          <div class="col-md-6">
		            <h3>Dell PowerEdge R610</h3>
		            <ul>
		              <li>Intel Xeon E5 2620</li>
		              <li>32 GB 1333 MHz Ram</li>
		              <li>1 TB SAS Disk</li>
		              <li>Limitsiz Trafik</li>
		              <li><strong>KVM Desteği:</strong> Yok</li>
		              <li><strong>Port Hızı:</strong> 1 GBit/s</li>
		              <li><strong>Hat Hızı:</strong> 200 Mbit/s</li>
		            </ul>
		          </div>
		          <hr class="goster" style="display: none;">
		          <div class="col-md-3">
		               <div class="ffiyatbg"><h3>TR Plan-103</h3><div class="ffiyat">Fiyat: <strong>250 <i class="fa fa-try" aria-hidden="true"></i></strong></div>
		                    <a href="#" class="btn btn-destek" style="padding: 20px 30%;color: #fff;">Satın Al</a>
		                    <div class="clearfix"></div>
		               </div>
		               <div class="clearfix"></div>
		          </div>
		          </div>
		        </div>
		        <div class="clearfix"></div>
		        <div class="fizikselbg">
		          <div class="row">
		          <div class="col-md-3"><img src="images/r610.png"></div>
		          <div class="col-md-6">
		            <h3>Dell PowerEdge R610</h3>
		            <ul>
		              <li>Intel Xeon E5 2620</li>
		              <li>32 GB 1333 MHz Ram</li>
		              <li>240 GB <strong style="color: #F44336;">SSD</strong> Disk</li>
		              <li>Limitsiz Trafik</li>
		              <li><strong>KVM Desteği:</strong> Yok</li>
		              <li><strong>Port Hızı:</strong> 1 GBit/s</li>
		              <li><strong>Hat Hızı:</strong> 500 Mbit/s</li>
		            </ul>
		          </div>
		          <hr class="goster" style="display: none;">
		          <div class="col-md-3">
		               <div class="ffiyatbg"><h3>TR Plan-104</h3><div class="ffiyat">Fiyat: <strong>350 <i class="fa fa-try" aria-hidden="true"></i></strong></div>
		                    <a href="#" class="btn btn-destek" style="padding: 20px 30%;color: #fff;">Satın Al</a>
		                    <div class="clearfix"></div>
		               </div>
		               <div class="clearfix"></div>
		          </div>
		          </div>
		        </div>
		        <div class="clearfix"></div>
		        <div class="fizikselbg">
		          <div class="row">
		          <div class="col-md-3"><img src="images/r610.png"></div>
		          <div class="col-md-6">
		            <h3>Dell PowerEdge R610</h3>
		            <ul>
		              <li>Intel Xeon E5 2620</li>
		              <li>32 GB 1333 MHz Ram</li>
		              <li>2 TB SAS Disk</li>
		              <li>Limitsiz Trafik</li>
		              <li><strong>KVM Desteği:</strong> Yok</li>
		              <li><strong>Port Hızı:</strong> 1 GBit/s</li>
		              <li><strong>Hat Hızı:</strong> 500 Mbit/s</li>
		            </ul>
		          </div>
		          <hr class="goster" style="display: none;">
		          <div class="col-md-3">
		               <div class="ffiyatbg"><h3>TR Plan-105</h3><div class="ffiyat">Fiyat: <strong>380 <i class="fa fa-try" aria-hidden="true"></i></strong></div>
		                    <a href="#" class="btn btn-destek" style="padding: 20px 30%;color: #fff;">Satın Al</a>
		                    <div class="clearfix"></div>
		               </div>
		               <div class="clearfix"></div>
		          </div>
		          </div>
		        </div>
		        <div class="clearfix"></div>
		        <div class="GenelBilgi">
		            <h1>Genel Bilgi;</h1>
		            <p>Stok durumuna göre <strong>Dell PowerEdge R610</strong> yada <strong>Dell PowerEdge R620</strong> yada <strong>Dell PowerEdge R720</strong> temin edilebilmektedir.</p>
		        </div>
		        <h1 class="text-center">Ortak Özellikler</h1>
		        <div class="col-md-4">
		            <ul class="list-group">
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>HTML,PHP Desteği</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Zend Optimizer Desteği</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>IonCube Desteği</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>cURL Desteği</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>GD Grafik Kütüphanesi</li>
		            </ul>
		        </div>
		        <div class="col-md-4">
		            <ul class="list-group">
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Mail Yönlendirme</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>(Cron) Zamanlanmış Görev Yönetimi</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Kontrol Panelde Yazma İzni Ayarları</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>WebMail Desteği</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Türkçe Kontrol Panel</li>
		            </ul>
		        </div>
		        <div class="col-md-4">
		            <ul class="list-group">
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Pop3,Smtp ve Imap Desteği</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Frontpage Desteği</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Kişisel Hata Sayfaları</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Spam Filtresi ve Anti-Virus Koruması</li>
		                <li class="list-group-item"><span class="badge"><i class="fa fa-cogs" aria-hidden="true"></i></span>Web Sayfası İstatistikleri</li>
		            </ul>
		        </div>
		      </div>
		    </div>
<?php } ?>
    </div>
</div>
<div class="clearfix"></div>
<div class="Destek-area">
    <div class="col-md-12">
        <div class="row">
            <div class="container">
                <div class="LinuxPaketler" style="width: 320px;">
                    <h1><strong>7/24</strong> Destek</h1>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-8">
                <h5>Tüm soru ve sorunlarınızda yanınızda olduğumuzu bilmenizi ister, bizleri 7 gün 24 saat hiç çekinmeden arayabilir veya bizden teknik destek alabilirsiniz.</h5>
                  <ul>
                    <li><h3>TELEFON & E-MAİL</h3>
                        <span>Ekibimiz 7/24 iş başındadır. Taleplerinize en kısa sürede yanıt vermek için çalışmaktayız.</span>
                    </li>
                    <li><h3>MÜŞTERİ PANELİ</h3>
                        <span>Satın almış olduğunuz hizmetlerle ilgili tüm işlemlerinizi tamamen kullanıcı ve müşteri odaklı geliştirdiğimiz kontrol panelimizden gerçekleştirebilirsiniz..</span>
                    </li>
                    <li><h3>CANLI DESTEK</h3>
                        <span>Canlı destek üzerinde günün her saati her dakikası satış öncesi sorularınızı sorabilir, bizden bütün hizmetlerimiz hakkında bilgi alabilirsiniz.</span>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="MutluMuster">
    <div class="container">
        <section id="carousel">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-8 col-md-offset-2">
                        <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
        				<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
        				  <!-- Carousel indicators -->
                          <ol class="carousel-indicators">
        				    <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
        				    <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
        				    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
        				  </ol>
        				  <!-- Carousel items -->
        				  <div class="carousel-inner">
        				    <div class="item active">
                                <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
        				    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
        				    <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
        				    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
        				    <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
        				    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
                            <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
            			    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
                            <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
            			    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
                            <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
            			    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
        				  </div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
    </div>
</div>
<div class="clearfix"></div>
<div class="cozum">
    <div class="container">
        <div class="row">
    		<div class="col-md-12">
               <div id="Carousel" class="carousel slide">
                    <div class="carousel-inner">
                    <div class="item active">
                    	<div class="row">
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/drupal.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/joomla.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/jquery.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/magento.png" alt="Image" /></a></div>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="row">
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/mysql.png" alt="Image" /></a></div>
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/python.png" alt="Image" /></a></div>
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/wordpress.png" alt="Image" /></a></div>
                    	</div>
                    </div>
                    </div>
                      <a data-slide="prev" href="#Carousel" class="left carousel-control"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a>
                      <a data-slide="next" href="#Carousel" class="right carousel-control"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
               </div>
    		</div>
    	</div>
    </div>
</div>
<?php include('inc/footer.php'); ?>