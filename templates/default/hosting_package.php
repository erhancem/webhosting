<?php
include('inc/header.php');
?>
<section>
    <div class="HostingBG">
        <div class="container">
            <h1 class="text-center"><?php if($package_type == 1){ ?><?=cm_lang('Linux Hosting Paketleri')?><?php }else{ ?><?=cm_lang('Windows Hosting Paketleri')?><?php } ?></h1>
        </div>
    </div>
<div class="container">
    <div class="BosBG">
        <div id="flip-scroll">
<?php if($package_type == 1){ ?>
            <table class="host_paket table-bordered table-striped table-condensed table-responsive cf">
              <thead class="cf">
            	  <tr>
            		  <th>ÖZELLİKLER</th>
            		  <th>LİNUX-1</th>
            		  <th>LİNUX-2</th>
            		  <th>LİNUX-3</th>
            		  <th>LİNUX-4</th>
            	  </tr>
              </thead>
            	<tbody>
            		<tr>
            			<td class="text-bold">DİSK ALANI</td>
            			<td>150 MB</td>
            			<td>200 MB</td>
            			<td>250 MB</td>
            			<td>300 MB</td>
            		</tr>
            		<tr>
            			<td class="text-bold">AYLIK TRAFİK</td>
            			<td>150 MB</td>
            			<td>200 MB</td>
            			<td>250 MB</td>
            			<td>300 MB</td>
            		</tr>
            		<tr>
            			<td class="text-bold">BARINDIRMA</td>
            			<td>150 MB</td>
            			<td>200 MB</td>
            			<td>250 MB</td>
            			<td>300 MB</td>
            		</tr>
            		<tr>
            			<td class="text-bold">E-POSTA HESABI</td>
            			<td>150 MB</td>
            			<td>200 MB</td>
            			<td>250 MB</td>
            			<td>300 MB</td>
            		</tr>
                    <tr>
                        <td></td>
                        <td><a href="?product_id=4&action=add" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
                        <td><a href="/basket?cm_action=basket&product_id=5&action=add" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
                        <td><a href="/basket?cm_action=basket&product_id=6&action=add" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
                        <td><a href="#" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
                    </tr>
            	</tbody>
            </table>
<?php }else{ ?>
			<table class="host_paket table-bordered table-striped table-condensed cf">
              <thead class="cf">
            	  <tr>
            		  <th>ÖZELLİKLER</th>
            		  <th>WİNDOWS-1</th>
            		  <th>WİNDOWS-2</th>
            		  <th>WİNDOWS-3</th>
            		  <th>WİNDOWS-4</th>
            		  <th>WİNDOWS-5</th>
            	  </tr>
              </thead>
            	<tbody>
            		<tr>
            			<td class="text-bold">DİSK ALANI</td>
            			<td>150 MB</td>
            			<td>200 MB</td>
            			<td>250 MB</td>
            			<td>300 MB</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">AYLIK TRAFİK</td>
            			<td>1 GB</td>
            			<td>5 GB</td>
            			<td>10 GB</td>
            			<td>15 GB</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">BARINDIRMA</td>
            			<td>1 ADET</td>
            			<td>5 ADET</td>
            			<td>10 ADET</td>
            			<td>15 ADET</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">E-POSTA HESABI</td>
            			<td>1 ADET</td>
            			<td>5 ADET</td>
            			<td>10 ADET</td>
            			<td>15 ADET</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">SUBDOMAIN</td>
            			<td>1 ADET</td>
            			<td>5 ADET</td>
            			<td>10 ADET</td>
            			<td>15 ADET</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">MYSQL VERİTABANI</td>
            			<td>1 ADET</td>
            			<td>5 ADET</td>
            			<td>10 ADET</td>
            			<td>15 ADET</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">FTP HESABI</td>
            			<td>1 ADET</td>
            			<td>5 ADET</td>
            			<td>10 ADET</td>
            			<td>15 ADET</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">PARK DOMAIN</td>
            			<td>1 ADET</td>
            			<td>5 ADET</td>
            			<td>10 ADET</td>
            			<td>15 ADET</td>
            			<td>Limitsiz</td>
            		</tr>
            		<tr>
            			<td class="text-bold">SATIN AL</td>
            			<td><a href="#" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
            			<td><a href="#" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
            			<td><a href="#" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
            			<td><a href="#" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
            			<td><a href="#" class="btn-sepet"><i class="fa fa-shopping-cart"></i> SATIN AL</a></td>
            		</tr>
            	</tbody>
            </table>
<?php } ?>
        </div>
    </div>
</div>
<?php
include('detail.php');
include('inc/footer.php');
?>