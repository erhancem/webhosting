{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="container">
        <div class="TalepOkuIc">
            <div class="page-header">
                <h1>#{$ticket['ticket_id']} <small class="pull-right">{cm_htmlclear($ticket['ticket_subject'])}</small></h1>
            </div>
            <div class="row talepbilgi" style="margin-bottom: 15px;">
                <div class="col-md-4">
                    <p>{cm_lang('Talep Bildirimi')}</p>
                    <span style="text-align: center;display: block;">{cm_date(null,$ticket['ticket_start_time'])}</span>
                </div>
                <div class="col-md-4">
                    <p>{cm_lang('Departman')}</p>
                    <span style="text-align: center;display: block;">{cm_htmlclear($department['ticket_dep_name'])}</span>
                </div>
                <div class="col-md-4">
                    <p>{cm_lang('Durumu')}</p>
                    <span style="text-align: center;display: block;"><strong>{$ticketstatus}</strong></span>
                </div>
            </div>
            {foreach from=$ticketList key=key item=ticketanswer}
            <div class="row TalepGenel">
                <div class="col-md-2 Mgizle">
                    <img src="{$directory}/templates/{$template}/{if $ticketanswer['user_id']==$ticket['user_id']}images/destek_avatar_m.png{else}images/destek_avatar.png{/if}" class="img-rounded img-responsive" draggable="false" />
                </div>
                <div class="col-md-10 col-xs-12">
                    <div class="panel panel-{if $ticketanswer['user_id']==$ticket['user_id']}info{else}success{/if}">
                        <div class="panel-heading">
                            <h3 class="panel-title pull-left">{cm_user_name($ticketanswer)}</h3>
                            <div class="date text-right">{cm_date(null,$ticketanswer['ticket_answer_time'])}</div>
                        </div>
                        <div class="panel-body DestekMesaj">
                            {cm_textshow($ticketanswer['ticket_message'])}
                            {if $ticketanswer['user_id'] != $ticket['user_id']}
                            <hr />
                            <span>{cm_lang('Bu cevaba nasıl puan verirdiniz')}?
                            <ul>
                                <li><span><i class="fa fa-star-o" aria-hidden="true"></i></span></li>
                                <li><span><i class="fa fa-star-o" aria-hidden="true"></i></span></li>
                                <li><span><i class="fa fa-star-o" aria-hidden="true"></i></span></li>
                                <li><span><i class="fa fa-star-o" aria-hidden="true"></i></span></li>
                                <li><span><i class="fa fa-star-o" aria-hidden="true"></i></span></li>
                            </ul>
                        </span>
                            {/if}
                            {if is_array($ticketanswer['files'])}
                            <hr />{cm_lang('Ek Dosyalar')} :
                            {implode(" , ",$ticketanswer['files'])}
                            {/if}
                        </div>
                    </div>
                </div>
            </div>
            <hr />
            {/foreach}
        </div>
        <p>
            <input type="button" value="<< {cm_lang('Geri')}" class="btn" onclick="window.location='{$directory}/tickets'" />
            <input type="button" value="{cm_lang('Yanıtla')}" class="btn btn-destek" style="color: #fff;" onclick="jQuery('#cevapAc').slideToggle()" />
        </p>
        <div id="cevapAc" class="ticketreplybox" style="display: {if $action=='answer'}block{else}none{/if};">
            <form method="POST" action="" enctype="multipart/form-data">
                <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                <input type="hidden" name="action" value="answer" />
                {if $action=='answer'}
                <div class="{if $cm_status==true}BasariliMesaji{else}HataMesaji{/if}">{$cm_message}</div>
                {/if}
                <div class="form-group">
                    <label for="name">{cm_lang('İsim')}</label>
                    <input class="form-control disabled" type="text" id="name" value="{cm_user_name($user)}" disabled="disabled" />
                </div>

                <div class="form-group">
                    <label for="email">{cm_lang('E-posta Adresi')}</label>
                    <input class="form-control disabled" type="text" id="email" value="{cm_htmlclear($user['email_address'])}" disabled="disabled" />
                </div>
                <div class="form-group">
                    <label for="message">{cm_lang('Mesaj')}</label>
                    <textarea name="message" id="message" rows="12" class="form-control" required="">{cm_htmlclear(cm_get_request('ticket_message'))}</textarea>
                </div>
                <div class="control-group">
                    <label class="control-label bold" for="attachments">{cm_lang('Eklentiler')}:</label>
                    <div class="controls">
                        <input type="file" name="attachments[]" style="width:70%;" multiple="" /><br />
                        ({cm_lang('İzin verilen dosya türleri')}: .jpg, .gif, .jpeg, .png)
                    </div>
                </div>
                <br />
                <p><button class="btn btn-destek" style="color: #fff;">{cm_lang('Yanıtla')}</button></p>
            </form>
        </div>
    </div>
</div>
{include file="inc/footer.tpl"}