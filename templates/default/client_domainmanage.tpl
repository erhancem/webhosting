{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left"><i class="fa fa-globe" aria-hidden="true"></i> {cm_htmlclear($dmname)} {cm_lang('Alan Adı Yönetimi')}</div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <div class="row">
                <div class="col-sm-3">
                    <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well DomainList">
                        <li class="active"><a href="#detail" data-toggle="tab">{cm_lang('Alan Adı Bilgisi')}</a></li>
                        <li><a href="#dns" data-toggle="tab">DNS Yönlendirme</a></li>
                        <li><a href="#contact" data-toggle="tab">İletişim Bilgileri</a></li>
                        <li><a href="#authcode" data-toggle="tab">{cm_lang('Transfer Kod Al')}</a></li>
                        <li><a href="#cns" data-toggle="tab">{cm_lang('Alt İsim Sunucu')}</a></li>
                        <li><a href="#protection" data-toggle="tab">Güvenlik Koruması</a></li>
                    </ul>
                </div>
                <div class="col-sm-9">
                    <div class="tab-content">

                        <div role="tabpanel" class="tab-pane fade in active" id="detail">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="pull-left">{cm_lang('Alan Adı Bilgilerim')}</div>
                                        <div class="pull-right"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" style="overflow: auto;">
                                        <table class="table table-responsive DestekTab">
                                            <tbody>
                                            <tr>
                                                <th style="border: none!important;font-weight: bold; text-align: right;">{cm_lang('Alan Adı')} :</th>
                                                <td style="border: none!important;text-align: left;">{$domain_name}</td>
                                            </tr>
                                            <tr>
                                                <th style="border: none!important;font-weight: bold; text-align: right;">{cm_lang('Kayıt Tarihi')} :</th>
                                                <td style="border: none!important;text-align: left;">{cm_date('d-m-Y',$domain['domain_create_time'])}</td>
                                            </tr>
                                            <tr>
                                                <th style="border: none!important;font-weight: bold; text-align: right;">{cm_lang('Bitiş Tarihi')} :</th>
                                                <td style="border: none!important;text-align: left;">{cm_date('d-m-Y',$domain['domain_end_time'])}</td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="dns">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="pull-left">{cm_lang('DNS Yönetimi')}</div>
                                        <div class="pull-right"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" style="overflow: auto;">
                                        {if is_array($domain['dns'])}
                                            <form name="FormDNS" class="form-horizontal" method="POST" action="">
                                                <input type="hidden" name="cm_action" value="domain" />
                                                <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                                <input type="hidden" name="action" value="dns" />
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Alan Adı')}:</label>
                                                    <div class="col-sm-9">
                                                        <p>{$domain_name}</p>
                                                    </div>
                                                </div>
                                                {for $i=1 to 4}
                                                    {assign var="ns" value="ns{$i}"}
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('##number##. DNS',['number'=>$i])} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="ns[{$i}]" id="ns{$i}" placeholder="{cm_lang('##number##. DNS',['number'=>$i])}" value="{cm_htmlclear($domain['dns'][$ns])}" />
                                                    </div>
                                                </div>
                                                {/for}
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-9">
                                                        <button type="button" onclick="PostAction('FormDNS','id={$domain["domain_id"]}');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşleminiz yapılıyor')}" class="btn btn-default button-FormDNS">
                                                            {cm_lang('DNS Güncelle')}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        {else}
                                            <p style="color: red;">{$domain['dns']}</p>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="contact">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="pull-left">{cm_lang('İletişim Bilgileri')}</div>
                                        <div class="pull-right"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" style="overflow: auto;">
                                        {if $domain['contact']}
                                            <form name="FormContact" class="form-horizontal" method="POST" action="" onsubmit="return false;">
                                                <input type="hidden" name="cm_action" value="domain" />
                                                <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('İletişim Durumu')}:</label>
                                                    <div class="col-sm-9">
                                                        <p>
                                                        {if $domain['contact_verify']==1}
                                                            <span style="color: green;">{cm_lang('Onaylı')}</span>
                                                        {else}
                                                            <span style="color: red;">{cm_lang('Onaysız')}</span>
                                                        {/if}
                                                        </strong></p>
                                                    </div>
                                                </div>
                                                <div class="form-group companyName">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Firma Adı')} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" name="company_name" class="form-control companyInput" id="company_name" placeholder="{cm_lang('Firma Adı')}" value="{cm_htmlclear($domain['contact']['company_name'])}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Ad Soyad')} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="name" id="name" placeholder="{cm_lang('Ad Soyad')}" value="{cm_htmlclear($domain['contact']['name'])}" required="" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Email Adresi')} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="email" class="form-control" name="email_address" id="email_address" placeholder="{cm_lang('Email Adresi')}" value="{cm_htmlclear($domain['contact']['email_address'])}" />
                                                        <p style="margin-top: 15px;"><strong>{cm_lang('Not')}</strong> : {cm_lang('Mail adresi değiştirilir ise şuanki ve yeni mail adresine onaylama email gönderilir ve onaylamanız gerekir onay verilmez ise alan adı sağlayıcı tarafından suspend olur')}</p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="email">{cm_lang('Telefon')} :</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control phone_code" name="phone_code" id="phone_code" readonly="" value="{cm_htmlclear($domain['contact']['phone_code'])}" />
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" id="phone_number" name="phone_number" value="{cm_htmlclear($domain['contact']['phone_number'])}" required=""  />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="email">{cm_lang('Faks')} :</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control phone_code" name="fax_code" id="fax_code" readonly="" value="{cm_htmlclear($domain['contact']['fax_code'])}" />
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <input type="text" class="form-control" id="fax_number" name="fax_number" value="{cm_htmlclear($domain['contact']['fax_number'])}"  />
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Ülke')} :</label>
                                                    <div class="col-sm-9">
                                                        <select name="country" class="form-control countrySelect" onchange="countryChange();">
                                                            <option value="">{cm_lang('Lütfen ülke seçin')}</option>
                                                            {foreach from=$countryList key=key item=country}
                                                                <option value="{$country['country_code']}" title="{$country['country_phone_code']}"{if $domain['contact']['country']==$country['country_code']} selected=""{/if}>{$country['country_name']}</option>
                                                            {/foreach}
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Şehir')} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="city" id="city" placeholder="{cm_lang('Şehir')}" value="{cm_htmlclear($domain['contact']['city'])}" required="" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('İlçe')}:</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="state" id="state" placeholder="{cm_lang('İlçe')}" value="{cm_htmlclear($domain['contact']['state'])}" required="" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Adres 1')}:</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="address1" id="address1" placeholder="{cm_lang('Adres 1')}" value="{cm_htmlclear($domain['contact']['address1'])}" required="" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Adres 2')} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="address2" id="address2" placeholder="{cm_lang('Adres 2')}" value="{cm_htmlclear($domain['contact']['address2'])}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Adres 3')} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="address3" id="address3" placeholder="{cm_lang('Adres 3')}" value="{cm_htmlclear($domain['contact']['address3'])}" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Posta Kodu')} :</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="postcode" id="postcode" placeholder="{cm_lang('Posta Kodu')}" value="{cm_htmlclear($domain['contact']['postcode'])}" required="" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-offset-3 col-sm-9">
                                                        <button type="button" onclick="PostAction('FormContact','action=contact&id={$domain["domain_id"]}');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşleminiz yapılıyor')}" class="btn btn-default button-FormContact">
                                                            {cm_lang('Güncelle')}
                                                        </button>
                                                        <button type="button" onclick="PostAction('FormContactControl','action=contact_verify&id={$domain["domain_id"]}');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşleminiz yapılıyor')}" class="contactcontrol btn btn-danger button-FormContactControl">
                                                            {cm_lang('Onay Kontrol')}
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        {else}
                                            <p style="color: red;">{$domain['contact']}</p>
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="authcode">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="pull-left">{cm_lang('Transfer Kodu Öğren')}</div>
                                        <div class="pull-right"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" style="overflow: auto;">
                                        <form name="FormTransferCode" class="form-horizontal" method="POST" action="">
                                            <input type="hidden" name="cm_action" value="domain" />
                                            <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                            <input type="hidden" name="action" value="transfercode" />
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="pwd">{cm_lang('Transfer Durumu')} :</label>
                                                <div class="col-sm-9">
                                                    <p><strong>{if $domain['domain_transfer_protection']==0}<span style="color: green;">{cm_lang('Transfer edilebilir')}</span>{else}<span style="color: red;">{cm_lang('Transfer edilemez güvenlik ayarlarından transfer izin verin')}</span>{/if}</strong></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="pwd">{cm_lang('Transfer Kodunu Gönder')}:</label>
                                                <div class="col-sm-9">
                                                    <label class="radio-inline"><input type="radio" name="sendtype" value="1" />{cm_lang('WHOIS Adresimdeki Email adresime gönderilsin')}</label>
                                                    <label class="radio-inline"><input type="radio" name="sendtype" value="2" />{cm_lang('Üyeliğimdeki Email adresine gönderilsin')}</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="button" onclick="PostAction('FormTransferCode','id={$domain["domain_id"]}');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşleminiz yapılıyor')}" class="btn btn-default button-FormTransferCode">
                                                        {cm_lang('Gönder')}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="cns">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="pull-left">{cm_lang('Alt İsim Sunucu')}</div>
                                        <div class="pull-right"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" style="overflow: auto;">
                                        <form name="FormCNS" class="form-horizontal" method="POST" action="" onsubmit="return false;">
                                            <input type="hidden" name="cm_action" value="domain" />
                                            <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                            <input type="hidden" name="action" value="cns_add" />
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="pwd">{cm_lang('Alan Adı')}:</label>
                                                <div class="col-sm-9">
                                                    <p>{$domain_name}</p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="email">{cm_lang('NS / IP')} :</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" name="cns_name" id="cns_name" value="" placeholder="{cm_lang('NS Adresi')}" required="" />
                                                </div>
                                                <div class="col-sm-5">
                                                    <input type="text" class="form-control" id="cns_ip" name="cns_ip" value="" placeholder="{cm_lang('IP Adresi')}" required=""  />
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="button" onclick="PostAction('FormCNS','id={$domain["domain_id"]}');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşleminiz yapılıyor')}" class="btn btn-default button-FormCNS">
                                                        {cm_lang('NS Ekle')}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        {if is_array($domain['ns'])}
                                            {foreach from=$domain['ns'] key=key item=value}
                                                <form name="FormCNSdel" class="form-horizontal" method="POST" action="" onsubmit="return false;">
                                                    <input type="hidden" name="cm_action" value="domain" />
                                                    <input type="hidden" name="action" value="cns_delete" />
                                                    <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                                    <div class="form-group">
                                                        <label class="control-label col-sm-3" for="email">{cm_lang('NS / IP')} :</label>
                                                        <div class="col-sm-4">
                                                            <input type="text" class="form-control" name="cns_name" id="cns_name" readonly="" value="{cm_htmlclear($value['ns'])}" />
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <input type="text" class="form-control" name="cns_ip" id="cns_ip" readonly="" value="{cm_htmlclear($value['ip'])}" />
                                                        </div>
                                                        <div class="col-sm-2">
                                                            <button type="button" onclick="PostAction('FormCNSdel','id={$domain["domain_id"]}');" data-loading-text="<i class='fa fa-spinner fa-spin'></i>" class="btn btn-danger button-FormCNSdel">
                                                                {cm_lang('Sil')}
                                                            </button>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </form>
                                            {/foreach}
                                        {/if}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="protection">
                            <div class="col-md-12">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <div class="pull-left">{cm_lang('Güvenlik Ayarları')}</div>
                                        <div class="pull-right"></div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="panel-body" style="overflow: auto;">
                                        <form name="FormProtection" class="form-horizontal" method="POST" action="" onsubmit="return false;">
                                            <input type="hidden" name="cm_action" value="domain" />
                                            <input type="hidden" name="action" value="transfer_lock" />
                                            <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                            <div class="form-group">
                                                <label class="control-label col-sm-3" for="pwd">{cm_lang('Transfer Kilidi')}:</label>
                                                <div class="col-sm-9">
                                                    <label class="radio-inline"><input type="radio" name="status" value="1"{if $domain['domain_transfer_protection']==1} checked=""{/if} />{cm_lang('Transfer Edilemez')}</label>
                                                    <label class="radio-inline"><input type="radio" name="status" value="0"{if $domain['domain_transfer_protection']==0} checked=""{/if} />{cm_lang('Transfer Edilebilir')}</label>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                    <button type="button" onclick="PostAction('FormProtection','id={$domain["domain_id"]}');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşlem Yapılıyor')}" class="btn btn-default button-FormProtection">
                                                        {cm_lang('Kaydet')}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
{include file="inc/footer.tpl"}