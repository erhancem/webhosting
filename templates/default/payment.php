<?php
$theme['login'] = true;
include('control.php');
$module_type	= 'payment';
$order_id		= cm_get_request('id');
$module_id		= cm_get_request('module')?cm_get_request('module'):null;
if($order_id == false or ($order_id and !cm_numeric($order_id))){ cm_url_go('orders'); }
$orders_params = array(
	'user_id'	=> $user['user_id'],
	'and'		=> array(
		'order_id'		=> $order_id,
		'order_status'	=> 0,
		'order_delete'	=> 0,
	),
	'orderby'	=> array('order_id'=>'DESC'),
);
$order_list = orderGetList($orders_params);
if($order_list == false or ($order_list and $order_list['total'] == 0)){ cm_url_go('orders'); }

list($order_status,$order_response) = orderControl($order_id,$user);

$order = $order_list['list'][$order_id];
$payments = array();
$payment_moduleX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."modules WHERE module_type='".$module_type."' and module_status=1");
if($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($payment_moduleX) > 0){
	while($payment_module = $cm_db->sql_fetch_assoc($payment_moduleX)){
		$payments[$payment_module['module_id']] = $payment_module;
	}
}

$module_output	= null;
$module_status	= true;
$total_method	= count($payments);
if($module_id != "credit" and $total_method > 0){
	$module_ids = array_keys($payments);
	if($module_id == null or !in_array($module_id,$module_ids)){
		$module_id = $module_ids[0];
	}
	if($module_id){
		cm_function_call($module_type);
		$module_dir	= cm_path.'modules/'.$module_type.'/';
		list($module_status,$module_params) = paymentUserPanel($module_id,$order_id);
		if($module_status == true){
			$params	= $module_params['params'];
			if(!is_array($module_params['module_output'])){
				$module_output = $module_params['module_output'];
			}elseif(array_key_exists("theme",$module_params['module_output'])){
				$module_theme	= $module_dir.$module_params['params']['module']['module'].'/'.$module_params['module_output']['theme'];
				if(file_exists($module_theme)){
					ob_start();
					include($module_theme);
					$module_output = ob_get_contents();
					ob_end_clean();
				}else{
					$module_output = cm_lang('Modülün html kaynağı çekilemedi');
				}
			}elseif(array_key_exists("html",$module_params['module_output'])){
				$module_output	= $module_params['module_output']['html'];
			}else{
				$module_output = cm_lang('Modül html çıktı gelmedi');
			}
		}else{
			$module_output	= $module_params;
		}
	}
}else{
	$module_id = 'credit';
}

include('inc/header.php');
include('inc/hesabim_menu.php');
?>
<div class="BosBG">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="pull-left"><i class="fa fa-money" aria-hidden="true"></i> <?=cm_lang('Ödeme Yap')?></div>
			<div class="pull-right"><strong><?=cm_lang('Sipariş Numarası')?></strong> #<?=$order_id?></div>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body" style="overflow: auto;">
<?php if($order_status == false){ ?>
			<p><strong><?=cm_lang('Uyarı')?></strong> : <?=$order_response?></p>
<?php }else{ ?>
			<ul class="nav nav-tabs">
<?php if($total_method > 0){ foreach($payments as $key=>$value){ ?>
				<li class="<?=$key==$module_id?'active':null?>"><a href="<?=$directory?>/payment?id=<?=$order_id?>&module=<?=$key?>"><?=cm_htmlclear($value['module_name'])?></a></li>
<?php }} ?>
				<li class="<?=$module_id=='credit'?'active':null?>"><a href="<?=$directory?>/payment?id=<?=$order_id?>&module=credit"><?=cm_lang('Üye Kredim')?></a></li>
			</ul>
			<div id="payment<?=$module_id?>" class="tab-content" style="padding: 20px;">
				<div class="tab-pane fade in active">
<?php if($module_output != null){ ?>
					<?php echo $module_output;?>
<?php }else{ ?>
	                <form name="FormDomainTransfer" class="form-horizontal" method="POST" action="">
<?php if(isset($cm_status)){ ?>
						<div class="<?=$cm_status==true?'BasariliMesaji':'HataMesaji'?>"><?=is_array($cm_message)?$cm_message['message']:$cm_message?></div>
<?php } ?>
<?php if($user['credit'] >= $order['paid']){ ?>
						<input type="hidden" name="cm_action" value="payment" />
						<input type="hidden" name="cm_token" value="<?=$cm_set_token?>" />
						<input type="hidden" name="action" value="credit_pay" />
						<input type="hidden" name="order_id" value="<?=$order_id?>" />
<?php } ?>
						<div class="form-group">
							<label class="control-label col-sm-3"><?=cm_lang('Krediniz')?>:</label>
							<div class="col-sm-9">
							  <p><?=cm_price_format($user['credit'],$cm_config['default_currency'])?></p>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-sm-3"><?=cm_lang('Sipariş Toplam')?>:</label>
							<div class="col-sm-9">
							  <p><?=cm_price_format($order['paid'],$order['currency'])?></p>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-3 col-sm-9">
<?php if($user['credit'] >= $order['paid']){ ?>
							  <button type="submit" class="btn btn-success"><?=cm_lang('Öde')?></button>
<?php }else{ ?>
							  <button onclick="return false;" class="btn btn-danger"><?=cm_lang('Kredi Yetersiz')?></button>
<?php } ?>
							</div>
						</div>
					</form>
<?php } ?>
				</div>
			</div>
<?php } ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php include('inc/footer.php'); ?>
<script>
$(document).ready(function(){
	document.getElementById('contentHeader').scrollIntoView();
});
</script>