<?php
include('inc/header.php');
?>
<section>
    <div class="AlanAdiBG"></div>
    <div class="tabbg">
        <div class="container">
            <ul class="nav nav-tabs">
              <li class="Kayit active">
                <a data-toggle="tab" href="#Kayit">
                    <div class="AlanAdiTab">
                        <h1><strong>ALAN ADI</strong> KAYIT</h1>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </li>
              <li class="TopluKayit">
                  <a data-toggle="tab" href="#TopluKayit">
                    <div class="AlanAdiTab">
                        <h1><strong>TOPLU</strong> SORGULAMA</h1>
                        <div class="clearfix"></div>
                    </div>
                  </a>
              </li>
              <li class="Transfer">
                  <a data-toggle="tab" href="#Transfer">
                    <div class="AlanAdiTab">
                        <h1><strong>DOMAİN</strong> TRANSFER</h1>
                        <div class="clearfix"></div>
                    </div>
                  </a>
              </li>
              <li class="tr_kayit">
                  <a data-toggle="tab" href="#TRKayit">
                    <div class="AlanAdiTab">
                        <h1><strong>TR DOMAİN</strong> KAYIT</h1>
                        <div class="clearfix"></div>
                    </div>
                  </a>
              </li>
            </ul>
        </div>
    </div>
<div class="container">
    <div class="BosBG">
        <div class="row">
            <div class="tab-content">
                <div id="Kayit" class="tab-pane fade in active">
                    <div class="col-md-10 col-md-offset-1 margin-top20">
                        <form autocomplate="off" action="" method="POST">
                        	<input type="hidden" value="available" name="action" />
                        	<input type="hidden" value="<?=$cm_set_token?>" name="cm_token" />
                        	<input type="hidden" value="6" id="domain_type" name="domain_type" />
                     	   	<div class="search-domain">
                                <div class="input-group">
                                    <input type="text" id="domain_name" autocomplete="off" name="domain_name" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                                    <span class="input-group-btn">
                                        <button class="btn btn-yesil btn-lg" type="submit" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                </div>
                            </div>
                        </form>
	                    <div class="clearfix"></div>
                    </div>
                </div>
                <div id="TopluKayit" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-12">
                            <form autocomplate="off" action="" method="POST">
	                        	<input type="hidden" value="available" name="action" />
	                        	<input type="hidden" value="<?=$cm_set_token?>" name="cm_token" />
	                        	<input type="hidden" value="6" id="domain_type" name="domain_type" />
                                <div class="search-domain">
                                    <div class="input-group">
                                        <textarea id="multi_domain" rows="5" name="domain_name" class="form-control input-lg" placeholder="Toplu domain kayıt."></textarea>
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-yesil btn-lg" type="submit" id="" data-original-text="<i class='fa fa-search'></i> Sorgula" style="margin: 0px 20px;"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="Transfer" class="tab-pane fade">
                    <div class="col-md-10 col-md-offset-1 margin-top20">
                        <form autocomplate="off" action="" method="POST">
                        	<input type="hidden" value="available" name="action" />
                        	<input type="hidden" value="<?=$cm_set_token?>" name="cm_token" />
                        	<input type="hidden" value="8" id="domain_type" name="domain_type" />
                            <div class="search-domain">
                                <div class="input-group">
                                    <input type="text" autocomplete="off" name="domain_name" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                                    <span class="input-group-btn">
                                        <button class="btn btn-yesil btn-lg" type="submit" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="TRKayit" class="tab-pane fade">
                    <div class="col-md-10 col-md-offset-1 margin-top20">
                        <form autocomplate="off" onsubmit="return false;">
                            <div class="search-domain">
                                <div class="input-group">
                                    <input type="text" id="domain_name_tr" autocomplete="off" name="domain_name_tr" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                                    <span class="input-group-btn">
                                        <button class="btn btn-yesil btn-lg" onclick="domainavailable(document.getElementById('domain_name_tr').value);" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
<?php if($action == 'available'){ ?>
        <div class="AramaSonuc">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed" id="domainTable">
                	<thead>
                		<tr>
                			<th colspan="4" class="text-center"><?=cm_lang('ARAMA SONUÇLARI')?></th>
                		</tr>
                	</thead>
                	<tbody>
<?php if(!is_array($cm_message)){ ?>
						<tr>
							<td colspan="4" style="color: red;"><?=$cm_message?></td>
						</tr>
<?php }else{ ?>
<?php foreach($cm_message as $key=>$value){
$searchStatus = true;
if($value['product_type_id'] == 6){
	if($value['status'] == 1){
		$div_class 	= 'DomainBos';
		$div_write	= cm_lang('MÜSAİT');
	}else{
		$searchStatus = false;
		$div_class 	= 'DomainDolu';
		$div_write	= cm_lang('DOLU');
	}
}elseif($value['product_type_id'] == 8){
	if($value['status'] == 1){
		$div_class 	= 'DomainBos';
		$div_write	= cm_lang('TRANSFER EDİLEBİLİR');
	}else{
		$searchStatus = false;
		$div_class 	= 'DomainDolu';
		$div_write	= cm_lang('TRANSFER EDİLEMEZ');
	}
}
?>
	            		<tr>
	            			<td><?=cm_htmlclear($value['domain'])?></td>
	            			<td><span class="<?=$div_class?>"><?=cm_htmlclear($value['message'])?></span></td>
<?php if($searchStatus == true){ ?>
	            			<td>
<?php if($value['product_type_id'] == 6){ ?>
	                            <select class="form-control" name="year" id="year_<?=$value['name']?>.<?=$value['tld']?>">
<?php foreach($value['price'] as $year=>$val){ ?>
	                                <option value="<?=$year?>"><?=$year?> <?=cm_lang('Yıl')?> / <?=cm_price_format(cm_money_format(($val['price']-$val['discount'])),$val['currency'])?></option>
<?php } ?>
	                            </select>
<?php }elseif($value['product_type_id'] == 8){ ?>
								<input type="text" name="code" id="code<?=$value['name']?>.<?=$value['tld']?>" class="form-control" />
<?php } ?>
	                        </td>
	                        <td><a href="#" class="btn-sepet"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs">Sepete Ekle</span></a></td>
<?php } ?>
	            		</tr>
<?php }} ?>
					</tbody>
                </table>
            </div>
        </div>
<?php } ?>
        <div class="Bilgi">
            <div class="col-md-1"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
            <div class="col-md-11">
                <h2>Domain nedir?</h2>
                 <span><strong>Domain(alan adı)</strong>, internette yer almak için sahip olmamız gereken kimlikdir. Yani alan adları web sitemizin adı ve adresidir. Bu adres olmadan ziyaretçiler sitemize ulaşamazlar.</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="DomainFiyatListesi">
            <div class="LinuxPaketler">
                <h1><strong>Alan Adı</strong> Fiyat Listesi</h1>
                <div class="clearfix"></div>
            </div>
            <table class="table table-bordered table-striped table-hover table-condensed table-responsive">
            	<thead>
            		<tr>
            			<th>KAMPANYA</th>
                        <th>UZANTI</th>
            			<th>YENİ KAYIT ( 1 YIL )</th>
            			<th>TRANSFER</th>
            			<th>YENİLEME ( 1 YIL )</th>
            		</tr>
            	</thead>
            	<tbody>
<?php foreach($Tlds as $key=>$value){
	$Kampanya = array();
	if($value['register_discount'] > 0){
		$Kampanya[] = cm_lang('Kayıt');
	}
	if($value['renewal_discount'] > 0){
		$Kampanya[] = cm_lang('Yenileme');
	}
	if($value['transfer_discount'] > 0){
		$Kampanya[] = cm_lang('Transfer');
	}
?>
            		<tr>
<?php if(count($Kampanya) > 0){ ?>
            			<td><span class="btn-kampanya flashefekt" title="<?=implode(" ",$Kampanya)?>">KAMPANYA</span></td>
<?php }else{ ?>
						<td>-</td>
<?php } ?>
                        <td>.<?=$key?></td>
            			<td><?=cm_price_format($value['register_price'],$value['tld_currency'])?></td>
            			<td><?=cm_price_format($value['transfer_price'],$value['tld_currency'])?></td>
            			<td><?=cm_price_format($value['renewal_price'],$value['tld_currency'])?></td>
            		</tr>
<?php } ?>
            	</tbody>
            </table>
        </div>
    </div>
</div>
<?php
include('detail.php');
include('inc/footer.php');
?>









<?php
include('inc/header.php');
?>
<section>
    <div class="AlanAdiBG"></div>
    <div class="tabbg">
        <div class="container">
            <ul class="nav nav-tabs">
              <li class="Kayit active">
                <a data-toggle="tab" href="#Kayit">
                    <div class="AlanAdiTab">
                        <h1><strong>ALAN ADI</strong> KAYIT</h1>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </li>
              <li class="TopluKayit">
                  <a data-toggle="tab" href="#TopluKayit">
                    <div class="AlanAdiTab">
                        <h1><strong>TOPLU</strong> SORGULAMA</h1>
                        <div class="clearfix"></div>
                    </div>
                  </a>
              </li>
              <li class="Transfer">
                  <a data-toggle="tab" href="#Transfer">
                    <div class="AlanAdiTab">
                        <h1><strong>DOMAİN</strong> TRANSFER</h1>
                        <div class="clearfix"></div>
                    </div>
                  </a>
              </li>
              <li class="tr_kayit">
                  <a data-toggle="tab" href="#TRKayit">
                    <div class="AlanAdiTab">
                        <h1><strong>TR DOMAİN</strong> KAYIT</h1>
                        <div class="clearfix"></div>
                    </div>
                  </a>
              </li>
            </ul>
        </div>
    </div>
<div class="container">
    <div class="BosBG">
        <div class="row">
            <div class="tab-content">
                <div id="Kayit" class="tab-pane fade in active">
                    <div class="col-md-10 col-md-offset-1 margin-top20">
                        <form autocomplate="off" onsubmit="return false;">
                        	<input type="hidden" value="<?=$cm_set_token?>" name="cm_token" id="cm_token" />
                        	<input type="hidden" value="6" id="domain_type" name="domain_type" />
                        	<input type="hidden" value="<?=cm_lang('Sepete Ekle')?>" id="basket_button_value" name="basket_button_value" />
                     	   	<div class="search-domain">
                                <div class="input-group">
                                    <input type="text" id="domain_name" autocomplete="off" name="domain_name" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                                    <span class="input-group-btn">
                                        <button class="btn btn-yesil btn-lg" onclick="domainavailable(document.getElementById('domain_name').value);" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                </div>
                            </div>
                        </form>
	                    <div class="clearfix"></div>
                    </div>
                </div>
                <div id="TopluKayit" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-12">
                            <form autocomplate="off" onsubmit="return false;">
                                <div class="search-domain">
                                    <div class="input-group">
                                        <textarea id="multi_domain" rows="5" name="multi_domain" class="form-control input-lg" placeholder="Toplu domain kayıt."></textarea>
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-yesil btn-lg" onclick="domainavailable(document.getElementById('multi_domain').value);" id="" data-original-text="<i class='fa fa-search'></i> Sorgula" style="margin: 0px 20px;"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div id="Transfer" class="tab-pane fade">
                    <div class="col-md-10 col-md-offset-1 margin-top20">
                        <form autocomplate="off" onsubmit="return false;">
                            <div class="search-domain">
                                <div class="input-group">
                                    <input type="text" id="domain_name_transfer" autocomplete="off" name="domain_name_transfer" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                                    <span class="input-group-btn">
                                        <button class="btn btn-yesil btn-lg" onclick="domainavailable(document.getElementById('domain_name_transfer').value);" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="TRKayit" class="tab-pane fade">
                    <div class="col-md-10 col-md-offset-1 margin-top20">
                        <form autocomplate="off" onsubmit="return false;">
                            <div class="search-domain">
                                <div class="input-group">
                                    <input type="text" id="domain_name_tr" autocomplete="off" name="domain_name_tr" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                                    <span class="input-group-btn">
                                        <button class="btn btn-yesil btn-lg" onclick="domainavailable(document.getElementById('domain_name_tr').value);" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="AramaSonuc">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed domain_register_table" id="domainTable" style="display: none;">
                	<thead>
                		<tr>
                			<th colspan="4" class="text-center">ARAMA SONUÇLARI</th>
                		</tr>
                	</thead>
                	<tbody></tbody>
                </table>
            </div>
        </div>
        <div class="Bilgi">
            <div class="col-md-1"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
            <div class="col-md-11">
                <h2>Domain nedir?</h2>
                 <span><strong>Domain(alan adı)</strong>, internette yer almak için sahip olmamız gereken kimlikdir. Yani alan adları web sitemizin adı ve adresidir. Bu adres olmadan ziyaretçiler sitemize ulaşamazlar.</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="DomainFiyatListesi">
            <div class="LinuxPaketler">
                <h1><strong>Alan Adı</strong> Fiyat Listesi</h1>
                <div class="clearfix"></div>
            </div>
            <table class="table table-bordered table-striped table-hover table-condensed table-responsive">
            	<thead>
            		<tr>
            			<th>KAMPANYA</th>
                        <th>UZANTI</th>
            			<th>YENİ KAYIT ( 1 YIL )</th>
            			<th>TRANSFER</th>
            			<th>YENİLEME ( 1 YIL )</th>
            		</tr>
            	</thead>
            	<tbody>
<?php foreach($Tlds as $key=>$value){
	$Kampanya = array();
	if($value['register_discount'] > 0){
		$Kampanya[] = cm_lang('Kayıt');
	}
	if($value['renewal_discount'] > 0){
		$Kampanya[] = cm_lang('Yenileme');
	}
	if($value['transfer_discount'] > 0){
		$Kampanya[] = cm_lang('Transfer');
	}
?>
            		<tr>
<?php if(count($Kampanya) > 0){ ?>
            			<td><span class="btn-kampanya flashefekt" title="<?=implode(" ",$Kampanya)?>">KAMPANYA</span></td>
<?php }else{ ?>
						<td>-</td>
<?php } ?>
                        <td>.<?=$key?></td>
            			<td><?=cm_price_format($value['register_price'],$value['tld_currency'])?></td>
            			<td><?=cm_price_format($value['transfer_price'],$value['tld_currency'])?></td>
            			<td><?=cm_price_format($value['renewal_price'],$value['tld_currency'])?></td>
            		</tr>
<?php } ?>
            	</tbody>
            </table>
        </div>
    </div>
</div>
<script>
function domain_add(domain,domain_feature){
	$("#"+domain+"button").attr("disabled", true);
	var post = domain_feature+'&cm_action=basket&action=add&format=json';
	$.ajax({
		url: '/post.php',
		global: false,
		type: "POST",
		dataType: 'json',
		data: post,
		cache: false,
		success: function(c){
			if(c.status == true){
				$("#"+domain+"button").css("background-color", "#81bc00");
				$("#"+domain+"button").text("Eklendi");
			}else{
				$("#"+domain+"button").attr("disabled", false);
				alert(c.message);
			}
		}
	});
	return true;
}
</script>
<?php
include('detail.php');
include('inc/footer.php');
?>