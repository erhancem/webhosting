{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="UyeBG">
        <div class="col-md-12">
            <div class="HizmetAramaIc">
                <input type="text" name="" class="form-control HizmetArama" value="" placeholder="{cm_lang('Aramak istediğiniz hizmeti yazınız')}..." /> <i class="fa fa-search" aria-hidden="true"></i>
                <div class="clearfix"></div>
            </div>
            {if $user['user_verify'] == 0 or !is_array($user['address'])}
            <div class="alert alert-danger">
                <strong><i class="fa fa-bullhorn"></i> {cm_lang('Dikkat')}:</strong>
                <div  id="HesabimDuyuru">
                    <ul>
                        {if $user['user_verify'] == 0}
                            <li><a href="{$directory}/client_info.php">{cm_lang('Alışveriş yapabilmeniz için hesabınızı doğrulamanız gerekir')}</a></li>
                        {/if}
                        {if !is_array($user['address'])}
                            <li><a href="{$directory}/client_address.php">{cm_lang('Fatura adresinizi tanımlayınız')}</a></li>
                        {/if}
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
            {/if}
        </div>
        <div class="clearfix"></div>
        <div class="col-md-6">
            <div class="UyeBaslik">
                <i class="fa fa-user fa-2x"></i> <span>{cm_lang('Hesap Bilgilerim')} </span>
            </div>
            <table class="table table-bordred table-striped">
                <tbody>
                <tr>
                    <th><strong>{cm_lang('Adınız Soyadınız')}</strong></th>
                    <td>{cm_user_name($user)}</td>
                </tr>
                <tr>
                    <th><strong>{cm_lang('Kullanıcı Adınız')}</strong></th>
                    <td>{cm_htmlclear($user['username'])}</td>
                </tr>
                <tr>
                    <th><strong>{cm_lang('E-Posta Adresiniz')}</strong></th>
                    <td>{cm_htmlclear($user['email_address'])}</td>
                </tr>
                <tr>
                    <th><strong>{cm_lang('Cinsiyet')}</strong></th>
                    <td>{cm_gender($user)}</td>
                </tr>
                <tr>
                    <th><strong>{cm_lang('Kredi Bakiyeniz')}</strong></th>
                    <td>{cm_price_format($user['credit'],$cm_config['default_currency'])}<br /><br /><a href="javascript:;" onclick="cm_creditAdd();" class="btn btn-success btn-xs"><i class="fa fa-money" aria-hidden="true"></i> {cm_lang('Kredi Yükle')}</a> <a href="javascript:;" onclick="cm_warning('{cm_lang('Hesabınıza bakiye olarak kredi eklemesi yapabilir ve krediniz ile Alan Adı, Hosting, Sunucu v.b hizmetleri alabilirsiniz.')}','success','{cm_lang('Kredi')}');" class="btn btn-info btn-xs"><i class="fa fa-info" aria-hidden="true"></i> {cm_lang('Kredi Nedir')}</a></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-6">
            <div class="UyeBaslik">
                <i class="fa fa-cogs fa-2x"></i> <span>Hizmetlerim</span>
            </div>
            <ul class="hizmetlist">
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('hosting')])}"><span class="btnD">{cm_number_format($TotalHosting)}</span> {cm_lang('Hostinglerim')} </a></li>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('domain')])}"><span class="btnD">{cm_number_format($TotalDomains)}</span> {cm_lang('Alan Adlarım')} </a></li>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('server')])}"><span class="btnD">{cm_number_format($TotalServers)}</span> {cm_lang('Sunucu Hizmetlerim')} </a></li>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('order')])}"><span class="btnD">{cm_number_format($TotalOrders)}</span> {cm_lang('Siparişlerim')} </a></li>
                <li><a href="{cm_link("tickets.php")}"><span class="btnD">{cm_number_format($TotalTickets)}</span> {cm_lang('Destek Taleplerim')} </a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
        <hr />
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="pull-left"><i class="fa fa-envelope" aria-hidden="true"></i> {cm_lang('Son 10 Destek Taleplerim')}</div>
                <div class="pull-right"><a href="{cm_link('ticket.php',['page' => 'new'])}" class="btn btn-destek">{cm_lang('Yeni Talep Oluştur')}</a>
                    <a href="{cm_link('ticket.php')}" class="btn btn-destek">{cm_lang('Destek Taleplerim')}</a></div>
                <div class="clearfix"></div>
            </div>
            <div class="panel-body" style="overflow: auto;">
                <table class="table table-responsive DestekTab">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>{cm_lang('Talep Başlığı')}</th>
                        <th>{cm_lang('Durum')}</th>
                        <th>{cm_lang('Son Cevap')}</th>
                        <th>{cm_lang('İşlem')}</th>
                    </tr>
                    </thead>
                    <tbody>
                    {if !is_array($list_return)}
                        <tr>
                            <td colspan="5"><strong>{cm_htmlclear($list_return)}</strong></td>
                        </tr>
                    {else}
                        {foreach from=$list_return key=key item=value}
                            {if $value['ticket_status'] == 1}
                                {assign var="ticketStatus" value="<span class=\"DurumAktif DurumAktifBG\">{cm_lang($ticket_status[$value['ticket_status']]['name'])}</span>"}
                            {elseif $value['ticket_status'] == 2}
                                {assign var="ticketStatus" value="<span class=\"DurumBekleme DurumBeklemeBG\">{cm_lang($ticket_status[$value['ticket_status']]['name'])}</span>"}
                            {elseif $value['ticket_status'] == 3}
                                {assign var="ticketStatus" value="<span class=\"DurumPasif DurumPasifBG\">{cm_lang($ticket_status[$value['ticket_status']]['name'])}</span>"}
                            {/if}
                            <tr>
                                <th>#{$key}</th>
                                <td>{cm_htmlclear($value['ticket_subject'])}</td>
                                <td>{$ticketStatus}</td>
                                <td>{cm_date(null,$value['ticket_end_time'])}</td>
                                <td><a href="{cm_link('ticket.php',['page' => 'view', 'id' => $value["ticket_id"]])}"><span class="btn btn-yesil"><i class="fa fa-search" aria-hidden="true"></i> {cm_lang('İncele')}</span></a></td>
                            </tr>
                        {/foreach}
                    {/if}
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
{include file="inc/footer.tpl"}
<script src="{$directory}/templates/{$template}/js/jquery.vticker.min.js"></script>
<script>
    $(function(){
        $('#HesabimDuyuru').vTicker();
    });
    function cm_creditAdd(){
        swal({
            title: '{cm_lang('Kredi Bakiyesi Yükle')}',
            input: 'text',
            inputValue: '10.00',
            showCancelButton: true,
            confirmButtonText: '{cm_lang('Yükle')}',
            cancelButtonText: '{cm_lang('İptal')}',
            showLoaderOnConfirm: true,
            preConfirm: function (credit) {
                return new Promise(function (resolve, reject) {
                    if (credit == '') {
                        reject('{cm_lang('Kredi miktarı girin')}')
                    } else {
                        basket_add('action=add&product_type_id=1&credit='+credit,'','{$directory}/sepet');
                    }
                })
            },
            allowOutsideClick: false
        });
    }
</script>