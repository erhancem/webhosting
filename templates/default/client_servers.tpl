{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left"><i class="fa fa-cloud"></i> {cm_lang('Sunucularım')}</div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <table class="table table-bordered table-striped table-condensed table-responsive">
                <thead>
                <tr>
                    <th>{cm_lang('Paket')}</th>
                    <th>{cm_lang('Kayıt Tarihi')}</th>
                    <th>{cm_lang('Bitiş Tarihi')}</th>
                    <th>{cm_lang('Durumu')}</th>
                    <th>{cm_lang('İşlem')}</th>
                </tr>
                </thead>
                <tbody>
                {if (!is_array($list_return))}
                    <tr>
                        <td colspan="7" style="text-align: center;"><strong>{cm_htmlclear($list_return)}</strong></td>
                    </tr>
                {else}
                    {foreach from=$list_return key=key item=value}
                        {assign var="serverstatus" value="<span style=\"color: {$server_status[$value['server_status']]['color']};\"><strong>{cm_lang($server_status[$value['server_status']]['name'])}</strong></span>"}
                        {if $value['product']['price_status'] == 2}
                            {assign var="endtime" value="{cm_date("d-m-Y",$value['server_end_time'])}"}
                        {else}
                            {assign var="endtime" value="{cm_lang('Sınırsız')}"}
                        {/if}
                        <tr>
                            <td>{cm_htmlclear($value['product']['name'])}</td>
                            <td>{cm_date("d-m-Y",$value['server_create_time'])}</td>
                            <td>{$endtime}</td>
                            <td>{$serverstatus}</td>
                            <td>
                                {if $value['server_status'] == 1}
                                    <a href=""><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> {cm_lang('YÖNET')}</span></a>
                                    <a href="javascript:;" onclick="basket_add('action=renew&product_type_id={$value['product_type_id']}&id={$value['server_id']}','','{cm_link('basket')}');"><span class="btn btn-danger"><i class="fa fa-cog" aria-hidden="true"></i> {cm_lang('UZAT')}</span></a>
                                {else}
                                    -
                                {/if}
                            </td>
                        </tr>
                    {/foreach}
                {/if}
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
{include file="inc/footer.tpl"}