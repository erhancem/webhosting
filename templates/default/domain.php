<?php
include('inc/header.php');
?>
<section>
    <div class="AlanAdiBG"></div>
    <div id="domainContent" class="tabbg">
        <div class="container">
            <ul class="nav nav-tabs">
              <li class="Kayit active">
                <a data-toggle="tab" href="#Kayit">
                    <div class="AlanAdiTab">
                        <h1><strong><?=cm_lang('ALAN ADI')?></strong> <?=cm_lang('SORGULAMA')?></h1>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </li>
              <li class="TopluKayit">
                  <a data-toggle="tab" href="#TopluKayit">
                    <div class="AlanAdiTab">
                        <h1><strong><?=cm_lang('TOPLU')?></strong> <?=cm_lang('SORGULAMA')?></h1>
                        <div class="clearfix"></div>
                    </div>
                  </a>
              </li>
            </ul>
        </div>
    </div>
<div class="container">
    <div class="BosBG">
        <div class="row">
            <div class="tab-content">
                <div id="Kayit" class="tab-pane fade in active">
                    <div class="col-md-10 col-md-offset-1 margin-top20">
                        <form autocomplate="off" onsubmit="return false;">
                        	<input type="hidden" value="<?=$cm_set_token?>" name="cm_token" id="cm_token" />
                        	<input type="hidden" value="6" id="domain_type" name="domain_type" />
                        	<input type="hidden" value="<?=cm_lang('Sepete Ekle')?>" id="basket_button_value" name="basket_button_value" />
                     	   	<div class="search-domain">
                                <div class="input-group">
                                    <input type="text" id="domain_name" autocomplete="off" value="<?=cm_get_request('domain')?cm_htmlclear(cm_get_request('domain')):null?>" name="domain_name" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$" />
                                    <span class="input-group-btn">
                                        <button class="btn btn-yesil btn-lg" id="domainSearch" onclick="domainavailable(document.getElementById('domain_name').value);" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                </div>
                            </div>
                        </form>
	                    <div class="clearfix"></div>
                    </div>
                </div>
                <div id="TopluKayit" class="tab-pane fade">
                    <div class="row">
                        <div class="col-md-12">
                            <form autocomplate="off" onsubmit="return false;">
                                <div class="search-domain">
                                    <div class="input-group">
                                        <textarea id="multi_domain" rows="5" name="multi_domain" class="form-control input-lg" placeholder="Toplu domain kayıt."></textarea>
                                        <span class="input-group-btn">
                                            <button type="submit" class="btn btn-yesil btn-lg" onclick="domainavailable(document.getElementById('multi_domain').value);" id="" data-original-text="<i class='fa fa-search'></i> Sorgula" style="margin: 0px 20px;"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                        </span>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="AramaSonuc">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-condensed domain_register_table" id="domainTable" style="display: none;">
                	<thead>
                		<tr>
                			<th colspan="4" class="text-center">ARAMA SONUÇLARI</th>
                		</tr>
                	</thead>
                	<tbody></tbody>
                </table>
            </div>
        </div>
        <div class="Bilgi">
            <div class="col-md-1"><i class="fa fa-info-circle" aria-hidden="true"></i></div>
            <div class="col-md-11">
                <h2>Domain nedir?</h2>
                 <span><strong>Domain(alan adı)</strong>, internette yer almak için sahip olmamız gereken kimlikdir. Yani alan adları web sitemizin adı ve adresidir. Bu adres olmadan ziyaretçiler sitemize ulaşamazlar.</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="DomainFiyatListesi">
            <div class="LinuxPaketler">
                <h1><strong>Alan Adı</strong> Fiyat Listesi</h1>
                <div class="clearfix"></div>
            </div>
            <table class="table table-bordered table-striped table-hover table-condensed table-responsive">
            	<thead>
            		<tr>
            			<th onclick="don();">KAMPANYA</th>
                        <th>UZANTI</th>
            			<th>YENİ KAYIT ( 1 YIL )</th>
            			<th>TRANSFER</th>
            			<th>YENİLEME ( 1 YIL )</th>
            		</tr>
            	</thead>
            	<tbody>
<?php foreach($Tlds as $key=>$value){
	$Kampanya = array();
	if($value['register_discount'] > 0){
		$Kampanya[] = cm_lang('Kayıt');
	}
	if($value['renewal_discount'] > 0){
		$Kampanya[] = cm_lang('Yenileme');
	}
	if($value['transfer_discount'] > 0){
		$Kampanya[] = cm_lang('Transfer');
	}
?>
            		<tr>
<?php if(count($Kampanya) > 0){ ?>
            			<td><span class="btn-kampanya flashefekt" title="<?=implode(" ",$Kampanya)?>">KAMPANYA</span></td>
<?php }else{ ?>
						<td>-</td>
<?php } ?>
                        <td>.<?=$key?></td>
            			<td><?=cm_price_format($value['register_price'],$value['tld_currency'])?></td>
            			<td><?=cm_price_format($value['transfer_price'],$value['tld_currency'])?></td>
            			<td><?=cm_price_format($value['renewal_price'],$value['tld_currency'])?></td>
            		</tr>
<?php } ?>
            	</tbody>
            </table>
        </div>
    </div>
</div>
<?php
include('detail.php');
include('inc/footer.php');
?>
<?php if(cm_get_request('domain')){ ?>
<script>
$(document).ready(function(){
	$('html,body').animate({
	   scrollTop: $("#domainContent").offset().top
	});
	$("#domainSearch").click();
});
</script>
<?php } ?>