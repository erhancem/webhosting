{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="UyeBG">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="UyeBaslik">
                <i class="fa fa-address-card fa-2x" aria-hidden="true"></i> <span>{cm_lang('Fatura Adresim')} </span>
            </div>
            <form name="FormAddress" class="form-horizontal" method="POST" action="" onsubmit="return false;">
                <input type="hidden" name="cm_action" value="account" />
                <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                <input type="hidden" name="action" value="address" />
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Fatura Türü')}:</label>
                    <div class="col-sm-3">
                        <label class="radio-inline"><input type="radio" name="billing_type"{if $user['address']['billing_type']==0} checked=""{/if} class="billing_type" onchange="userTypeChange();" value="0" />{cm_lang('Bireysel')}</label>
                    </div>
                    <div class="col-sm-6">
                        <label class="radio-inline"><input type="radio" name="billing_type"{if $user['address']['billing_type']==1} checked=""{/if} class="billing_type" onchange="userTypeChange();" value="1" />{cm_lang('Kurumsal')}</label>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Ad Soyad / Ünvan')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="name" id="name" placeholder="{cm_lang('Ad Soyad / Ünvan')}" value="{cm_htmlclear($user['address']['name'])}" required="" />
                    </div>
                </div>
                <div class="form-group tax_tr">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('TC No / Vergi No')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control tax_tr_input" name="tax_number" id="name" placeholder="{cm_lang('TC No / Vergi No')}" value="{cm_htmlclear($user['address']['tax_number'])}" required="" />
                    </div>
                </div>
                <div class="form-group tax_office">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Vergi Dairesi')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control tax_office_input" name="tax_office" id="name" placeholder="{cm_lang('Vergi Dairesi')}" value="{cm_htmlclear($user['address']['tax_office'])}" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Ülke')}:</label>
                    <div class="col-sm-9">
                        <select name="country_id" class="form-control countrySelect" onchange="countryChange();">
                            <option value="">{cm_lang('Lütfen ülke seçin')}</option>
                            {foreach from=$country key=key item=value}
                                <option value="{$value['country_id']}" title="{$value['country_phone_code']}"{if ($user['address']['country_id']==$value['country_id'])} selected="selected"{/if}>{$value['country_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Şehir')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="city" id="pwd" value="{cm_htmlclear($user['address']['city'])}" placeholder="{cm_lang('Şehir')}" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('İlçe')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="state" id="pwd" value="{cm_htmlclear($user['address']['state'])}" placeholder="{cm_lang('İlçe')}" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Adres')}:</label>
                    <div class="col-sm-9">
                        <textarea name="address" class="form-control" rows="5" required="">{cm_htmlclear($user['address']['address'])}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Posta Kodu')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="postcode" id="pwd" value="{cm_htmlclear($user['address']['postcode'])}" placeholder="{cm_lang('Posta Kodu')}" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="button" onclick="PostAction('FormAddress');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşlem Yapılıyor')}" class="btn btn-default button-FormAddress">
                            {cm_lang('Güncelle')}
                        </button>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Not')}:</label>
                    <div class="col-sm-9">
                        <span style="color: red;">{cm_lang('Daha önce kesilen faturalar değişmez')}<br /><br />{cm_lang('Alan adı için ön tanımlı iletişim (Whois) bilginiz kayıtlı değil ise fatura adresiniz baz alınır')}</span>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
        <div class="clearfix"></div>
        <hr />
        <div class="clearfix"></div>
    </div>
</div>
{include file="inc/footer.tpl"}
<script>
    $(document).ready(function(){
        countryChange();
        userTypeChange();
    });
    function countryChange(){
        var select = $(".countrySelect :selected");
        var code = select.attr('title');
        $(".phone_code").val(code);
        if(code == 90){
            $('.tax_tr_input').attr('required', 'required');
            $('.tax_office_input').attr('required', 'required');
        }else{
            $('.tax_tr_input').removeAttr('required');
            $('.tax_office_input').removeAttr('required');
        }
    }
    function userTypeChange(){
        var select = $(".billing_type:checked");
        var val = select.val();
        if(val == 1){
            $('.tax_office_input').attr('required', 'required');
        }else{
            $('.tax_office_input').removeAttr('required');
        }
    }
</script>