<?php include('inc/header.php'); ?>
<section>
    <div class="HesabimBG">
        <div class="container">
            <div class="uye"> 
                <div class="col-md-9">                           
                    <div class="MusteriProfil">
                        <a href="#"><img src="images/Avatar-Erkek.png" /> Merhaba, Fırat!</a>                    
                    </div> 
                </div>
                <div class="col-md-3"><a href="hesabim.php"><span class="mpanel">Müşteri Paneli</span></a></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
<?php include('inc/hesabim_menu.php'); ?>
<div class="BosBG">
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="pull-left"><i class="fa fa-server" aria-hidden="true"></i> Hostinglerim</div>
    <div class="clearfix"></div>
  </div>
  <div class="panel-body" style="overflow: auto;">
    <table class="table table-bordered table-striped table-condensed table-responsive DestekTab">
        <thead>
            <tr>
                <th>ID</th>
                <th>SİTE ADRESİ</th>
                <th>KAYIT TARİHİ</th>
                <th>BİTİŞ TARİHİ</th>
                <th>KALAN</th>
                <th>UZATMA İŞLEMİ</th>
                <th>İŞLEM</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>1</th>
                <td>cembilisim.com</td>
                <td>07/07/2017</td>
                <td>07/07/2018</td>
                <td>365 (Gün)</td>
                <td>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('-',1)">-</a>
                    <a onmouseout="YilAtriEksi(null,1);" id="UzaYil1" title="366">1 Yıl</a>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('+',1)">+</a>
                    <a href="javascript:;" onclick="sepet_ekle('UzaYil1',15,'id=1&amp;gun='+document.getElementById('UzaYil1').title);">Sepete Ekle</a>
                </td>
                <td><a href="#"><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> YÖNET</span></a></td>
            </tr>
            <tr>
                <th>2</th>
                <td>asyaofis.com</td>
                <td>07/07/2017</td>
                <td>07/07/2018</td>
                <td>156 (Gün)</td>
                <td>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('-',1)">-</a>
                    <a onmouseout="YilAtriEksi(null,1);" id="UzaYil1" title="366">1 Yıl</a>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('+',1)">+</a>
                    <a href="javascript:;" onclick="sepet_ekle('UzaYil1',15,'id=1&amp;gun='+document.getElementById('UzaYil1').title);">Sepete Ekle</a>
                </td>
                <td><a href="#"><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> YÖNET</span></a></td>
            </tr>
            <tr>
                <th>3</th>
                <td>google.com</td>
                <td>07/07/2017</td>
                <td>07/07/2018</td>
                <td>365 (Gün)</td>
                <td>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('-',1)">-</a>
                    <a onmouseout="YilAtriEksi(null,1);" id="UzaYil1" title="366">1 Yıl</a>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('+',1)">+</a>
                    <a href="javascript:;" onclick="sepet_ekle('UzaYil1',15,'id=1&amp;gun='+document.getElementById('UzaYil1').title);">Sepete Ekle</a>
                </td>
                <td><a href="#"><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> YÖNET</span></a></td>
            </tr>
            <tr>
                <th>4</th>
                <td>facebook.com</td>
                <td>07/07/2017</td>
                <td>07/07/2018</td>
                <td>365 (Gün)</td>
                <td>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('-',1)">-</a>
                    <a onmouseout="YilAtriEksi(null,1);" id="UzaYil1" title="366">1 Yıl</a>
                    <a href="javascript:void(0)" class="UztBTN" onclick="YilAtriEksi('+',1)">+</a>
                    <a href="javascript:;" onclick="sepet_ekle('UzaYil1',15,'id=1&amp;gun='+document.getElementById('UzaYil1').title);">Sepete Ekle</a>
                </td>
                <td><a href="#"><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> YÖNET</span></a></td>
            </tr>
        </tbody> 
    </table>
      <div class="pagination-wrap">
        <ul class="pagination pagination-v3">
          <li><a href="#">1</a></li>
          <li><a class="active" href="#">2</a></li>
          <li><a href="#">3</a></li>
          <li><a href="#">.....</a></li>
          <li><a href="#">4</a></li>
          <li><a href="#">5</a></li>
        </ul>
      </div>
  </div>
</div>
        <div class="clearfix"></div>
        </div>
    </div>
<div class="clearfix"></div>
<?php include('inc/footer.php'); ?>