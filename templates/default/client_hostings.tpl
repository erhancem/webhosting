{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left"><i class="fa fa-cloud"></i> {cm_lang('Hostinglerim')}</div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <table class="table table-bordered table-striped table-condensed table-responsive">
                <thead>
                <tr>
                    <th>{cm_lang('Paket')}</th>
                    <th>{cm_lang('Alan Adı')}</th>
                    <th>{cm_lang('Kayıt Tarihi')}</th>
                    <th>{cm_lang('Bitiş Tarihi')}</th>
                    <th>{cm_lang('Durumu')}</th>
                    <th>{cm_lang('İşlem')}</th>
                </tr>
                </thead>
                <tbody>
                {if (!is_array($list_return))}
                    <tr>
                        <td colspan="7" style="text-align: center;"><strong>{cm_htmlclear($list_return)}</strong></td>
                    </tr>
                {else}
                    {foreach from=$list_return key=key item=value}
                        {assign var="serverstatus" value="<span style=\"color: {$hosting_status[$value['hosting_status']]['color']};\"><strong>{cm_lang($hosting_status[$value['hosting_status']]['name'])}</strong></span>"}
                        {assign var="endtime" value="{cm_date("d-m-Y",$value['hosting_end_time'])}"}
                        <tr>
                            <td>{cm_htmlclear($value['product']['name'])}</td>
                            <td>{cm_htmlclear($value['domain_name'])}</td>
                            <td>{cm_date("d-m-Y",$value['hosting_update_time'])}</td>
                            <td>{cm_date("d-m-Y",$value['hosting_end_time'])}</td>
                            <td>{$serverstatus}</td>
                            <td>
                                <a href="{if $value['hosting_status']==1}{cm_link("client.php", ['view' => 'hosting','page' => 'manage','id' => $value['hosting_id']])}{else}javascript:;{/if}"{if $value['hosting_status']!=1}onclick="return cm_warning('{cm_lang('Hosting aktif değil yönetemezsiniz')}','error');"{/if}><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> {cm_lang('YÖNET')}</span></a>
                                {*
                                <a href="javascript:;" onclick="basket_add('action=renew&product_type_id=<?=$value['product_type_id']?>&id=<?=$value['hosting_id']?>','','<?=$directory?>/sepet');"><span class="btn btn-danger"><i class="fa fa-cog" aria-hidden="true"></i> <?=cm_lang('UZAT')?></span></a>
                                *}
                            </td>
                        </tr>
                    {/foreach}
                {/if}
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
{include file="inc/footer.tpl"}