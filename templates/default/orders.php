<?php
$theme['login'] = true;
include('control.php');
$orders_params = array(
	'user_id'	=> $user['user_id'],
	'and'		=> array(
		'order_delete'	=> 0,
	),
	'orderby'	=> array('order_id'=>'DESC'),
);
$order_list = orderGetList($orders_params);
include('inc/header.php');
include('inc/hesabim_menu.php');
?>
<div class="BosBG">
    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="pull-left"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <?=cm_lang('Siparişlerim')?></div>
        <div class="pull-right"></div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body" style="overflow: auto;">
    <table class="table table-responsive DestekTab">
        <thead>
            <tr>
                <th><?=cm_lang('Sipariş NO')?></th>
                <th><?=cm_lang('Durum')?></th>
                <th><?=cm_lang('Fiyat')?></th>
                <th><?=cm_lang('Tarih')?></th>
                <th><?=cm_lang('İşlem')?></th>
            </tr>
        </thead>
        <tbody>
<?php if($order_list == false){ ?>
            <tr>
                <td colspan="5"><strong><?=cm_lang('Sipariş listesi çekilemedi')?></strong></td>
            </tr>
<?php }elseif($order_list['total'] == 0){ ?>
            <tr>
                <td colspan="5"><strong><?=cm_lang('Siparişiniz bulunamadı')?></strong></td>
            </tr>
<?php }else{
foreach($order_list['list'] as $key=>$value){
	$status = '<span style="color: '.$order_status[$value['order_status']]['color'].';"><strong>'.cm_lang($order_status[$value['order_status']]['name']).'</strong></span>';
	$payment = null;
	if($value['order_status'] == 0){
		$payment = '&nbsp;<a href="'.$directory.'/payment?id='.$key.'"><span class="btn btn-danger"><i class="fa fa fa-money" aria-hidden="true"></i> '.cm_lang('Ödeme Yap').'</span></a>';
	}
?>
			<tr>
                <td><?=$key?></td>
                <td><?=$status?></td>
                <td><?=cm_price_format($value['paid'],$value['currency'])?></td>
                <td><?=cm_date(null,$value['order_time'])?></td>
                <td><a href="<?=$directory?>/ticket?ticket_id=<?=$key?>"><span class="btn btn-yesil"><i class="fa fa-search" aria-hidden="true"></i> <?=cm_lang('Ürünler')?></span></a><?=$payment?></td>
            </tr>
<?php }} ?>
        </tbody>
    </table>
    </div>
    <div class="clearfix"></div>
    </div>
</div>
<?php include('inc/footer.php'); ?>