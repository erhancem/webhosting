<?php
$default = array(
	'template_name'		=> 'Cembilişim v1',
	'file'				=> array(
		'domain_register.php'	=> array(
			'seo'		=> array('alan-adi-kayit','domain-register','domain-sorgula','alan-adi-transfer','tr-alan-adi','turkce-alan-adi'),
			'login'		=> false
		),
		'hosting_package.php'	=> array(
			'seo'		=> array('linux-bireysel','linux-standart','linux-profesyonel','linux-sinirsiz','sinirsiz-hosting','windows-bireysel','windows-standart','windows-profesyonel'),
			'login'		=> false
		),
		'server_package.php'	=> array(
			'seo'		=> array('vds-sunucular','vps-sunucular','fiziksel-sunucular','vds','vps','fiziksel'),
			'login'		=> false
		),
		'basket.php'	=> array(
			'seo'		=> array('sepetim','basket','sepet'),
			'login'		=> false
		),
		'basket_configuration.php'	=> array(
			'seo'		=> array('sepet-yapilandirma','basket-configuration'),
			'login'		=> false
		),
		'hesabim.php'			=> array(
			'seo'		=> null,
			'login'		=> false
		)
	),
);