<?php
include('inc/header.php');
?>
<section>
<div class="container">
    <div class="BosBG">
        <div class="SepetBG">
        <div class="clearfix"></div>
            <div>
                <div class="row">
                	<div class="col-md-12">
                        <div id="SepetGetir">
                            <div id="SepetAlan">
                                    <div class="Baslik">
                                        <i class="fa fa-list-ul"></i> <?=cm_lang('Hosting Alan Adı Seçimi')?>
                                    </div>
                                    <div style="padding: 20px;">
<?php if(isset($message)){ ?>
<div class="form-group">
	<p style="color: red;"><?=$message?></p>
</div>
<?php } ?>
	<div class="HostingDomain" style="margin-bottom: 15px;">
		<ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#NewDomain"><i class="fa fa-plus" aria-hidden="true"></i> <?=cm_lang('Alan Adı Kayıt')?></a></li>
            <li><a data-toggle="tab" href="#SelectDomain"><i class="fa fa-plus" aria-hidden="true"></i> <?=cm_lang('Alan Adım Var')?></a></li>
            <li><a data-toggle="tab" href="#BasketDomain"><i class="fa fa-plus" aria-hidden="true"></i> <?=cm_lang('Sepetimden Seç')?></a></li>
        </ul>
        <form id="HostingBuy">
			<input type="hidden" value="add" name="action" />
			<input type="hidden" value="<?=$product['product_id']?>" name="product_id" id="product_id" />
			<input type="hidden" value="<?=$product['product_type_id']?>" name="product_type_id" id="product_type_id" />
			<input type="hidden" value="" name="domain_name" id="domain_name" />
		</form>
        <div class="tab-content">
            <div id="NewDomain" class="tab-pane fade in active">
                <div class="col-md-12">
                    <div class="AnasayfaDomainArama">
                    	<form id="availableDomain" action="" method="POST">
                    		<input type="hidden" value="<?=$cm_set_token?>" name="cm_token" />
	                        <div class="input-group input-group-lg" style="margin-top: 20px;">
	                            <input type="text" class="form-control domain_name" name="domain_name" required="" placeholder="example.com" style="box-shadow: none" value="<?=isset($domainSearch['domain'])?cm_htmlclear($domainSearch['domain']):null?>" autocomplete="off" />
	                            <span class="input-group-btn">
<?php if(!isset($domainSearch) or (isset($domainSearch) and $domainSearch['status'] != 1)){ ?>
<button type="submit" value="available" class="btn btn-danger" name="action" style="border-radius:0 4px 4px 0; z-index: 199"><?=cm_lang('Sorgula')?></button>
<?php }else{ ?>
<button type="button" value="available" class="btn btn-success" onclick="hosting_add(1,'<?=$domainSearch['domain']?>');" style="border-radius:0 4px 4px 0; z-index: 199"><?=cm_lang('Kullan')?></button>
<?php } ?>
	                            </span>
	                        </div>
                   		</form>
                        <div class="clearfix"></div>
                        <table class="table table-striped custab">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
<?php if($action == 'available'){ ?>
<?php if(is_array($domainSearch)){ ?>
                            <tr>
                                <td class="text-center"><?=cm_htmlclear($domainSearch['key'])?></td>
                                <td colspan="2" class="text-center"><span style="color: <?=$domainSearch['status']==1?'green':'red'?>;"><?=cm_htmlclear(($domainSearch['status']==1?'Uygun':'Uygun Değil'))?></span></td>
                            </tr>
<?php }else{ ?>
							<tr>
                                <td colspan="3" class="text-center"><?=cm_htmlclear($domainSearch)?></td>
                            </tr>

<?php }} ?>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="SelectDomain" class="tab-pane fade">
                <div class="col-md-12">
                    <div class="AnasayfaDomainArama">
                    	<form id="domainSelect" action="" method="POST" onsubmit="return false;">
	                        <div class="input-group input-group-lg" style="margin-top: 20px;">
	                            <input type="text" class="form-control domain_name" name="domain" required="" placeholder="example.com" style="box-shadow: none" value="<?=cm_get_request('domain')?cm_htmlclear(cm_get_request('domain')):null?>" autocomplete="off" />
	                            <span class="input-group-btn"><button type="button" value="available" class="btn btn-success" onclick="hosting_add(2,$('#domainSelect .domain_name').val());" style="border-radius:0 4px 4px 0; z-index: 199"><?=cm_lang('Kullan')?></button>
	                            </span>
	                        </div>
                        </form>
                        <div class="clearfix"></div>
                        <div style="margin-top: 15px;">
							<p><strong><?=cm_lang('NOT')?></strong> : <?=cm_lang('Alan adınızı kullanabilmeniz için DNS adreslerinizi firmamıza yönlendirmeniz gerekir')?></p>
						</div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="BasketDomain" class="tab-pane fade">
                <div class="col-md-12">
                    <div class="AnasayfaDomainArama">
                    	<form id="Domainbasket">
                        <div class="input-group input-group-lg" style="margin-top: 20px;">
<?php if(count($basket_select) > 0){ ?>
                            <select class="form-control domain_name" name="basket_domain" style="width: 100%;" id="basket_domain">
<?php foreach($basket_select as $val){ ?>
								<option value="<?=$val?>"><?=$val?></option>
<?php } ?>
							</select>
							<span class="input-group-btn"><button type="button" value="available" class="btn btn-success" onclick="hosting_add(2,$('#Domainbasket .domain_name').val());" style="border-radius:0 4px 4px 0; z-index: 199"><?=cm_lang('Kullan')?></button>
                            </span>
<?php }else{ ?>
							<p><?=cm_lang('Sepetinizde alan adı bulunmamaktadır')?></p>
<?php } ?>
                        </div>
                        </form>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
	</div>
	<div class="clearfix"></div>
										<div class="clearfix"></div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script>
function hosting_add(id,domain){
	if(domain == ""){
		return alert('Lütfen alan adı seçin');
	}
	$("#HostingBuy #domain_name").val(domain);
	var post = $("#HostingBuy").serialize();
	if(id == 1){
		basket_add("action=add&product_type_id=6&domain_name="+domain+"&year=1");
	}
	return basket_add(post,'','<?=$directory?>/sepet');
}
</script>
<?php include('inc/footer.php'); ?>