<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>{$cm_title}</title>
    <meta http-equiv="Copyright" content="{$copyright}"/>
    <meta name="copyright" content="{$copyright}"/>
    <meta name="description" content="{$description}"/>
    <meta name="keywords" content="{$keywords}"/>
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="{$cm_title}"/>
    <meta property="og:description" content="{$description}"/>
    <meta name="author" content=""/>
    <meta name="content-language" content="tr" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
    <!-- CSS Dosyalarim -->
    <link rel="shortcut icon" href="resimler/favicon.ico" />
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/default.css"/>
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/reset.css"/>
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/menu.css"/>
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/animate.css"/>
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/responsive.css"/>
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="{$directory}/templates/{$template}/css/sweetalert2.min.css"/>
    <link rel="alternate" href="{cm_siteurl()}" hreflang="{strtolower($cm_config['default_lang'])}" />
</head>
<body>
<div class="top-bar">
    <div class="container">
        <div class="pull-left m-left40"><span class="text-green">{$cm_lang->langGet('Müşteri Hizmetleri')}</span> 0(850) 555 555 55</div>
        <ul class="top-bar-menu pull-right">
            <li><a href="{"login.php"|cm_link}" class="btn-yesil musteriBTN">{$cm_lang->langGet("Müşteri İşlemleri")}</a></li>
            <li><a href="{"ticket.php"|cm_link}" class="btn-destek">{$cm_lang->langGet("Destek")}</a></li>
            <li><a href="{"basket.php"|cm_link}" class="btn-mobilSepet" style="display: none;"><i class="fa fa-shopping-cart"></i>{$cm_lang->langGet("Sepet")} (<span class="basket_total">0</span>)</a></li>
            <li><a href="#">İnsan Kaynakları</a></li>
            <li><a href="#">Blog</a></li>
            <li><a href="#">İletişim</a></li>
        </ul>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Menu Baslat -->
<div class="menulogo">
    <div class="container Mlogo">
        <div class="row">
            <div class="col-md-8"><a href="{$directory}/"><img src="{$directory}/templates/{$template}/images/cem_logo.png" title="logo" alt="logo" /></a></div>
            <div class="col-md-4">
                <ul class="nav navbar-nav navbar-right Mgizle">
                    <li>
                        <a href="{"basket.php"|cm_link}" class="top-Sepet"><i class="fa fa-shopping-cart"></i> Sepetim (<span class="basket_total">0</span>)</a>
                        <div class="SepetGoster" style="display: none;">Sepetiniz boş</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- role="navigation" data-spy="affix" data-offset-top="50" -->
    <div class="menulink">
        <div class="container">
            <nav class="navbar">
                <div class="navbar-header">
                    <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse js-navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class="dropdown mega-dropdown">
                            <a href="#">ALAN ADI <span class="caret"></span></a>
                            <ul class="dropdown-menu mega-dropdown-menu">
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">ALAN ADI İŞLEMLERİ</li>
                                        <li><a href="{$directory}/alan-adi-kayit">Alan Adı Kayıt</a></li>
                                        <li><a href="{$directory}/alan-adi-transfer">Alan Adı Transfer</a></li>
                                        <li><a href="{$directory}/tr-alan-adi">.COM.TR Alan Adı</a></li>
                                        <li><a href="{$directory}/turkce-alan-adi">Türkçe Alan Adı</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-9 text-center">
                                    <h3>Alan adınızı hemen kayıt edin!</h3>
                                    <form action="{$directory}/alan-adi-kayit" method="GET" autocomplate="off">
                                        <div class="search-domain">
                                            <div class="input-group">
                                                <input type="text" id="domain-text" autocomplete="off" name="domain" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                                                <span class="input-group-btn">
                                        <button type="submit" class="btn btn-yesil btn-lg" id="" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                                    </span>
                                            </div>
                                        </div>
                                    </form>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown mega-dropdown">
                            <a href="#" class="dropdown-toggle">HOSTİNG <span class="caret"></span></a>
                            <ul class="dropdown-menu mega-dropdown-menu HostMenu">
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header">Linux Hosting</li>
                                        <li><a href="{$directory}/hosting/linux-bireysel">Linux Bireysel</a></li>
                                        <li><a href="{$directory}/hosting/linux-standart">Linux Standart</a></li>
                                        <li><a href="{$directory}/hosting/linux-profesyonel">Linux Profesyonel</a></li>
                                        <li><a href="{$directory}/hosting/sinirsiz-hosting">Linux Limitsiz</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header">Windows Hosting</li>
                                        <li><a href="{$directory}/hosting/windows-bireysel">Windows Bireysel</a></li>
                                        <li><a href="{$directory}/hosting/windows-standart">Windows Standart</a></li>
                                        <li><a href="{$directory}/hosting/windows-profesyonel">Windows Profesyonel</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header">Reseller Paketler</li>
                                        <li><a href="#">Reseller Bireysel</a></li>
                                        <li><a href="#">Reseller Standart</a></li>
                                        <li><a href="#">Reseller Profesyonel</a></li>
                                        <li><a href="#">Reseller Limitsiz</a></li>
                                    </ul>
                                </li>
                                <div class="col-md-6 text-center">
                                </div>
                            </ul>
                        </li>
                        <li class="dropdown mega-dropdown">
                            <a href="#" class="dropdown-toggle">SUNUCU <span class="caret"></span></a>
                            <ul class="dropdown-menu" style="left: inherit;">
                                <li><a href="{$directory}/sunucu/vps">VPS Paketleri</a></li>
                                <li><a href="{$directory}/sunucu/vds">VDS Paketleri</a></li>
                                <li><a href="{$directory}/sunucu/dedicated">Fiziksel Paketler</a></li>
                            </ul>
                        </li>
                        <li class="dropdown mega-dropdown"><a href="#">SSL HİZMETLERİ <span class="caret"></span></a>
                            <ul class="dropdown-menu mega-dropdown-menu">
                                <li class="col-sm-4">
                                    <ul>
                                        <li class="dropdown-header">SSL Sertfikaları</li>
                                        <li><a href="{$directory}/ssl/ssl-sertifikasi">Rapid SSL Sertifikası</a></li>
                                        <li><a href="{$directory}/ssl/ssl-sertifikasi">Rapid SSL Wilcard</a></li>
                                        <li><a href="{$directory}/ssl/ssl-sertifikasi">Quick SSL Premium</a></li>
                                        <li><a href="{$directory}/ssl/ssl-sertifikasi">True BusinessID Wildcard SSL</a></li>
                                        <li><a href="{$directory}/ssl/ssl-sertifikasi">True BusinessID Ev SSL</a></li>
                                        <li><a href="{$directory}/ssl/ssl-sertifikasi">True BusinessID + SAN SSL</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-3">
                                    <ul>
                                        <li class="dropdown-header">Genel Bilgi</li>
                                        <li><a href="#">SSL nedir, nasıl alınır?</a></li>
                                        <li><a href="#">Hangi SSL'i almalıyım?</a></li>
                                        <li><a href="#">Gerekli Belgeler</a></li>
                                        <li><a href="#">CSR nasıl oluştuturum?</a></li>
                                        <li><a href="#">SSL kurulumu nasıl yapılır?</a></li>
                                    </ul>
                                </li>
                                <div class="col-md-5 text-center">
                                    <div class="MenuKampanya">
                                        <img src="" />
                                    </div>
                                </div>
                            </ul>
                        </li>
                        <li><a href="#">WEB TASARIM</a></li>
                        <li><a href="{$directory}/marka">MARKA TESCİL</a></li>
                    </ul>
                </div>
            </nav>
        </div>

    </div>
</div>