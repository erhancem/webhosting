<section>
    <div class="HesabimBG">
        <div class="container">
            <div class="uye">
                <div class="col-md-9">
                    <div class="MusteriProfil">
                        <a href="{cm_link("client.php")}"><img src="{$directory}/templates/{$template}/images/Avatar-Erkek.png" /> {cm_lang('Merhaba, ##fullname##',['fullname'=>cm_user_name($user)])}</a>
                    </div>
                </div>
                <div class="col-md-3"><a href="{cm_link("client.php")}"><span class="mpanel">{"Müşteri Paneli"|cm_lang}</span></a></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="HesabimNavBG" id="contentHeader">
        <div class="container text-center">
            <ul class="HesabimNav">
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('hosting')])}"><i class="fa fa-tasks fa-3x"></i> {"Hostinglerim"|cm_lang} </a></li>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('domain')])}"><i class="fa fa-globe fa-3x"></i>{"Alan Adlarım"|cm_lang} </a></li>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('server')])}"><i class="fa fa-cloud fa-3x"></i>{"Sunucularım"|cm_lang}</a></li>
                <li><a href="{$directory}/ssl-list"><i class="fa fa-lock fa-3x"></i>{"SSL Sertifika"|cm_lang} </a></li>
                <li><a href="{$directory}/services"><i class="fa fa-gear fa-3x"></i>{"Eklentiler"|cm_lang} </a></li>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('order')])}"><i class="fa fa-shopping-cart fa-3x"></i> {"Siparişlerim"|cm_lang}</a></li>
                <li><a href="{cm_link("ticket.php")}"><i class="fa fa-envelope fa-3x"></i> {"Destek Taleplerim"|cm_lang}</a></li>
                <li><a href="{$logout_url}"><i class="fa fa-sign-out fa-3x"></i>{"Çıkış Yap"|cm_lang}</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="HesabimNavMin">
            <ul>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('account'),'page'=>cm_link_seo('info')])}">{"Bilgilerimi Düzenle"|cm_lang}</a></li>
                <li><a href="{cm_link("client.php",['view'=>cm_link_seo('account'),'page'=>cm_link_seo('address')])}">{"Fatura Bilgilerim"|cm_lang}</a></li>
                <li><a href="{$directory}/payment_report.php">{"Ödeme Bildirimi"|cm_lang}</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>