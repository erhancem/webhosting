</div>
<div class="clearfix"></div>
<div class="Footer">
    <div class="container">
        <div class="col-md-2 mtext-center">
            <h4>ALAN ADI HİZMETİ</h4>
            <ul>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Alan Adı Kayıt</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Alan Adı Transer</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Toplu Kayıt</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Whois Sorgulama</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Backorder Kayıt</a></li>
            </ul>
        </div>
        <div class="col-md-2 mtext-center">
            <h4>SUNUCU</h4>
            <ul>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Linux Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
            </ul>
        </div>
        <div class="col-md-2 mtext-center">
            <h4>WEB HOSTİNG</h4>
            <ul>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Linux Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
            </ul>
        </div>
        <div class="col-md-2 mtext-center">
            <h4>DİĞER HİZMETLER</h4>
            <ul>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Linux Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
                <li><a href="#"><i class="fa fa-ellipsis-v" aria-hidden="true"></i> Windows Hosting</a></li>
            </ul>
        </div>
        <div class="col-md-4 mtext-center text-center">
            <a href="{$directory}/"><img src="{$directory}/templates/{$template}/images/cem_alt_logo.png" title="logo" alt="logo" /></a>
            <ul>
                <li><a href="#"><i class="fa fa-exchange" aria-hidden="true"></i> ns1.cembilisim.com</a></li>
                <li><a href="#"><i class="fa fa-exchange" aria-hidden="true"></i> ns2.cembilisim.com</a></li>
            </ul>
            <span>İstanbul / TÜRKİYE</span>
        </div>
    </div>
    <div class="footerhizli">
        <div class="container">
            <div class="col-md-3"><a href="mailto:info@cembilisim.com"><i class="fa fa-envelope" aria-hidden="true" style="color: #24CE00;"></i> <span>info@cembilisim.com</span></a></div>
            <div class="col-md-3"><a href="tel:+0850000000"><i class="fa fa-phone-square" aria-hidden="true" style="color:  #ED3636;"></i> <span>0850 000 00 00</span></a></div>
            <div class="col-md-3"><a href="musteri_islemleri.php"><i class="fa fa-power-off" aria-hidden="true" style="color: #fd7f00;"></i> <span>Müşteri Girişi</span></a></div>
            <div class="col-md-3">
                <div class="FooterSosyal">
                    <ul>
                        <li><a href="#"><i class="fa fa-facebook-official" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
                        <li><a href="#" title="HATA BİLDİ" alt="Hata Bildir" data-toggle="tooltip" title="HATA BİLDİR!"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
</section>
<a id="back-to-top" href="#top" class="btn btn-sepet btn-lg back-to-top" role="button" style="z-index: 999;"><i class="fa fa-chevron-up" aria-hidden="true"></i></a>
</body>
<!-- JS Dosyalarim -->
<script src="{$directory}/templates/{$template}/js/jquery-3.2.0.min.js"></script>
<script src="{$directory}/templates/{$template}/js/bootstrap.min.js"></script>
<script src="{$directory}/templates/{$template}/js/sweetalert2.min.js"></script>
<script src="{$directory}/templates/{$template}/js/genel.js"></script>
<script src="{$directory}/templates/{$template}/js/site.js"></script>
</head>
</html>