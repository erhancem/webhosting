<?php
if(strstr($_SERVER['REQUEST_URI'],'?')){
	$logout_url = $_SERVER['REQUEST_URI'].'&logout=1';
}else{
	$logout_url = $_SERVER['REQUEST_URI'].'?logout=1';
}
?>
<section>
    <div class="HesabimBG">
        <div class="container">
            <div class="uye">
                <div class="col-md-9">
                    <div class="MusteriProfil">
                        <a href="<?=cm_link("client.php")?>"><img src="<?=$directory?>/templates/<?=$template?>/images/Avatar-Erkek.png" /> <?=cm_lang('Merhaba, ##fullname##',array('fullname'=>cm_htmlclear($user['first_name'].' '.$user['last_name'])))?></a>
                    </div>
                </div>
                <div class="col-md-3"><a href="<?=cm_link("client.php")?>"><span class="mpanel"><?=cm_lang('Müşteri Paneli')?></span></a></div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
<div class="HesabimNavBG" id="contentHeader">
    <div class="container text-center">
    <ul class="HesabimNav">
        <li><a href="<?=cm_link("client.php",array('view'=>cm_link_seo('hosting')))?>"><i class="fa fa-tasks fa-3x"></i><?=cm_lang('Hostinglerim')?></a></li>
        <li><a href="<?=cm_link("client.php",array('view'=>cm_link_seo('domain')))?>"><i class="fa fa-globe fa-3x"></i><?=cm_lang('Alan Adlarım')?></a></li>
        <li><a href="<?=cm_link("client.php",array('view'=>cm_link_seo('server')))?>"><i class="fa fa-cloud fa-3x"></i><?=cm_lang('Sunucularım')?></a></li>
        <li><a href="<?=$directory?>/ssl-list"><i class="fa fa-lock fa-3x"></i><?=cm_lang('SSL Sertifika')?></a></li>
        <li><a href="<?=$directory?>/services"><i class="fa fa-gear fa-3x"></i><?=cm_lang('Eklentiler')?></a></li>
        <li><a href="<?=cm_link("client.php",array('view'=>cm_link_seo('order')))?>"><i class="fa fa-shopping-cart fa-3x"></i><?=cm_lang('Siparişlerim')?></a></li>
        <li><a href="<?=cm_link("ticket.php")?>"><i class="fa fa-envelope fa-3x"></i><?=cm_lang('Destek Taleplerim')?></a></li>
        <li><a href="<?=$logout_url?>"><i class="fa fa-sign-out fa-3x"></i><?=cm_lang('Çıkış Yap')?></a></li>
    </ul>
    </div>
</div>
<div class="container">
	<div class="HesabimNavMin">
	    <ul>
	        <li><a href="<?=cm_link("client.php",array('view'=>cm_link_seo('account'),'page'=>cm_link_seo('info')))?>"><?=cm_lang('Bilgilerimi Düzenle')?></a></li>
	        <li><a href="<?=cm_link("client.php",array('view'=>cm_link_seo('account'),'page'=>cm_link_seo('address')))?>"><?=cm_lang('Fatura Bilgilerim')?></a></li>
	        <li><a href="<?=$directory?>/payment_report.php"><?=cm_lang('Ödeme Bildirimi')?></a></li>
	    </ul>
	    <div class="clearfix"></div>
	</div>