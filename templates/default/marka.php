<?php
include('inc/header.php');
$Marka 				= product_list(2);
$Ozellikler			= $Marka['feature'][1];
$SinifUcreti		= $Ozellikler[2]['feature_list'][3]['feature_price'];
$EkSinifUcreti		= $Ozellikler[2]['extra_price'];
$MarkaTakipsiz		= $Ozellikler[1]['feature_list'][1];
$MarkaTakip			= $Ozellikler[1]['feature_list'][2];
?>
<section>
    <div class="MarkaBG">
        <div class="container">
            <h1 class="text-center">MARKA TESCİL HİZMETİ</h1>
        </div>
    </div>
<div class="container">
    <div class="BosBG">
        <div class="col-md-8">
            <h2>Marka Tesil</h2>
            <p>Marka tescili için en uygun ve ucuzun adres.</p>
            <form id="MarkaForm" onsubmit="return false;">
                <div class="row">
                    <div class="col-md-12">
                        <input type="text" id="marka_adi" autocomplete="off" name="marka_adi" class="form-control input-lg" placeholder="Marka adınızı yazınız..." />
                    <hr />
                    </div>
                </div>
                <div class="pull-left"><h3>Marka Sınıfları</h3></div>
                <div class="pull-right"><h3><a href="#"class="btn-destek" style="color: #fff;">Tüm marka sınıfları detayları</a></h3></div>
                <div class="clearfix"></div>
                <hr />
                <input type="hidden" name="product_type_id" id="product_type_id" value="<?=$Marka['product_type_id']?>" />
                <input type="hidden" name="product_id" id="product_id" value="<?=$Marka['product_id']?>" />
                <input type="hidden" name="sinif_ucret" id="sinif_ucret" value="<?=$SinifUcreti?>" />
                <input type="hidden" name="ek_sinif_ucret" id="ek_sinif_ucret" value="<?=$EkSinifUcreti?>" />
<?php foreach($Ozellikler[2]['feature_list'] as $key=>$value){ ?>
                <div class="Marka_CheckBox">
                  <input id="box<?=$key?>" class="marka_sinif" onclick="MarkaHesapla()" name="marka_sinif" value="<?=$key?>" type="checkbox" />
                  <label for="box<?=$key?>"><?=$value['feature_name']?></label>
                </div>
<?php } ?>
                <div class="col-md-6">
                    <table class="table table-condensed table-hover">
                        <tbody>
                            <tr>
                                <td>Toplam Sınıf</td>
                                <td>:</td>
                                <td><span id="ToplamSinif">0</span></td>
                            </tr>
                            <tr>
                                <td>Hizmet Bedeli</td>
                                <td>:</td>
                                <td><span id="HizmetBedel">0.00</span> <?=$Marka['currency']?></td>
                            </tr>
                            <tr>
                                <td>Harç Bedeli</td>
                                <td>:</td>
                                <td><span id="HarcBedel">0.00</span> <?=$Marka['currency']?></td>
                            </tr>
                            <tr>
                                <td>Toplam Tutar</td>
                                <td>:</td>
                                <td><span id="ToplamTutar">0.00</span> <?=$Marka['currency']?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="col-md-6">
                    <button onclick="MarkaSatinAl()" class="btn btn-destek" id="MarkaButton" style="color: #fff;font-size:18px;margin-top: 10px;line-height: 30px">Sepete Ekle</button>
                    <button class="btn btn-yesil" style="color: #fff;font-size:18px;margin-top: 10px;line-height: 30px">Online Araştır</button>
                </div>
            </form>
        </div>
        <div class="col-md-4">
        <h2>Marka Tescili</h2>
        <p>Detaylı bilgi için formu doldurun, sizi arayalım.</p>
        <form action="" method="">
            <div class="form-group">
              <input type="text" name="" id="" class="form-control"  placeholder="Marka Adın"/>
            </div>
            <div class="form-group">
              <input type="text" name="" id="" class="form-control"  placeholder="Adınız Soyadınız"/>
            </div>
            <div class="form-group">
              <input type="text" name="" id="" class="form-control"  placeholder="Telefon"/>
            </div>
            <div class="form-group">
              <input type="text" name="" id="" class="form-control"  placeholder="E-Posta"/>
            </div>
            <button class="btn btn-destek" style="color: #fff;">Hızlı Başvur</button>
        </form>
        </div>
        <div class="clearfix"></div>
        <hr />
            <div class="col-md-6">
                <div class="MarkaTur">
                    <input id="takipsiz" name="marka_hizmet" type="radio" onclick="MarkaHesapla();" alt="<?=$MarkaTakipsiz['feature_price']?>" class="with-font marka_tur" value="<?=$MarkaTakipsiz['pgf_id']?>" checked="" />
                    <label for="takipsiz"> Sadece Başvuru (.com.tr Almak için yeterlidir)</label> <br />
                    <span>Alan adı belge talebi içindir, başvuru sonrası herhangi bir takip hizmeti verilmez. Vekil başvurudan sorumludur ve hak sahibi dilerse TPE süreçlerini kendisi takip eder. Takibe devam etmek isteyen hak sahipleri TPE azilname ibraz etmek zorundadır, müracaat sonrası olası hak kayıplarından vekil sorumlu değildir ve vekil sadece başvuru aşamasında dosyanın hazırlanması ve TPE ibrazı ile yükümlüdür.</span>
                </div>
            </div>
            <div class="col-md-6">
                <div class="MarkaTur">
                    <input id="takipli" name="marka_hizmet" type="radio" onclick="MarkaHesapla();" alt="<?=$MarkaTakip['feature_price']?>" class="with-font marka_tur" value="<?=$MarkaTakip['pgf_id']?>" />
                    <label for="takipli"> Başvuru, Tescil ve Aktifliği İçin Dosya Takibi</label><br />
                    <span>Marka Tescil sertifikası alınana kadar marka durumu vekillerimizce takip edilir. Olası red ve itirazlar değerlendirilir ve hak sahibine vekil tarafından bildirilir.</span>
                </div>
            </div>
        <div class="clearfix"></div>
        <hr />
        <ul class="nav nav-tabs">
          <li class="active">
            <a data-toggle="tab" href="#Takipsiz">
                <div class="LinuxPaketler">
                    <h1><strong>Sadece Başvuru</strong> Belgeleri</h1>
                    <div class="clearfix"></div>
                </div>
            </a>
        </li>
          <li>
              <a data-toggle="tab" href="#Takipli">
                <div class="LinuxPaketler">
                    <h1><strong>Başvuru + Takip</strong> Belgeleri</h1>
                    <div class="clearfix"></div>
                </div>
              </a>
          </li>
        </ul>

        <div class="tab-content">
            <div id="Takipsiz" class="tab-pane fade in active">
                <div class="row">
                    <div class="col-md-12">
                        <div class="MarkaBelgeIcerik">
                            <ul>
                                <li><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> Sadece Başvuru Belgesi</a></li>
                                <li><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> Hizmet Sözleşmesi</a></li>
                                <li><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> Kimlik Fotokopisi</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Takipli" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-12">
                        <div class="MarkaBelgeIcerik">
                            <ul>
                                <li><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> Başvuru + Takip Belgesi</a></li>
                                <li><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> Hizmet Sözleşmesi</a></li>
                                <li><a href="#"><i class="fa fa-file-text-o" aria-hidden="true"></i> Kimlik Fotokopisi</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading1">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
                            Marka Nedir?
                        </a>
                    </h4>
                </div>
                <div id="collapse1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading1">
                    <div class="panel-body">
                        <p>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR></p>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading2">
                    <h4 class="panel-title">
                        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="false" aria-controls="collapse2">
                            Kimler Marka Yapabilir?
                        </a>
                    </h4>
                </div>
                <div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
                    <div class="panel-body">
                        <p>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR>conteudo conteudo conteudo<BR></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('detail.php'); ?>
<?php include('inc/footer.php'); ?>