<div class="clearfix"></div>
<div class="Destek-area">
    <div class="col-md-12">
        <div class="row">
            <div class="container">
                <div class="LinuxPaketler" style="width: 320px;">
                    <h1><strong>7/24</strong> Destek</h1>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-8">
                <h5>Tüm soru ve sorunlarınızda yanınızda olduğumuzu bilmenizi ister, bizleri 7 gün 24 saat hiç çekinmeden arayabilir veya bizden teknik destek alabilirsiniz.</h5>
                  <ul>
                    <li><h3>TELEFON & E-MAİL</h3>
                        <span>Ekibimiz 7/24 iş başındadır. Taleplerinize en kısa sürede yanıt vermek için çalışmaktayız.</span>
                    </li>
                    <li><h3>MÜŞTERİ PANELİ</h3>
                        <span>Satın almış olduğunuz hizmetlerle ilgili tüm işlemlerinizi tamamen kullanıcı ve müşteri odaklı geliştirdiğimiz kontrol panelimizden gerçekleştirebilirsiniz..</span>
                    </li>
                    <li><h3>CANLI DESTEK</h3>
                        <span>Canlı destek üzerinde günün her saati her dakikası satış öncesi sorularınızı sorabilir, bizden bütün hizmetlerimiz hakkında bilgi alabilirsiniz.</span>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="MutluMuster">
    <div class="container">
        <section id="carousel">
        	<div class="container">
        		<div class="row">
        			<div class="col-md-8 col-md-offset-2">
                        <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
        				<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
        				  <!-- Carousel indicators -->
                          <ol class="carousel-indicators">
        				    <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
        				    <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
        				    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                            <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
        				  </ol>
        				  <!-- Carousel items -->
        				  <div class="carousel-inner">
        				    <div class="item active">
                                <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
        				    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
        				    <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
        				    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
        				    <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
        				    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
                            <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
            			    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
                            <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
            			    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
                            <div class="item">
                                <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
            			    	<blockquote>
        				    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
        				    	</blockquote>
        				    </div>
        				  </div>
        				</div>
        			</div>
        		</div>
        	</div>
        </section>
    </div>
</div>
<div class="clearfix"></div>
<div class="cozum">
    <div class="container">
        <div class="row">
    		<div class="col-md-12">
               <div id="Carousel" class="carousel slide">
                    <div class="carousel-inner">
                    <div class="item active">
                    	<div class="row">
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/drupal.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/joomla.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/jquery.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/magento.png" alt="Image" /></a></div>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="row">
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/mysql.png" alt="Image" /></a></div>
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/python.png" alt="Image" /></a></div>
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/wordpress.png" alt="Image" /></a></div>
                    	</div>
                    </div>
                    </div>
                      <a data-slide="prev" href="#Carousel" class="left carousel-control"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a>
                      <a data-slide="next" href="#Carousel" class="right carousel-control"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
               </div>
    		</div>
    	</div>
    </div>
</div>