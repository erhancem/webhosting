<?php
$page['title']			= 'CEM Bilişim - Web Hosting Hizmetleri';
$page['keywords']		= 'cem bilişim,vps,hosting,vds,fiziksel sunucu,Arma 3,oyun sunucu, Arma 3 oyun sunucu,sınırsız hosting, SSD hosting';
$page['description']	= 'Cem bilişim Kaliteli hosting (Sınırsız SSD Hosting), VPS, VDS, Fiziksel Sunucu, Oyun sunucuları ve Yazılım Hizmeti';
$page['copyright']		= 'Copyright © 2016, Cem Bilişim - Sunucu ve Yazılım Hizmetleri';
include('inc/header.php');
?>

    <div class="GirisDomainBG">
        <div class="container text-center">
            <div class="col-xs-12"><h1>ALAN ADI KAYIT</h1></div>
            <div class="col-md-10 col-md-offset-1 margin-top20">
                <form action="<?=$directory?>/alan-adi-kayit" method="GET" autocomplate="off">
                    <div class="search-domain">
                        <div class="input-group">
                            <input type="text" id="domain-text" autocomplete="off" name="domain" class="form-control input-lg" placeholder=".club 0,50$ / .info 2,99$ / .site 0,99$ / .website 1,99$ / .email 3,99$">
                            <span class="input-group-btn">
                                <button type="submit" class="btn btn-yesil btn-lg" id="" data-original-text="<i class='fa fa-search'></i> Sorgula"><i class="fa fa-search"></i> <span class="hidden-xs hidden-sm">Sorgula</span></button>
                            </span>
                        </div>
                    </div>
                </form>
            </div>
           <div class="col-md-12">
           <div class="SliderDomainUzanti">
             <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                            <ul class="item active">
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                            </ul>
                            <ul class="item">
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                                <li><a href="#"><img src="<?=$directory?>/templates/<?=$template?>/images/uzanti/club.png" /><span>9,99 $</span>8,49 $</a></li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
               </div>
           </div>
        </div>
    </div>
    <div class="container">
        <div class="BosBG">
            <div class="row">
                <ul class="nav nav-tabs">
                  <li class="active">
                    <a data-toggle="tab" href="#Linux">
                        <div class="LinuxPaketler">
                            <h1><strong>Linux</strong> Hosting</h1>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </li>
                  <li>
                      <a data-toggle="tab" href="#Windows">
                        <div class="LinuxPaketler">
                            <h1><strong>Windows</strong> Hosting</h1>
                            <div class="clearfix"></div>
                        </div>
                      </a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#VDS">
                        <div class="LinuxPaketler">
                            <h1><strong>VDS</strong> Paketlerimiz</h1>
                            <div class="clearfix"></div>
                        </div>
                      </a>
                  </li>
                  <li>
                      <a data-toggle="tab" href="#Fiziksel">
                        <div class="LinuxPaketler">
                            <h1><strong>Fiziksel</strong> Paketlerimiz</h1>
                            <div class="clearfix"></div>
                        </div>
                      </a>
                  </li>
                </ul>

<div class="tab-content">
  <div id="Linux" class="tab-pane fade in active">
                <div class="row wow slideInLeft">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <div class="Kampanya">Kampanyalı</div>
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wow slideInRight">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-BİREYSEL</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>500 MB Disk Alanı</li>
                                            <li>3000 MB Aylık Trafik</li>
                                            <li>3 Adet Mail Hesabı</li>
                                            <li>10 MB Mailbox Boyutu</li>
                                            <li>1 Adet Subdomain</li>
                                            <li>1 MySQL Veritabanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wow slideInLeft">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wow slideInRight">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
  </div>
  <div id="Windows" class="tab-pane fade">
            <div class="row wow slideInLeft">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <div class="Kampanya">Kampanyalı</div>
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wow slideInRight">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-BİREYSEL</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>500 MB Disk Alanı</li>
                                            <li>3000 MB Aylık Trafik</li>
                                            <li>3 Adet Mail Hesabı</li>
                                            <li>10 MB Mailbox Boyutu</li>
                                            <li>1 Adet Subdomain</li>
                                            <li>1 MySQL Veritabanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wow slideInLeft">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row wow slideInRight">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
  </div>
<div id="VDS" class="tab-pane fade">
    <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <div class="Kampanya">Kampanyalı</div>
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-BİREYSEL</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>500 MB Disk Alanı</li>
                                            <li>3000 MB Aylık Trafik</li>
                                            <li>3 Adet Mail Hesabı</li>
                                            <li>10 MB Mailbox Boyutu</li>
                                            <li>1 Adet Subdomain</li>
                                            <li>1 MySQL Veritabanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="Fiziksel" class="tab-pane fade">
                <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <div class="Kampanya">Kampanyalı</div>
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-BİREYSEL</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>500 MB Disk Alanı</li>
                                            <li>3000 MB Aylık Trafik</li>
                                            <li>3 Adet Mail Hesabı</li>
                                            <li>10 MB Mailbox Boyutu</li>
                                            <li>1 Adet Subdomain</li>
                                            <li>1 MySQL Veritabanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="PaketList">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="LinPaketAdi">
                                        <h1>LN-SINIRSIZ</h1>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="LinPaketOzellik">
                                        <ul>
                                            <li>Sınırsız Web Site Barındırma</li>
                                            <li>Sınırsız Aylık Trafik</li>
                                            <li>Sınırsız MySQL Veritabanı</li>
                                            <li>Sınırsız E-Mail Hesabı</li>
                                            <li>Sınırsız Disk Alanı</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="LinPaketFiyat">
                                        <span>48.99<small><i class="fa fa-try" aria-hidden="true"></i></small> <p>YILLIK</p></span>
                                        <a href="#" class="btn btn-destek">SATIN AL</a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
  </div>
  </div>
</div>
        </div>
    </div>
<div class="clearfix"></div>
<div class="AnasayfaPaketBG">
    <div class="container wow slideInLeft">
        <div class="LinuxPaketler">
            <h1><strong>SSL</strong> Sertifika Paketlerimiz</h1>
            <div class="clearfix"></div>
        </div>
         <div id="myCarousel" class="carousel data-ride="carousel">
        <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
        </div>
        </div>
     </div>
    </div>
</div>
<div class="clearfix"></div>
<div class="Destek-area">
    <div class="col-md-12">
        <div class="row">
            <div class="container">
                <div class="LinuxPaketler" style="width: 320px;">
                    <h1><strong>7/24</strong> Destek</h1>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-8">
                <h5>Tüm soru ve sorunlarınızda yanınızda olduğumuzu bilmenizi ister, bizleri 7 gün 24 saat hiç çekinmeden arayabilir veya bizden teknik destek alabilirsiniz.</h5>
                  <ul>
                    <li><h3>TELEFON & E-MAİL</h3>
                        <span>Ekibimiz 7/24 iş başındadır. Taleplerinize en kısa sürede yanıt vermek için çalışmaktayız.</span>
                    </li>
                    <li><h3>MÜŞTERİ PANELİ</h3>
                        <span>Satın almış olduğunuz hizmetlerle ilgili tüm işlemlerinizi tamamen kullanıcı ve müşteri odaklı geliştirdiğimiz kontrol panelimizden gerçekleştirebilirsiniz..</span>
                    </li>
                    <li><h3>CANLI DESTEK</h3>
                        <span>Canlı destek üzerinde günün her saati her dakikası satış öncesi sorularınızı sorabilir, bizden bütün hizmetlerimiz hakkında bilgi alabilirsiniz.</span>
                    </li>
                  </ul>
                </div>
                <div class="col-md-4"></div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
</div>
<div class="clearfix"></div>
<div class="MutluMuster">
    <section id="carousel">
    		<div class="container">
                <div class="quote"><i class="fa fa-quote-left fa-4x"></i></div>
    			<div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
    			  <!-- Carousel indicators -->
                  <ol class="carousel-indicators">
    			    <li data-target="#fade-quote-carousel" data-slide-to="0" class="active"></li>
    			    <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
    			    <li data-target="#fade-quote-carousel" data-slide-to="2"></li>
                    <li data-target="#fade-quote-carousel" data-slide-to="3"></li>
                    <li data-target="#fade-quote-carousel" data-slide-to="4"></li>
                    <li data-target="#fade-quote-carousel" data-slide-to="5"></li>
    			  </ol>
    			  <!-- Carousel items -->
    			  <div class="carousel-inner">
    			    <div class="item active">
                        <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
    			    	<blockquote>
    			    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
    			    	</blockquote>
    			    </div>
    			    <div class="item">
                        <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
    			    	<blockquote>
    			    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
    			    	</blockquote>
    			    </div>
    			    <div class="item">
                        <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
    			    	<blockquote>
    			    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
    			    	</blockquote>
    			    </div>
                    <div class="item">
                        <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
    			    	<blockquote>
    			    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
    			    	</blockquote>
    			    </div>
                    <div class="item">
                        <div class="profile-circle"><img src="images/Avatar-Erkek.png" /></div>
    			    	<blockquote>
    			    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
    			    	</blockquote>
    			    </div>
                    <div class="item">
                        <div class="profile-circle"><img src="images/Avatar-Kadin.png" /></div>
    			    	<blockquote>
    			    		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quidem, veritatis nulla eum laudantium totam tempore optio doloremque laboriosam quas, quos eaque molestias odio aut eius animi. Impedit temporibus nisi accusamus.</p>
    			    	</blockquote>
    			    </div>
    			  </div>
    			</div>
    		</div>
            <div class="clearfix"></div>
    </section>
</div>
<div class="clearfix"></div>
<div class="cozum">
    <div class="container">
        <div class="row">
    		<div class="col-md-12">
               <div id="Carousel" class="carousel slide">
                    <div class="carousel-inner">
                    <div class="item active">
                    	<div class="row">
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/drupal.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/joomla.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/jquery.png" alt="Image" /></a></div>
                    	  <div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/magento.png" alt="Image" /></a></div>
                    	</div>
                    </div>
                    <div class="item">
                    	<div class="row">
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/mysql.png" alt="Image" /></a></div>
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/python.png" alt="Image" /></a></div>
                    		<div class="col-md-3"><a href="#" class="thumbnail"><img src="images/cozum/wordpress.png" alt="Image" /></a></div>
                    	</div>
                    </div>
                    </div>
                      <a data-slide="prev" href="#Carousel" class="left carousel-control"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i></a>
                      <a data-slide="next" href="#Carousel" class="right carousel-control"><i class="fa fa-chevron-circle-right" aria-hidden="true"></i></a>
               </div>
    		</div>
    	</div>
    </div>
</div>
<?php include('inc/footer.php'); ?>
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>