{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left"><i class="fa fa-globe" aria-hidden="true"></i> {cm_lang('Alan Adlarım')}</div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <table class="table table-bordered table-striped table-condensed table-responsive"><!--DestekTab-->
                <thead>
                <tr>
                    <th>{cm_lang('Alan Adı')}</th>
                    <th>{cm_lang('Kayıt Tarihi')}</th>
                    <th>{cm_lang('Bitiş Tarihi')}</th>
                    <th>{cm_lang('Durumu')}</th>
                    <th>{cm_lang('İşlem')}</th>
                </tr>
                </thead>
                <tbody>
                {if (!is_array($list_return))}
                    <tr>
                        <td colspan="7" style="text-align: center;"><strong>{cm_htmlclear($list_return)}</strong></td>
                    </tr>
                {else}
                    {foreach from=$list_return key=key item=value}
                        {assign var="dom_status" value="<span style=\"color: {$domain_status[$value['domain_status']]['color']};\"><strong>{cm_lang($domain_status[$value['domain_status']]['name'])}</strong></span>"}
                        {if $value['domain_idn_status']==1}
                            {assign var="dom_name" value="{cm_htmlclear($value['domain_name'])}<br />( {cm_htmlclear(domain_idn_utf8($value['domain_name']))} )"}
                        {else}
                            {assign var="dom_name" value="{cm_htmlclear($value['domain_name'])}"}
                        {/if}
                        <tr>
                            <td>{$dom_name}</td>
                            <td>{cm_date("d-m-Y",$value['domain_create_time'])}</td>
                            <td>{cm_date("d-m-Y",$value['domain_end_time'])}</td>
                            <td>{$dom_status}</td>
                            <td>

                                <a href="{if $value['domain_status']==1}{cm_link("client.php", ['view' => 'domain','page' => 'manage','id' => $value['domain_id']])}{else}javascript:;{/if}"{if $value['domain_status']!=1}onclick="return cm_warning('{cm_lang('Alan adı aktif değil yönetemezsiniz')}','error');"{/if}><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> {cm_lang('YÖNET')}</span></a>
                                {*
                                <a href="javascript:;" onclick="basket_add('action=add&product_type_id=7&id=<?=$value['domain_id']?>&year=1','','<?=$directory?>/sepet');"><span class="btn btn-danger"><i class="fa fa-cog" aria-hidden="true"></i> <?=cm_lang('UZAT')?></span></a>
                                *}
                            </td>
                        </tr>
                    {/foreach}
                {/if}
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
{include file="inc/footer.tpl"}