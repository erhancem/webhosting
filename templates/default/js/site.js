var jsonPostAction = function (form) {

    $("#"+form+" .alert").addClass('hidden');
    var buttonText  = $("."+form+'Button').text();
    var formUrl     = $("#"+form+"Form").attr('action');

    $("."+form+'Button').html('<i class="fa fa-spinner fa-spin"></i>');
    $("."+form+'Button').addClass('disabled', true);
    $("."+form+'Button').attr('disabled', true);

    console.log(formUrl);

    $.ajax({
        type: "POST",
        url:  formUrl,
        data: $("#"+form+"Form").serialize(),
        dataType: "json",
        success: function(data){

            $("#"+form+" .alert").removeClass('hidden');

            if(data["status"] == true){

                $("#"+form+" .alert").removeClass('alert-danger');
                $("#"+form+" .alert").addClass('alert-success');
                $("#"+form+" .alert").html(data['response']['message']);

                if(typeof(data['response']['redirect']) != "undefined" && data['response']['redirect'] !== null) {
                    return window.location.href = data['response']['redirect'];
                }

            }else{

                $("#"+form+" .alert").removeClass('alert-success');
                $("#"+form+" .alert").addClass('alert-danger');
                $("#"+form+" .alert").html(data["response"]);
            }

            $("."+form+'Button').html(buttonText);
            $("."+form+'Button').addClass('disabled', false);
            $("."+form+'Button').attr('disabled', false);

        }
    });

    console.log(form);

    return true;

};

var alertClear = function () {
    $(".alert").addClass('hidden');
};