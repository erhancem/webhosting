/* TopUp */
var BasketTotal		= 0;
var cm_siteurl = $('link[rel=alternate]').attr('href');

function NewFormToken(old_token){
	if(old_token && old_token != ""){
		$.ajax({
			url: cm_siteurl+'post.php?format=json',
			global: false,
			type: "POST",
			dataType: 'json',
			data: 'cm_action=token&action=new&cm_token='+old_token,
			cache: false,
			success: function(c){
				if(c.status == true){
					$('input[name=cm_token]').val(c.message["token"]);
				}
			}
		});
	}
	return true;
}

function buttonLoading(button,status){
	if(status == 1){
		$('.button-' + button).button('loading');
	}else{
		$('.button-' + button).button('reset');
	}
	return true;
}

function DataArray(string){
	var arr     = string.split("&");
	var array   = [];
	var t       = null;
	for(var i=0;i<arr.length;i++){
		t =  arr[i].split("=");
		array[t[0]] = t[1];
	}
	return array;
}

function PostAction(form,data,warning = 1){
	var post = $('form[name='+form+']').serialize();
	if(post == ""){
		return false;
	}
	buttonLoading(form, 1);
	if(typeof(data)  != "undefined"){
		post = post+'&'+data;
	}

	var data_array = DataArray(post);

	if(data_array['cm_token'] == ''){
		return false;
	}

	$.ajax({
		url: cm_siteurl+'post.php?format=json',
		global: false,
		type: "POST",
		dataType: 'json',
		data: post,
		cache: false,
		success: function(c){
			if(c.status == true){
				cm_warning(c.message["message"], 'success');
			}else{
				if(warning == 1) {
					cm_warning(c.message, 'error');
				}
			}

			NewFormToken(data_array['cm_token']);

			buttonLoading(form, 0);
		}
	});

	return true;
}
$(document).ready(function(){
	$('#accordion').on('hidden.bs.collapse', toggleChevron);
	$('#accordion').on('shown.bs.collapse', toggleChevron);
	 $('#Carousel').carousel({
        interval: 5000
    });
		$('[data-toggle="tooltip"]').tooltip();
     $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#back-to-top').fadeIn();
            } else {
                $('#back-to-top').fadeOut();
            }
        });
        // scroll body to 0px on click
        $('#back-to-top').click(function () {
            $('#back-to-top').tooltip('hide');
            $('body,html').animate({
                scrollTop: 0
            }, 800);
            return false;
        });

        $('#back-to-top').tooltip('show');
        $(".dropdown").hover(
	        function() {
	            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true);
	            $(this).toggleClass('open');
	        },
	        function() {
	            $('.dropdown-menu', this).not('.in .dropdown-menu').stop(true,true);
	            $(this).toggleClass('open');
	        }
	    );

});
function cm_warning(message,type = '',title = ''){
	var ms_title = 'Mesaj';
	if(title != ""){
		ms_title = title;
	}
	if(type == ''){
		type = 'error';
	}
	return swal({
		title: ms_title,
		text: message,
		type: type,
		showCancelButton: false,
	  	confirmButtonText: 'Tamam',
	});
}
function toggleChevron(e) {
    $(e.target)
        .prev('.panel-heading')
        .find("i.indicator")
        .toggleClass('fa fa fa-minus');
}

function domainpost(domain) {
    post = {'cm_action':'domain','action':'available','domain_name':domain,'format':'json'};
    $.ajax({
        url: "/post.php",
        global: true,
        type: "POST",
        dataType: 'json',
        data: post,
        cache: true,
        success: function(c){
            if(c.status == true){
                $.each(c.message, function(key, value){
                    domainSearchAdd(value);
                });
            }else{
                cm_warning(c.message, 'error');
            }
            $('.btn-lg').attr("disabled", false);
        }
    });
    return false;
}

/** Alan adı */
function domainavailable(domain){
	if(domain == ""){
		return cm_warning("Lütfen alan adı girin", 'error');
	}
	var post = null;
	$('.btn-lg').attr("disabled", true);
	var search_domains	= new Array();
	$(".domain_register_table").show();
	$("#domainTable > tbody").html("");
    search_domains		= domain.split("\n");
    if(search_domains.length > 0 && search_domains.length <= 50){
	    for(var dom=0;dom<search_domains.length;dom++){
	    	if(search_domains[dom] != ""){
                domainpost(search_domains[dom]);
	    	}
	    }
    }else{
    	$('.btn-lg').attr("disabled", false);
    	cm_warning("Maksimum 50 adet alan adı aratabilirsiniz", 'error');
    }
}

function domainSearchAdd(DomainInfo){

	var basket_button_value = $("#basket_button_value").val();
	if(typeof(basket_button_value)  == "undefined"){
		basket_button_value = 'Sepete Ekle';
	}
	var domain_name	= DomainInfo["domain"];
	var domain_div	= domain_name.replace(".", "-");
	var return_tr = '<tr><td>'+DomainInfo["domain"]+'</td>';
	if(DomainInfo["status"] > 0){
		var sts = 'DomainBos';
		var sts_w = 'Musait';
		if(DomainInfo["status"] == 2){
			sts = 'DomainDolu';
			sts_w = 'Dolu';
		}
		return_tr += '<td><span class="'+sts+'">'+sts_w+'</span></td>';
		var options = null;
		var basket = null;
		if(DomainInfo["status"] == 1){
			options = '<select class="form-control" id="'+domain_div+'year">';
			$.each(DomainInfo.register_price, function(key, value){
			  	options += '<option value="'+key+'">'+key+' / '+value["price"]+' '+value["currency"]+'</option>';
			});
			options += '</select';
			basket = 'action=add&product_type_id=6&domain_name='+domain_name+'&year=\'+document.getElementById(\''+domain_div+'year\').value+\'';
			return_tr += '<td>'+options+'</td>';
			var onclick = "basket_add('"+basket+"','"+domain_div+"button','');";
			return_tr += '<td><button onclick="'+onclick+'" id="'+domain_div+'button" class="btn-sepet"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs domain_write">'+basket_button_value+'</span></button></td>';
		}else{
			basket = 'action=add&product_type_id=8&domain_name='+domain_name+'&year=1&code=\'+document.getElementById(\''+domain_div+'code\').value+\'';
			options = '<input type="text" name="code" placeholder="Transfer Kodu" id="'+domain_div+'code" class="form-control" />';
			return_tr += '<td>'+options+'</td>';
			var onclick = "basket_add('"+basket+"','"+domain_div+"button','');";
			if(DomainInfo["transfer_status"] == 0){
    			onclick = "cm_warning('"+DomainInfo["message"]+"', 'error')";
			}
			return_tr += '<td><button onclick="'+onclick+'" id="'+domain_div+'button" class="btn-sepet"><i class="fa fa-shopping-cart"></i> <span class="hidden-xs domain_write">Transfer Et</span></button></td>';
		}
	}else{
		return_tr += '<td colspan="3"><span class="DomainDolu">'+DomainInfo["message"]+'</span></td>';
	}
	return_tr += '</tr>';
	$('#domainTable > tbody:last-child').append(return_tr);
}
/** Sepet */
function basket_add(feature,button,redirect){
	if(button != ""){
		$("#"+button).attr("disabled", true);
	}
	var post = feature+'&cm_action=basket&format=json';
	$.ajax({
		url: '/post.php',
		global: false,
		type: "POST",
		dataType: 'json',
		data: post,
		cache: false,
		success: function(c){
			if(c.status == true){
				if(redirect != ""){
					return document.location.href = redirect;
				}else if(button != ""){
					if(button != ""){
						$("#"+button).css("background-color", "#81bc00");
						$("#"+button).text("Eklendi");
						$("#"+button).attr("disabled", false);
					}
				}
			}else{
				if(button != ""){
					$("#"+button).attr("disabled", false);
				}
				cm_warning(c.message, 'error');
			}
		}
	});
}
function basket_total(){
	post = {'cm_action':'basket','action':'total','format':'json'};
	$.ajax({
		url: '',
		global: false,
		type: "POST",
		dataType: 'json',
		data: post,
		cache: false,
		success: function(c){
			if(c.status == true){
				$(".basket_total").html(c.message["total"]);
				BasketTotal = c.message["total"];
			}else{
				BasketTotal = 0;
				$(".basket_total").html('0');
			}
		}
	});
	return false;
}
/** Sunucu */
function serverPrice(){
	var productPrice	= parseFloat($('#product_id').data('product_price'));
	var osPrice			= parseFloat($('#os_id option:selected').data('os_price'));
	var osName			= $('#os_id option:selected').text();
	var panelPrice		= parseFloat($('#panel_id option:selected').data('panel_price'));
	var panelName		= $('#panel_id option:selected').text();
	var ipv4Count		= parseInt($('#ipv4').val());
	var ipv4Price		= parseFloat($('#ipv4').data('ipv4_price'));
	var ipv4Total		= (ipv4Price * ipv4Count);
	var total = 0;

	if(productPrice > 0){
		total = (total + productPrice);
	}
	if(osPrice > 0){
		total = (total + osPrice);
	}
	if(panelPrice > 0){
		total = (total + panelPrice);
	}
	if(ipv4Total > 0){
		total = (total + ipv4Total);
	}
	$("#os_name").html(osName);
	$("#os_price").html(osPrice.toFixed(2));

	$("#panel_name").html(panelName);
	$("#panel_price").html(panelPrice.toFixed(2));

	$("#ipv4_number").html(ipv4Count);
	$("#ipv4_price").html(ipv4Total);

	$("#ProductPrice").html(total.toFixed(2));
}
function os_select(id,type_id){
	post = {'action':'os_select','os_id':id,'type_id':type_id};
	$.ajax({
	    url: '',
	    global: false,
		type: "POST",
        dataType: 'json',
	    data: post,
	    cache: false,
	    success: function(c){
			if(c.status == true){
				$("#panel_id option").remove();
    			$.each(c.message, function(key, value){
				    $('#panel_id').append('<option value="'+key+'" data-panel_price="'+value.price+'">'+value.name+'</option>');
				});
				serverPrice();
    		}else{
    			cm_warning(c.message, 'error');
    		}
	    }
 	});
 	return false;
}
function MarkaHesapla(){
	var sinif_ucret = parseFloat($("#sinif_ucret").val());
	var ToplamSinif	= $(".marka_sinif:checkbox:checked").length;
	var HizmetUcret	= parseFloat($(".marka_tur:checked").attr("alt"));
	var Eksinif_ucret = parseFloat($("#ek_sinif_ucret").val());
	if(typeof HizmetUcret != "undefined" && ToplamSinif > 0){
		var Harc	= (ToplamSinif*sinif_ucret);
		var Toplam	= (HizmetUcret+Harc);
		if(ToplamSinif > 1){
			var extra = ((ToplamSinif-1)*Eksinif_ucret);
			Toplam = (Toplam+extra);
		}
		$("#HizmetBedel").html(HizmetUcret);
		$("#HarcBedel").html(Harc);
		$("#ToplamTutar").html(Toplam);
		$("#ToplamSinif").html(ToplamSinif);
	}else{
		$("#HizmetBedel").html("0");
		$("#HarcBedel").html("0");
		$("#ToplamTutar").html("0");
		$("#ToplamSinif").html("0");
	}
	return false;
}
function MarkaSatinAl(){
	var product_type_id = $("#product_type_id").val();
	var product_id = $("#product_id").val();
	var marka_adi = $("#marka_adi").val();
	if(marka_adi == ''){
		return alert('Lütfen marka adı girin');
	}
	var ToplamSinif	= $(".marka_sinif:checkbox:checked").length;
	if(ToplamSinif == 0){
		return alert('Lütfen marka sınıfı seçin');
	}
	var HizmetTuru	= $(".marka_tur:checked").val();
	if(HizmetTuru == ""){
		return alert('Lütfen hizmet türü seçin');
	}
	var MarkaOzellikleri = 'product_id='+product_id+'&feature[1]='+HizmetTuru+'&feature[3]='+marka_adi;
	$('.marka_sinif:checkbox:checked').each(function (){
	    var sThisVal = $(this).val();
	    MarkaOzellikleri += '&feature[2][]='+sThisVal;
	});
	basket_add(MarkaOzellikleri,'MarkaButton');
}