<?php
$theme['login'] = true;
include('control.php');
$order_id		= cm_get_request('id');
if($order_id == false or ($order_id and !cm_numeric($order_id))){ cm_url_go('orders'); }
$orders_params = array(
	'user_id'	=> $user['user_id'],
	'and'		=> array(
		'order_id'		=> $order_id,
		'order_delete'	=> 0,
	),
	'orderby'	=> array('order_id'=>'DESC'),
);
$order_list = orderGetList($orders_params);
if($order_list == false or ($order_list and $order_list['total'] == 0)){ cm_url_go('orders'); }

$order = $order_list['list'][$order_id];

include('inc/header.php');
include('inc/hesabim_menu.php');
?>
<div class="BosBG">
	<div class="panel panel-default">
		<div class="panel-heading">
			<div class="pull-left"><i class="fa fa-money" aria-hidden="true"></i> <?=cm_lang('Ödeme Tamamlandı')?></div>
			<div class="pull-right"><strong><?=cm_lang('Sipariş Numarası')?></strong> #<?=$order_id?></div>
			<div class="clearfix"></div>
		</div>
		<div class="panel-body" style="overflow: auto;">
<?php if($order['order_status'] == 1){ ?>
			<p style="color: green;font-weight: bold;text-align: center;"><?=cm_lang('Ödemeniz tamamlandı siparişleriniz alındı')?></p>
<?php }else{ ?>
			<p style="color: red;font-weight: bold;text-align: center;"><?=cm_lang('Ödemeniz tamamlanmamış ödeme yaptıktan sonra siparişleriniz aktif olacaktır')?></p>
<?php } ?>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
<?php include('inc/footer.php'); ?>