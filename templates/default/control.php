<?php
if(!isset($cm_config)){
	exit;
}
if(isset($theme['login']) and $theme['login'] == true){
	if($user['user_id'] == 0){
		cm_url_go('member-login');
	}
}
if(isset($_SESSION['user_id']) and cm_get_request('logout')){
	userLogout($user);
	cm_url_go();
}