{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left"><i class="fa fa-envelope" aria-hidden="true"></i> {cm_lang('Destek Taleplerim')}</div>
            <div class="pull-right"><a href="{cm_link('ticket.php',['page'=>'new'])}" class="btn btn-destek">{cm_lang('Yeni Talep Oluştur')}</a></div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <table class="table table-responsive DestekTab">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>{cm_lang('Talep Başlığı')}</th>
                    <th>{cm_lang('Durum')}</th>
                    <th>{cm_lang('Son Cevap')}</th>
                    <th>{cm_lang('İşlem')}</th>
                </tr>
                </thead>
                <tbody>
                {if !is_array($list_return)}
                    <tr>
                        <td colspan="5"><strong>{cm_htmlclear($list_return)}</strong></td>
                    </tr>
                {else}
                    {foreach from=$list_return key=key item=value}
                        {if $value['ticket_status'] == 1}
                            {assign var="ticketStatus" value="<span class=\"DurumAktif DurumAktifBG\">{cm_lang($ticket_status[$value['ticket_status']]['name'])}</span>"}
                        {elseif $value['ticket_status'] == 2}
                            {assign var="ticketStatus" value="<span class=\"DurumBekleme DurumBeklemeBG\">{cm_lang($ticket_status[$value['ticket_status']]['name'])}</span>"}
                        {elseif $value['ticket_status'] == 3}
                            {assign var="ticketStatus" value="<span class=\"DurumPasif DurumPasifBG\">{cm_lang($ticket_status[$value['ticket_status']]['name'])}</span>"}
                        {/if}
                        <tr>
                            <th>#{cm_htmlclear($value['ticket_id'])}</th>
                            <td>{cm_htmlclear($value['ticket_subject'])}</td>
                            <td>{$ticketStatus}</td>
                            <td>{cm_date(null,$value['ticket_end_time'])}</td>
                            <td>
                                <a href="{cm_link("ticket.php", ['page' => 'view', 'id' => $value['ticket_id']])}"><span class="btn btn-yesil"><i class="fa fa-search" aria-hidden="true"></i> {cm_lang('Mesajlar')}</span></a>
                            </td>
                        </tr>
                    {/foreach}
                {/if}
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
{include file="inc/footer.tpl"}