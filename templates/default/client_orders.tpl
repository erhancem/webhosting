{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left"><i class="fa fa-shopping-cart"></i> {cm_lang('Siparişlerim')}</div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <table class="table table-bordered table-striped table-condensed table-responsive">
                <thead>
                <tr>
                    <th>{cm_lang('Sipariş ID')}</th>
                    <th>{cm_lang('Durum')}</th>
                    <th>{cm_lang('Fiyat')}</th>
                    <th>{cm_lang('Tarih')}</th>
                    <th>{cm_lang('İşlem')}</th>
                </tr>
                </thead>
                <tbody>
                {if (!is_array($list_return))}
                    <tr>
                        <td colspan="7" style="text-align: center;"><strong>{cm_htmlclear($list_return)}</strong></td>
                    </tr>
                {else}
                    {foreach from=$list_return key=key item=value}
                        {assign var="status" value="<span style=\"color: {$order_status[$value['order_status']]['color']};\"><strong>{cm_lang($order_status[$value['order_status']]['name'])}</strong></span>"}
                        {assign var="payment" value=""}
                        <tr>
                            <td>{$key}</td>
                            <td>{$status}</td>
                            <td>{cm_price_format($value['paid'],$value['currency'])}</td>
                            <td>{cm_date(null,$value['order_time'])}</td>
                            <td><a href=""><span class="btn btn-yesil"><i class="fa fa-search" aria-hidden="true"></i> {cm_lang('Ürünler')}</span></a>{$payment}</td>
                        </tr>
                    {/foreach}
                {/if}
                </tbody>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
{include file="inc/footer.tpl"}