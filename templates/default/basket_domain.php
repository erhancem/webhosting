<?php
$page['title']			= 'CEM Bilişim - '.cm_lang('Sepet Yapılandırma');
$page['keywords']		= 'cem bilişim,vps,hosting,vds,fiziksel sunucu,Arma 3,oyun sunucu, Arma 3 oyun sunucu,sınırsız hosting, SSD hosting';
$page['description']	= 'Cem bilişim Kaliteli hosting (Sınırsız SSD Hosting), VPS, VDS, Fiziksel Sunucu, Oyun sunucuları ve Yazılım Hizmeti';
$page['copyright']		= 'Copyright © 2016, Cem Bilişim - Sunucu ve Yazılım Hizmetleri';
$basket_id	= cm_get_request('basket_id');
$action		= cm_get_request('action');
$Status = false;
if($basket_id and cm_numeric($basket_id) and $basket_id > 0){
	list($Status,$Basket) = basket_list($user,$basket_id);
}
if($Status == false){
	cm_url_go("sepetim");
}
$basket_detail	= $Basket['products'][$basket_id];
if($basket_detail['product_id'] == 0 or !array_key_exists('domain',$basket_detail['basket_feature'])){
	cm_url_go("sepetim");
}
cm_function_call('domain');
$domain_name	= cm_get_request('domain_name')?cm_get_request('domain_name'):null;
$domain_search	= false;
if(cm_get_request('domain_search')){
	if(cm_get_request('domain')){
		list($status_search,$message_search) = domainSearch(cm_get_request('domain'));
		if($status_search == true){
			$dm = $message_search[cm_get_request('domain')];
			if($dm['search_status'] == 1){
				$domain_name = $dm['name'].'.'.$dm['tld'];
			}
			$domain_search = $dm;
		}else{
			$domain_search = $message_search;
		}
	}else{
		$domain_search = cm_lang('Alan adı girin');
	}
}elseif(cm_get_request('domain_add')){
	if($domain_name != null and domainName($domain_name)){
		$basket_feature = "product_type_id=6&domain_name=".$domain_name."&year=1";
		basket_add($basket_feature,$user);
		list($sts,$msg) = basket_edit($basket_id,'domain_name='.$domain_name);
		if($sts == true){
			cm_url_go('basket-configuration?basket_id='.$basket_id);
		}else{
			$domain_search = cm_lang('Alan adı seçilemedi');
		}
	}else{
		$domain_search = cm_lang('Alan adı seçmediniz');
	}
}elseif(cm_get_request('domain_select')){
	if($domain_name != null and domainName($domain_name)){
		list($sts,$msg) = basket_edit($basket_id,'domain_name='.$domain_name);
		if($sts == true){
			cm_url_go('basket-configuration?basket_id='.$basket_id);
		}else{
			$domain_search = cm_lang('Alan adı seçilemedi');
		}
	}else{
		$domain_search = cm_lang('Alan adı seçmediniz');
	}
}
include('inc/header.php');
?>
<section>
<div class="container">
    <div class="BosBG">
        <div class="SepetBG">
        <div class="clearfix"></div>
            <div>
                <div class="row">
                	<div class="col-md-12">
                        <div id="SepetGetir">
                            <div id="SepetAlan">
                                <form action="" method="POST">
                                	<input type="hidden" name="domain_name" value="<?=$domain_name?>" />
                                    <div class="Baslik">
                                        <i class="fa fa-list-ul"></i> <?=cm_lang('Sepet Yapılandırma')?>
                                    </div>
                                    <div style="padding: 20px;">
<?php if(isset($message)){ ?>
<div class="form-group">
	<p style="color: red;"><?=$message?></p>
</div>
<?php } ?>
	<div class="HostingDomain" style="margin-bottom: 15px;">
		<ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#home"><i class="fa fa-plus" aria-hidden="true"></i> <?=cm_lang('Alan Adı Seç')?></a></li>
        </ul>
        <div class="tab-content">
            <div id="home" class="tab-pane fade in active">
                <div class="col-md-12">
                    <div class="AnasayfaDomainArama">
                        <div class="input-group input-group-lg" style="margin-top: 20px;">
                            <input type="text" class="form-control" name="domain" required="" placeholder="example.com" style="box-shadow: none" value="<?=cm_get_request('domain')?cm_htmlclear(cm_get_request('domain')):null?>" autocomplete="off" />
                            <span class="input-group-btn"><input type="submit" name="domain_search" class="btn btn-danger" value="Sorgula" style="border-radius:0 4px 4px 0; z-index: 199" />
                            </span>
                        </div>
                        <div class="clearfix"></div>
                        <table class="table table-striped custab">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="text-center"></th>
                                </tr>
                            </thead>
<?php if(is_array($domain_search)){ ?>
                            <tr>
                                <td class="text-center"><?=cm_htmlclear($domain_search['domain'])?></td>
                                <td class="text-center"><span style="color: <?=$domain_search['status']==1?'green':'red'?>;"></span><?=cm_htmlclear($domain_search['message'])?></td>
<?php if($domain_search['search_status'] == 1 and $domain_search['status']==1){ ?>
                                <td class="text-center"><button class='btn btn-success btn-sm' name="domain_add" value="1" type="submit"><span class="fa fa-shopping-cart"></span> <?=cm_lang('Sepete Ekle')?></button></td>
<?php }elseif($domain_search['search_status'] == 1 and $domain_search['status']==0){ ?>
                                <td class="text-center"><button class='btn btn-danger btn-sm' name="domain_select" value="1" type="submit"> <?=cm_lang('Kullan')?> </button></td>
<?php } ?>
                            </tr>
<?php }elseif($domain_search){ ?>
							<tr>
                                <td colspan="3"><?=$domain_search?></td>
                            </tr>
<?php } ?>
                        </table>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
	</div>
	<div class="clearfix"></div>
										<div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php include('inc/footer.php'); ?>