{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="UyeBG">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="UyeBaslik">
                <i class="fa fa-user fa-2x"></i> <span>{cm_lang('Hesap Bilgilerim')} </span>
            </div>
            <form name="FormInfo" class="form-horizontal" method="POST" action="" onsubmit="return false;">
                <input type="hidden" name="cm_action" value="account" />
                <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                <input type="hidden" name="action" value="edit" />
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Üyelik Durumu')}:</label>
                    <div class="col-sm-9">
                        <p><strong>{if $user['user_verify']==1}<span style="color: green;">{cm_lang('Onaylı')}</span>{else}<span style="color: red;">{cm_lang('Onaysız')} - {cm_lang('Telefon ile onay yapmanız gerekir')}</span>{/if}</strong></p>

                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Üyelik Türü')}:</label>
                    <div class="col-sm-3">
                        <label class="radio-inline"><input type="radio" name="user_type"{if $user['user_type']==0} checked=""{/if} class="user_type" onchange="userTypeChange()" value="0"{if $user['user_verify']==1} disabled=""{/if} />{cm_lang('Bireysel')}</label>
                    </div>
                    <div class="col-sm-6">
                        <label class="radio-inline"><input type="radio" name="user_type"{if $user['user_type']==1} checked=""{/if} class="user_type" onchange="userTypeChange()" value="1"{if $user['user_verify']==1} disabled=""{/if} />{cm_lang('Kurumsal')}</label>
                    </div>
                </div>
                <div class="form-group companyName" style="display: none;">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Kurum Adı')}:</label>
                    <div class="col-sm-9">
                        <input type="text" name="company_name" class="form-control companyInput" id="username" placeholder="{cm_lang('Kurum Adı')}" value="{cm_htmlclear($user['company_name'])}"{if $user['user_verify']==1} disabled=""{/if} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Kullanıcı Adı')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="username" id="username" placeholder="{cm_lang('Kullanıcı Adı')}" value="{cm_htmlclear($user['username'])}" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Email Adresi')}:</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="email_address" id="username" placeholder="{cm_lang('Email Adresi')}" value="{cm_htmlclear($user['email_address'])}" required="" />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Adınız')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="first_name" id="name" placeholder="{cm_lang('Adınız')}" value="{cm_htmlclear($user['first_name'])}" required=""{if $user['user_verify']==1} disabled=""{/if} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Soyadınız')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="last_name" id="name" placeholder="{cm_lang('Adınız')}" value="{cm_htmlclear($user['last_name'])}" required=""{if $user['user_verify']==1} disabled=""{/if} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Doğum Tarih')}:</label>
                    <div class="col-sm-9">
                        <input type="date" class="form-control" name="birth_date" id="name" value="{cm_htmlclear($user['birth_date'])}" required=""{if $user['user_verify']==1} disabled=""{/if} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="pwd">{cm_lang('Ülke')}:</label>
                    <div class="col-sm-9">
                        <select name="country_id" class="form-control countrySelect" onchange="countryChange();"{if $user['user_verify']==1} disabled=""{/if}>
                            <option value="">{cm_lang('Lütfen ülke seçin')}</option>
                            {foreach from=$country key=key item=value}
                                <option value="{$value['country_id']}" title="{$value['country_phone_code']}"{if ($user['address']['country_id']==$value['country_id'])} selected="selected"{/if}>{$value['country_name']}</option>
                            {/foreach}
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Telefon')}:</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control phone_code" name="phone_code" id="email" disabled="" value="0" />
                    </div>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="email" name="phone_number" value="{cm_htmlclear($user['phone_number'])}" required=""{if $user['phone_verify']==1} disabled=""{/if} />
                    </div>
                    <div class="clearfix"></div>
                    {if $user['phone_verify'] == 0}
                    <div class="col-sm-offset-3 col-sm-9" style="margin-top: 10px;">
                        <p><a href="#">{cm_lang('Telefonunuzu onaylayın')}</a></p>
                    </div>
                    {/if}
                </div>
                <div class="form-group tcno" style="display: none;">
                    <label class="control-label col-sm-3" for="email">{cm_lang('TC Kimlik Numarası')}:</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control tcinput" name="country_no" id="pwd" value="{cm_htmlclear($user['country_no'])}" placeholder="{cm_lang('TC Kimlik Numarası')}"{if $user['user_verify']==1} disabled=""{/if} />
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-3" for="email">{cm_lang('Cinsiyet')}:</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="gender">
                            <option value="1"{if $user['gender']==1} selected=""{/if}>{cm_lang('Erkek')}</option>
                            <option value="2"{if $user['gender']==2} selected=""{/if}>{cm_lang('Bayan')}</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="button" onclick="PostAction('FormInfo');" data-loading-text="<i class='fa fa-spinner fa-spin'></i> {cm_lang('İşlem Yapılıyor')}" class="btn btn-default button-FormInfo">
                            {cm_lang('Güncelle')}
                        </button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-2"></div>
        <div class="clearfix"></div>
        <hr />
        <div class="clearfix"></div>
    </div>
</div>
{include file="inc/footer.tpl"}
<script>
    $(document).ready(function(){
        countryChange();
        userTypeChange();
    });
    function countryChange(){
        var select = $(".countrySelect :selected");
        var code = select.attr('title');
        $(".phone_code").val(code);
        if(code == 90){
            $(".tcno").show();
            $('.tcinput').attr('required', 'required');
        }else{
            $(".tcno").hide();
            $('.tcinput').removeAttr('required');
        }
    }
    function userTypeChange(){
        var select = $(".user_type:checked");
        var val = select.val();
        if(val == 1){
            $(".companyName").show();
            $('.companyInput').attr('required', 'required');
        }else{
            $(".companyName").hide();
            $('.companyInput').removeAttr('required');
        }
    }
</script>