<?php
$theme['login'] = true;
include('control.php');
$server_id		= cm_get_request('id')?cm_get_request('id'):0;
if(!cm_numeric($server_id)){
	cm_url_go("services");
}
$service_type	= 'service';
$server			= serviceGetDetails($server_id,$service_type);

if(!$server or ($server and ($server['user_id'] != $user['user_id'] or $server['service_delete'] == 1 or $server['service_status'] != 1 or $server['service_end_time'] < cm_time()))){
	cm_url_go("services");
}

$module_id		= $server['module_id'];
$module_status	= false;
$module_menu	= null;
$module_title	= null;
$modmenu		= 'detail';
$module_output	= null;
if($module_id > 0){
	list($module_status,$module) = cm_module_list($module_id);
	if($module_status == true){
		$modmenu	= (cm_get_request('modmenu')?cm_get_request('modmenu'):'detail');
		$module_dir	= cm_path.'modules/'.$module['module_type'].'/';
		list($menu_status,$menu_list) = moduleUserMenu($server['server_id'],$service_type);
		if($menu_status == true){
			$module_menu = $menu_list;
			if($modmenu != "detail"){
				if(!in_array($modmenu,array_keys($module_menu))){
					cm_url_go("service-manage?id=".$server_id);
				}
			}
		}
		if(is_array($module_menu) and in_array($modmenu,array_keys($module_menu))){
			list($module_status,$module_params) = moduleUserPanel($server['server_id'],$service_type);
			if($module_status == true){
				if(!is_array($module_params)){
					$module_output = $module_params;
				}elseif(array_key_exists("theme",$module_params)){
					$module_theme	= $module_dir.$module['module'].'/'.$module_params['theme'];
					if(is_file($module_theme)){
						ob_start();
						include($module_theme);
						$module_output = ob_get_contents();
						ob_end_clean();
					}else{
						$module_output = cm_lang('Modülün html kaynağı çekilemedi');
					}
				}elseif(array_key_exists("html",$module_params)){
					$module_output	= $module_params['html'];
				}else{
					$module_output = cm_lang('Modül html çıktı gelmedi');
				}
			}else{
				$module_output	= $module_params;
			}
		}
	}
}

include('inc/header.php');
include('inc/hesabim_menu.php');
?>
<div class="BosBG">
	<div class="panel panel-default">
	    <div class="panel-heading">
	        <div class="pull-left"><i class="fa fa-gear" aria-hidden="true"></i> <?=cm_htmlclear($server['product']['name'])?></div>
	        <div class="clearfix"></div>
	    </div>
	    <div class="panel-body" style="overflow: auto;">
	    <div class="row">
	        <div class="col-sm-3">
	            <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well ServerList">
	              <li<?=$modmenu=='detail'?' class="active"':null?>><a href="<?=(is_array($module_menu) and $modmenu != "detail")?$directory.'/server-manage?id='.$server_id:'javascript:;'?>"><?=cm_lang('Bilgi')?></a></li>
<?php
if(is_array($module_menu)){
foreach($module_menu as $key=>$value){
	$url = $directory.'/server-manage?id='.$server_id.'&modmenu='.urlencode($key);
	if($key == $modmenu){
		$module_title = $value;
		$url = 'javascript:;';
	}
?>
	              <li<?=$modmenu==$key?' class="active"':null?>><a href="<?=$url?>"><?=cm_htmlclear($value)?></a></li>
<?php }} ?>
	            </ul>
	        </div>
	        <div class="col-sm-9">
	            <div class="tab-content">
	                <div role="tabpanel" class="tab-pane fade<?=$modmenu=='detail'?' in active':null?>">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="pull-left"><?=cm_lang('Paket Bilgilerim')?></div>
									<div class="pull-right"></div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-body" style="overflow: auto;">
									<table class="table table-responsive DestekTab">
										<tbody>
											<tr>
												<th style="border: none!important;font-weight: bold; text-align: right;"><?=cm_lang('Paket Adı')?> :</th>
												<td style="border: none!important;text-align: left;"><?=cm_htmlclear($server['product']['name'])?></td>
											</tr>
											<tr>
												<th style="border: none!important;font-weight: bold; text-align: right;"><?=cm_lang('Kayıt Tarihi')?> :</th>
												<td style="border: none!important;text-align: left;"><?=cm_date("d-m-Y",$server['service_create_time'])?></td>
											</tr>
											<tr>
												<th style="border: none!important;font-weight: bold; text-align: right;"><?=cm_lang('Bitiş Tarihi')?> :</th>
												<td style="border: none!important;text-align: left;"><?=cm_date("d-m-Y",$server['service_end_time'])?></td>
											</tr>
										</tbody>
									</table>
								</div>
							</div>
						</div>
	                </div>
<?php if($module_output != null){ ?>
	                <div role="tabpanel" class="tab-pane fade<?=$modmenu!='detail'?' in active':null?>">
						<div class="col-md-12">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="pull-left"><?=cm_htmlclear($module_title)?></div>
									<div class="pull-right"></div>
									<div class="clearfix"></div>
								</div>
								<div class="panel-body" style="overflow: auto;">
<?=$module_output?>

								</div>
							</div>
						</div>
	                </div>
<?php } ?>
	            </div>
	        </div>
	    </div>
	    </div>
	</div>
	<div class="clearfix"></div>
</div>
<?php include('inc/footer.php'); ?>