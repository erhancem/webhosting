<?php
$basket_id = cm_get_request('basket_id');
$Status = false;
if($basket_id and cm_numeric($basket_id) and $basket_id > 0){
	list($Status,$Basket) = basket_list($user,$basket_id);
}
if($Status == false){
	cm_url_go("sepetim");
}
$basket_detail	= $Basket['products'][$basket_id];
$basket_feature	= $basket_detail['feature'];
$product 		= $basket_detail['product'];
$Input = array();
foreach($Basket['products'] as $key=>$value){
?>
                                        <div class="Urun Basket<?=$key?>">
                                            <div class="pull-left UrunAdi"><strong>Ürün</strong>: <?=cm_htmlclear($value['product_name'])?></div>
                                            <div class="pull-right UrunFiyat"><?=cm_price_format($value['price'],$value['currency'])?></div>
                                            <div class="clearfix"></div>
                                            <div>
<?php
if(is_array($value['basket_feature'])){
	echo "<div style=\"margin-top: 10px;\"></div>\n";
	foreach($value['basket_feature'] as $key1=>$value1){
?>
                                            <ul>
                                                <li>
                                                    <div class="pull-left"><span><?=cm_htmlclear($value1['name'])?></span>: <?=cm_htmlclear($value1['value'])?></div>
<?php if($value1['status'] == 1){ ?>
                                                    <div class="pull-right IcerikFiyat"><?=cm_price_format($value1['price'],$value['currency'])?></div>
<?php }else{ ?>
													<div class="pull-right IcerikFiyat"></div>
<?php } ?>
                                                    <div class="clearfix"></div>
                                                </li>
                                            </ul>
<?php }} ?>
											</div>
                                            <div class="clearfix"></div>
                                            <div class="pull-left"></div>
                                            <div class="pull-right"></div>
                                            <div class="clearfix"></div>
                                        </div>
<?php } ?>