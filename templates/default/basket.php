<?php
include('inc/header.php');
?>
<section>
<div class="container">
    <div class="BosBG">
        <div class="SepetBG">
        <div class="clearfix"></div>
<?php if($basket_list == false){ ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="SebetBox">
                        <div class="box-content">
                            <div class="empty-cart recommendation">
                                <h1 class="h1"><?=cm_lang('Sepetinizde hiç ürün yok')?></h1>
                                <p><?=cm_lang('Ürünlerimize göz atmak ister misiniz?')?></p>
                                <hr />
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="AnasayfaPaket">
                                            <div class="PaketIcon"><i class="fa fa-server" aria-hidden="true"></i></div>
                                            <div class="PaketIsmi"><span><?=cm_lang('HOSTING')?></span> <?=cm_lang('Paketleri')?></div>
                                            <br />
                                            <a href="#" class="btn-satinal"><?=cm_lang('Paketi İncele')?></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="AnasayfaPaket">
                                            <div class="PaketIcon"><i class="fa fa-cloud" aria-hidden="true"></i></div>
                                            <div class="PaketIsmi"><span><?=cm_lang('SUNUCU')?></span> <?=cm_lang('Paketleri')?></div>
                                            <br />
                                            <a href="#" class="btn-satinal"><?=cm_lang('Paketi İncele')?></a>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="AnasayfaPaket">
                                            <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                                            <div class="PaketIsmi"><span><?=cm_lang('SSL')?></span> <?=cm_lang('Paketleri')?></div>
                                            <br />
                                            <a href="#" class="btn-satinal"><?=cm_lang('Paketi İncele')?></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
<?php }else{ ?>
			<div class="row">
				<div class="col-md-8">
					<div class="panel panel-info" style="border-color: #222222!important;">
						<div class="panel-heading" style="color: #ffffff;background-color: #333333;border-color: #1f1e1e;">
							<div class="panel-title">
								<div class="row">
									<div class="col-xs-10">
										<span id="basketmessage" style="display: none;"><?=cm_lang('Ürün sepetinizden silinecektir Onaylıyormusunuz?')?></span>
										<h5><span class="fa fa-shopping-cart"></span> <?=cm_lang('Alışveriş Sepetim')?></h5>
									</div>
									<div class="col-xs-2">

									</div>
								</div>
							</div>
						</div>
						<div class="panel-body">
<?php foreach($basket_list as $key=>$value){ ?>
							<div class="row">
								<div class="col-xs-4">
									<h4 class="product-name" style="font-size: 15px;"><strong id="product<?=$key?>"><?=cm_htmlclear($value['product_name'])?></strong></h4>
<?php if(count($value['basket_config']) > 0){
foreach($value['basket_config'] as $key1=>$value1){
?>
									<h4><small><?=cm_htmlclear($value1['name'])?> : <?=cm_htmlclear($value1['value'])?></small></h4>
<?php }} ?>
								</div>
								<div class="col-xs-8">
									<div class="col-xs-6">
<?php if(isset($value['product']['prices_list']) and $value['product']['price_status'] == 2){ ?>
										<select class="form-control" name="period" onchange="basket_period(<?=$key?>,this.value);">
<?php foreach($value['product']['prices_list'] as $key2=>$value2){
?>
											<option value="<?=$key2?>"<?=$value['feature']['period']==$key2?' selected=""':null?>><?=product_period($value['feature']['action'],$value['product_type_id'],$value2)?></option>
<?php } ?>
										</select>
<?php }elseif(isset($value['product']['prices_list']) and $value['product']['price_status'] == 1 and $value['product']['product_multi'] == 1){ ?>
										<input type="number" min="1" value="<?=$value['amount']?>" class="form-control" onchange="basket_amount(<?=$key?>,this.value);" />
<?php } ?>
<?php if(isset($value['domain_price_list'])){ ?>
										<select class="form-control" name="domain_year" onchange="basket_domain_year(<?=$key?>,this.value);">
<?php foreach($value['domain_price_list'] as $key2=>$value2){
?>
											<option value="<?=$key2?>"<?=$value['feature']['year']==$key2?' selected=""':null?>><?=$key2?> <?=cm_lang('Yıl')?></option>
<?php } ?>
										</select>
<?php } ?>
									</div>
									<div class="col-xs-4 text-right">
										<h6><strong><?=cm_price_format($value['price'],$cm_config['default_currency'])?></strong></h6>
									</div>
									<div class="col-xs-2">
										<button type="button" class="btn btn-link btn-xs" onclick="basket_delete(<?=$key?>);">
											<span style="color: red;" class="fa fa-trash"> </span>
										</button>
									</div>
								</div>
							</div>
							<hr />
<?php } ?>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-default" style="border-color: #222222!important;">
					    <div class="panel-heading text-center" style="color: #ffffff;background-color: #333333;border-color: #1f1e1e;">
					        <h4><?=cm_lang('Sipariş Tutar')?></h4>
					    </div>
					    <div class="panel-body">
					            <div class="col-md-12">
					                <strong><?=cm_lang('Toplam Fiyat')?></strong>
					                <div class="pull-right"><?=cm_price_format($basket_calc['price'],$cm_config['default_currency'])?></div>
					            </div>
					            <div class="col-md-12">
					                <strong><?=cm_lang('İndirim')?></strong>
					                <div class="pull-right"><?=cm_price_format($basket_calc['discount'],$cm_config['default_currency'])?></div>
					            </div>
					            <div class="col-md-12">
					                <strong><?=cm_lang('KDV')?></strong>
					                <div class="pull-right"><?=cm_price_format($basket_calc['tax'],$cm_config['default_currency'])?></div>
					                <hr />
					            </div>
					            <div class="col-md-12">
					                <strong><?=cm_lang('Genel Toplam')?></strong>
					                <div class="pull-right"><?=cm_price_format($basket_calc['total'],$cm_config['default_currency'])?></div>
					                <hr>
					            </div>

					            <button type="button" class="btn btn-success btn-lg btn-block"><?=cm_lang('Ödeme Yap')?></button>

					    </div>

					</div>
				</div>
			</div>
<script>
function basket_domain_year(id,year){
	post = {'cm_action':'basket','action':'domain_year','year':year,'id':id,'format':'json'};
	$.ajax({
		url: '/post.php',
		global: false,
		type: "POST",
		dataType: 'json',
		data: post,
		cache: false,
		success: function(c){
			if(c.status == true){
				return window.location.href = '';
			}else{
				alert(c.message);
			}
		}
	});
	return true;
}
function basket_amount(id,amount){
	post = {'cm_action':'basket','action':'amount','amount':amount,'id':id,'format':'json'};
	$.ajax({
		url: '/post.php',
		global: false,
		type: "POST",
		dataType: 'json',
		data: post,
		cache: false,
		success: function(c){
			if(c.status == true){
				return window.location.href = '';
			}else{
				alert(c.message);
			}
		}
	});
	return true;
}
function basket_period(id,period){
	post = {'cm_action':'basket','action':'period_change','period':period,'id':id,'format':'json'};
	$.ajax({
		url: '/post.php',
		global: false,
		type: "POST",
		dataType: 'json',
		data: post,
		cache: false,
		success: function(c){
			if(c.status == true){
				return window.location.href = '';
			}else{
				alert(c.message);
			}
		}
	});
	return true;
}
function basket_delete(id){
	var productName = $("#product"+id).text();
	var basketmessage = $("#basketmessage").text();
	swal({
		title: productName,
		text: basketmessage,
		type: "warning",
		showCancelButton: true,
		confirmButtonText: 'Evet!',
		cancelButtonText: 'Hayır'
	}).then(function() {
		post = {'cm_action':'basket','action':'delete','id':id,'format':'json'};
		$.ajax({
			url: '/post.php',
			global: false,
			type: "POST",
			dataType: 'json',
			data: post,
			cache: false,
			success: function(c){
				if(c.status == true){
					return window.location.href = '';
				}else{
					alert(c.message);
				}
			}
		});
	}, function(dismiss) {
	});
	return false;
}
</script>
<?php } ?>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php include('inc/footer.php'); ?>