{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="pull-left"><i class="fa fa-cloud" aria-hidden="true"></i> {cm_lang('Hosting Yönetimi')}</div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body" style="overflow: auto;">
            <div class="row">
                {if $module_status==true}
                    {$module_message}
                {else}
                    <div class="col-md-12">
                    {if ($module_message!=null)}
                        <p>{$module_message}</p>
                    {else}
                        <table class="table table-responsive DestekTab">
                            <tbody>
                            <tr>
                                <th style="border: none!important;font-weight: bold; text-align: right;">{cm_lang('Paket Adı')} :</th>
                                <td style="border: none!important;text-align: left;">{cm_htmlclear($serviceinfo['product']['name'])}</td>
                            </tr>
                            <tr>
                                <th style="border: none!important;font-weight: bold; text-align: right;">{cm_lang('Alan Adı')} :</th>
                                <td style="border: none!important;text-align: left;">{cm_htmlclear($serviceinfo['domain_name'])}</td>
                            </tr>
                            <tr>
                                <th style="border: none!important;font-weight: bold; text-align: right;">{cm_lang('Başlama Tarihi')} :</th>
                                <td style="border: none!important;text-align: left;">{cm_date('d-m-Y',$serviceinfo['hosting_create_time'])}</td>
                            </tr>
                            <tr>
                                <th style="border: none!important;font-weight: bold; text-align: right;">{cm_lang('Bitiş Tarihi')} :</th>
                                <td style="border: none!important;text-align: left;">{cm_date('d-m-Y',$serviceinfo['hosting_end_time'])}</td>
                            </tr>
                            </tbody>
                        </table>
                    {/if}
                    </div>
                {/if}
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
{include file="inc/footer.tpl"}