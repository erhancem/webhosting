<?php
include('inc/header.php');
?>
<section>
<div class="AnasayfaPaketBG">
    <div class="container">
        <div class="LinuxPaketler">
            <h1><strong>SSL</strong> Sertifika Paketlerimiz</h1>
            <div class="clearfix"></div>
        </div>
         <div id="myCarousel" class="carousel data-ride="carousel">
        <div class="carousel-inner" role="listbox">
        <div class="item active">
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="javascript:;" onclick="basket_add('action=add&product_type_id=13&product_id=5','','<?=$directory?>/sepet');" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="javascript:;" onclick="basket_add('action=add&product_type_id=2&product_id=6','','<?=$directory?>/sepet');" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="javascript:;" onclick="basket_add('action=add&product_type_id=17&product_id=7','','<?=$directory?>/sepet');" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="item">
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="AnasayfaPaket">
                    <div class="PaketIcon"><i class="fa fa-lock" aria-hidden="true"></i></div>
                    <div class="PaketIsmi">Comodo <span>SSL</span></div>
                    <div class="PaketAciklama">128 Bit SSL koruması</div>
                    <div class="PaketFiyat">
                        <div class="PaketEskiFiyat"><span class="ortacizgi">100<small>,88 TL</span></small><p>/YIL</p></div>
                        <div class="clearfix"></div>
                        <div class="PaketYeniFiyat">100<small>,88 TL</small><p>/YIL</p></div>
                    </div>
                    <a href="#" class="btn-satinal">SATIN AL</a>
                    <div class="PaketIcerik">
                        <div class="row">Belge <span>Gerektirmez</span></div>
                        <div class="row"><span>Anında</span> Aktivasyon</div>
                        <div class="row"><span>10.000$</span> Garanti</div>
                    </div>
                </div>
            </div>
        </div>
        </div>
     </div>
    </div>
</div>
<?php
include('detail.php');
include('inc/footer.php');
?>