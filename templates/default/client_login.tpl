{include file="inc/header.tpl"}
<section>
    <div class="UyeGirisBG">
        <div class="container">
            <h1 class="text-center">{$cm_lang->langGet('Müşteri İşlemleri')}</h1>
        </div>
    </div>
    <div class="container">
        <div class="BosBG">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <div class="UyeIslemleriBaslikBG">
                        <div class="UyeIslemleriBaslik">
                            <span>Müşteri Paneli</span>
                        </div>
                        <ul id="nav-tabs-wrapper">
                            <li><a href="#memberLogin" onclick="alertClear();" class="memberLogin" data-toggle="tab">Müşteri Oturum Aç</a></li>
                            <li><a href="#memberRegister" onclick="alertClear();" class="memberRegister" data-toggle="tab">Müşterimiz Olun</a></li>
                            <li><a href="#memberForgot" onclick="alertClear();" class="memberForgot" data-toggle="tab">Şifremi Unuttum</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-9 col-sm-3">

                    <!-- Musteri Giris Formu -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="memberLogin">
                            <div class="UyeGirisForm">
                                <form action="/login.php" id="memberLoginForm">
                                    <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                    <input type="hidden" name="action" value="login" />

                                    <div class="hidden alert alert-success">Başarılı</div>

                                    <h1>{$cm_lang->langGet('Müşteri Girişi')}</h1>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>{$cm_lang->langGet('Kullanıcı Adı veya E-Posta')}</label>
                                            <input type="text" name="username" id="username" class="form-control" value="{cm_htmlclear(cm_get_request('username'))}" required="" autocomplete="off" />
                                        </div>
                                        <div class="form-group">
                                            <label>{$cm_lang->langGet('Parolanız')}</label>
                                            <input type="password" name="password" id="password" class="form-control" required="" autocomplete="off" />
                                        </div>
                                        <button type="button" onclick="jsonPostAction('memberLogin');" class="GirisBTN memberLoginButton">{$cm_lang->langGet('Üye Girişi Yap')}</button>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-2"></div>
                                </form>
                            </div>
                        </div>

                        <!-- Musteri Kayıt Formu -->
                        <div role="tabpanel" class="tab-pane fade" id="memberRegister">
                            <div class="UyeGirisForm">

                                <div class="hidden alert alert-success">Başarılı</div>

                                <h1>{$cm_lang->langGet('Müşterimiz Olun')}</h1>
                                <div class="clearfix"></div>
                                <form id="memberRegisterForm" action="" method="POST">
                                    <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                                    <input type="hidden" name="action" value="register" />
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>{$cm_lang->langGet('E-Posta')}</label>
                                                <input type="text" name="email_address" class="form-control" value="{cm_htmlclear(cm_get_request('email_address'))}" />
                                            </div>
                                            <div class="form-group">
                                                <label>{$cm_lang->langGet('Parolanız')}</label>
                                                <input type="password" name="password" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <label>{$cm_lang->langGet('Parola Tekrar')}</label>
                                                <input type="password" name="password_control" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4"></div>
                                    <div class="col-md-4">
                                        <button id="UyeOl" class="GirisBTN">{$cm_lang->langGet('Üye Ol')}</button>
                                    </div>
                                    <div class="col-md-4"></div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>

                        <!-- Sifremi Unuttum -->
                        <div role="tabpanel" class="tab-pane fade " id="memberForgot">
                            <form action="hesabim" method="POST" data-action="demo">
                                <div class="UyeGirisForm">
                                    <h1>Şifremi Unuttum :(</h1>
                                    <div class="col-md-2"></div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label>TC Kimlik Numaranız</label>
                                            <input type="number" min="1" max="99999999999" maxlength="11" class="form-control">
                                        </div>
                                        <div class="form-group">
                                            <label>E-Posta Hesabınız</label>
                                            <input type="text" class="form-control">
                                        </div>
                                        <input type="submit" class="GirisBTN" value="Şifremi Sıfırla">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-2"></div>

                                </div>
                            </form>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    {literal}
        <script>

        </script>
    {/literal}
{include file="inc/footer.tpl"}