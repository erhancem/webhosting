<?php
include('inc/header.php');
include('inc/hesabim_menu.php');
?>
<div class="BosBG">
    <div class="container">
        <div class="TalepOkuIc">
            <div class="TalepAcIc">
                <form name="submitticket" method="POST" action="">
                	<input type="hidden" name="action" value="report" />
                	<input type="hidden" name="cm_token" value="<?=$cm_set_token?>" />
                    <div class="row">
                        <div class="col-xs-6 col-md-2"><strong><?=cm_lang('Sipariş Numarası')?></strong></div>
                        <div class="col-xs-1 col-md-1 Mgizle">:</div>
                        <div class="col-xs-4 col-md-9"><?=$order['order_id']?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-md-2"><strong><?=cm_lang('Ödenecek Tutar')?></strong></div>
                        <div class="col-xs-1 col-md-1 Mgizle">:</div>
                        <div class="col-xs-4 col-md-9"><?=cm_price_format($order['paid'],$order['currency'])?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-2"><strong><?=cm_lang('Banka')?></strong></div>
                        <div class="col-xs-1 col-md-1 Mgizle">:</div>
                        <div class="col-xs-12 col-md-9">
                            <select name="deptid" class="form-control borderradius">
                                <option value="1" selected="selected">BANKA SEÇİN</option>
                                <option value="2">İş Bankası</option>
                                <option value="3">Garanti</option>
                                <option value="4">Yapı Kredi</option>
                                <option value="5">Kuveyt Türk</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-2"><strong><?=cm_lang('Ödenen Tutar')?></strong></div>
                        <div class="col-xs-1 col-md-1 Mgizle">:</div>
                        <div class="col-xs-12 col-md-9"><input type="text" class="form-control reportPrice" name="amount" value="<?=cm_htmlclear($order['paid'])?>"/></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-2"><strong><?=cm_lang('Ödeme Yapan')?></strong></div>
                        <div class="col-xs-1 col-md-1 Mgizle">:</div>
                        <div class="col-xs-12 col-md-9"><input type="text" name="name" class="form-control" value="<?=cm_get_request('name')?cm_htmlclear(cm_get_request('name')):cm_htmlclear(cm_user_name($user))?>"/></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-md-2"><strong><?=cm_lang('Açıklama veya Not')?></strong></div>
                        <div class="col-xs-1 col-md-1 Mgizle">:</div>
                        <div class="col-xs-12 col-md-9"><textarea class="form-control" rows="5" name="message"><?=cm_get_request('message')?cm_htmlclear(cm_get_request('message')):null?></textarea></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6 col-md-2"></div>
                        <div class="col-xs-1 col-md-1"></div>
                        <div class="col-xs-12 col-md-9">
                            <label><input type="checkbox" /> <a href="#">Sözleşme</a> 'i ve eksik ödeme varsa kredimden alınmasını onaylıyorum.</label>
                             <br /><input type="submit" class="btn btn-destek" value="<?=cm_lang('Gönder')?>" style="color: #fff;" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php include('inc/footer.php'); ?>