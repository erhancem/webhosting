<?php
$page['title']			= 'CEM Bilişim - '.cm_lang('Sepet Yapılandırma');
$page['keywords']		= 'cem bilişim,vps,hosting,vds,fiziksel sunucu,Arma 3,oyun sunucu, Arma 3 oyun sunucu,sınırsız hosting, SSD hosting';
$page['description']	= 'Cem bilişim Kaliteli hosting (Sınırsız SSD Hosting), VPS, VDS, Fiziksel Sunucu, Oyun sunucuları ve Yazılım Hizmeti';
$page['copyright']		= 'Copyright © 2016, Cem Bilişim - Sunucu ve Yazılım Hizmetleri';
$basket_id	= cm_get_request('basket_id');
$action		= cm_get_request('action');
$Status = false;
if($basket_id and cm_numeric($basket_id) and $basket_id > 0){
	list($Status,$Basket) = basket_list($user,$basket_id);
}
if($Status == false){
	cm_url_go("sepetim");
}
$basket_detail	= $Basket['products'][$basket_id];
if($basket_detail['product_id'] == 0){
	cm_url_go("sepetim");
}
$basket_feature	= $basket_detail['feature'];
$product 		= $basket_detail['product'];
$Input 			= array();
if(is_array($basket_detail['basket_feature']) and array_key_exists('domain',$basket_detail['basket_feature']) and $basket_detail['basket_feature']['domain']['status'] == 0){
	cm_url_go("basket-domain?basket_id=".$basket_id);
}
$basket_configuration = product_configuration_input($product,$basket_feature);
if($basket_configuration == false){
	cm_url_go("sepetim");
}
$basket_features = null;
foreach($Basket['products'] as $key=>$value){
	$basket_features .= '<div class="Urun Basket'.$key.'">
	    <div class="pull-left UrunAdi"><strong>'.cm_lang('Ürün').'</strong>: '.cm_htmlclear($value['product_name']).'</div>
	    <div class="pull-right UrunFiyat">'.cm_price_format($value['price'],$value['currency']).'</div>
	    <div class="clearfix"></div>
	    <div>';
	if(is_array($value['basket_feature'])){
		$basket_features .= "<div style=\"margin-top: 10px;\"></div>\n";
		foreach($value['basket_feature'] as $key1=>$value1){
		    $basket_features .= '<ul>
			        <li>
			            <div class="pull-left"><span>'.cm_htmlclear($value1['name']).'</span>: '.cm_htmlclear($value1['value']).'</div>';
			if($value1['status'] == 1){
			            $basket_features .= '<div class="pull-right IcerikFiyat">'.cm_price_format($value1['price'],$value['currency']).'</div>';
			}else{
						$basket_features .= '<div class="pull-right IcerikFiyat"></div>';
			}
			            $basket_features .= '<div class="clearfix"></div>
			        </li>
			    </ul>';
		}
	}
	$basket_features .= '</div>
	    <div class="clearfix"></div>
	</div>';
}
$basket_go = cm_get_request('basket_go');
if($basket_go == false and $action and $action == 'edit'){
	echo $basket_features;
	exit;
}
if(isset($cm_status) and $basket_go){
	if($cm_status == true){
		cm_url_go("sepetim");
	}else{
		$message = $cm_message;
	}
}
include('inc/header.php');
?>
<section>
<div class="container">
    <div class="BosBG">
        <div class="SepetBG">
        <div class="clearfix"></div>
<script>
function panelList(e){
	$.ajax({
        data: 'panel_list=1',
        type: 'POST',
        url: '',
        success: function (response) {
			return window.location.href = '';
        }
    });
}
function BasketControl(e){
	$.ajax({
        data: $(e).serialize(),
        type: $(e).attr('method'),
        url: $(e).attr('action'),
        success: function (response) {
            $('#basket_feature').html(response);
        }
    });
}
</script>
            <div>
                <div class="row">
                	<div class="col-md-8">
                        <div id="SepetGetir">
                            <div id="SepetAlan">
                                <form action="" method="POST" onchange="BasketControl(this);">
                                	<input type="hidden" name="cm_action" value="basket" />
                                	<input type="hidden" name="action" value="edit" />
                                    <div class="Baslik">
                                        <i class="fa fa-list-ul"></i> <?=cm_lang('Sepet Yapılandırma')?>
                                    </div>
                                    <div style="padding: 20px;">
<?php if(isset($message)){ ?>
<div class="form-group">
	<p style="color: red;"><?=$message?></p>
</div>
<?php } ?>
<?php echo ($basket_configuration)?$basket_configuration:null; ?>
<?php if($basket_configuration){ ?>
<div class="form-group">
	<input type="submit" name="basket_go" value="<?=cm_lang('Sepete Devam Et')?>" class="btn btn-success" />
</div>
<?php } ?>
										<div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div id="SepetGetir">
                            <div id="SepetAlan">
                                    <div class="Baslik">
                                        <i class="fa fa-list-ul"></i> <?=cm_lang('Sipariş')?>
                                    </div>
                                    <div id="basket_feature" class="SepetUrunler">
									<?=$basket_features?>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php include('inc/footer.php'); ?>