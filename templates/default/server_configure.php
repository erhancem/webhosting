<?php
include('inc/header.php');
?>
<section>
<div class="container">
    <div class="BosBG">
        <div class="SepetBG">
        <div class="clearfix"></div>
            <div>
                <div class="row">
                	<div class="col-md-8">
                        <div id="SepetGetir">
                            <div id="SepetAlan">
                                <form id="server_configure" action="" method="POST" onchange="serverPrice();" onsubmit="return false;">
                                	<input type="hidden" name="action" value="add" />
                                	<input type="hidden" name="product_type_id" value="<?=$product['product_type_id']?>" />
                                	<input type="hidden" name="product_id" id="product_id" data-product_price="<?=$product['product_price']?>" value="<?=$product['product_id']?>" />
                                    <div class="Baslik">
                                        <i class="fa fa-list-ul"></i> <?=cm_lang('Sunucu Yapılandırma')?>
                                    </div>
                                    <div style="padding: 20px;">
<?php if($action == 'add' and isset($cm_status)){ ?>
									<div class="<?=$cm_status==true?'BasariliMesaji':'HataMesaji'?>"><?=$cm_message?></div>
<?php } ?>
<?php if($product['os_select'] == 1){ ?>
									<div class="form-group">
										<label><?=cm_lang('İşletim Sistemi')?></label>
										<select class="form-control" name="os_id" id="os_id" onchange="os_select(this.value,<?=$product['product_type_id']?>);">
											<option value="0" data-os_price="0.00" selected=""><?=cm_lang('Lütfen işletim sistemi seçin')?></option>
									<?php foreach($os_list as $key=>$value){ ?>
											<option value="<?=$key?>" data-os_price="<?=$value["price"]?>"><?=cm_htmlclear($value['name'])?></option>
									<?php } ?>
										</select>
									</div>
<?php } ?>
<?php if($product['panel_select'] == 1){ ?>
									<div class="form-group">
										<label><?=cm_lang('Kontrol Paneli')?></label>
										<select class="form-control" name="panel_id" id="panel_id">
											<option value="0" data-panel_price="0.00" selected=""><?=cm_lang('Önce İşletim sistemi seçin')?></option>
										</select>
									</div>
<?php } ?>
<?php if($product['ipv4_select'] == 1){ ?>
									<div class="form-group">
										<label><?=cm_lang('Ek IPv4 Adresi')?></label>
										<input type="number" name="ipv4" id="ipv4" data-ipv4_price="<?=$product['ip_price_v4']?>" value="<?=$product['ipv4_min']?>" min="<?=$product['ipv4_min']?>" max="<?=$product['ipv4_max']?>" class="form-control" />
									</div>
<?php } ?>
<?php if($product['ipv6_select'] == 1){ ?>
									<div class="form-group">
										<label><?=cm_lang('Ek IPv6 Adresi')?></label>
										<input type="number" name="ipv6" value="<?=$product['ipv6_min']?>" min="<?=$product['ipv6_min']?>" max="<?=$product['ipv6_max']?>" class="form-control" />
									</div>
<?php } ?>
									<div class="form-group">
										<button type="button" class="btn btn-success" onclick="basket_add($('#server_configure').serialize(),'','<?=$directory?>/sepet');"><?=cm_lang('Sepete Ekle')?></button>
									</div>
										<div class="clearfix"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4" style="margin: 0px;padding: 0px;">
						<div id="SepetGetir">
							<div id="SepetAlan">
								<form action="" method="POST">
							        <div class="Baslik">
										<i class="fa fa-list-ul"></i> <?=cm_lang('Sunucu Özellikleri')?></span>
									</div>
									<div class="SepetUrunler"><div class="Urun">
							                <div class="pull-left UrunAdi"><strong><?=cm_lang('Ürün')?></strong> : <?=cm_htmlclear($product['name'])?></div>
							                <div class="pull-right UrunFiyat"><span id="ProductPrice"><?=$product['product_price']?></span> ₺</div>
							                <div class="clearfix"></div>
							                <ul>
<?php if($product['os_select'] == 1){ ?>
												<li>
													<div class="pull-left" style="width: 80%;">
														<span><strong><?=cm_lang('İşletim Sistemi')?></strong></span>: <span id="os_name"></span>
													</div>
													<div class="pull-right IcerikFiyat" style="width: 20%;"><span id="os_price"></span></div>
													<div class="clearfix"></div>
												</li>
<?php } ?>
<?php if($product['panel_select'] == 1){ ?>
												<li>
													<div class="pull-left" style="width: 80%;">
														<span><strong><?=cm_lang('Kontrol Paneli')?></strong></span>: <span id="panel_name"></span>
													</div>
													<div class="pull-right IcerikFiyat" style="width: 20%;"><span id="panel_price"></span></div>
													<div class="clearfix"></div>
												</li>
<?php } ?>
<?php if($product['ipv4_select'] == 1){ ?>
												<li>
													<div class="pull-left" style="width: 80%;">
														<span><strong><?=cm_lang('Ek IPv4 Adres')?></strong></span>: <span id="ipv4_number"></span>
													</div>
													<div class="pull-right IcerikFiyat" style="width: 20%;"><span id="ipv4_price"></span></div>
													<div class="clearfix"></div>
												</li>
<?php } ?>
<?php if($product['ipv6_select'] == 1){ ?>
												<li>
													<div class="pull-left" style="width: 80%;">
														<span><strong><?=cm_lang('Ek IPv6 Adres')?></strong></span>: <span id="ipv6_number"></span>
													</div>
													<div class="pull-right IcerikFiyat" style="width: 20%;"><span id="ipv6_price"></span></div>
													<div class="clearfix"></div>
												</li>
<?php } ?>
							                <div class="clearfix"></div>
							                <div class="pull-left"></div>
							                <div class="pull-right"></div>
							                <div class="clearfix"></div>
							            </div>
									</div>
								</form>
							</div>
						</div>
					</div>
                </div>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<?php include('inc/footer.php'); ?>
<script>
$(document).ready(function(){
	serverPrice();
});
</script>