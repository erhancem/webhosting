{include file="inc/header.tpl"}
{include file="inc/account_menu.tpl"}
<div class="BosBG">
    <div class="container">
        <div class="TalepOkuIc">
            <div class="TalepAcIc">
                <form name="submitticket" method="POST" action="" enctype="multipart/form-data">
                    <input type="hidden" name="cm_token" value="{$cm_set_token}" />
                    <input type="hidden" name="action" value="new" />
                    {if $action=='new'}
                    <div class="{if $cm_status==true}BasariliMesaji{else}HataMesaji{/if}">{$cm_message}</div>
                    {/if}
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">{cm_lang('İsim')}</label>
                                <input class="form-control disabled" type="text" id="name" value="{cm_user_name($user)}" disabled="disabled" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="email">{cm_lang('E-posta Adresi')}</label>
                                <input class="form-control disabled" type="text" id="email" value="{cm_htmlclear($user['email_address'])}" disabled="disabled" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="subject">{cm_lang('Başlık')}</label>
                                <input class="form-control" type="text" name="subject" id="subject" value="{cm_htmlclear(cm_get_request('ticket_subject'))}" required="" />
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label for="name">{cm_lang('Departman')}</label>
                                <select name="dep_id" class="form-control borderradius">
                                    {foreach from=$ticket_departments key=key item=value}
                                    <option value="{$key}">{cm_htmlclear($value['ticket_dep_name'])}</option>
                                    {/foreach}
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="message">{cm_lang('Mesaj')}</label>
                        <textarea name="message" id="message" rows="12" class="form-control" required="">{cm_htmlclear(cm_get_request('ticket_message'))}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="attachments">{cm_lang('Eklentiler')}:</label>
                        <input type="file" name="attachments[]" style="width:70%;" multiple="" /><br />
                        ({cm_lang('İzin verilen dosya türleri')}: .jpg, .gif, .jpeg, .png)
                    </div>
                    <hr />
                    <button class="btn btn-destek" style="color: #fff;">{cm_lang('Talebi Gönder')}</button>
                    <input class="btn" type="reset" value="{cm_lang('İptal')}" />
                </form>
            </div>
        </div>
    </div>
</div>
{include file="inc/footer.tpl"}