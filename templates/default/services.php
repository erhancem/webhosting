<?php
$theme['login'] = true;
include('control.php');
$server_params = array(
	'user_id'	=> $user['user_id'],
	'and'		=> array(
		'product_type_id'	=> array(array(2),2),
		'service_delete'	=> 0,
	),
	'orderby'	=> array('sid'=>'DESC'),
);
$server_list = serviceGetList($server_params,'service');
include('inc/header.php');
include('inc/hesabim_menu.php');
?>
<div class="BosBG">
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="pull-left"><i class="fa fa-gear" aria-hidden="true"></i> <?=cm_lang('Eklentiler')?></div>
    <div class="clearfix"></div>
  </div>
  <div class="panel-body" style="overflow: auto;">
<table class="table table-bordered table-striped table-condensed table-responsive DestekTab">
    <thead>
        <tr>
            <th><?=cm_lang('Paket')?></th>
            <th><?=cm_lang('Kayıt Tarihi')?></th>
            <th><?=cm_lang('Bitiş Tarihi')?></th>
            <th><?=cm_lang('Durumu')?></th>
            <th><?=cm_lang('İşlem')?></th>
        </tr>
    </thead>
    <tbody>
<?php if($server_list == false){ ?>
		<tr>
		    <td colspan="7"><strong><?=cm_lang('Eklentiler listesi çekilemedi')?></strong></td>
		</tr>
<?php }elseif($server_list['total'] == 0){ ?>
		<tr>
		    <td colspan="7"><strong><?=cm_lang('Eklentiniz bulunmamaktadır')?></strong></td>
		</tr>
<?php }else{
foreach($server_list['list'] as $key=>$value){
	$serverstatus = '<span style="color: '.$service_status[$value['service_status']]['color'].';"><strong>'.cm_lang($service_status[$value['service_status']]['name']).'</strong></span>';
	$renew = null;
	$endtime = cm_lang('Sınırsız');
	if($value['product']['price_type'] == 1){
		$endtime = cm_date("d-m-Y",$value['service_end_time']);
		$renew = ' <a href="'.$directory.'/basket?cm_action=basket&id='.$value['sid'].'&product_type_id='.$value['product_type_id'].'&action=renew"><span class="btn btn-danger"><i class="fa fa-cog" aria-hidden="true"></i> '.cm_lang('UZAT').'</span></a>';
	}
?>
        <tr>
            <td><?=cm_htmlclear($value['product']['name'])?></td>
            <td><?=cm_date("d-m-Y",$value['service_create_time'])?></td>
            <td><?=$endtime?></td>
            <td><?=$serverstatus?></td>
            <td><a href="<?=$value['service_status']==1?$directory.'/service-manage?id='.$key:'javascript:;'?>"<?=$value['service_status']!=1?' onclick="return alert(\''.cm_lang('Hizmet aktif değil yönetemezsiniz').'\');"':null?>><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> <?=cm_lang('YÖNET')?></span></a><?=$renew?></td>
        </tr>
<?php }} ?>
    </tbody>
</table>
  </div>
</div>
<div class="clearfix"></div>
</div>
<?php include('inc/footer.php'); ?>