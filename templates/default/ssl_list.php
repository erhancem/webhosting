<?php
$theme['login'] = true;
include('control.php');
$server_params = array(
	'user_id'	=> $user['user_id'],
	'and'		=> array(
		'ssl_delete'	=> 0,
	),
	'orderby'	=> array('ssl_id'=>'DESC'),
);
$server_list = serviceGetList($server_params,'ssl');
include('inc/header.php');
include('inc/hesabim_menu.php');
?>
<div class="BosBG">
<div class="panel panel-default">
  <div class="panel-heading">
    <div class="pull-left"><i class="fa fa-lock" aria-hidden="true"></i> <?=cm_lang('SSL')?></div>
    <div class="clearfix"></div>
  </div>
  <div class="panel-body" style="overflow: auto;">
<table class="table table-bordered table-striped table-condensed table-responsive DestekTab">
    <thead>
        <tr>
            <th><?=cm_lang('Paket')?></th>
            <th><?=cm_lang('Alan Adı')?></th>
            <th><?=cm_lang('Kayıt Tarihi')?></th>
            <th><?=cm_lang('Bitiş Tarihi')?></th>
            <th><?=cm_lang('Durumu')?></th>
            <th><?=cm_lang('İşlem')?></th>
        </tr>
    </thead>
    <tbody>
<?php if($server_list == false){ ?>
		<tr>
		    <td colspan="7"><strong><?=cm_lang('SSL listesi çekilemedi')?></strong></td>
		</tr>
<?php }elseif($server_list['total'] == 0){ ?>
		<tr>
		    <td colspan="7"><strong><?=cm_lang('SSL paketiniz bulunmamaktadır')?></strong></td>
		</tr>
<?php }else{
foreach($server_list['list'] as $key=>$value){
	$serverstatus = '<span style="color: '.$ssl_status[$value['ssl_status']]['color'].';"><strong>'.cm_lang($ssl_status[$value['ssl_status']]['name']).'</strong></span>';
?>
        <tr>
            <td><?=cm_htmlclear($value['product']['name'])?></td>
            <td><?=cm_htmlclear($value['domain_name'])?></td>
            <td><?=cm_date("d-m-Y",$value['ssl_create_time'])?></td>
            <td><?=cm_date("d-m-Y",$value['ssl_end_time'])?></td>
            <td><?=$serverstatus?></td>
            <td><a href="<?=$value['ssl_status']==1?$directory.'/server-manage?id='.$key:'javascript:;'?>"<?=$value['ssl_status']!=1?' onclick="return alert(\''.cm_lang('Sunucu aktif değil yönetemezsiniz').'\');"':null?>><span class="btn btn-yesil"><i class="fa fa-cog" aria-hidden="true"></i> <?=cm_lang('YÖNET')?></span></a></td>
        </tr>
<?php }} ?>
    </tbody>
</table>
  </div>
</div>
<div class="clearfix"></div>
</div>
<?php include('inc/footer.php'); ?>