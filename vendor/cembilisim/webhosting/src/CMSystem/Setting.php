<?php
namespace Cembilisim\Webhosting\CMSystem;

class Setting extends System {

    public $getConfig = [];

    public function __construct(){

        $this->getConfig = $this->setConfig();

    }

    private function setConfig(){
        return [
            'max_password'  => 50,
            'min_password'  => 5,
            'max_username'  => 30,
            'min_username'  => 4
        ];
    }

}