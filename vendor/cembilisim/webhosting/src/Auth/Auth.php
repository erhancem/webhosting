<?php
namespace Cembilisim\Webhosting\Auth;

class Auth {

    private $loginType = 1;

    public function __construct($type){

        $this->loginType = $type;

    }

    public function check(){

        $return = false;
        if($this->loginType == 1){
            $return = (isset($_SESSION["IDUSER"]) ? true : false);
        }

        return $return;
    }

}