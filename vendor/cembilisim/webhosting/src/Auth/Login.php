<?php
namespace Cembilisim\Webhosting\Auth;


use Cembilisim\Webhosting\CMSystem\Request;
use Cembilisim\Webhosting\CMSystem\Setting;

class Login extends Setting {

    private $loginType = 1;
    public $getAuth = false;

    function __construct($data = []){


        parent::__construct();

        $this->getAuth = new Auth($this->loginType);

    }

    private function siteLogin($username, $password){
        $status     = false;
        $response   = 'Girdiğiniz bilgiler hatalı';

        print_R($_POST);
        return ['status' => $status, 'response' => $response];

    }

    private function login($request){

        $return = [
            'status'    => true,
            'response'  => [
                'message'   => $this->langGet('işlem başarılı'),
                //'redirect'  => '/login.php',
            ]
        ];
        return $return;
    }

    public function action(Request $request){

        $action = $request->getRequest('action', 'POST');
        if($action == false){
            return false;
        }

        $return = ['status' => false, 'response' => $this->langGet('Yapmak istediğiniz işlem bulunamadı')];
        if(in_array($action, ['login'])){

            if(method_exists($this, $action)){
                $return = $this->$action($request);
            }

        }

        return $return;

    }

}