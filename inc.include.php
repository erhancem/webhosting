<?php
define("cm_path", realpath(dirname(__FILE__))."/");

if(!is_file(cm_path."config.php")){
    if(file_exists(cm_path. "install") and file_exists(cm_path. "install/index.php")){
        header("Location: install"); exit;
    }else{
        echo 'Kurulum dosyası eksik';
        exit;
    }
}
require_once ('./vendor/autoload.php');

spl_autoload_register(function ($className) {
    $file = str_replace('\\', DIRECTORY_SEPARATOR, __DIR__ . '\\include\\class\\' . $className . '.php');
    if (file_exists($file)) { include $file; }else{ die('<strong>'.$className.'</strong> Sınıfı bulunamadı'); }
});

include(cm_path."config.php");

$cm_lang = new \Cembilisim\Webhosting\CMSystem\Lang();

foreach (scandir(cm_path.'include/') as $val){
    if(stristr($val, 'inc.') && stristr($val, '.php')){
        require_once (cm_path.'include/'.$val);
    }
}


register_shutdown_function(function(){
    $error = @error_get_last();
    //$detail = "\nPHP için Kullanılan RAM: <b>".memory_get_usage()."</b>\nPHP için Kullanılan Azami RAM: <b>".memory_get_peak_usage()."</b>\nÇalışan PHP'nin sürümü: " . phpversion();
    //echo $detail;
    //print_r(get_defined_functions());
    if($error){
        print_R($error);
        if(isset($error['type']) and $error['type'] == 1){
            if(function_exists('cm_systemlog')){
                cm_systemlog($error);
            }elseif(function_exists('cm_systemlogDir')){
                cm_systemlogDir($error);
            }
        }
    }
});