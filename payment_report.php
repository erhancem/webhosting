<?php
$page_settings['session'] 	= true;
$page_settings['login']		= 'payment_report';
include("inc.include.php");

$theme 		= 'payment_report';
$cm_token	= cm_get_request('cm_token');
$action		= cm_get_request('action');
$orderid	= cm_get_request('id');

if(!$orderid or ($orderid and !cm_numeric($orderid))){
	$orderid = $cm_db->sql_fetch_assoc($cm_db->sql_query("SELECT order_id FROM ".$cm_config['db_prefix']."orders WHERE user_id='".$cm_db->sql_escape($user['user_id'])."' AND order_delete=0 AND order_status=1 ORDER BY order_id DESC LIMIT 1"));
	if($cm_db->sql_errno() != 0){
		cm_redirect("client_orders.php");
	}
	$orderid = $orderid['order_id'];
}
$OrderWhere = array(
	'order_id'		=> $orderid,
	'user_id'		=> $user['user_id'],
	'order_delete'	=> 0,
);
$order = $cm_db->sql_assoc('orders',$OrderWhere);
if(!$order){
	cm_redirect("client_orders.php");
}

if($action){
	$cm_status = false;
	if($cm_token and cm_get_token($cm_token)){
		if($action == 'report'){
			list($cm_status,$cm_message) = userEdit($user,$_REQUEST);
			if($cm_status == true){
				$user		= $cm_message['user'];
				$cm_message	= cm_lang('Bilgiler güncellendi');
			}
		}else{
			$cm_message = cm_lang('İşlem bulunamadı');
		}
	}else{
		$cm_message = cm_lang('İşlem token geçersiz');
	}
}

$cm_set_token	= cm_set_token();
$template_call	= $template_directory.$theme.'.php';
if(!cm_isfile($template_call)){
	echo cm_lang('##theme## tema dosyası bulunamadı',array('theme'=>$theme));
}
include($template_call);