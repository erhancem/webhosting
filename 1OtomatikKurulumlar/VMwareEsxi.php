<?php

/*
Modül olarak sisteme entegre edilecek.!!

Gerekli Araçlar
    vmware esxi 5.5 minimum
    Uzak windows masaüstü gerekli ve vmware powercli programı kurulmalı sanal makinalara komut göndermek için
    uzak windows masaüstü SSH command aktif edilmesi.
*/


class VMwareEsxi{
	private $EsxiSunucu = array(),$EsxiSurum = '5.5.0',$sanal_sistem,$sanal_makina_adi,$sanal_ip,$sanal_datastore,$sanal_sifre,$sanal_netmask,$sanal_gateway,$sanal_network,$sanal_cpu,$sanal_ram,$sanal_disk,$sanal_vnc,$sanal_vncport;
	public function __construct($ip,$kullanici,$sifre){
		$this->EsxiSunucu		= array(
			'sunucu_ip'			=> $ip,
			'sunucu_kullanici'	=> $kullanici,
			'sunucu_sifre'		=> $sifre
		);
	}
	public function SunucuOVAYukle($Veriler = array(),$SunucuSistem = array()){
		global $APISunucular;
		$Durum = false;
		$KurulumBaslama = zaman();
		if(is_array($Veriler) and count($Veriler) > 0){
			if(is_array($this->EsxiSunucu) and count($this->EsxiSunucu) > 0){
				list($EsxiDurum,$EsxiBilgi) = ssh2_islem($this->EsxiSunucu,'vmware -v');
				if($EsxiDurum == true){
					preg_match("#^VMware ESXi(.*?)$#si",$EsxiBilgi,$VMwareSurum);
					$VMwareSurum = explode(" ",trim($VMwareSurum[1]),2);
					$VMwareSurum = $VMwareSurum[0];
					if($VMwareSurum >= $this->EsxiSurum){
						$Temp 	= sys_get_temp_dir().'/';
						$FTPps1	= $Temp.$Veriler['makina_adi'].'.ps1';
						$FTPbat	= $Temp.$Veriler['makina_adi'].'.bat';
						if(file_put_contents($FTPps1,$SunucuSistem['vm_komut'])){
							list($FTPDurum1,$FTPBilgi1) = FTPDosyaYukle($APISunucular['vmware_ftp'],$FTPps1);
							if($FTPDurum1 == true){
								if(file_put_contents($FTPbat,$SunucuSistem['vm_cmd'])){
									list($FTPDurum2,$FTPBilgi2) = FTPDosyaYukle($APISunucular['vmware_ftp'],$FTPbat);
									if($FTPDurum2 == true){
										$IPKomutDosya = 'C:\\PowerCLI-Script\\'.$FTPBilgi2;
										$MakinaConfig = "echo '\nSMBIOS.reflectHost = \"true\"\nnumvcpus = \"".$Veriler['makina_cpu']."\"\ncpuid.coresPerSocket = \"".$Veriler['makina_cpu']."\"\nmemsize = \"".$Veriler['makina_ram']."\"\nsched.cpu.max = \"".($Veriler['makina_cpu']*1000)."\"\nsched.cpu.units = \"mhz\"\nsched.mem.max = \"".$Veriler['makina_ram']."\"'";
										$VMwareOVA = array(
											'VMware_ara'		=> 'vim-cmd vmsvc/getallvms | grep "'.$Veriler['makina_adi'].'"',
											'VMware_Eskikapat'	=> "vim-cmd vmsvc/power.off `vim-cmd vmsvc/getallvms | grep \"".$Veriler['makina_adi']."\" | awk '{print $1}'`",
											'VMware_Eskikaldir'	=> "vim-cmd vmsvc/unregister `vim-cmd vmsvc/getallvms | grep \"".$Veriler['makina_adi']."\" | awk '{print $1}'`",
											'VMware_Eskisil'	=> 'rm -rf "/vmfs/volumes/##eski_datastore##/'.$Veriler['makina_adi'].'/"',
											'VMware_Ovayukle'	=> 'ovftool "--noSSLVerify" "--overwrite" "--powerOffTarget" "--diskMode=thin" "--datastore='.$Veriler['makina_store'].'" "--name='.$Veriler['makina_adi'].'" "--net:VM Network='.$Veriler['makina_network'].'" "ftp://'.$APISunucular['vmware_template']['sunucu_kullanici'].':'.$APISunucular['vmware_template']['sunucu_sifre'].'@'.$APISunucular['vmware_template']['sunucu_ip'].'/'.$SunucuSistem['vm_ova_adi'].'.ova" "vi://'.$this->EsxiSunucu['sunucu_kullanici'].':'.$this->EsxiSunucu['sunucu_sifre'].'@'.$this->EsxiSunucu['sunucu_ip'].'"',
											'VMware_Ovakayitsil'=> "vim-cmd vmsvc/unregister `vim-cmd vmsvc/getallvms | grep \"".$Veriler['makina_adi']."\" | awk '{print $1}'`",
											'VMware_DiskEkle'	=> 'vmkfstools -X '.$Veriler['makina_disk'].'G "/vmfs/volumes/'.$Veriler['makina_store'].'/'.$Veriler['makina_adi'].'/'.$Veriler['makina_adi'].'.vmdk"',
											'VMware_Configal'	=> "cat \"/vmfs/volumes/".$Veriler['makina_store']."/".$Veriler['makina_adi']."/".$Veriler['makina_adi'].".vmx\" | egrep -iv 'numvcpus|cpuid.coresPerSocket|memsize|sched.cpu|sched.mem' > /tmp/".$Veriler['makina_adi'].".cnf",
											'VMware_ConfigDuz'	=> 'cat /tmp/'.$Veriler['makina_adi'].'.cnf > "/vmfs/volumes/'.$Veriler['makina_store'].'/'.$Veriler['makina_adi'].'/'.$Veriler['makina_adi'].'.vmx"',
											'VMware_Configekle'	=> $MakinaConfig.' >> "/vmfs/volumes/'.$Veriler['makina_store'].'/'.$Veriler['makina_adi'].'/'.$Veriler['makina_adi'].'.vmx"',
											'VMware_Ekle'		=> 'vim-cmd solo/registervm "/vmfs/volumes/'.$Veriler['makina_store'].'/'.$Veriler['makina_adi'].'/'.$Veriler['makina_adi'].'.vmx"',
											'VMware_Ac'			=> "vim-cmd vmsvc/power.on `vim-cmd vmsvc/getallvms | grep \"".$Veriler['makina_adi']."\" | awk '{print $1}'`",
											'VMware_Tempsil'	=> "rm -rf /tmp/".$Veriler['makina_adi'].".cnf"
										);
										//print_r($IPKomutDosya);
										//print_r($VMwareOVA);
										list($EsxiDurum1,$EsxiBilgi1) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_ara']);
										if($EsxiDurum1 == true){
											$SSHIslem = array();
											if($EsxiBilgi1 != ""){
												@preg_match("#\[(.*?)\]#",$EsxiBilgi1,$Datastore);
												list($EsxiDurum2,$EsxiBilgi2) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Eskikapat']);
												if($EsxiDurum2 == true){
													list($EsxiDurum3,$EsxiBilgi3) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Eskikaldir']);
													if($EsxiDurum3 == true){
														if(isset($Datastore) and is_array($Datastore) and isset($Datastore[1]) and $Datastore[1] != ""){
															$EskiDatastore = str_replace('##eski_datastore##',$Datastore[1],$VMwareOVA['VMware_Eskisil']);
															list($EsxiDurum4,$EsxiBilgi4) = ssh2_islem($this->EsxiSunucu,$EskiDatastore);
															if($EsxiDurum4 == true){

															}else{
																$Mesaj = 'VMware eski makina silinemedi';
															}
														}else{
															$Mesaj = 'VMware eski makina datastore bulunamadı';
														}
													}else{
														$Mesaj = 'VMware eski makina kaldırılamadı';
													}
												}else{
													$Mesaj = 'VMware eski makina kapatılamadı';
												}
											}
											if(!isset($Mesaj)){	/** Eskisi silmede problem olmaz ise devam edecek*/
												list($EsxiDurum5,$EsxiBilgi5) = ssh2_islem($APISunucular['vmware_ovftool'],$VMwareOVA['VMware_Ovayukle']);
												if($EsxiDurum5 == true){
													if(stristr($EsxiBilgi5,'Completed successfully')){
														list($EsxiDurum6,$EsxiBilgi6) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Ovakayitsil']);
														if($EsxiDurum6 == true){
															$EsxiDurumDisk = true;
															if($Veriler['makina_disk'] > $SunucuSistem['vm_disk']){ /** Template disk alanından fazla isteniyor ise genişletme yapılıyor */
																list($EsxiDurumDisk,$EsxiBilgiDisk) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_DiskEkle']);
															}
															if($EsxiDurumDisk == true){
																list($EsxiDurum7,$EsxiBilgi7) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Configal']);
																if($EsxiDurum7 == true){
																	list($EsxiDurum8,$EsxiBilgi8) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_ConfigDuz']);
																	if($EsxiDurum8 == true){
																		list($EsxiDurum9,$EsxiBilgi9) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Configekle']);
																		if($EsxiDurum9 == true){
																			list($EsxiDurum10,$EsxiBilgi10) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Ekle']);
																			if($EsxiDurum10 == true){
																				list($EsxiDurum11,$EsxiBilgi11) = ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Ac']);
																				if($EsxiDurum11 == true){
																					if(stristr($EsxiBilgi11,'Powering on')){
																						ssh2_islem($this->EsxiSunucu,$VMwareOVA['VMware_Tempsil']);
																						$say = 0;
																						sleep(60); /** Makina açılması için 60 saniye bekliyor */
																						YenidenCmd:
																						list($IPDurum,$IPBilgi) = ssh2_islem($APISunucular['vmware_powercli'],$IPKomutDosya);
																						if($IPDurum == true){
																							$KurulumBitis = zaman();
																							$Mesaj = array_merge($_REQUEST,array('Kurulum_Sure'=>($KurulumBitis-$KurulumBaslama)));
																							$Durum = true;
																						}else{
																							if($say <= 1){ $say++; goto YenidenCmd; }
																							$Mesaj = 'Makina IP ayarları yapılamadı'.$IPBilgi;
																						}
																					}else{
																						$Mesaj = 'Makina başlatılamadı IP ayarları yapılamadı';
																					}
																				}else{
																					$Mesaj = 'VMware makina başlatılamadı';
																				}
																			}else{
																				$Mesaj = 'VMware template kayıt edilemedi';
																			}
																		}else{
																			$Mesaj = 'VMware template config eklenemedi';
																		}
																	}else{
																		$Mesaj = 'VMware template config düzenlenemedi';
																	}
																}else{
																	$Mesaj = 'VMware template config dosyası alınamadı';
																}
															}else{
																$Mesaj = 'VMware template disk genişletilemedi';
															}
														}else{
															$Mesaj = 'VMware template kaydı silinemedi işlemi manuel tamamlayın';
														}
													}else{
														goto hata1;
													}
												}else{
													hata1:
													$Mesaj = 'VMware template yüklenemedi: '.$EsxiBilgi5;
												}
											}
										}else{
											$Mesaj = 'VMware makina bilgi alınamadı';
										}
									}else{
										$Mesaj = 'Windows komut yüklenemedi';
									}
									@unlink($FTPbat);
								}else{
									$Mesaj = 'Windows komut yazılamadı';
								}
							}else{
								$Mesaj = 'Windows script yüklenemedi';
							}
							@unlink($FTPps1);
						}else{
							$Mesaj = 'Windows script yazılamadı';
						}
					}else{
						$Mesaj = 'VMware Esxi minimum '.$this->EsxiSurum.' olmalıdır';
					}
				}else{
					$Mesaj = 'VMware Esxi sunucuya bağlanılamadı bilgilerin doğru olduğuna ve SSH2 aktif olduğuna emin olunuz';
				}
			}else{
				$Mesaj = 'VMware sunucu bilgileri hatalı';
			}
		}else{
			$Mesaj = 'Gelen veriler eksik veya hatalı';
		}
		return array($Durum,$Mesaj);
	}
}