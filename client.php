<?php
$page_settings['session'] 	= true;
$page_settings['login']		= true;
include("inc.include.php");

use CMtemplateSystem\cm_template;

$cm_temp 			= new cm_template(new Smarty());
$cm_temp->caching	= false;

$cm_temp->cm_templatedir($cm_dirlist["template"].$template.'/');
$cm_temp->cm_variable('user',$cm_user);
$cm_temp->cm_variable('directory',$directory);
$cm_temp->cm_variable('template',$template);
$cm_temp->cm_variable('cm_title','CEM Bilişim - Web Hosting Hizmetleri');
$cm_temp->cm_variable('keywords','cem bilişim,vps,hosting,vds,fiziksel sunucu,Arma 3,oyun sunucu, Arma 3 oyun sunucu,sınırsız hosting, SSD hosting');
$cm_temp->cm_variable('description','Cem bilişim Kaliteli hosting (Sınırsız SSD Hosting), VPS, VDS, Fiziksel Sunucu, Oyun sunucuları ve Yazılım Hizmeti');
$cm_temp->cm_variable('copyright','Copyright © 2016, Cem Bilişim - Sunucu ve Yazılım Hizmetleri');
$cm_temp->cm_variable('logout_url',htmlspecialchars((strstr($_SERVER['REQUEST_URI'],'?')?$_SERVER['REQUEST_URI'].'&logout=1':$_SERVER['REQUEST_URI'].'?logout=1')));
$cm_temp->cm_variable('cm_config',$cm_config);


//$cm_token		= cm_get_request('cm_token','POST');
//$action		= cm_get_request('action','POST');
//cm_action		= cm_get_request('cm_action','POST');
$view			= (cm_get_request('view')?strtolower(cm_get_request('view')):'home'); // domain,server,hosting v.s
$page			= (cm_get_request('page')?cm_get_request('page'):'list'); // servis ile alakalı domain => manage
$p				= cm_get_request('p'); // Sayfa numarası
$page_number	= (($p and cm_numeric($p) and $p > 0)?$p:1);
$clientRedirect	= false;
$max_limit		= 10;
$total_page		= 0;
$theme			= "";
$PageParams		= [];
$where 			= [];
//$where[] 		= "user_id='".$cm_db->sql_escape($cm_user['user_id'])."'";
$serviceid		= cm_get_request('id');

/** Hizmet yönetim ve Modül İşlemleri */
if(!in_array($view, ['domain']) and $page == 'manage'){

    if(!$serviceid or !cm_numeric($serviceid) or $serviceid == 0){
        cm_redirect(cm_link('client', ['view' => $view]));
    }

    $serviceinfo    = serviceGetDetails($serviceid, $view);
    $serviceStatus  = false;
    $module_status	= false;
    $module_message	= null;

    if(is_array($serviceinfo) and isset($serviceinfo['user_id']) and $cm_user['user_id'] > 0 and $cm_user['user_id'] == $serviceinfo['user_id']){ /** Bilgiler kontrol ediliyor ve üye kontrol yapılıyor */

        if(isset($serviceinfo[$view.'_delete']) and $serviceinfo[$view.'_delete'] == 0 and isset($serviceinfo[$view.'_status']) and $serviceinfo[$view.'_status'] == 1){

            $serviceStatus = true;
            if($serviceinfo['module_id'] > 0) {

                list($module_status,$module_response) = ModuleUserPanel($serviceid, $view); /** Aktif pasif kontrol modüller fonksiyon içinde olacak   */
                if($module_status){
                    if(isset($module_response['variable']) and is_array($module_response['variable']) and count($module_response['variable']) > 0){
                        foreach($module_response['variable'] as $key=>$value){
                            $cm_temp->cm_Modulevariable($key,$value);
                        }
                    }
                    if(array_key_exists("theme", $module_response)){
                        $module_theme 	= $cm_temp->viewModuleTemplate($module_response['theme']);
                        if($module_theme){
                            $module_message = $module_theme;
                        }else{
                            $module_message = cm_lang('Modül html kaynağı okunamadı');
                        }
                    }elseif(array_key_exists("html", $module_response)){
                        $module_theme 	= $cm_temp->viewModuleHtml($module_response);
                        if($module_theme){
                            $module_message = $module_theme;
                        }else{
                            $module_message = cm_lang('Modül html kaynağı yazılamadı');
                        }
                    }else{
                        $module_message = (!is_array($module_response)?$module_response:cm_lang('Modül çıktısı okunamadı'));
                    }
                }else{
                    $module_message = (!is_array($module_response)?$module_response:(cm_lang('Modül Hata').' : '.cm_lang('Modül çıktısı okunamadı')));
                }

            }else{

                $module_message = null;

            }

        }

    }

    if($serviceStatus == false){
        cm_redirect(cm_link('client', ['view' => $view]));
    }

    $cm_temp->cm_variable('module_status', $module_status);
    $cm_temp->cm_variable('module_message', $module_message);
    $cm_temp->cm_variable('serviceinfo', $serviceinfo);
    unset($module_status,$module_message,$module_theme,$module_response);

}

if($view){

	if($view == cm_link_seo('home')){	/** Müşteri giriş paneli */

		$cm_temp->cm_variable('ticket_status',$ticket_status);
		$theme			= 'home';
		$list_return 	= serviceGetList('ticket',['orderby' => ['ticket_id' => 'DESC'], 'limit' => $max_limit]);
		if(is_array($list_return)){
			if($list_return['total'] > 0){
				$list_return = $list_return['list'];
			}else{
				$list_return = cm_lang('Kayıt bulunamadı');
			}
		}

		$cm_temp->cm_variable('list_return',$list_return);

		list($CountStatus,$TotalHosting)	= $cm_db->sql_count('hosting',array('hosting_delete'=>0,'user_id'=>$cm_user['user_id']));
        $cm_temp->cm_variable('TotalHosting',$TotalHosting);
		list($CountStatus,$TotalDomains) 	= $cm_db->sql_count('domains',array('domain_delete'=>0,'user_id'=>$cm_user['user_id']));
        $cm_temp->cm_variable('TotalDomains',$TotalDomains);
		list($CountStatus,$TotalServers)	= $cm_db->sql_count('servers',array('server_delete'=>0,'user_id'=>$cm_user['user_id']));
        $cm_temp->cm_variable('TotalServers',$TotalServers);
		list($CountStatus,$TotalOrders)		= $cm_db->sql_count('orders',array('order_delete'=>0,'user_id'=>$cm_user['user_id']));
        $cm_temp->cm_variable('TotalOrders',$TotalOrders);
		list($CountStatus,$TotalTickets)	= $cm_db->sql_count('tickets',array('ticket_delete'=>0,'user_id'=>$cm_user['user_id']));
        $cm_temp->cm_variable('TotalTickets',$TotalTickets);
        unset($CountStatus,$TotalTickets,$TotalOrders,$TotalServers,$TotalDomains,$TotalHosting,$ticketList,$list_return);


	}elseif($view == cm_link_seo('domain')){	/** Alan adı işlemleri */

		if($page == cm_link_seo('manage')){

			if(!$serviceid or !cm_numeric($serviceid) or $serviceid <= 0){
				cm_redirect(cm_link("client.php", ['view' => 'domain']));
			}

			$domain = domainGetDetails($serviceid);
			if(!$domain or $domain['user_id'] != $cm_user['user_id'] or $domain['domain_status'] != 1 or $domain['domain_delete'] != 0){
				cm_redirect(cm_link("client.php", ['view' => 'domain']));
			}

			$theme			= 'domainmanage';
			$domain_name	= cm_htmlclear($domain['domain_name']);
			$dmname			= $domain['domain_name'];

			if($domain['domain_idn_status'] == 1){
				$domain_name 	.= ' ( '.cm_htmlclear(domain_idn_utf8($domain['domain_name'])).' )';
				$dmname 		= domain_idn_utf8($domain['domain_name']);
			}

			if($domain['contact'] == false){
				$domain['contact'] = domainGetContact($domain['domain_id']);
				$domain['contact'] = $domain['contact'][1];
			}

			if($domain['dns'] == false){
				$domain['dns'] = domainGetDNS($domain['domain_id']);
				$domain['dns'] = $domain['dns'][1];
			}

			$countryList = [];
			$countryX = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."country ORDER BY country_name ASC");
			while($country = $cm_db->sql_fetch_assoc($countryX)){
				$countryList[] = $country;
			}

			$cm_temp->cm_variable('domain',$domain);
			$cm_temp->cm_variable('domain_name',$domain_name);
			$cm_temp->cm_variable('dmname',$dmname);
			$cm_temp->cm_variable('countryList',$countryList);
			unset($domain,$domain_name,$dmname,$countryList,$country);

		}else{

            $theme		    = 'domains';
            $list_return    = serviceGetList('domain', ['orderby' => ['domain_id' => 'DESC'], 'limit' => $max_limit]);
			if(is_array($list_return)){
				if($list_return['total'] > 0){
					$list_return = $list_return['list'];
				}else{
					$list_return = cm_lang('Aktif alan adınız bulunmamaktadır');
				}
			}
			$cm_temp->cm_variable('list_return',$list_return);
			$cm_temp->cm_variable('domain_status',$domain_status);
			unset($domain_status,$list_return);

		}

	}elseif($view == cm_link_seo('order')){	/** Sipariş İşlemleri */

		if($page == cm_link_seo('detail')){

		}else{

			$theme			= 'orders';
			$list_return 	= serviceGetList('order',['orderby' => ['order_id' => 'DESC'], 'limit' => $max_limit]);
			if(is_array($list_return)){
				if($list_return['total'] > 0){
					$list_return = $list_return['list'];
				}else{
					$list_return = cm_lang('Sipariş bulunamadı');
				}
			}
			$cm_temp->cm_variable('order_status', $order_status);
			$cm_temp->cm_variable('list_return', $list_return);
			unset($list_return);

		}

	}elseif($view == cm_link_seo('hosting')){	/** Hosting İşlemleri */

		if($page == cm_link_seo('manage')){

            $theme          = 'hostingmanage';

		}else{

            $theme	        = 'hostings';
            $list_return    = serviceGetList('hosting', ['orderby' => ['hosting_id' => 'DESC'], 'limit' => $max_limit]);
			if(is_array($list_return)){
				if($list_return['total'] > 0){
					$list_return = $list_return['list'];
				}else{
					$list_return = cm_lang('Aktif hosting paketiniz bulunmamaktadır');
				}
			}
            $cm_temp->cm_variable('hosting_status',$hosting_status);
            $cm_temp->cm_variable('list_return',$list_return);
            unset($hosting_status,$list_return);

		}

	}elseif($view == cm_link_seo('server')){	/** Sunucu İşlemleri */

		if($page == cm_link_seo('manage')){

			$theme          = 'servermanage';

		}else{

			$theme		    = 'servers';
			$list_return 	= serviceGetList('server', ['orderby' => ['server_id' => 'DESC'], 'limit' => $max_limit]);
			if(is_array($list_return)){
				if($list_return['total'] > 0){
					$list_return = $list_return['list'];
				}else{
					$list_return = cm_lang('Aktif sunucunuz bulunmamaktadır');
				}
			}
            $cm_temp->cm_variable('server_status',$server_status);
            $cm_temp->cm_variable('list_return',$list_return);
            unset($server_status,$list_return);

		}

	}elseif($view == cm_link_seo('account')){

		$country 	= [];
		$countryX 	= $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."country ORDER BY country_name ASC");
		while($countryA = $cm_db->sql_fetch_assoc($countryX)){
			$country[] = $countryA;
		}
		$cm_temp->cm_variable('country', $country);
		unset($country);

		if($page == cm_link_seo('address')){

			$theme 		= 'address';

		}else{

			$theme = 'info';

		}

	}else{
		$clientRedirect = true;
	}
	if($clientRedirect){ cm_redirect("client.php"); }
}

$theme = 'client_'.$theme;	/** Tema Yolu */
$cm_temp->cm_variable('cm_set_token',cm_set_token());
$cm_temp->cm_Templateview($theme);