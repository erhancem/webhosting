<?php
namespace CMtemplateSystem;

class cm_template {
    private $theme              = null;
    public  $variable           = array();
    public  $Modulevariable     = array();
    function __construct($thm){
        $this->theme = $thm;
    }

    function cm_variable($name,$value,$boolean = null){
        $name = ltrim($name,'$');
        $this->variable[$name] = $value;
        if($boolean != null) {
            $this->theme->assign($name, $value, $boolean);
        }else{
            $this->theme->assign($name, $value);
        }
    }

    function cm_templatedir($temp_dir){
        $this->theme->setTemplateDir($temp_dir);
    }

    function cm_Getvariable($name){
        return $this->variable[$name];
    }

    function cm_viewPHP($file){
        if(file_exists($file)) {
            if (count($this->variable) > 0) {
                extract($this->variable, EXTR_PREFIX_SAME, 'temp_');
            }
            ob_start();
            include($file);
            $html = ob_get_contents();
            ob_end_clean();
            echo $html;
        }else{
            echo 'Tema bulunamadı';
            exit;
        }
    }

    function cm_variableWrite(){
        if(count($this->variable) > 0) {
            extract($this->variable, EXTR_PREFIX_SAME, 'temp_');
        }
    }

    function cm_Modulevariable($name,$value,$boolean = null){
        $name = ltrim($name,'$');
        $this->Modulevariable[$name] = $value;
        if($boolean != null) {
            $this->theme->assign($name, $value, $boolean);
        }else{
            $this->theme->assign($name, $value);
        }
    }

    function cm_ModulevariableWrite(){
        if(count($this->Modulevariable) > 0) {
            extract($this->Modulevariable, EXTR_PREFIX_SAME, 'temp_');
        }
    }

    function cm_view($theme){
        $themeDir = $this->theme->getTemplateDir();
        if(file_exists($themeDir[0].$theme)) {
            $this->theme->display($theme);
        }else{
            echo 'Tema bulunamadı'; exit;
        }
    }

    function cm_Templateview($theme){
        $themeDir = $this->theme->getTemplateDir();
        if(file_exists($themeDir[0].$theme)) {
            $this->theme->display($theme);
        }elseif(file_exists($themeDir[0].$theme.'.tpl')){
            $this->theme->display($theme.'.tpl');
        }elseif(file_exists($themeDir[0].$theme.'.php')){
            $this->cm_viewPHP($themeDir[0].$theme.'.php');
        }else{
            echo 'Tema bulunamadı'; exit;
        }
    }

    function viewModuleTemplate($filename){
        $response = false;
        if(file_exists($filename)) {
            $ext = explode(".", $filename);
            $ext = end($ext);
            if($ext == 'tpl'){

                $this->cm_ModulevariableWrite();
                ob_start();
                $this->theme->display($filename);
                $html = ob_get_contents();
                ob_end_clean();
                $response = $html;

            }elseif($ext == 'php'){

                if (count($this->Modulevariable) > 0) {
                    extract($this->Modulevariable, EXTR_PREFIX_SAME, 'temp_');
                }
                ob_start();
                include($filename);
                $html = ob_get_contents();
                ob_end_clean();
                $response = $html;

            }
        }
        return $response;
    }

    function viewModuleHtml($params,$admin = false){
        $response = false;

        if(!isset($params['html_type']) or $params['html_type'] == 'tpl'){
            $response = true;
            $this->cm_ModulevariableWrite();
            $filename = cm_path.'../moduleTemp/'.$params['module'].'/user_html.tpl';
            if($admin){
                $filename = cm_path.'../moduleTemp/'.$params['module'].'/admin_html.tpl';
            }

            if(!file_exists(cm_path.'../moduleTemp/'.$params['module'].'/')){
                if(!mkdir(cm_path.'../moduleTemp/'.$params['module'].'/', 0777, true)){
                    $response = false;
                }
            }

            if($response){
                if(!file_put_contents($filename,$params['html'])){
                    $response = false;
                }
            }

            if($response){
                ob_start();
                $this->theme->display($filename);
                $html = ob_get_contents();
                ob_end_clean();
                $response = $html;
            }
        }else{

            if (count($this->Modulevariable) > 0) {
                extract($this->Modulevariable, EXTR_PREFIX_SAME, 'temp_');
            }
            $response = $params['html'];

        }

        return $response;
    }
}
