<?php
if(!isset($file)){ exit; }
use MatthiasMullie\Minify;
require_once __DIR__ . '/src/Minify.php';
require_once __DIR__ . '/src/JS.php';
require_once __DIR__ . '/src/Exception.php';
require_once __DIR__ . '/../path-converter/src/ConverterInterface.php';
require_once __DIR__ . '/../path-converter/src/Converter.php';

$minifier = new Minify\JS($file);
echo $minifier->minify();