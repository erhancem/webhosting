<?php
if(!isset($file)){ exit; }
use MatthiasMullie\Minify;
require_once __DIR__ . '/src/Minify.php';
require_once __DIR__ . '/src/CSS.php';
require_once __DIR__ . '/src/Exception.php';
require_once __DIR__ . '/../path-converter/src/ConverterInterface.php';
require_once __DIR__ . '/../path-converter/src/Converter.php';

try {
    $minifier = new Minify\CSS($file);
    echo $minifier->minify();
} catch (Exception $e) {
    echo $e->getMessage(); exit;
}