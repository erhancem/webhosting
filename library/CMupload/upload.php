<?php

class CMUpload{
    private $file;
    private $path;
    private $mime_types = array();
    private $max_size	= 0;
    private $file_name;

    public function __construct($image,$path,$MaxSize = 0,$file_name = null){ // MaxSize 0 => sınırsız
        $this->file 		= $image;
        $this->mime_types	= file_mime_types();
        $this->max_size		= $MaxSize;
        $this->path			= $path;
        $this->path2		= $path;
        $this->file_name	= $file_name;
        if(!$this->PathDircontrol($this->path2)){
            echo 'Dizin olmadı'; exit;
        }
    }

    public function upload(){
        $Durum = false;
        if(isset($this->file['name'])){
            if(is_array($this->file['name'])){
                return $this->Multiupload();
            }
            if($this->file['name'] != "" and strstr($this->file['name'],'.')){
                $FileExtension = explode(".",$this->file['name']);
                $FileExtension = end($FileExtension);
                if(in_array($FileExtension,$this->mime_types)){
                    if(isset($this->file['size']) and $this->file['size'] > 0){
                        if($this->max_size == 0 or ($this->max_size > 0 and $this->max_size <= $this->file['size'])){
                            $FileName = time();
                            if($this->file_name[0]['name'] != ""){
                                $FileName = $this->file_name[0]['name'];
                            }
                            $this->path	= $this->path.implode("/",str_split(substr($this->file_name[0]['id'],0,strlen($this->file_name[0]['id'])))).'/';
                            if(!$this->PathDircontrol($this->path)){
                                return array(false,cm_lang('File directory not created'));
                            }
                            if(move_uploaded_file($this->file['tmp_name'],$this->path.$FileName.'.'.$FileExtension)){
                                $Durum = true;
                                $Mesaj = array(
                                    'id'				=> array($this->file_name[0]['id']),
                                    'file_name'			=> array($FileName.'.'.$FileExtension),
                                    'file_extension'	=> array($FileExtension),
                                    'file_path'			=> array($this->path),
                                    'file_size'			=> array($this->file['size']),
                                );
                            }else{
                                $Mesaj = cm_lang('Resim Yüklenemedi');
                            }
                        }else{
                            $Mesaj = cm_lang('Dosya boyutu çok fazla');
                        }
                    }else{
                        $Mesaj = cm_lang('Dosya boyutu çok fazla');
                    }
                }else{
                    $Mesaj = cm_lang('Uzantı desteklenmiyor');
                }
            }else{
                $Mesaj = cm_lang('Uzantı bulunamadı');
            }
        }else{
            $Mesaj = cm_lang('Dosya seçilmedi');
        }
        if(isset($this->file['error']) and $this->file['error'] == 0){
            @unlink($this->file['tmp_name']);
        }
        return array($Durum,$Mesaj);
    }
    public function Multiupload(){
        $Durum = false;
        if(isset($this->file['name']) and is_array($this->file['name'])){
            $Gnd1 = array();
            $Gnd2 = array();
            $Gnd3 = array();
            $Gnd4 = array();
            $Gnd5 = array();
            foreach($this->file['name'] as $key=>$value){
                if($value != "" and strstr($value,'.')){
                    $FileExtension = explode(".",$value);
                    $FileExtension = end($FileExtension);
                    if(in_array($FileExtension,$this->mime_types)){
                        if(isset($this->file['size'][$key]) and $this->file['size'][$key] > 0){
                            if($this->max_size == 0 or ($this->max_size > 0 and $this->max_size <= $this->file['size'][$key])){
                                $FileName = time();
                                if(isset($this->file_name[$key]['name'])){
                                    $drt = true;
                                    $FileName = $this->file_name[$key]['name'];
                                }
                                $NewPath = $this->path.implode("/",str_split(substr($this->file_name[$key]['id'],0,strlen($this->file_name[$key]['id'])))).'/';
                                if(!$this->PathDircontrol($NewPath)){
                                    return array(false,cm_lang('Dosya yolu oluşturulamadı'));
                                }
                                if(@move_uploaded_file($this->file['tmp_name'][$key],$NewPath.$FileName.'.'.$FileExtension)){
                                    $Durum = true;
                                    if(isset($drt)){
                                        $Gnd5[$key] = $this->file_name[$key]['id'];
                                        unset($drt);
                                    }
                                    $Gnd1[$key] = $FileName.'.'.$FileExtension;
                                    $Gnd2[$key] = $FileExtension;
                                    $Gnd3[$key] = $NewPath;
                                    $Gnd4[$key] = $this->file['size'][$key];
                                }
                            }
                        }
                    }
                }
            }
            if($Durum == true){
                $Mesaj = array(
                    'id'				=> $Gnd5,
                    'file_name'			=> $Gnd1,
                    'file_extension'	=> $Gnd2,
                    'file_path'			=> $Gnd3,
                    'file_size'			=> $Gnd4,
                );
            }else{
                $Mesaj = cm_lang('Pictures could not be uploaded');
            }
        }else{
            $Mesaj = cm_lang('File not found');
        }
        return array($Durum,$Mesaj);
    }
    private function PathDircontrol($path){
        if(!is_dir($path)){
            if(mkdir($path, 0777, true)){
                chmod($path, 0777);
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }
        return false;
    }
    public function __destruct(){

    }
}