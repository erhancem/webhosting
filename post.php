<?php

$page_settings['session'] = true;
include("inc.include.php");

$cm_method  = 'POST';
$cm_action	= cm_get_request('cm_action',$cm_method);
$action     = cm_get_request('action',$cm_method);
$cm_status	= false;
$cm_message = cm_lang('İşlem bulunamadı');
$cm_format	= cm_get_request('format','REQUEST');
$cm_token   = cm_get_request('cm_token',$cm_method);

if($cm_action){
	if(cm_postfile($cm_dirlist["post"].$cm_action.".php")){
		include($cm_dirlist["post"].$cm_action.".php");
	}
}

if($cm_format and $cm_format == 'json'){
	$return = [
		'status'    => $cm_status,
		'message'   => $cm_message
	];
	echo cm_json_encode($return);
	exit;
}