<?php
$page_settings['session'] 	= true;
$page_settings['login']		= true;
$page_settings['security']['upload'] = true;
include("inc.include.php");

use CMtemplateSystem\cm_template;
$cm_temp 			= new cm_template(new Smarty());
$cm_temp->caching	= false;

$cm_temp->cm_templatedir($cm_dirlist["template"].$template.'/');
$cm_temp->cm_variable('user',$cm_user);
$cm_temp->cm_variable('directory',$directory);
$cm_temp->cm_variable('template',$template);
$cm_temp->cm_variable('ticket_status',$ticket_status);
$cm_temp->cm_variable('cm_title','CEM Bilişim - Web Hosting Hizmetleri');
$cm_temp->cm_variable('keywords','cem bilişim,vps,hosting,vds,fiziksel sunucu,Arma 3,oyun sunucu, Arma 3 oyun sunucu,sınırsız hosting, SSD hosting');
$cm_temp->cm_variable('description','Cem bilişim Kaliteli hosting (Sınırsız SSD Hosting), VPS, VDS, Fiziksel Sunucu, Oyun sunucuları ve Yazılım Hizmeti');
$cm_temp->cm_variable('copyright','Copyright © 2016, Cem Bilişim - Sunucu ve Yazılım Hizmetleri');
$cm_temp->cm_variable('logout_url',htmlspecialchars((strstr($_SERVER['REQUEST_URI'],'?')?$_SERVER['REQUEST_URI'].'&logout=1':$_SERVER['REQUEST_URI'].'?logout=1')));
$cm_temp->cm_variable('cm_config',$cm_config);

$cm_method      = 'POST';
$cm_token		= cm_get_request('cm_token',$cm_method);
$action			= cm_get_request('action',$cm_method);
$page			= (cm_get_request('page')?cm_get_request('page'):'list');
$ticketid		= cm_get_request('id');
$p				= cm_get_request('p');
$cm_message     = null;
$cm_status      = false;
$pageNum		= (($p and cm_numeric($p) and $p > 0)?$p:1);
$clientRedirect	= false;
$theme			= "";
$max_limit		= 10;
$total_page		= 0;
$PageParams		= array();
$where 			= array();
$postFile       = $cm_dirlist["post"].'ticket.php';
$cm_temp->cm_variable('action',$action);



if($action){
    $cm_status = false;
    if(file_exists($postFile)){
        include($postFile);
        if($action == 'new' and $cm_status == true){
            cm_redirect(cm_link("ticket.php",array('page' => 'view','id' => $cm_message['ticket_id'])));
        }elseif($action == 'answer' and $cm_status == true){
            $cm_message = $cm_message['message'];
        }
    }else{
        $cm_message = cm_lang('POST işlemi için dosya bulunamadı');
    }
}

if($page){
	if($page == cm_link_seo('view')){	/** Destek görüntüleme */

		$theme	= 'view';
		if(!$ticketid or !cm_numeric($ticketid) or $ticketid == 0){
		    cm_redirect(cm_link('ticket.php'));
		}

		$ticketWhere = array(
			'ticket_id'		=> $ticketid,
			'user_id'		=> $cm_user['user_id'],
			'ticket_delete'	=> 0,
		);
		$ticket = $cm_db->sql_assoc('tickets',$ticketWhere);
		if(!$ticket){
		    cm_redirect(cm_link('ticket.php'));
		}

		$department		= ticket_departments();
		$cm_temp->cm_variable('department',$department[$ticket['ticket_dep_id']]);
		$cm_temp->cm_variable('ticket',$ticket);
		$cm_temp->cm_variable('ticketstatus','<span style="color: '.$ticket_status[$ticket['ticket_status']]['color'].';">'.cm_lang($ticket_status[$ticket['ticket_status']]['name']).'</span>');

		$ticketList 	= array();
		$ticketQuery 	= $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."ticket_answer AS ta LEFT JOIN ".$cm_config['db_prefix']."users AS u ON ta.user_id=u.user_id WHERE ta.ticket_id=".$ticket['ticket_id']." AND ta.ticket_answer_delete=0 ORDER BY ticket_answer_time ASC");
		if($cm_db->sql_errno() == 0){
			while($tickets = $cm_db->sql_fetch_assoc($ticketQuery)){
				if($tickets['end_reading_time'] == 0){
					$cm_db->sql_query_update('ticket_answer',array('end_reading_time'=>cm_time()),array('ticket_answer_id'=>$tickets['ticket_answer_id']));
				}
				$add_files = null;
				$addfiles = $cm_db->sql_fetch_assoc($cm_db->sql_query("SELECT COUNT(ticket_answer_id) as Total FROM ".$cm_config['db_prefix']."ticket_answer_files WHERE ticket_answer_id=".$tickets['ticket_answer_id']));
				if($addfiles['Total'] > 0){
					$fileQuery = $cm_db->sql_query("SELECT * FROM ".$cm_config['db_prefix']."ticket_answer_files AS tf LEFT JOIN ".$cm_config['db_prefix']."file_additional AS f ON tf.additional_id=f.additional_id WHERE tf.ticket_answer_id=".$tickets['ticket_answer_id']." AND f.file_delete=0");
					while($files = $cm_db->sql_fetch_assoc($fileQuery)){
						$add_files[] = '<a href="'.$directory.'/file_open.php?id='.$files['additional_id'].'" target="_blank">'.cm_htmlclear($files['old_file_name']).'</a>';
					}
				}
				$tickets['files'] = $add_files;
				$ticketList[$tickets['ticket_answer_id']] = $tickets;
			}
		}else{
			$ticketList = cm_lang('Destek cevapları çekilemedi');
		}
		$cm_temp->cm_variable('ticketList',$ticketList);

		unset($ticketList,$department,$ticketQuery,$ticket);

	}elseif($page == cm_link_seo('new')){	// Yeni destek talebi

		$theme = 'new';
		$cm_temp->cm_variable('ticket_departments',ticket_departments());

	}else{	// Destek listeleme

		$theme			= 'list';
		$list_return    = serviceGetList('ticket', ['orderby' => ['ticket_id' => 'DESC'], 'limit' => $max_limit]);
		if(is_array($list_return)){
			if($list_return['total'] > 0){
				$list_return = $list_return['list'];
			}else{
				$list_return = cm_lang('Destek talebiniz bulunmamaktadır');
			}
		}
		$cm_temp->cm_variable('list_return',$list_return);
		unset($list_return);

	}
	if($clientRedirect){ cm_redirect(cm_link("client.php")); }
}

$theme = 'ticket_'.$theme;

$cm_temp->cm_variable('cm_set_token',cm_set_token());
$cm_temp->cm_variable('cm_message',$cm_message);
$cm_temp->cm_variable('cm_status',$cm_status);

unset($action,$cm_user,$cm_token,$cm_message,$cm_status,$template,$directory);

$cm_temp->cm_Templateview($theme);