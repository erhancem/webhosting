<?php
function cpanel_ModuleTest($params){
    return [
        true,
        ['message'   => cm_lang('Cpanel ile bağlantı kuruldu')],
    ];
}

function cpanel_Create(){

}

function cpanel_Suspend(){

}

function cpanel_Unsuspend(){

}

function cpanel_PowerOff(){

}

function cpanel_PowerOn(){

}

function cpanel_UserPanel($params){
    $status     = true;
    $response   = [
        'theme'         => $params['module_directory'].'view/user_home.tpl',
        //'html'        => '<p><b>{$test}</b></p>',
        //'html_type'   => 'tpl',
        'variable'      => [
            'test'      => 'Erhan Yazılımcı'
        ],
    ];
    return [$status,$response];
}