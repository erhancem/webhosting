<?php

$cpanel_hosting	= [
    'type'			=> 'hosting',
    'name'			=> 'CPanel Hosting Modülü',
    'logo'			=> 'https://www.yoncu.com/resimler/genel/logo.png',
    'information'	=> 'https://www.yoncu.com/forum/yoncu-bilisim-eklentileri',
    'data'	=> [
        'description'	=> cm_lang('CPanel otomatik hosting oluşturma modülü'),
        'permissions'   => ['server_list'],
        'submit_test'   => true,
        'product'  => [
            'subdomain' => [
                'tag'			=> 'input',
                'type'			=> 'text',
                'name'			=> 'subdomain',
                'placeholder'	=> cm_lang('Alt Alan Adı'),
                'min_char'	    => null,
                'max_char'	    => null,
                'required'	    => false,
                'form_name'		=> cm_lang('Alt Alan Adı'),
                'form_desc'	    => cm_lang('Sınırsız alt alan adı için 0 girin'),
            ],
        ],
    ],
];