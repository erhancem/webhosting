<?php
$cembilisim_server	= array(
    'type'			=> 'server',
    'name'			=> 'Cembilişim Sunucu Modülü',
    'logo'			=> 'http://manager.cembilisim.com/templates/default/images/cem_logo.png',
    'url'			=> 'https://www.cembilisim.com/',
    'data'	=> array(
        'description'	=> 'Sunucu yönetim modülü',
        'form'	=> null,
        'footer_description'	=> 'Api Erişim Bilgilerinizi Yöncü Panelinizdeki Üye İşlemleri -> Menü Devamı -> Güvenlik Ayarları -> API ERişimi menü adımlarını izleyerek alabilirsiniz. Bu sistemin api erişimi sağlayabilmesi için ilgili sayfadaki güvenli ip adreslerine '.$_SERVER['SERVER_ADDR'].' IP adresini eklemelisiniz.',
    ),
);