<?php
$banka_odeme_payment	= array(
    'type'			=> 'payment',
    'name'			=> 'Banka Havale/EFT Ödeme',
    'logo'			=> 'http://manager.cembilisim.com/templates/default/images/cem_logo.png',
    'url'			=> 'https://www.cembilisim.com/',
    'data'	=> array(
        'description'	=> 'Banka ödeme sistemi',
        'form'	=> array(
            'BankList'	=> array(
                'tag'			=> 'textarea',
                'type'			=> 'text',
                'placeholder'	=> 'Banka Bilgileriniz',
                'min_karakter'	=> 1,
                'max_karakter'	=> 11,
                'required'		=> true,
                'description'	=> 'API Erişimi için Kullanıcı ID Bilgisi',
            ),
        ),
        'footer_description'	=> 'Api Erişim Bilgilerinizi Yöncü Panelinizdeki Üye İşlemleri -> Menü Devamı -> Güvenlik Ayarları -> API ERişimi menü adımlarını izleyerek alabilirsiniz. Bu sistemin api erişimi sağlayabilmesi için ilgili sayfadaki güvenli ip adreslerine '.$_SERVER['SERVER_ADDR'].' IP adresini eklemelisiniz.',
    ),
);