<?php
include ('db_wizard.class.php');

function db_wizard_ModuleInstall($params){
    global $cm_db,$cm_dirlist;
    $sqlFile = $cm_dirlist['module'].$params['module_dir'].'/'.$params['module'].'/'.$params['module'].'.sql';
    list($sql_status,$sql_response) = $cm_db->sql_file_upload($sqlFile);
    if($sql_status == true){
        $response = [
            'message'   => cm_lang('Modül dosyaları kuruldu'),
        ];
        return array(true,$response);
    }
    return array(false,cm_lang('Modül sql dosyası kurulamadı'));
}

function db_wizard_ConnectTest($params){
    $test = @mysqli_connect($params['db_server'],$params['db_user'],$params['db_password'],$params['db_name']);
    if(mysqli_connect_errno() == 0){
        mysqli_close($test);
        return true;
    }
    return false;
}

function db_wizard_SaveDB($id = false){
    global $cm_db;
    $status     = false;
    $response   = null;
    if(isset($_POST['db_name']) and $_POST['db_name'] != '' and isset($_POST['db_server']) and $_POST['db_server'] != '' and isset($_POST['db_user']) and $_POST['db_user'] != '' and isset($_POST['db_port']) and $_POST['db_port'] != ''){
        $add = array(
            'db_server'     => $_POST['db_server'],
            'db_user'       => $_POST['db_user'],
            'db_password'   => isset($_POST['db_password'])?$_POST['db_password']:null,
            'db_port'       => $_POST['db_port'],
            'db_name'       => $_POST['db_name'],
            'update'        => time()
        );
        if(db_wizard_ConnectTest($add)) {
            if ($id == false) {
                $add['time'] = time();
            }
            if ($id == false) {
                list($add_status, $add_id) = $cm_db->sql_insert_add('dbwizard_databases', $add, 0);
            } else {
                list($add_status, $add_id) = $cm_db->sql_query_update('dbwizard_databases', $add, array('id' => $id), 0);
            }
            if ($add_status) {
                $status = true;
            } else {
                $response = cm_lang('Veritabanı eklenemedi');
            }
        }else{
            $response = cm_lang('Veritabanı bağlantısı kurulamadı. Bilgilerinizi kontrol ediniz');
        }
    }else{
        $response = cm_lang('Lütfen zorunlu alanları doldurun');
    }
    return array($status,$response);
}

function db_wizard_MessageCode($code){
    if($code == 0){
        $message = cm_lang('Veritabanına bağlanılamadı');
    }elseif($code == 1){
        $message = cm_lang('Yedek dosyası yazılamadı dizin veya yazma izinlerini kontrol edin');
    }elseif($code == 2){
        $message = cm_lang('Lütfen ayarlar kısmından yedek alınacak klasör belirleyin');
    }elseif($code == 3){
        $message = cm_lang('Veritabanı bulunamadı');
    }elseif($code == 4){
        $message = cm_lang('Veritabanı seçimi yapınız');
    }elseif($code == 5){
        $message = cm_lang('Backup klasör oluşturulamadı');
    }elseif($code == 99){
        $message = cm_lang('Yedek Alındı');
    }

    return $message;
}

function db_wizard_Backup($params) {
    global $cm_db;

    $id         = cm_get_request('id');
    if(!cm_numeric($id) or (cm_numeric($id) and $id == 0)){
        return array(false,db_wizard_MessageCode(4));
    }

    $db_query = $cm_db->sql_query("SELECT * FROM dbwizard_databases WHERE id='".$cm_db->sql_escape($id)."'");
    if($cm_db->sql_errno() != 0 or ($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($db_query) == 0)){
        return array(false,db_wizard_MessageCode(3));
    }


    $db_info    = $cm_db->sql_fetch_assoc($db_query);
    $db_info['backup_dir'] = rtrim($db_info['backup_dir'],'/').'/';
    $db_wizard  = new db_wizardDB($db_info['db_server'],$db_info['db_user'],$db_info['db_password'],$db_info['db_name']);
    if(!$db_wizard->connect()){
        return array(false,db_wizard_MessageCode(0));
    }

    if($db_info['backup_dir'] == ''){
        return array(false,db_wizard_MessageCode(2));
    }

    if(!file_exists($db_info['backup_dir'])){
        if(!mkdir($db_info['backup_dir'], 0777, true)){
            return array(false,db_wizard_MessageCode(5));
        }
    }

    $tables = '*';
    if(isset($_REQUEST["db_backup"]) and !in_array(strtolower($_REQUEST["db_backup"]),array('','all'))){
        $tables = $_REQUEST["db_backup"];
    }

    $db_wizard->sql_query("SET NAMES 'utf8'");

    if($tables == '*'){
        $tables = array();
        $result = $db_wizard->sql_query('SHOW TABLES');
        while($row = $db_wizard->sql_fetch_row($result)){
            $result1    = $db_wizard->sql_query("SHOW COLUMNS FROM `".$row[0]."`;");
            $col        = $db_wizard->sql_fetch_row($result1);
            $tables[] = array(
                'name'  => $row[0],
                'col'   => $col[0],
            );
        }
    }else{
        $tabless    = is_array($tables) ? $tables : explode(',',$tables);
        $tables     = array();
        foreach($tabless as $val){
            $result1 = $db_wizard->sql_query("SHOW COLUMNS FROM `".$val."`;");
            if($db_wizard->sql_errno() == 0){
                $col = $db_wizard->sql_fetch_row($result1);
                $tables[] = array(
                    'name'  => $val,
                    'col'   => $col[0],
                );
            }
        }
    }

    $return = '';
    foreach($tables as $key=>$tablev){
        $table          = $tablev["name"];
        $Query          = "SELECT ##qw## FROM ".$table;
        $result         = $db_wizard->sql_query(str_replace('##qw##','*',$Query));
        $num_fields     = $db_wizard->sql_num_fields($result);
        $num_rows       = $db_wizard->sql_fetch_assoc($db_wizard->sql_query(str_replace('##qw##','COUNT('.$tablev["col"].') AS total',$Query)));
        $num_rows       = $num_rows["total"];

        $return     .= 'DROP TABLE IF EXISTS '.$table.';';
        $row2       = $db_wizard->sql_fetch_row($db_wizard->sql_query('SHOW CREATE TABLE '.$table));
        $return     .= "\n\n".$row2[1].";\n\n";
        $counter    = 1;
        for ($i = 0; $i < $num_fields; $i++){
            while($row = $db_wizard->sql_fetch_row($result)){
                if($counter == 1){
                    $return.= 'INSERT INTO '.$table.' VALUES(';
                } else{
                    $return.= '(';
                }

                for($j=0; $j<$num_fields; $j++){
                    $row[$j] = addslashes($row[$j]);
                    $row[$j] = str_replace("\n","\\n",$row[$j]);
                    if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
                    if ($j<($num_fields-1)) { $return.= ','; }
                }

                if($num_rows == $counter){
                    $return.= ");\n";
                } else{
                    $return.= "),\n";
                }
                ++$counter;
            }
        }
        $return .= "\n\n\n";
        $tables[$key] = $table;
    }

    $fileName = 'db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql';
    if(file_put_contents($db_info['backup_dir'].$fileName,$return)){
        $add = array(
            'db_id'     => $db_info["id"],
            'filename'  => $fileName,
            'path'      => $db_info['backup_dir'],
            'table'     => implode(',',$tables),
            'time'      => cm_time(),
            'filesize'  => filesize($db_info['backup_dir'].$fileName),
        );
        $cm_db->sql_insert_add('dbwizard_backup',$add,0);
        return array(true,array('message'=>cm_lang('Yedek alındı')));
    }else{
        return array(false,db_wizard_MessageCode(1));
    }
}

function db_wizard_DBbackup($params){
    parse_str($_SERVER["QUERY_STRING"],$urlQuery);
    unset($urlQuery["db_backup"]);
    $urlQuery = trim($_SERVER["SCRIPT_NAME"].'?'.http_build_query($urlQuery), '/');
    list($status,$response) = db_wizard_Backup($params);
    if($status){
        $response = $response["message"];
    }
    echo "<script>
            alert('".$response."');
            window.location = '".cm_siteurl() . $urlQuery."';
          </script>";
exit;
}

function db_wizard_Add($params){
    global $admindir;
    $redirect       = cm_siteurl() . trim($admindir, '/') . '/plugins.php?mod=' . $params['module_dir'] . '&module_name=' . $params['module'];
    if (isset($_POST['module_add'])) {
        list($add_status, $add_response) = db_wizard_SaveDB();
        if ($add_status) {
            cm_redirect($redirect);
        } else {
            $response['variable'] = array(
                'add_message' => $add_response,
            );
        }
    }
    $response['theme'] = $params['module_directory'].'view/admin_add.php';
    return $response;
}

function db_wizard_Edit($params){
    global $admindir,$cm_db;
    $redirect   = cm_siteurl() . trim($admindir, '/') . '/plugins.php?mod=' . $params['module_dir'] . '&module_name=' . $params['module'];
    $response   = array();
    $id         = cm_get_request('id');
    if(!cm_numeric($id) or (cm_numeric($id) and $id == 0)){ cm_redirect($redirect); }

    $db_query = $cm_db->sql_query("SELECT * FROM dbwizard_databases WHERE id='".$cm_db->sql_escape($id)."'");
    if($cm_db->sql_errno() != 0 or ($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($db_query) == 0)){ cm_redirect($redirect); }

    if (isset($_POST['module_edit'])) {
        list($add_status, $add_response) = db_wizard_SaveDB($id);
        if ($add_status) {
            cm_redirect($redirect);
        } else {
            $response['variable']['add_message'] = $add_response;
        }
    }
    $response['variable']['db_info'] = $cm_db->sql_fetch_assoc($db_query);
    $response['theme'] = $params['module_directory'].'view/admin_edit.php';
    return $response;
}

function db_wizard_Delete($params){
    global $admindir,$cm_db;
    $redirect   = cm_siteurl() . trim($admindir, '/') . '/plugins.php?mod=' . $params['module_dir'] . '&module_name=' . $params['module'];
    $response   = array();
    $id         = cm_get_request('id');
    if(!cm_numeric($id) or (cm_numeric($id) and $id == 0)){ cm_redirect($redirect); }

    $db_query = $cm_db->sql_query("SELECT * FROM dbwizard_databases WHERE id='".$cm_db->sql_escape($id)."'");
    if($cm_db->sql_errno() != 0 or ($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($db_query) == 0)){ cm_redirect($redirect); }

    $cm_db->sql_query("DELETE FROM dbwizard_databases WHERE id='".$cm_db->sql_escape($id)."'");
    cm_redirect($redirect);

    $response['variable']['db_info'] = $cm_db->sql_fetch_assoc($db_query);
    $response['theme'] = $params['module_directory'].'view/admin_edit.php';
    return $response;
}

function db_wizard_Table($params){
    global $admindir,$cm_db;
    $redirect   = cm_siteurl() . trim($admindir, '/') . '/plugins.php?mod=' . $params['module_dir'] . '&module_name=' . $params['module'];
    $response   = array();
    $id         = cm_get_request('id');
    if(!cm_numeric($id) or (cm_numeric($id) and $id == 0)){ cm_redirect($redirect); }

    $db_query = $cm_db->sql_query("SELECT * FROM dbwizard_databases WHERE id='".$cm_db->sql_escape($id)."'");
    if($cm_db->sql_errno() != 0 or ($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($db_query) == 0)){ cm_redirect($redirect); }
    $db_info    = $cm_db->sql_fetch_assoc($db_query);


    $db_wizard  = new db_wizardDB($db_info['db_server'],$db_info['db_user'],$db_info['db_password'],$db_info['db_name']);
    if($db_wizard->connect()){
        $table_response = cm_lang('Tablo bulunamadı');
        $table_query = $db_wizard->sql_query("SHOW TABLE STATUS FROM `".$db_info['db_name']."`;");
        if($db_wizard->sql_errno() == 0){
            $tables = array();
            while($table = $db_wizard->sql_fetch_assoc($table_query)){
                $tables[] = $table;
            }
        }
        $response['variable']['db_table_list'] = $tables;
    }else{
        $response['variable']['db_table_list'] = cm_lang('Veritabanına bağlanılamadı');
    }



    $response['variable']['db_info']    = $db_info;
    $response['variable']['db_id']      = $id;
    $response['theme'] = $params['module_directory'].'view/admin_table.php';
    return $response;
}

function db_wizard_Store($params){
    global $admindir,$cm_db;
    $redirect   = cm_siteurl() . trim($admindir, '/') . '/plugins.php?mod=' . $params['module_dir'] . '&module_name=' . $params['module'];
    $response   = array();
    $id         = cm_get_request('id');
    if(!cm_numeric($id) or (cm_numeric($id) and $id == 0)){ cm_redirect($redirect); }

    $db_query = $cm_db->sql_query("SELECT * FROM dbwizard_databases WHERE id='".$cm_db->sql_escape($id)."'");
    if($cm_db->sql_errno() != 0 or ($cm_db->sql_errno() == 0 and $cm_db->sql_num_rows($db_query) == 0)){ cm_redirect($redirect); }
    $db_info    = $cm_db->sql_fetch_assoc($db_query);

    $query = $cm_db->sql_query("SELECT * FROM dbwizard_backup WHERE db_id=".$db_info["id"]." ORDER BY `time` DESC");
    if($cm_db->sql_errno() == 0){
        $tables = array();
        while($table = $cm_db->sql_fetch_assoc($query)){
            $tables[] = $table;
        }
        $response['variable']['db_table_list'] = (count($tables)>0?$tables:cm_lang('Kayıt bulunamadı'));
    }else{
        $response['variable']['db_table_list'] = cm_lang('Veritabanına bağlanılamadı');
    }

    $response['variable']['db_info']    = $db_info;
    $response['variable']['db_id']      = $id;
    $response['theme'] = $params['module_directory'].'view/admin_store.php';
    return $response;
}

function db_wizard_AdminPanel($params){
    global $cm_db;
    //echo '<pre>';
    //print_r($params); exit;
    $module_page    = cm_get_request('module_page');
    $db_backup      = cm_get_request('db_backup');
    $response       = array();
    $status         = false;

    if($db_backup){
        db_wizard_DBbackup($params);
    }elseif($module_page == 'add'){
        $status     = true;
        $response   = db_wizard_Add($params);
    }elseif($module_page == 'edit'){
        $status     = true;
        $response   = db_wizard_Edit($params);
    }elseif($module_page == 'delete'){
        $status     = true;
        $response   = db_wizard_Delete($params);
    }elseif($module_page == 'table'){
        $status     = true;
        $response   = db_wizard_Table($params);
    }elseif($module_page == 'setting'){
        $status     = true;
        $response   = db_wizard_Table($params);
    }elseif($module_page == 'store'){
        $status     = true;
        $response   = db_wizard_Store($params);
    }else{
        $status     = true;
        $db_list    = array();
        $db_query   = $cm_db->sql_query("SELECT * FROM dbwizard_databases ORDER BY id ASC");
        while($db_lst = $cm_db->sql_fetch_assoc($db_query)){ $db_list[] = $db_lst; }

        $response['variable']['dbwizard_list']   = $db_list;
        $response['theme'] = $params['module_directory'].'view/admin_home.php';
    }

    return array($status,$response);
}

function db_wizard_UserPanel($params){
    $status     = true;
    $response   = array(
        'theme'         => $params['module_directory'].'view/user_home.tpl',
        //'html'        => '<p><b>{$test}</b></p>',
        //'html_type'   => 'tpl',
        'variable'      => array(
            'test'      => 'Erhan Yazılımcı'
        ),
    );
    return array($status,$response);
}