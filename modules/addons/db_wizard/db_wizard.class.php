<?php
class db_wizardDB{
    private $db_connect = false;
    private $db_config;
    
    function __construct($db_server,$db_user,$db_password,$db_name,$charset = 'utf8'){
        $this->db_config = array(
            'db_host'       => $db_server,
            'db_user'       => $db_user,
            'db_password'   => $db_password,
            'db_name'       => $db_name,
            'db_charset'    => $charset,
        );
    }
    
    function connect(){
        $this->db_connect = @mysqli_connect($this->db_config["db_host"],$this->db_config["db_user"],$this->db_config["db_password"],$this->db_config["db_name"]);
        if($this->db_connect){
            return true;
        }else{
            return false;
        }
    }
    
    function sql_set_charset($karakter){
        return mysqli_set_charset($this->db_connect,$karakter);
    }
    
    function sql_query($sorgu){
        $query = @mysqli_query($this->db_connect,$sorgu);
        return $query;
    }
    
    function sql_fetch_assoc($sorgu){
        return mysqli_fetch_assoc($sorgu);
    }

    function sql_fetch_array($sorgu,$liste = null){
        
        if($liste != ""){
            if(stristr($liste,"MYSQLI_NUM")){
                return mysqli_fetch_array($sorgu,MYSQLI_NUM);
            }elseif(stristr($liste,"MYSQLI_ASSOC")){
                return mysqli_fetch_array($sorgu,MYSQLI_ASSOC);
            }elseif(stristr($liste,"MYSQLI_BOTH")){
                return mysqli_fetch_array($sorgu,MYSQLI_BOTH);
            }else{
                return mysqli_fetch_array($sorgu,MYSQLI_BOTH);
            }
        }else{
            return mysqli_fetch_array($sorgu,MYSQLI_ASSOC);
        }
    }
    
    function sql_fetch_row($sorgu){
        
        return mysqli_fetch_row($sorgu);
    }
    
    function sql_fetch_object($sorgu){
        
        return mysqli_fetch_object($sorgu);
    }
    
    function sql_num_rows($sorgu){
        
        return mysqli_num_rows($sorgu);
    }

    function sql_insert_id(){
        
        return mysqli_insert_id($this->connect);
    }
    
    function sql_escape($veri){
        
        return mysqli_real_escape_string($this->connect,$veri);
    }
    
    function sql_info(){
        
        return mysqli_info($this->connect);
    }
    
    function sql_free_result($sorgu){
        
        return mysqli_free_result($sorgu);
    }
    
    function sql_connect_errno(){
        return mysqli_connect_errno();
    }
    
    function sql_connect_error(){
        return mysqli_connect_error();
    }

    function sql_num_fields($sorgu){
        return mysqli_num_fields($sorgu);
    }
    
    function sql_error(){
        return mysqli_error($this->db_connect);
    }
    
    function sql_errno(){
        return mysqli_errno($this->db_connect);
    }
    
    function sql_insert_add($tablo,$veri){
        if(is_array($veri) and $tablo != ""){
            
            $Value  = array();
            foreach($veri as $anahtar=>$deger){
                if(!is_array($deger)) {
                    $deger = (string)$deger;
                    $Value["`".$anahtar."`"] = "'" . $this->sql_escape($deger) . "'";
                }
            }
            //echo "INSERT INTO ".($prefix==1?$cm_config['db_prefix']:null).$tablo." (".implode(",",array_keys($Value)).") VALUES (".implode(",",$Value).")"; exit;
            $this->sql_query("INSERT INTO ".$tablo." (".implode(",",array_keys($Value)).") VALUES (".implode(",",$Value).")");
            if($this->sql_errno() == 0){
                return array(true,$this->sql_insert_id());
            }else{
                return array(false,$this->sql_error());
            }
        }else{
            return array(false,'Gelen veriler hatalı');
        }
    }
    
    public function sql_query_update($tablo,$veriler = array(),$kosul){
        if(is_array($veriler) and $kosul){
            $Value  = array();
            foreach($veriler as $anahtar=>$deger){
                if(!is_array($deger)) {
                    $deger = (string)$deger;
                    $Value[] = "`".$anahtar."`" . "='" . $this->sql_escape($deger) . "'";
                }
            }
            $this->sql_query("UPDATE ".$tablo." SET ".implode(",",$Value)." WHERE ".$kosul);
            if($this->sql_errno() == 0){
                return array(true,'Güncellendi');
            }else{
                return array(false,$this->sql_error());
            }
        }else{
            return array(false,'Hatalı veri');
        }
    }

    function Close(){
        if($this->db_connect){ mysqli_close($this->db_connect); }
    }
    
    function __destruct(){
        $this->Close();
    }
}