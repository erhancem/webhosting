<div class="col-md-2"></div>
<div class="col-md-8">
    <form action="" method="POST">
        <?php if(isset($add_message)){ ?>
            <div class="form-group">
                <p style="text-align: center;font-weight: bold;font-size: 13px;color: red;"><?=$add_message?></p>
            </div>
        <?php } ?>
        <div class="form-group">
            <label for="exampleInputEmail1"><?=cm_lang('Veritabanı Sunucu')?></label>
            <input required="" name="db_server" value="<?=cm_htmlclear($db_info['db_server'])?>" type="text" class="form-control" aria-describedby="emailHelp" placeholder="<?=cm_lang('Veritabanı Sunucu')?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1"><?=cm_lang('Veritabanı Kullanıcı')?></label>
            <input required="" name="db_user" value="<?=cm_htmlclear($db_info['db_user'])?>" type="text" class="form-control" aria-describedby="emailHelp" placeholder="<?=cm_lang('Veritabanı Kullanıcı')?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1"><?=cm_lang('Veritabanı Şifre')?></label>
            <input name="db_password" value="<?=cm_htmlclear($db_info['db_password'])?>" type="password" class="form-control" aria-describedby="emailHelp" placeholder="<?=cm_lang('Veritabanı Şifre')?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1"><?=cm_lang('Veritabanı Adı')?></label>
            <input required="" name="db_name" value="<?=cm_htmlclear($db_info['db_name'])?>" type="text" class="form-control" aria-describedby="emailHelp" placeholder="<?=cm_lang('Veritabanı Adı')?>">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail1"><?=cm_lang('Veritabanı Port')?></label>
            <input required="" name="db_port" value="<?=cm_htmlclear($db_info['db_port'])?>" type="text" class="form-control" value="3306" placeholder="<?=cm_lang('Veritabanı Port')?>">
            <small id="emailHelp" class="form-text text-muted"><?=cm_lang('Port standart olarak 3306 girilmiştir farklı ise değişiklik yapabilirsiniz')?></small>
        </div>
        <button name="module_edit" value="edit" type="submit" class="btn btn-primary"><?=cm_lang('Kaydet')?></button>
    </form>
</div>
<div class="col-md-2"></div>