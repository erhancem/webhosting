<div class="col-md-12" style="margin-bottom: 15px;">
    <div class="col-md-6"><div class="pull-left" style="padding: 10px;"></div></div>
    <form action="" method="GET" class="" role="search">
        <div class="col-md-6">
            <div id="custom-search-input">
                <div class="input-group  pull-right">
                    <div class="form-group">
                        <div style="float: left;margin-left: 5px;"><a href="<?=$admindir?>/plugins.php?mod=<?=$mod?>&module_name=<?=$module_name?>&module_page=add" class="btn btn-success btn-xs" style="float: left;" id="advancedClose"><i class="fa fa-plus-circle" aria-hidden="true"></i> <?=cm_lang('Veritabanı Ekle')?></a></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<table class="table table-bordered table-hover results">
    <thead>
    <tr>
        <th><?=cm_lang('Veritabanı Sunucu')?></th>
        <th><?=cm_lang('Veritabanı Kullanıcı')?></th>
        <th><?=cm_lang('Veritabanı Adı')?></th>
        <th><?=cm_lang('Veritabanı Portu')?></th>
        <th><?=cm_lang('İşlem')?></th>
    </tr>
    </thead>
    <tbody>
        <?php if(is_array($dbwizard_list) and count($dbwizard_list) > 0){ foreach ($dbwizard_list as $key=>$value){ ?>
            <tr>
                <td><?=cm_htmlclear($value['db_server'])?></td>
                <td><?=cm_htmlclear($value['db_user'])?></td>
                <td><?=cm_htmlclear($value['db_name'])?></td>
                <td><?=cm_htmlclear($value['db_port'])?></td>
                <td>
                    <a href="<?=$admindir?>/plugins.php?mod=<?=cm_htmlclear($mod)?>&module_name=<?=cm_htmlclear($module_name)?>&module_page=edit&id=<?=$value['id']?>" class="btn btn-info btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i></a>
                    <a href="<?=$admindir?>/plugins.php?mod=<?=cm_htmlclear($mod)?>&module_name=<?=cm_htmlclear($module_name)?>&module_page=table&id=<?=$value['id']?>" class="btn btn-warning btn-xs"><i class="fa fa-database" aria-hidden="true"></i></a>
                    <a href="<?=$admindir?>/plugins.php?mod=<?=cm_htmlclear($mod)?>&module_name=<?=cm_htmlclear($module_name)?>&module_page=store&id=<?=$value['id']?>" class="btn btn-default btn-xs"><i class="fa fa-hdd-o" aria-hidden="true"></i></a>
                    <a href="<?=$admindir?>/plugins.php?mod=<?=cm_htmlclear($mod)?>&module_name=<?=cm_htmlclear($module_name)?>&module_page=delete&id=<?=$value['id']?>" onclick="return confirm('<?=cm_lang('Veritabanı silinecektir. Onaylıyormusunuz ?')?>')" class="btn btn-danger btn-xs"><i class="fa fa-trash" aria-hidden="true"></i></a>
                </td>
            </tr>
        <?php }}else{ ?>
            <tr>
                <td colspan="4" style="text-align: center;font-weight: bold;"><?=cm_lang('Ekli veritabanı bulunamadı')?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>