<div class="col-md-12" style="margin-bottom: 15px;">
    <div class="col-md-6"><div class="pull-left" style="padding: 10px;"></div></div>
    <form action="" method="GET" class="" role="search">
        <div class="col-md-6">
            <div id="custom-search-input">
                <div class="input-group  pull-right">
                    <div class="form-group">

                    </div>
                </div>
            </div>
        </div>
    </form>
    <div class="clearfix"></div>
</div>
<table class="table table-bordered table-hover results">
    <thead>
    <tr>
        <th><?=cm_lang('Veritabanı Sunucu')?></th>
        <th><?=cm_lang('Veritabanı Kullanıcı')?></th>
        <th><?=cm_lang('Veritabanı Adı')?></th>
        <th><?=cm_lang('Veritabanı Portu')?></th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <td><?=cm_htmlclear($db_info['db_server'])?></td>
        <td><?=cm_htmlclear($db_info['db_user'])?></td>
        <td><?=cm_htmlclear($db_info['db_name'])?></td>
        <td><?=cm_htmlclear($db_info['db_port'])?></td>
    </tr>
    </tbody>
</table>
<div style="margin-bottom: 15px;">
    <div class="pull-right">
        <a href="<?=$admindir?>/plugins.php?mod=<?=$mod?>&module_name=<?=$module_name?>" class="btn btn-default btn-xs"><i class="fas fa-list-alt"></i> <?=cm_lang('Veritabanı Listesi')?></a>
        <a href="<?=$admindir?>/plugins.php?mod=<?=$mod?>&module_name=<?=$module_name?>&id=<?=$db_id?>&module_page=table&db_backup=all" onclick="return confirm('<?=cm_lang('Tüm tablolar yedeklenecek. Onaylıyormusunuz ?')?>')" class="btn btn-success btn-xs"><i class="fa fa-table" aria-hidden="true"></i> <?=cm_lang('Tüm Tabloları Yedekle')?></a>
        <a href="<?=$admindir?>/plugins.php?mod=<?=$mod?>&module_name=<?=$module_name?>&id=<?=$db_id?>&module_page=setting" class="btn btn-info btn-xs"><i class="fa fa-cog" aria-hidden="true"></i> <?=cm_lang('Yedekleme Ayarları')?></a>
        <a href="<?=$admindir?>/plugins.php?mod=<?=$mod?>&module_name=<?=$module_name?>&id=<?=$db_id?>&module_page=store" class="btn btn-warning btn-xs"><i class="fa fa-hdd-o" aria-hidden="true"></i> <?=cm_lang('Yedekler')?></a>
    </div>
    <div class="clearfix"></div>
</div>
<table class="table table-bordered table-hover results">
    <thead>
    <tr>
        <th>#</th>
        <th><?=cm_lang('Tablo Adı')?></th>
        <th><?=cm_lang('Motor')?></th>
        <th><?=cm_lang('Kayıt')?></th>
        <th><?=cm_lang('Oluşturma Tarihi')?></th>
        <th><?=cm_lang('Güncelleme Tarihi')?></th>
        <th> - </th>
    </tr>
    </thead>
    <tbody>
        <?php
        if(is_array($db_table_list) and count($db_table_list) > 0){
            $say = 1;
            foreach($db_table_list as $key=>$value){
        ?>
        <tr>
            <td><?=$say?></td>
            <td><?=cm_htmlclear($value['Name'])?></td>
            <td><?=cm_htmlclear($value['Engine'])?></td>
            <td><?=cm_htmlclear($value['Rows'])?></td>
            <td><?=cm_htmlclear($value['Create_time'])?></td>
            <td><?=cm_htmlclear($value['Update_time'])?></td>
            <td>
                <a href="<?=$admindir?>/plugins.php?mod=<?=$mod?>&module_name=<?=$module_name?>&id=<?=$db_id?>&module_page=table&db_backup=<?=$value['Name']?>" class="btn btn-warning btn-xs" onclick="return confirm('<?=cm_lang('Tablo yedeklemesi yapılacak. Onaylıyormusunuz ?')?>')"><i class="fa fa-archive" aria-hidden="true"></i></a>
            </td>
        </tr>
        <?php
                ++$say;
            }
        }else{
        ?>

        <tr>
            <td colspan="6" style="text-align: center;font-weight: bold;"><?=cm_htmlclear($db_table_list)?></td>
        </tr>

    <?php } ?>
    </tbody>
</table>