DROP TABLE IF EXISTS `dbwizard_backup`;
CREATE TABLE IF NOT EXISTS `dbwizard_backup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_id` int(11) NOT NULL DEFAULT '0',
  `filename` char(255) DEFAULT NULL,
  `path` char(255) DEFAULT NULL,
  `filesize` bigint(20) NOT NULL DEFAULT '0',
  `table` text,
  `time` bigint(20) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `dbwizard_config`;
CREATE TABLE IF NOT EXISTS `dbwizard_config` (
  `db_id` int(11) NOT NULL DEFAULT '0',
  `cf_key` char(255) DEFAULT NULL,
  `cf_value` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
DROP TABLE IF EXISTS `dbwizard_databases`;
CREATE TABLE IF NOT EXISTS `dbwizard_databases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `db_server` char(255) DEFAULT NULL,
  `db_user` char(255) DEFAULT NULL,
  `db_password` text,
  `db_port` char(6) DEFAULT NULL,
  `db_name` char(255) DEFAULT NULL,
  `auto_backup` tinyint(1) NOT NULL DEFAULT '0',
  `backup_tables` text,
  `backup_dir` char(255) DEFAULT NULL,
  `time` bigint(20) NOT NULL DEFAULT '0',
  `update` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
