<?php

function mail_informationControl($params){
    $status = false;
    if(!isset($params)) {
        $response = cm_lang('Modül bilgileri eksik');
    }elseif(!array_key_exists('smtp_server',$params) or (array_key_exists('smtp_server',$params) and $params["smtp_server"] == "")){
        $response = cm_lang('SMTP sunucu adresi girmediniz');
    }elseif(!array_key_exists('smtp_user',$params) or (array_key_exists('smtp_user',$params) and $params["smtp_user"] == "")){
        $response = cm_lang('SMTP kullanıcı adı giriniz');
    }elseif(!array_key_exists('smtp_password',$params) or (array_key_exists('smtp_password',$params) and $params["smtp_password"] == "")){
        $response = cm_lang('SMTP şifreniz eksik');
    }elseif(!array_key_exists('smtp_port',$params) or (array_key_exists('smtp_port',$params) and $params["smtp_port"] == "")){
        $response = cm_lang('SMTP port adresiniz eksik');
    }elseif(!array_key_exists('smtp_security',$params) or (array_key_exists('smtp_security',$params) and $params["smtp_security"] == "")){
        $response = cm_lang('SMTP güvenlik türü eksik');
    }else{
        $status     = true;
        $response   = cm_lang('SMTP bilgileri girildi');
    }
    return array($status,$response);
}

function mail_mailControl($params){
    $status = false;
    if(!isset($params['email']) or (isset($params['email']) and !filter_var($params['email'],FILTER_VALIDATE_EMAIL))){
        $response = cm_lang('Email adresi hatalı');
    }elseif(!isset($params['subject']) or (isset($params['subject']) and $params['subject'] == '')){
        $response = cm_lang('Email konusu eksik');
    }else{
        $status     = true;
        $response   = cm_lang('Mail bilgileri doğru');
    }
    return array($status,$response);
}

function mail_send($params){
    global $cm_dirlist;
    list($status,$response) = mail_informationControl($params);
    if($status == true){
        list($status,$response) = mail_mailControl($params);
        if($status == true){
            $status = false;
            if(file_exists($cm_dirlist['library'] . 'PHPmailer/mail.php')) {
                include($cm_dirlist['library'] . 'PHPmailer/mail.php');
                $mail = new PHPMailer();
                $mail->IsSMTP();
                $mail->SMTPAuth = true;
                $mail->Host = $params['smtp_server'];
                $mail->Port = $params['smtp_port'];
                $mail->SMTPSecure = $params['smtp_security'];
                $mail->Username = $params['smtp_user'];
                $mail->Password = $params['smtp_password'];
                $mail->SetFrom($params['smtp_user'], $params['smtp_user']);
                $mail->Subject = $params['subject'];
                $body = $params['message'];
                $mail->MsgHTML($body);
                $mail->AddAddress($params['email'], $params['subject']);
                if ($mail->Send()) {
                    $response = array(
                        'message' => cm_lang('Mail Gönderildi'),
                    );
                    $status = true;
                } else {
                    $response = cm_lang('Mail gönderilemedi');
                }
            }else{
                $response = cm_lang('Mail kütüphanesi bulunamadı');
            }
        }
    }
    return array($status,$response);
}