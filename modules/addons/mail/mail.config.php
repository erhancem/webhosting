<?php

$mail_addons	= array(
    'type'			=> 'mail',
    'name'			=> cm_lang('SMTP Mail Modülü'),
    'logo'			=> 'https://hsto.org/web/8aa/86f/9dc/8aa86f9dcbba4d19b2fed2df582da15f.png',
    'information'	=> null,
    'data'	=> array(
        'description'	=> cm_lang('PHP SMTP Mail gönderme'),
        'form'	=> array(
            'smtp_name'	=> array(
                'tag'			=> 'input',
                'type'			=> 'text',
                'name'			=> 'smtp_name',
                'placeholder'	=> cm_lang('SMTP Gönderen Ad'),
                'min_char'	    => null,
                'max_char'	    => null,
                'required'	    => true,
                'form_name'		=> cm_lang('SMTP Gönderen Ad'),
                'form_desc'	    => '',
            ),
            'smtp_server'	=> array(
                'tag'			=> 'input',
                'type'			=> 'text',
                'name'			=> 'smtp_server',
                'placeholder'	=> cm_lang('SMTP sunucu'),
                'min_char'	    => null,
                'max_char'	    => null,
                'required'	    => true,
                'form_name'		=> cm_lang('SMTP Sunucu'),
                'form_desc'	    => 'SMTP sunucu adresini girin',
            ),
            'smtp_user'	=> array(
                'tag'			=> 'input',
                'type'			=> 'text',
                'name'			=> 'smtp_user',
                'placeholder'	=> cm_lang('SMTP Kullanıcı Adı'),
                'min_char'	    => 1,
                'max_char'	    => 11,
                'required'	    => true,
                'form_name'		=> cm_lang('SMTP Kullanıcı Adı'),
                'form_desc'	    => 'SMTP oturum açma işlemi için kullanıcı adı veya email adresi',
            ),
            'smtp_password'	=> array(
                'tag'			=> 'input',
                'type'			=> 'password',
                'name'			=> 'smtp_password',
                'placeholder'	=> cm_lang('SMTP Şifresi'),
                'min_char'	    => 32,
                'max_char'	    => 32,
                'required'	    => true,
                'form_name'		=> cm_lang('SMTP Şifresi'),
                'form_desc'	    => '',
            ),
            'smtp_port'	=> array(
                'tag'			=> 'input',
                'type'			=> 'text',
                'name'			=> 'smtp_port',
                'placeholder'	=> cm_lang('SMTP Sunucu portu'),
                'min_char'	    => 32,
                'max_char'	    => 32,
                'required'	    => false,
                'form_name'		=> cm_lang('SMTP Sunucu portu'),
                'form_desc'	    => null,
            ),
            'smtp_security'	=> array(
                'tag'			=> 'select',
                'options'       => array(
                    'tls'   => cm_lang('TLS'),
                    'ssl'   => cm_lang('SSL')
                ),
                'name'			=> 'smtp_security',
                'placeholder'	=> cm_lang('SMTP Güvenlik'),
                'min_char'	    => 32,
                'max_char'	    => 32,
                'required'	    => true,
                'form_name'		=> cm_lang('SMTP Güvenlik'),
                'form_desc'	    => null,
            ),
        )
    ),
);