DROP TABLE IF EXISTS `imageopt_images`;
CREATE TABLE IF NOT EXISTS `imageopt_images` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `image` char(255) DEFAULT NULL,
  `opt_image` char(255) DEFAULT NULL,
  `time` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
