<?php

function image_optimizer_ModuleInstall($params){
    global $cm_db,$cm_dirlist;
    $sqlFile = $cm_dirlist['module'].$params['module_dir'].'/'.$params['module'].'/'.$params['module'].'.sql';
    list($sql_status,$sql_response) = $cm_db->sql_file_upload($sqlFile);
    if($sql_status == true){
        $response = [
            'message'   => cm_lang('Modül dosyaları kuruldu'),
        ];
        return array(true,$response);
    }
    return array(false,cm_lang('Modül sql dosyası kurulamadı'));
}

function image_optimizer_AdminPanel($params){

}

function image_optimizer_UserPanel($params){

}