<?php
function resellerclub_Command($params, $command, $type, $postfields, $method, $nodecode = false){
	if ($params['api_test'] == 'test'){
		$url = $params['api_url_test'] . $type . "/" . $command . ".json";
	}else{
		$url = $params['api_url'] . $type . "/" . $command . ".json";
	}

	if($method == "GET"){
		$url .= "?";
		foreach($postfields as $field => $data){
			$url .= "" . $field . "=" . rawurlencode( $data ) . "&";
		}
		$url = substr( $url, 0, 0 - 1 );
		$postfields['url'] = $url;
	}
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 50 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	if($method == 'POST'){
		$postfield = "";
		foreach($postfields as $field => $data){
			$postfield .= "" . $field . "=" . $data . "&";
		}
		$postfield = substr( $postfield, 0, 0 - 1 );
		curl_setopt( $ch, CURLOPT_POST, 1 );
		curl_setopt( $ch, CURLOPT_POSTFIELDS, $postfield );
	}
	$data = curl_exec($ch);
	if(curl_errno($ch)){
		$return = array(false,cm_lang('Resellerclub bağlanılamadı'));
	}else{
		if($nodecode && is_numeric($data)){
			$result = $data;
			$return = array(true,array('status'=>'SUCCESS','id'=>$data));
		}else{
			$result = @json_decode($data, true);
			if(is_array($result)){
				if(array_key_exists('status',$result) and stristr($result['status'],"ERROR")){
					if(isset($result['message'])){
						$msg = $result['message'];
					}elseif(isset($result['error'])){
						$msg = $result['error'];
					}else{
						$msg = cm_lang('API Hatası');
					}
					$return = array(false,$msg);
				}else{
					if(!array_key_exists('status',$result)){
						$result['status'] = 'SUCCESS';
					}
					$return = array(true,$result);
				}
			}else{
				$return = array(true,cm_lang('API çıktı okunamadı'));
			}
		}
	}
	curl_close($ch);
	return $return;
}
function resellerclub_ContactType($tld) {
	if (preg_match( '/uk$/i', $tld )) {
		$contacttype = "UkContact";
	}
	else {
		if (preg_match( '/eu$/i', $tld )) {
			$contacttype = "EuContact";
		}
		else {
			if (preg_match( '/cn$/i', $tld )) {
				$contacttype = "CnContact";
			}
			else {
				if (preg_match( '/co$/i', $tld )) {
					$contacttype = "CoContact";
				}
				else {
					if (preg_match( '/ca$/i', $tld )) {
						$contacttype = "CaContact";
					}
					else {
						if (preg_match( '/es$/i', $tld )) {
							$contacttype = "EsContact";
						}
						else {
							if (preg_match( '/de$/i', $tld )) {
								$contacttype = "DeContact";
							}
							else {
								if (preg_match( '/ru$/i', $tld )) {
									$contacttype = "RuContact";
								}
								else {
									if (preg_match( '/nl$/i', $tld )) {
										$contacttype = "NlContact";
									}
									else {
										$contacttype = "Contact";
									}
								}
							}
						}
					}
				}
			}
		}
	}
	return $contacttype;
}
function resellerclub_customcontact($params){
	$custom = array();
	$return = array();
	if($params['tld']['tld_name'] == 'us'){
		$custom = array(
			array('purpose',"P2"),
			array('category',"C12"),
		);
		$return["product-key"] = "domus";
	}elseif($params['tld']['tld_name'] == 'ca'){
		$custom = array(
			array('CPR','CCT'),
			array('AgreementVersion','2.0'),
			array('AgreementValue','y'),
		);
		$return['product-key'] = "dotca";
	}elseif($params['tld']['tld_name'] == 'es'){
		$custom = array(
			array('es_form_juridica','877'),
			array('es_tipo_identificacion','9830413216'),
			array('es_identificacion','0'),
		);
		$return['product-key'] = "dotes";
	}elseif($params['tld']['tld_name'] == 'asia'){ //asia
		$custom = array(
			array('locality',$params['contact']['country']),
			array('legalentitytype','corporation'),
			array('identform','societyRegistry'),
			array('identnumber','9830413216')
		);
		$return['product-key'] = "dotasia";
	}elseif($params['tld']['tld_name'] == 'pro'){
		$custom = array(
			array('profession',"Internet Professional"),
		);
		$return['product-key'] = "dotpro";
	}elseif($params['tld']['tld_name'] == "nl"){
		$custom = array(
			array('legalForm',($params['contact']['company_name'] ? "ANDERS" : "PERSOON")),
		);
		$return['product-key'] = "dotnl";
	}
	if(count($custom) > 0){
		$a = 1;
		foreach($custom as $key=>$values){
			list($name,$value) = $values;
			$return["attr-name".$a] = $name;
			$return["attr-value".$a] = $value;
			$a++;
		}
	}
	return $return;
}
function resellerclub_customregister($params,$contact_id,$country){
	$custom = array();
	$return = array();
	if($params['domain_idn_status'] == 1 and $params['tld']['tld_name'] == 'eu'){
		$custom = array(
			array('idnLanguageCode',"latin"),
		);
	}elseif($params['domain_idn_status'] == 1 and $params['tld']['tld_name'] == 'asia'){
		$custom = array(
			array('idnLanguageCode','ko'),
			array('cedcontactid',$contact_id),
			array('locality',$country),
		);
	}elseif($params['domain_idn_status'] == 1 and ($params['tld']['tld_name'] == 'com' or $params['tld']['tld_name'] == 'net' or $params['tld']['tld_name'] == 'name')){
		$custom = array(
			array('idnLanguageCode','TUR'),
		);
	}elseif($params['domain_idn_status'] == 1 and $params['tld']['tld_name'] == 'org'){
		$custom = array(
			array('idnLanguageCode','de'),
		);
	}elseif($params['domain_idn_status'] == 1 and $params['tld']['tld_name'] == 'biz'){
		$custom = array(
			array('idnLanguageCode','da'),
		);
	}elseif($params['domain_idn_status'] == 1 and $params['tld']['tld_name'] == 'es'){
		$custom = array(
			array('idnLanguageCode','tr'),
		);
	}elseif($params['domain_idn_status'] == 1 and $params['tld']['tld_name'] == 'ca'){
		$custom = array(
			array('idnLanguageCode','fr'),
		);
	}
	if(count($custom) > 0){
		$a = 1;
		foreach($custom as $key=>$values){
			list($name,$value) = $values;
			$return["attr-name".$a] = $name;
			$return["attr-value".$a] = $value;
			$a++;
		}
	}
	return $return;
}
/** Domain CNS Start */

function resellerclub_addcns($params){
	global $cm_db;
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$cns	= array_keys($params['new_ns']);
	$cns	= $cns[0];
	$cns_ip	= $params['new_ns'][$cns];

	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['order-id']			= $params['domainid'];
	$postfields['cns']				= $cns;
	$postfields['ip'] 				= $cns_ip;
	list($add_status,$add_result) = resellerclub_Command($params,"add-cns", "domains", $postfields, "POST");
	if($add_status == false){
		return array(false,$add_result);
	}
	$add = array(
		'domain_id'	=> $params['domain_id'],
		'ns'		=> $cns,
		'ip'		=> $cns_ip,

	);
	$cm_db->sql_insert_add('domain_ns',$add);
	return array(true,array('message'=>cm_lang('NS eklendi')));
}

function resellerclub_deletecns($params){
	global $cm_db;
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$cns	= array_keys($params['delete_ns']);
	$cns	= $cns[0];
	$cns_ip	= $params['delete_ns'][$cns];
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['order-id']			= $params['domainid'];
	$postfields['cns']				= $cns;
	$postfields['ip'] 				= $cns_ip;
	list($add_status,$add_result) = resellerclub_Command($params,"delete-cns-ip", "domains", $postfields, "POST");
	if($add_status == false){
		return array(false,$add_result);
	}
	$delete = array(
		'domain_id'	=> $params['domain_id'],
		'ns'		=> $cns,
		'ip'		=> $cns_ip,

	);
	$cm_db->sql_delete('domain_ns',$delete);
	return array(true,array('message'=>cm_lang('NS Silindi')));
}

/** Domain CNS End */


function resellerclub_transferlock($params){
	global $cm_db;
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['order-id']			= $params['domainid'];

	if($params['domain_transfer_protection'] == 0){
		list($add_status,$add_result) = resellerclub_Command($params,"disable-theft-protection", "domains", $postfields, "POST");
		$message = cm_lang('Transfer kilidi pasif edildi');
	}else{
		list($add_status,$add_result) = resellerclub_Command($params,"enable-theft-protection", "domains", $postfields, "POST");
		$message = cm_lang('Transfer kilidi aktif edildi');
	}
	if($add_status == false){
		return array(false,$add_result);
	}
	userlog(cm_lang("##domain## alan adı için ##message##", ['domain' => $params["domain_name"], 'message' => $message]));
	$cm_db->sql_query_update("domains", ['domain_transfer_protection' => $params['domain_transfer_protection']], ['domain_id' => $params['domain_id']]);
	return array(true,array('message'=>$message));
}

function resellerclub_TransferCode($params){
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$postfields = array();
	$postfields['auth-userid']	= $params['api_id'];
	$postfields['api-key']		= $params['api_key'];
	$postfields['order-id']		= $params['domainid'];
	$postfields['options']		= "All";

    $return = cm_lang('Transfer kodu alınamadı');
	list($status,$result) = resellerclub_Command($params, "details", "domains", $postfields, "GET");
	if($status){
	    if(array_key_exists('domsecret',$result)){
	        $return = [
	            'message'   => cm_lang('Transfer kodu oluşturuldu'),
                'code'      => $result["domsecret"],
            ];
            userlog(cm_lang("##domain## alan adı için ##message##", ['domain' => $params["domain_name"], 'message' => $return["message"]]));
        }else{
	        $status = false;
        }
    }
	return [$status,$return];
}

/** Domain Contact Start */

function resellerclub_addcontact($params){
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['customer-id']		= $params['customer_id'];
	$postfields['email']			= $params['contact']['email_address'];
	$postfields['name'] 			= $params['contact']['name'];
	$companyname = $params['contact']['company_name'];
	if (!$companyname) {
		$companyname = "N/A";
	}
	$postfields['company']			= $companyname;
	$postfields['address-line-1']	= substr( $params['contact']['address1'], 0, 64 );
	if(64 < $params['contact']['address1']){
		$postfields['address-line-2'] = substr( $params['contact']['address1'] . ", " . $params['contact']['address2'], 64, 128 );
	}else{
		if($params['contact']['address2']){
			$postfields['address-line-2'] = substr( $params['contact']['address2'], 0, 64 );
		}
	}
	$postfields['city']			= $params['contact']['city'];
	$postfields['state']		= $params['contact']['state'];
	$postfields['zipcode']		= $params['contact']['postcode'];
	$postfields['country']		= $params['contact']['country'];
	$postfields['phone-cc'] 	= $params['contact']['phone_code'];
	$postfields['phone'] 		= $params['contact']['phone_number'];
	if($params['contact']['fax_code'] and $params['contact']['fax_number']){
		$postfields['fax-cc'] 	= $params['contact']['fax_code'];
		$postfields['fax'] 		= $params['contact']['fax_number'];
	}
	$postfields['type'] 		= resellerclub_ContactType($params['tld']['tld_name']);
	$postfields = array_merge( $postfields, resellerclub_customcontact($params));
	list($add_status,$add_result) = resellerclub_Command($params,"add", "contacts", $postfields, "POST",true);
	if($add_status == false){
		return array(false,$add_result);
	}
	return array(true,array(
		'id'		=> $add_result['id'],
		'message'	=> cm_lang('İletişim bilgileri kaydedildi'),
	));
}

function resellerclub_editcontact($params){
	global $cm_db;
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['customer-id']		= $params['customer_id'];
	$postfields['email']			= $params['contact']['email_address'];
	$postfields['name'] 			= $params['contact']['name'];
	$companyname = $params['contact']['company_name'];
	if (!$companyname) {
		$companyname = "N/A";
	}
	$postfields['company']			= $companyname;
	$postfields['address-line-1']	= substr( $params['contact']['address1'], 0, 64 );
	if(64 < $params['contact']['address1']){
		$postfields['address-line-2'] = substr( $params['contact']['address1'] . ", " . $params['contact']['address2'], 64, 128 );
	}else{
		if($params['contact']['address2']){
			$postfields['address-line-2'] = substr( $params['contact']['address2'], 0, 64 );
		}
	}
	$postfields['city']			= $params['contact']['city'];
	$postfields['state']		= $params['contact']['state'];
	$postfields['zipcode']		= $params['contact']['postcode'];
	$postfields['country']		= $params['contact']['country'];
	$postfields['phone-cc'] 	= $params['contact']['phone_code'];
	$postfields['phone'] 		= $params['contact']['phone_number'];
	if($params['contact']['fax_code'] and $params['contact']['fax_number']){
		$postfields['fax-cc'] 	= $params['contact']['fax_code'];
		$postfields['fax'] 		= $params['contact']['fax_number'];
	}
	$postfields['contact-id'] 		= $params['contact_id'];
	list($add_status,$add_result) = resellerclub_Command($params,"modify", "contacts", $postfields, "POST");
	if($add_status == false){
		return array(false,$add_result);
	}
	$update = $params['contact'];
	$update['update_time'] = cm_time();
	$cm_db->sql_query_update('domain_contacts', $update,['module_id' => $params['module_id'], 'contact_id' => $params['contact_id']]);
    userlog(cm_lang("##domain## alan adı için ##message##", ['domain' => $params["domain_name"], 'message' => cm_lang('iletişim bilgileri güncellendi')]));
	return array(true,array(
		'id'		=> $add_result['entityid'],
		'message'	=> cm_lang('Bilgiler güncellendi'))
	);
}

function resellerclub_deletecontact($params,$contact_id){
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['contact-id']		= $contact_id;
	list($status,$response) = resellerclub_Command($params,"delete", "contacts", $postfields, "POST");
	return array($status,$response);
}
function resellerclub_changecontact($params){
	global $cm_db;
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$return = array();
	list($addcontact_status,$contact_id) = resellerclub_addcontact($params);
	if($addcontact_status == false){
		return array(false,$contact_id);
	}
	$contact_id = $contact_id['id'];
	$return['id'] = $contact_id;

	$postfields = array();
	$postfields['auth-userid']			= $params['api_id'];
	$postfields['api-key']				= $params['api_key'];
	$postfields['order-id']				= $params['domainid'];
	$postfields['reg-contact-id']		= $contact_id;
	$postfields['admin-contact-id']		= $contact_id;
	$postfields['tech-contact-id']		= $contact_id;
	$postfields['billing-contact-id']	= $contact_id;
	$postfields['sixty-day-lock-optout']= "true";
	list($add_status,$add_result) = resellerclub_Command($params,"modify-contact", "domains", $postfields, "POST");
	if($add_status == false){
		resellerclub_deletecontact($params,$contact_id);
		return array(false,$add_result);
	}
	$update = $params['contact'];
	$update['create_time'] = cm_time();
	$update['update_time'] = cm_time();
	$addcontact = array_merge(array('module_id' => $params["module_id"],'contact_id' => $return['id']),$update);
	$cm_db->sql_delete('domain_contacts', array('module_id' => $params["module_id"], 'contact_id' => $params["contact_id"]));
	$cm_db->sql_insert_add('domain_contacts', $addcontact);
	$cm_db->sql_query_update('domains', array('contact_verify' => 0, 'contact_id' => $return['id']), array('domain_id' => $params['domain_id']));
	$return['message'] = cm_lang('İletişim bilgileri değiştirildi');
	return array(true,$return);
}

function resellerclub_getcontact($params){
	global $cm_db;
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}

	$postfields = array();
	$postfields['auth-userid']	= $params['api_id'];
	$postfields['api-key']		= $params['api_key'];
	$postfields['order-id']		= $params['domainid'];
	$postfields['options']		= "All";
	list($domain_status,$domain_result) = resellerclub_Command($params, "details", "domains", $postfields, "GET");
	if($domain_status == true){
		$contact_status = 0;
		if(stristr(trim($domain_result['raaVerificationStatus']),'Verified')){
			$contact_status = 1;
		}
		if(array_key_exists('irtp_status',$domain_result)){
			$contact_status = 0;
		}
		$return = array(
			'name'			=> $domain_result['registrantcontact']['name'],
			'company_name'	=> $domain_result['registrantcontact']['company'],
			'email_address'	=> $domain_result['registrantcontact']['emailaddr'],
			'phone_code'	=> $domain_result['registrantcontact']['telnocc'],
			'phone_number'	=> $domain_result['registrantcontact']['telno'],
			'fax_code'		=> null,
			'fax_number'	=> null,
			'address1'		=> $domain_result['registrantcontact']['address1'],
			'address2'		=> null,
			'address3'		=> null,
			'city'			=> $domain_result['registrantcontact']['city'],
			'state'			=> $domain_result['registrantcontact']['state'],
			'postcode'		=> $domain_result['registrantcontact']['zip'],
			'country'		=> $domain_result['registrantcontact']['country'],
			'status'		=> $contact_status,
			'contact_id'	=> $domain_result['registrantcontactid'],
		);
		if(isset($domain_result['registrantcontact']['faxnocc'])){
			$return['fax_code'] = $domain_result['registrantcontact']['faxnocc'];
			$return['fax_number'] = $domain_result['registrantcontact']['faxno'];
		}
		if(isset($domain_result['registrantcontact']['address2'])){
			$return['address2'] = $domain_result['registrantcontact']['address2'];
		}
		if(isset($domain_result['registrantcontact']['address3'])){
			$return['address3'] = $domain_result['registrantcontact']['address3'];
		}
		if($contact_status == 1 and $params['contact_id'] != $domain_result['registrantcontactid']){
			resellerclub_deletecontact($params,$params['contact_id']); // kontrol edersin
		}
		$domain_result = $return;
		if($domain_status == true){
			list($status,$ContactTotal) = $cm_db->sql_count("domain_contacts",array('module_id'=>$params['module_id'],'contact_id'=>$domain_result['contact_id']));
			if($status == true){
				if($params['contact_id'] != $domain_result['contact_id']){
					$cm_db->sql_delete('domain_contacts',array('module_id'=>$params['module_id'],'contact_id'=>$params['contact_id']));
				}
				if(isset($domain_result['status']) and in_array($domain_result['status'],array(1))){
					$cm_db->sql_query_update('domains',array('contact_verify'=>$domain_result['status'],'contact_id'=>$domain_result['contact_id']),array('domain_id'=>$params['domain_id']));
				}
				$contact = $domain_result;
				$contact['update_time']	= cm_time();
				$contact['module_id']	= $params['module_id'];
				$contact['contact_id']	= $params['contact_id'];
				if($ContactTotal == 0){
					$contact['create_time'] = cm_time();
					$cm_db->sql_insert_add('domain_contacts',$contact);
				}else{
					$where = array(
						'module'	=> $params['module_id'],
						'contact_id'=> $params['contact_id'],
					);
					$cm_db->sql_query_update('domain_contacts',$contact,$where);
				}
				unset($contact);
			}
		}
	}
	return array($domain_status,$domain_result);
}
/** Domain Contact End */

/** Domain DNS Start */

function resellerclub_getdns($params){
	global $cm_db;
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}

	$postfields = array();
	$postfields['auth-userid']	= $params['api_id'];
	$postfields['api-key']		= $params['api_key'];
	$postfields['order-id']		= $params['domainid'];
	$postfields['options']		= "NsDetails";
	list($domain_status,$domain_result) = resellerclub_Command($params, "details", "domains", $postfields, "GET");
	if($domain_status == true){
		$ns = array();
		for($i=1;$i<=4;$i++){
			$ns['ns'.$i] = isset($domain_result['ns'.$i])?$domain_result['ns'.$i]:null;
		}
		$domain_result = $ns;
		$cm_db->sql_delete('domain_dns', array('domain' => $params['domain_id']));
		foreach($domain_result as $key => $value){
			$number = trim(ltrim($key,"ns"));
			$add = array(
				'domain_id'	=> $params['domain_id'],
				'number'	=> $number,
				'dns'		=> $value,
			);
			$cm_db->sql_insert_add('domain_dns',$add);
		}
	}
	return array($domain_status,$domain_result);
}

function resellerclub_savedns($params){
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}

	$postfields = array();
	$postfields['auth-userid']	= $params['api_id'];
	$postfields['api-key']		= $params['api_key'];
	$postfields['order-id']		= $params['domainid'];
	$nss = array();
	foreach($params['dns'] as $key=>$value){
		if($value){
			$nss[] = 'ns='.$value;
		}
	}
	$nameserver = substr(implode("&",$nss),3);
	$postfields['ns'] 			= $nameserver;
	list($domain_status,$domain_result) = resellerclub_Command($params, "modify-ns", "domains", $postfields, "POST");
	if($domain_status == true){
        userlog(cm_lang("##domain## alan adı için DNS adresleri güncellendi. ##dnsaddress##", ['domain' => $params["domain_name"], 'dnsaddress' => implode(",",$params['dns'])]));
        resellerclub_getdns($params);
		$domain_result = array(
			'message'	=> cm_lang('DNS kaydedildi'),
		);
	}
	return array($domain_status,$domain_result);
}

/** Domain Register Start */

function resellerclub_register($params){

    global $cm_db;
	$return = array();
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	if($params['api_customer_id'] == ""){
		//return array(false,cm_lang('API key eksik')); müşteri eklenebilir
	}else{
		$customer_id = $params['api_customer_id'];
	}
	$return['customer_id'] = $customer_id;
	$params['customer_id'] = $customer_id;
	$contact_type = resellerclub_ContactType($params['tld']['tld_name']);
	$country = $params['contact']['country'];
	list($addcontact_status,$contact_id) = resellerclub_addcontact($params);
	if($addcontact_status == false){
		return array(false,$contact_id);
	}
	$contact_id = $contact_id['id'];
	$return['contact_id'] = $contact_id;
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['customer-id']		= $customer_id;
	$postfields['invoice-option']	= "NoInvoice";
	$postfields['protect-privacy']	= 'false';
	$postfields['domain-name']		= $params['domain_name'];
	$postfields['years']			= $params['domain_year'];
	$postfields['reg-contact-id']	= $contact_id;
	$postfields['admin-contact-id']	= $contact_id;
	if(in_array($params['tld']['tld_name'],array('eu','nz','ru','uk'))){
		$postfields['admin-contact-id']	= "-1";
	}
	$postfields['tech-contact-id']	= $contact_id;
	if(in_array($params['tld']['tld_name'],array('eu','nz','ru','uk'))){
		$postfields['tech-contact-id']	= "-1";
	}
	$postfields['billing-contact-id']	= $contact_id;
	if(in_array($params['tld']['tld_name'],array('eu','nz','ru','uk','london','berlin','ca'))){
		$postfields['billing-contact-id']	= "-1";
	}
	$nss = array();
	foreach($params['dns'] as $key=>$value){
		$nss[] = 'ns='.$value;
	}
	$nameserver = substr(implode("&",$nss),3);
	$postfields['ns'] = $nameserver;
	$postfields = array_merge($postfields,resellerclub_customregister($params,$contact_id,$country));
	list($domain_status,$domain_result) = resellerclub_Command($params,'register','domains',$postfields,'POST');
	if($domain_status == false){
		resellerclub_deletecontact($params,$contact_id);
		return array(false,$domain_result);
	}
	$return['domainid']         =  $domain_result['entityid'];
	$return['domain_status']    =  1;
	$return['domain_end_time']  =  strtotime("+".$params['domain_year']." year",$params['domain_create_time']);
    $cm_db->sql_query_update('domains', $return, array('domain_id' => $params['domain_id']));
	return array(true,$return);

}

/** Domain Register End */

/** Domain Transfer Start */

function resellerclub_transfer($params){
	$return = array();
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	if($params['api_customer_id'] == ""){
		//return array(false,cm_lang('API key eksik')); müşteri eklenebilir
	}else{
		$customer_id = $params['api_customer_id'];
	}
	$return['customer_id'] = $customer_id;
	$params['customer_id'] = $customer_id;
	$contact_type = resellerclub_ContactType($params['tld']['tld_name']);
	$country = $params['contact']['country'];
	list($addcontact_status,$contact_id) = resellerclub_addcontact($params);
	if($addcontact_status == false){
		return array(false,$contact_id);
	}
	$contact_id = $contact_id['id'];
	$return['contact_id'] = $contact_id;
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['customer-id']		= $customer_id;
	$postfields['auth-code']		= $params['authcode'];
	$postfields['invoice-option']	= "NoInvoice";
	$postfields['protect-privacy']	= 'false';
	$postfields['domain-name']		= $params['domain_name'];
	$postfields['reg-contact-id']	= $contact_id;
	$postfields['admin-contact-id']	= $contact_id;
	if(in_array($params['tld']['tld_name'],array('eu','nz','ru','uk'))){
		$postfields['admin-contact-id']	= "-1";
	}
	$postfields['tech-contact-id']	= $contact_id;
	if(in_array($params['tld']['tld_name'],array('eu','nz','ru','uk'))){
		$postfields['tech-contact-id']	= "-1";
	}
	$postfields['billing-contact-id']	= $contact_id;
	if(in_array($params['tld']['tld_name'],array('eu','nz','ru','uk','london','berlin','ca'))){
		$postfields['billing-contact-id']	= "-1";
	}
	$postfields = array_merge($postfields,resellerclub_customregister($params,$contact_id,$country));
	list($domain_status,$domain_result) = resellerclub_Command($params,'transfer','domains',$postfields,'POST');
	if($domain_status == false){
		resellerclub_deletecontact($params,$contact_id);
		return array(false,$domain_result);
	}
	if($domain_result['actionstatus'] == "Failed"){
		resellerclub_deletecontact($params,$contact_id);
		return array(false,$domain_result['actionstatusdesc']);
	}
	$return['domainid'] =  $domain_result['entityid'];
	return array(true,$return);
}

/** Domain Transfer End */

/** Domain Renew Start */

function resellerclub_renew($params){
	$return = array();
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['order-id'] 		= $params['domainid'];
	$postfields['options'] 			= "OrderDetails";
	list($expiry_status,$expiry) = resellerclub_Command($params, "details", "domains", $postfields, "GET" );
	if($expiry_status == false){
		return array(false,$expiry);
	}
	$expiry = $expiry['endtime'];
	if(!$expiry){
		return array(false,cm_lang('Alan adı bitiş zamanı alınamadı sağlayıcıdan'));
	}

	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['order-id']			= $params['domainid'];
	$postfields['years']			= $params['domain_year'];
	$postfields['exp-date']			= $expiry;
	$postfields['invoice-option']	= "NoInvoice";

	list($domain_status,$domain_result) = resellerclub_Command($params,'renew','domains',$postfields,'POST');
	if($domain_status == false){
		resellerclub_deletecontact($params,$contact_id);
		return array(false,$domain_result);
	}
	if($domain_result['actionstatus'] == "Failed"){
		resellerclub_deletecontact($params,$contact_id);
		return array(false,$domain_result['actionstatusdesc']);
	}
	$return['end_time'] =  strtotime("+".$params['domain_year']." year",$params['domain_end_time']);
	return array(true,$return);
}

/** Domain Renew End */

function resellerclub_AddDomain($params){
	if(!$params['api_id']){
		return array(false,cm_lang('API id eksik'));
	}
	if(!$params['api_key']){
		return array(false,cm_lang('API key eksik'));
	}
	$return = array();

	$postfields = array();
	$postfields['auth-userid']		= $params['api_id'];
	$postfields['api-key']			= $params['api_key'];
	$postfields['domain-name']		= $params['domain_name'];
	list($add_status,$add_result) = resellerclub_Command($params,'orderid','domains',$postfields,'POST',true);
	if($add_status == false){
		return array(false,$add_result);
	}
	$order_id = $add_result['id'];
	$postfields = array();
	$postfields['auth-userid']	= $params['api_id'];
	$postfields['api-key']		= $params['api_key'];
	$postfields['order-id']		= $order_id;
	$postfields['options']		= "All";
	list($add_status,$add_result) = resellerclub_Command($params, "details", "domains", $postfields, "GET");
	if($add_status == false){
		return array(false,$add_result);
	}
	$contact_status = 0;
	if(stristr(trim($add_result['raaVerificationStatus']),'Verified')){
		$contact_status = 1;
	}
	if(array_key_exists('irtp_status',$add_result)){
		$contact_status = 0;
	}
	$contact = array(
		'name'			=> $add_result['registrantcontact']['name'],
		'company_name'	=> $add_result['registrantcontact']['company'],
		'email_address'	=> $add_result['registrantcontact']['emailaddr'],
		'phone_code'	=> $add_result['registrantcontact']['telnocc'],
		'phone_number'	=> $add_result['registrantcontact']['telno'],
		'fax_code'		=> null,
		'fax_number'	=> null,
		'address1'		=> $add_result['registrantcontact']['address1'],
		'address2'		=> null,
		'address3'		=> null,
		'city'			=> $add_result['registrantcontact']['city'],
		'state'			=> $add_result['registrantcontact']['state'],
		'postcode'		=> $add_result['registrantcontact']['zip'],
		'country'		=> $add_result['registrantcontact']['country'],
		'status'		=> $contact_status,
	);
	if(isset($add_result['registrantcontact']['faxnocc'])){
		$contact['fax_code'] = $add_result['registrantcontact']['faxnocc'];
		$contact['fax_number'] = $add_result['registrantcontact']['faxno'];
	}
	if(isset($add_result['registrantcontact']['address2'])){
		$contact['address2'] = $add_result['registrantcontact']['address2'];
	}
	if(isset($add_result['registrantcontact']['address3'])){
		$contact['address3'] = $add_result['registrantcontact']['address3'];
	}
	$return['contact'] = $contact;
	$ns = array();
	if(array_key_exists('ns1',$add_result)){
		$ns['ns1'] = $add_result['ns1'];
	}
	if(array_key_exists('ns2',$add_result)){
		$ns['ns2'] = $add_result['ns2'];
	}
	if(array_key_exists('ns3',$add_result)){
		$ns['ns3'] = $add_result['ns3'];
	}
	if(array_key_exists('ns4',$add_result)){
		$ns['ns4'] = $add_result['ns4'];
	}
	$return['dns'] = $ns;
	$cns = array();
	if(array_key_exists('cns',$add_result) and is_array($add_result['cns']) and count($add_result['cns']) > 0){
		foreach($add_result['cns'] as $key=>$value){
			$name = str_replace(".".$params['domain_name'],"",$key);
			foreach($value as $val){
				$cns[$name] = $val;
			}
		}
	}
	$return['ns'] = $cns;
	$domain = array(
		'domain_end_time'		=> $add_result['endtime'],
		'domain_create_time'	=> $add_result['creationtime'],
		'contact_verify'		=> $contact_status,
		'customer_id'			=> $add_result['customerid'],
		'contact_id'			=> $add_result['registrantcontactid'],
		'domainid'				=> $add_result['entityid'],
		'domain_status'			=> $add_result['currentstatus']=='Active'?1:2,
	);
	$return['domain'] = $domain;
	return array(true,$return);
}